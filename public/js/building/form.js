var _DEFAULT_LATTITUDE = 13.84;
var _DEFAULT_LONGITUDE = 100.52;
var _DEFAULT_LATLNG = new L.LatLng(_DEFAULT_LATTITUDE, _DEFAULT_LONGITUDE)
var _DEFAULT_ZOOM_LEVEL = 12;
var _DEF_ICON = new L.Icon({iconUrl: 'public/images/marker-red.png', iconSize: [21, 25], iconAnchor: [10, 25]});
var _MOV_ICON = new L.Icon({iconUrl: 'public/images/marker-gold.png'});
var _map, _marker;

var _JSON_AMPHOE = {};

$(function() {
	_map = L.map("map_canvas", 
			{
				center: _DEFAULT_LATLNG
				,zoom: _DEFAULT_ZOOM_LEVEL
				,tap: false
				,touchZoom: false
				,closePopupOnClick: false
			}
		)
		.on("contextmenu", function (e) {
			alert("Context menu disabled");
		})
	; 
	//Add Scale Control
	L.control.scale({ maxWidth:200 }).addTo(_map);
	//Add Mouse Position Control
	L.control.mousePosition({ 
		position: 'bottomright'
		,latFormatter: formatLat
		,lngFormatter: formatLon
	}).addTo(_map);
	
	if (L.Browser.touch) {
		L.Util.setOptions(_map, 
			{
				tap: true
				,zoomAnimation: false
				,markerZoomAnimation: false
			}
		);
	}
	L.Util.emptyImageUrl = "public/images/blank.gif";
	var layerControl = false;
	if (typeof __fncGetLeafletLayerControl == 'function') layerControl = __fncGetLeafletLayerControl.apply(this, [_map]);
	if (! layerControl) {
		alert('ERROR: Map init function not found! ( __fncGetLeafletLayerControls )');
		return false;
	}
	_map.addControl(layerControl);

	_marker = L.marker(_DEFAULT_LATLNG, {icon: _DEF_ICON, draggable: true})
		.on('add', function() {
			var _latlng = _marker.getLatLng() || false;
			if (_latlng) $('#spn_map_position').html(formatLat(_latlng.lat) + ' : ' + formatLon(_latlng.lng));
		})
		.on('dragstart', function () {
			$('#spn_map_position').html('.. วางเพื่อรับตำแหน่ง ..');
			_marker.setIcon(_MOV_ICON);
		})
		.on('dragend', function () {
			_marker.setIcon(_DEF_ICON);
			var _latlng = _marker.getLatLng() || false;
			if (_latlng) $('#spn_map_position').html(formatLat(_latlng.lat) + ' : ' + formatLon(_latlng.lng));
		})
	;
/*
	//'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
	var _layerControl = new L.Control.Layers({"Default": _layOpenSeaMap});
	_map.addControl(_layerControl);
*/

	$('.cls-map-canvas-container').dialog({
		height:'auto',
		width:'auto',
		show:{effect:"flip",duration:1000},
		hide:{effect:"fade",duration:1000},
		resizable:true,
		modal:true,
		closeOnEscape:false,
		autoOpen:false,
		buttons:[
			{
				text: MSG_DLG_BUTTON_TEXT_SUBMIT,
				class:"cls-btn-form-submit",
				icons:{primary: "ui-icon-disk"},
				click: function(evnt) {
					var _rowid = $(this).attr('rowid') || 0;
					_doCommitBuildingMapPosition(_rowid);
					$(this).removeAttr('rowid');
				}
			},
			{
				text: MSG_DLG_BUTTON_TEXT_CANCEL,
				class:"cls-btn-form-cancel",
				icons: { primary: "ui-icon-extlink"},
				click: function(evnt) {
					$( this ).dialog("close");
				}
			}
		]
		,close: function(evnt) {
			if (_map.hasLayer(_marker)) _map.removeLayer(_marker);
			_map.setView(_DEFAULT_LATLNG, _DEFAULT_ZOOM_LEVEL);
			_marker.setLatLng(_DEFAULT_LATLNG);
			$('#spn_map_position').html('');
		}
	});
	
	var _fncTmpl___doFetchFormData = _doFetchFormData;
	var _fncTmpl__doSubmit = doSubmit;
	var _fncTmpl__blnDataChanged = blnDataChanged;

	blnDataChanged = function() {
		var _bln = false;
		//$('.cls-building-detail .user-input').addClass('no-validate');
		_bln = _fncTmpl__blnDataChanged.apply(this, arguments);
		//$('.cls-building-detail .user-input').removeClass('no-validate');
		if (_bln) {
			return true;
		} else {
			/*
			var _strCurrDetails = JSON.stringify(__getBuildingDetails());
			var _strLoadDetails = ('details' in _currEditData) && (_currEditData['details'].toString().trim().length > 0) ? _currEditData['details'].toString().trim() : '{}';
			if (_strLoadDetails != _strCurrDetails) return true;
			*/
			if ($('.user-input.clsCtrl-valueChanged').length > 0) return true;
		}
		return false;
	};

	var _displayImgPath = _baseUrl + 'building/get_building_image/';
	_doFetchFormData = function(_frm, objDataRow) {
/*
		_fncTmpl___doFetchFormData.apply(this, arguments);
		if (('details' in objDataRow) && (objDataRow['details'].toString().trim().length > 0)) {
			var _objDetails = JSON.parse(objDataRow['details'].toString().trim());
			//if (_arrDetails.length > 0) for (var _i=0;_i<_arrDetails.length;_i++) {
			if (typeof _objDetails == 'object') {
				_doFetchDataByJQSelector(_objDetails, $('.cls-building-detail', _frm), '.user-input');
			}
		}
*/
		if ((typeof objDataRow != 'object')) return false;
		for (var _i=1;_i<9;_i++) {
			var _imgRowID = ((('img_rowid' + _i) in objDataRow) && (objDataRow[('img_rowid' + _i)])) ? objDataRow[('img_rowid' + _i)] : -1;
			if (_imgRowID > 0) {
				var _elemImg = $('input.user-input[data=image' + _i + ']', _frm);
				var _prnt = $(_elemImg.parents('div.display-upload').get(0));
				if (_prnt.length > 0) _prnt.css('background-image', 'url(' + _displayImgPath + _imgRowID + ')');
			}
		}
		if (typeof _fncTmpl___doFetchFormData == 'function') return _fncTmpl___doFetchFormData.apply(this, arguments);
	};

	doSubmit = function (form) {
		var _formIndex = $(form).attr('index');
		if (_currEditData == undefined) _currEditData = {};
		//var _objDetails = __getBuildingDetails();
		//_currEditData['new_details'] = _objDetails;
		if (typeof _fncTmpl__doSubmit == 'function') return _fncTmpl__doSubmit.apply(this, arguments);
	};
	
});

/*++ Map function */
function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}
function formatLon(long) {
	var pos = Math.abs(long);
	var dir = (long>0 ? 'E' : 'W');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ew = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ew + ' (' + padLeft((Math.round(long * 10000) / 10000).toFixed(4), 8) + ')';
}
function formatLat(lat) {
	var pos = Math.abs(lat);
	var dir = (lat>0 ? 'N' : 'S');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ns = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ns + ' (' + padLeft((Math.round(lat * 10000) / 10000).toFixed(4), 8) + ')';
}

function _doPinMapPosition(obj, map_position, map_area, rowid, title) {
	var _map_position = map_position || [];
	var _map_area = map_area || [];
	var _rowid = rowid || 0;
	var _strDisplayTitle = (typeof title == 'string')?'ของ "' + title + '"':'';
	var _divDialog = $('.cls-map-canvas-container');
	if ((_rowid <= 0) || (_divDialog.length < 1)) return false;

	_divDialog.attr('rowid', _rowid); //use in commit function
	if (_map_position.length != 2) _map_position = false;
	if (_map_area.length != 2) _map_area = false;

	if (_map_position) {
		_map_position = new L.LatLng(_map_position[1], _map_position[0]);
		_marker.setLatLng(_map_position).addTo(_map);
		_map.panTo(_map_position);
	} else if (_map_area) {
		_map_area = new L.LatLng(_map_area[1], _map_area[0]);
		_map.panTo(_map_area);
	}
	
	if ($('#spn_map_position').length <= 0) $('.ui-dialog-buttonset', _divDialog.parent()).before('<span id="spn_map_position" class="cls-spn-map-position"></span>');	
	if (! _map.hasLayer(_marker)) {
		$('#spn_map_position').html('Double click บนแผนที่เพื่อวางตำแหน่งอาคาร  ( ปรับเปลี่ยนได้ด้วยการลาก icon และกด "บันทึก" )');
		_map.on('dblclick', function (ev) {
			_marker.setLatLng(ev.latlng).addTo(_map);
		});
	}
	_divDialog.dialog('option', {
			title: 'กำหนดพิกัดแผนที่' + _strDisplayTitle + ' ( rowid ' + rowid + ' )'
			,position: {my: 'right top', at: 'left-10 center-10', of: obj}
		}).dialog("open");
	
}

/*-- Map function */

function _doManageDisplay(obj, link_article_id, title) {
	var _id = link_article_id || false;
	if (_id) {
		var _href = '../index.php?option=com_content&view=article&id=' + link_article_id; //&tmpl=component
		window.open(_href, '_blank', 'location=0,menubar=0,titlebar=0,toolbar=0,width=600,height=450');
	}
}

function __getBuildingDetails() {
	var _obj = {};
	$('.cls-building-detail .user-input').each(function() {
		var _val = getValue(this) || false;
		var _data = getData(this) || false;
		if ((_val > 0) && (_data)) {
			_obj[_data] = _val;
		}
	});
	return _obj;
}

function evnt_onSelectProvince(str, ev, ui) {
	_prnt = $(ui.item).parents('form') || false;
	if ((! _prnt) || (_prnt.length <= 0)) return false; 
	_val = ui.item.value || -1;
	_elemSelAmphoe = $('.sel-amphoe', _prnt);
	_clearElemValue(_elemSelAmphoe);
	_elemSelAmphoe.empty();
	if (_val <= 0) {
		_setEnableElem(_elemSelAmphoe, false);
	} else {
		_val = _val.toString();
		if (_val in _JSON_AMPHOE) {
			for (_x in _JSON_AMPHOE[_val]) {
				_ea = _JSON_AMPHOE[_val][_x];
				if (('rowid' in _ea) && ('name' in _ea)) _elemSelAmphoe.append('<option value=' + _ea['rowid'] + '>' + _ea['name'] + '</option>');
			}
			_setEnableElem(_elemSelAmphoe, isEnable($('.sel-province', _prnt)));
		}
	}
}

function _doCommitBuildingMapPosition(rowid) {
	if (! _map.hasLayer(_marker)) {
		alert('ยังไม่ได้กำหนดตำแหน่ง');
		return false;
	}
	var _rowid = rowid || 0;
	var _latlng = _marker.getLatLng() || false;
	if ((! _latlng) || (_rowid < 1)) {
		alert('Invalid parameters ( rowid = ' + _rowid + ', LatLng = ' + _latlng + ' )');
		return false;
	}
	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_COMMIT + "</p>")
		.dialog('option', 'title', MSG_DLG_TITLE_COMMIT)
		.dialog( "open" );
	var _params = {"rowid": _rowid, "lat": _latlng.lat, "lng": _latlng.lng};
	_doAjaxRequest(1, "./" + CONTROLLER_NAME + "/commit_map_position", _params, MSG_DLG_TITLE_COMMIT
		, function(data, textStatus, jqXHR) { //success callback
			if (data.success == true) {
				alert(MSG_ALERT_COMMIT_SUCCESS.replace(/v_XX_1/g, ''));
				doSearch(true);
				$('.cls-map-canvas-container').dialog("close");
			} else {
				alert(MSG_ALERT_COMMIT_FAILED.replace(/v_XX_1/g, data.error));
			}				
		}
		, function() { //done callback (no matter success or fail)
			$("#dialog-modal").dialog("close");
		}
	);
}