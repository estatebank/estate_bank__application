var __global = new __clsGlobal();
$(function() {
	$(window).on('beforeunload', function(){
		var _arr = __global._arrListCurrentLockMessage_Strict();
		if (_arr.length > 0) {
			var _msg = _arr.join('\n    - ');
			if (_msg.length > 0) _msg = '    - ' + _msg + '\n\n';
			return 'PAGE_LOCKED: [ CAUTION ] Strict mode!\n\nLeaving page may cause some unsaved data loss.\nPlease recheck lock messages.\n\n' + _msg;
		} else {
			_arr = __global._arrListCurrentLockMessage_Normal();
			if (_arr.length > 0) {
				var _msg = _arr.join('\n    - ');
				if (_msg.length > 0) _msg = '    - ' + _msg + '\n\n';				
				return 'PAGE_LOCKED: Normal mode.\n\n' + _msg;
			}
		}
	});
});

function __clsGlobal() {
	this.__arrPAGE_LOCKS = [];
	
	this._blnSetLock = function (key, message, blnStrict) {
		var _key = ((typeof key == 'string') && (key.trim() != ''))?key.trim():false;
		var _msg = (typeof message == 'string')?message.trim():'';
		var _bln_strick = (typeof blnStrict == 'boolean')?blnStrict:false;
		if (_key === false) return false;
		var _exist = this._objGetLock(_key);
		if (!_exist) {
			this.__arrPAGE_LOCKS.push({'key': _key, 'message': _msg, 'strict': _bln_strick, 'lock': true});
		} else {
			if (_exist['message'] != _msg) {
				if (_exist['message'] != '') _exist['message'] += ', ';
				_exist['message'] += _msg;
			}
			_exist['lock'] = (_exist['lock'] || true);
			_exist['strict'] = (_exist['strict'] || _bln_strick);
		}
		return true;
	};
	this._blnUnLock = function (key) {
		var _key = ((typeof key == 'string') && (key.trim() != ''))?key.trim():false;
		if (_key === false) return false;
		return _removeObjectFromList(this.__arrPAGE_LOCKS, 'key', _key);
	};
	this._objGetLock = function (key) {
		var _key = ((typeof key == 'string') && (key.trim() != ''))?key.trim():false;
		if (_key === false) return false;
		return _findObjectInList(this.__arrPAGE_LOCKS, 'key', _key) || false;
	};
	this._blnSetLock_Normal = function (key, message) {
		return this._blnSetLock(key, message);
	};
	this._blnSetLock_Strict = function (key, message) {
		return this._blnSetLock(key, message, true);
	};	
	this.__arrGetLockObjectList = function (objSpecCond) {
		var _obj_special_cond = (typeof objSpecCond == 'object')?objSpecCond:{};
		var _arrLockedObj = [];
		for (var _i=0;_i<this.__arrPAGE_LOCKS.length;_i++) {
			var _ea = this.__arrPAGE_LOCKS[_i];
			if (('lock' in _ea) && (_ea['lock'] == true)) {
				if (('strict' in _obj_special_cond) && (typeof _obj_special_cond['strict'] == 'boolean')) {
					if (_obj_special_cond['strict'] == _ea['strict']) _arrLockedObj.push($.extend({}, _ea));
				} else {
					_arrLockedObj.push($.extend({}, _ea));
				}		
			}
		}
		return _arrLockedObj;
	};
	this._isPageLocked = function() {
		var _arr = this.__arrGetLockObjectList();
		return (_arr.length > 0);
	}
	this._isPageStrictLocked = function() {
		var _arr = this.__arrGetLockObjectList({'strict':true});
		return (_arr.length > 0);
	};
	this._arrListCurrentLockKey = function() {
		var _arr_list_key = [];
		var _arr = this.__arrGetLockObjectList();
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) _arr_list_key.push(_arr[_i]['key']);
		}
		return _arr_list_key;
	};
	this._arrListCurrentLockKey_Normal = function() {
		var _arr_list_key = [];
		var _arr = this.__arrGetLockObjectList({'strict':false});
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) _arr_list_key.push(_arr[_i]['key']);
		}
		return _arr_list_key;
	};
	this._arrListCurrentLockKey_Strict = function() {
		var _arr_list_key = [];
		var _arr = this.__arrGetLockObjectList({'strict':true});
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) _arr_list_key.push(_arr[_i]['key']);
		}
		return _arr_list_key;
	};
	this._arrListCurrentLockMessage = function() {
		var _arr_list_msg = [];
		var _arr = this.__arrGetLockObjectList();
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) {
				var _str = _arr[_i]['key'];
				if (_arr[_i]['message']) _str += ': ' + _arr[_i]['message'];
				_arr_list_msg.push(_str);
			}
		}
		return _arr_list_msg;
	};
	this._arrListCurrentLockMessage_Normal = function() {
		var _arr_list_msg = [];
		var _arr = this.__arrGetLockObjectList({'strict':false});
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) {
				var _str = _arr[_i]['key'];
				if (_arr[_i]['message']) _str += ': ' + _arr[_i]['message'];
				_arr_list_msg.push(_str);
			}
		}
		return _arr_list_msg;
	};
	this._arrListCurrentLockMessage_Strict = function() {
		var _arr_list_msg = [];
		var _arr = this.__arrGetLockObjectList({'strict':true});
		for (var _i=0;_i<_arr.length;_i++) {
			if (_arr[_i]['key']) {
				var _str = _arr[_i]['key'];
				if (_arr[_i]['message']) _str += ': ' + _arr[_i]['message'];
				_arr_list_msg.push(_str);
			}
		}
		return _arr_list_msg;
	};
}
