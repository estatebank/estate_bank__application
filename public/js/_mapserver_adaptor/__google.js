function __fncGetLeafletLayerControl(map) {
	var _map = map || false;
	if (! _map) return false;
	
	var ggl_sat = new L.Google("SATELLITE", { "unloadInvisibleTiles": true, "updateWhenIdle": true, "detectRetina": true, "reuseTiles": true });
	var ggl_rdm = new L.Google("ROADMAP", { "unloadInvisibleTiles": true, "updateWhenIdle": true, "detectRetina": true, "reuseTiles": true });
	var ggl_hyb = new L.Google("HYBRID", { "unloadInvisibleTiles": true, "updateWhenIdle": true, "detectRetina": true, "reuseTiles": true });
	var ggl_ter = new L.Google("TERRAIN", { "unloadInvisibleTiles": true, "updateWhenIdle": true, "detectRetina": true, "reuseTiles": true });	
	
	_map.addLayer(ggl_rdm);

	var _layerCtrl = new L.Control.Layers(
			{
				"Google Hybrid":ggl_hyb, 
				"Google Road Map":ggl_rdm, 
				"Google Satelltie":ggl_sat, 
				"Google Terrain":ggl_ter
			}, 
			{}
		);
	return _layerCtrl;
}