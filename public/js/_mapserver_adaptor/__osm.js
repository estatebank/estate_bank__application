function __fncGetLeafletLayerControl(map) {
	var _map = map || false;
	if (! _map) return false;
	
	var OpenStreetMapAttribution = 'Map data &copy; 2012 OpenStreetMap contributors';
	var _layOpenSeaMap = new L.TileLayer('http://c.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 17, attribution: OpenStreetMapAttribution});
	//var CloudmadeAttribution = '(V'+L.VERSION+') Imagery &copy; 2012 CloudMade' + ', ' + OpenStreetMapAttribution;	
	_map.addLayer(_layOpenSeaMap);

	var _layers = {"Default": _layOpenSeaMap/*, "NightMode": layCloudmadeNight*/};
	var _layerControl = new L.Control.Layers(_layers);
	
	return _layerControl;
}