function __fncGetLeafletLayerControl(map) {
	var _map = map || false;
	if (! _map) return false;

	var _def = MQ.mapLayer().addTo(_map);
	var _layerControl = new L.Control.Layers({
		'Map': _def
		,'Hybrid': MQ.hybridLayer()
		,'Satellite': MQ.satelliteLayer()
		,'Dark': MQ.darkLayer()
		,'Light': MQ.lightLayer()
	}, null, {"collapse": false});
	
	return _layerControl;
}