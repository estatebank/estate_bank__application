$(function() {
	var _modalImg = $('<img>').addClass('cls-modal-content').attr("id", "img01");
	var _caption = $('<div>').addClass('cls-modal-caption').attr("id", "caption");
	var _close = $('<span>').addClass("cls-modal-close").html('&times;').on("click", function() {
		var _prnt = $($(this).parents('div.cls-modal')[0]);
		if (_prnt.length > 0) _prnt.css('display', 'none');
	});
	var _modal = $('<div>').addClass('cls-modal').attr("id", "myModal").append(_close).append(_modalImg).append(_caption);
	
	$('body').append(_modal);
	
	$('div.cls-image-container:not([style*="background-image:none"])').attr('title', 'Click to enlarge');
	$('*').on('click', 'div.cls-image-container', function() {
		var _div = $(this);
		var _imgPath = _div.css('background-image') || '';
		var _title = _div.attr('alt') || '';
		if (_imgPath == '') return false;
		if (_imgPath.indexOf('url(') >= 0) {
			_imgPath = _imgPath.replace('url(', '');
			_imgPath = _imgPath.substring(0, _imgPath.length - 1).replace(/'|"/g, '');
		}
		if ((_imgPath.toLowerCase() == 'none') || (_imgPath.trim() == '')) return false;
		_modalImg.attr("src", _imgPath);
		_modalImg.alt = _title;
		_caption.html(_title);
		_modal.css("display", "block");
	});
/*
	$('#div_room_images').on('click', 'img', function() {
		var _img = $(this);
		_modalImg.attr("src", _img.attr("src"));
		_modalImg.alt = _img.attr("alt");
		_caption.html(_img.attr("alt"));
		_modal.css("display", "block");
	});
*/
});