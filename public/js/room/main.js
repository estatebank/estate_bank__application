var _displayRoomImgPath, _displayBuildingImgPath;
$(function() {
	_displayRoomImgPath = _baseUrl + 'room/get_room_image/';
	_displayBuildingImgPath = _baseUrl + 'building/get_image_by_building_rowid/';
	$('#frm_edit #sel-owner_type_rowid').on('change', function() {
		if (getValue(this, 0) == 2) {
			_enableElem($('#frm_edit #sel-owner_rowid'), false);
		} else {
			_enableElem($('#frm_edit #sel-owner_rowid'), true);			
		}
	});
	
	var _fncTmpl__blnDataChanged = (typeof blnDataChanged == 'function') ? blnDataChanged : false;
	var _fncTmpl__doSubmit = (typeof doSubmit == 'function') ? doSubmit : false;
	var _fncTmpl___doFetchFormData = (typeof _doFetchFormData == 'function') ? _doFetchFormData : false;
	var _fncTmpl___doClearForm = (typeof _doClearForm == 'function') ? _doClearForm : false;
	
	blnDataChanged = function() {
		var _bln = false;
		//$('.cls-building-detail .user-input').addClass('no-validate');
		_bln = _fncTmpl__blnDataChanged.apply(this, arguments);
		//$('.cls-building-detail .user-input').removeClass('no-validate');
		if (_bln) {
			return true;
		} else {
			/*
			var _strCurrDetails = JSON.stringify(__getBuildingDetails());
			var _strLoadDetails = ('details' in _currEditData) && (_currEditData['details'].toString().trim().length > 0) ? _currEditData['details'].toString().trim() : '{}';
			if (_strLoadDetails != _strCurrDetails) return true;
			*/
			if ($('.user-input.clsCtrl-valueChanged').length > 0) return true;
		}
		return false;
	};

	_doClearForm = function(frm) {
		$('#div_building_image div.cls-image-container').css('background-image', 'none');
		if (typeof _fncTmpl___doClearForm == 'function') return _fncTmpl___doClearForm.apply(this, arguments);
	};
	_doFetchFormData = function(_frm, dataRowObj) {
		if ((typeof dataRowObj != 'object')) return false;
		for (var _i=1;_i<9;_i++) {
			var _imgRowID = ((('img_rowid' + _i) in dataRowObj) && (dataRowObj[('img_rowid' + _i)])) ? dataRowObj[('img_rowid' + _i)] : -1;
			if (_imgRowID > 0) {
				var _elemImg = $('input.user-input[data=image' + _i + ']', _frm);
				var _prnt = $(_elemImg.parents('div.display-upload').get(0));
				if (_prnt.length > 0) _prnt.css('background-image', 'url(' + _displayRoomImgPath + _imgRowID + ')');
			}
		}
		if (('building_rowid' in dataRowObj) && (dataRowObj['building_rowid'] > 0)) __displayBuildingImage(dataRowObj['building_rowid']);
		if (typeof _fncTmpl___doFetchFormData == 'function') return _fncTmpl___doFetchFormData.apply(this, arguments);
	};
	doSubmit = function(frm, opt_fncCallback) {
		var _frm = frm || false;
		if (! _frm) return false;
		_currEditData = {};
		for (var _i=1;_i<9;_i++) {
			_currEditData['img_rowid' + _i] = getValue($('input[type=hidden][data=img_rowid' + _i + ']', _frm), -1);
			_currEditData['image' + _i] = getValue($('input.user-input[data=image' + _i + ']', _frm), '');
			_currEditData['img_title' + _i] = getValue($('input.user-input[data=img_title' + _i + ']', _frm), '');
		}
		$('#div_room_images user-input').addClass('no-commit');
		if (typeof _fncTmpl__doSubmit == 'function') _fncTmpl__doSubmit.apply(this, arguments);
		$('#div_room_images user-input').removeClass('no-commit');
	};
});

function __displayBuildingImage(rowid, elem) {
	var _elem = elem || $('#div_building_image div.cls-image-container');
	if (_elem.length < 1) return false;
	var _buildingRowID = rowid || 0;
	if ((_buildingRowID > 0)) {
		_elem.css('background-image', 'url(' + _displayBuildingImgPath + _buildingRowID + ')');
	} else {
		_elem.css('background-image', 'none');
	}	
}
function _evnt__onChangeBuildingRowID(str, ev, ui) {
	var _buildingRowID = getValue($('#frm_edit #sel-building_rowid'), 0);
	__displayBuildingImage(_buildingRowID);
}

function _doManageDisplay(obj, row_id, title) {
	var _rowid = row_id || false;
	if (_rowid) {
		//var _href = '../index.php?option=com_content&view=article&id=' + link_article_id; //&tmpl=component
		var _href = 'room/display/' + _rowid;
		var _strOptions = 'location=0,menubar=0,status=0,titlebar=0,toolbar=0,resizebar=1,width=' + (screen.width * 0.9) + ',height=' + screen.height + ',left=' + (screen.width * 0.05) + ',top=0,fullscreen=1';
		window.open(_href, '_blank', _strOptions); //
	}
}
