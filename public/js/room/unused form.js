$(function() {
	
	var _fncTemplate_doFetchFormData = _doFetchFormData;
	var _fncTemplate_doSubmit = doSubmit;
	var _fncTemplate_blnDataChanged = blnDataChanged;

	_doFetchFormData = function(_frm, dataRowObj) {
		_fncTemplate_doFetchFormData.apply(this, arguments);
		if (('details' in dataRowObj) && (dataRowObj['details'].toString().trim().length > 0)) {
			var _arrDetails = JSON.parse(dataRowObj['details'].toString().trim());
			if (_arrDetails.length > 0) for (var _i=0;_i<_arrDetails.length;_i++) {
					var _ea = _arrDetails[_i];
					if (typeof _ea == 'string') {
						setValue(_ea, true);
					}
				}
		}
	};

	doSubmit = function (form) {
		var _formIndex = $(form).attr('index');
		if (_currEditData == undefined) _currEditData = {};
		var _arrDetails = __getBuildingDetails();

		$('#hdn-details').addClass('no-commit');
		_currEditData['details'] = _arrDetails;
		//$('#hdn-details').val(JSON.stringify(_arrDetails));
		_fncTemplate_doSubmit.apply(this, arguments);
	};
	
	blnDataChanged = function() {
		if (_fncTemplate_blnDataChanged.apply(this, arguments)) {
			return true;
		} else if ((typeof(_currEditData) == 'object') && ('details' in _currEditData)) {
			var _strCurrDetails = JSON.stringify(__getBuildingDetails());
			if (_currEditData['details'] != _strCurrDetails) return true;
		}
		return false;
	};
});

/*++ Map function */
function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}
function formatLon(long) {
	var pos = Math.abs(long);
	var dir = (long>0 ? 'E' : 'W');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ew = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ew + ' (' + padLeft((Math.round(long * 10000) / 10000).toFixed(4), 8) + ')';
}
function formatLat(lat) {
	var pos = Math.abs(lat);
	var dir = (lat>0 ? 'N' : 'S');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ns = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ns + ' (' + padLeft((Math.round(lat * 10000) / 10000).toFixed(4), 8) + ')';
}

function _doPinMapPosition(obj, map_position, map_area, rowid, title) {
	var _map_position = map_position || [];
	var _map_area = map_area || [];
	var _rowid = rowid || 0;
	var _strDisplayTitle = (typeof title == 'string')?'ของ "' + title + '"':'';
	var _divDialog = $('.cls-map-canvas-container');
	if ((_rowid <= 0) || (_divDialog.length < 1)) return false;

	_divDialog.attr('rowid', _rowid); //use in commit function
	if (_map_position.length != 2) _map_position = false;
	if (_map_area.length != 2) _map_area = false;

	if (_map_position) {
		_map_position = new L.LatLng(_map_position[1], _map_position[0]);
		_marker.setLatLng(_map_position).addTo(_map);
		_map.panTo(_map_position);
	} else if (_map_area) {
		_map_area = new L.LatLng(_map_area[1], _map_area[0]);
		_map.panTo(_map_area);
	}
	
	if ($('#spn_map_position').length <= 0) $('.ui-dialog-buttonset', _divDialog.parent()).before('<span id="spn_map_position" class="cls-spn-map-position"></span>');	
	if (! _map.hasLayer(_marker)) {
		$('#spn_map_position').html('Double click บนแผนที่เพื่อวางตำแหน่งอาคาร  ( ปรับเปลี่ยนได้ด้วยการลาก icon และกด "บันทึก" )');
		_map.on('dblclick', function (ev) {
			_marker.setLatLng(ev.latlng).addTo(_map);
		});
	}
	_divDialog.dialog('option', {
			title: 'กำหนดพิกัดแผนที่' + _strDisplayTitle + ' ( rowid ' + rowid + ' )'
			,position: {my: 'right top', at: 'left-10 center-10', of: obj}
		}).dialog("open");
	
}

/*-- Map function */

function _doManageDisplay(obj, link_article_id, title) {
	var _id = link_article_id || false;
	if (_id) {
		var _href = '../index.php?option=com_content&view=article&id=' + link_article_id; //&tmpl=component
		window.open(_href, '_blank', 'location=0,menubar=0,titlebar=0,toolbar=0,width=600,height=450');
	}
}

function __getBuildingDetails() {
	var _arr = [];
	$('.cls-building-detail .user-input').each(function() {
		var _val = getValue(this) || false;
		var _data = getData(this) || false;
		if ((_val > 0) && (_data)) {
			_arr.push(_data);
		}
	});
	return _arr;
}

function evnt_onSelectProvince(str, ev, ui) {
	_prnt = $(ui.item).parents('form') || false;
	if ((! _prnt) || (_prnt.length <= 0)) return false; 
	_val = ui.item.value || -1;
	_elemSelAmphoe = $('.sel-amphoe', _prnt);
	_clearElemValue(_elemSelAmphoe);
	_elemSelAmphoe.empty();
	if (_val <= 0) {
		_setEnableElem(_elemSelAmphoe, false);
	} else {
		_val = _val.toString();
		if (_val in _JSON_AMPHOE) {
			for (_x in _JSON_AMPHOE[_val]) {
				_ea = _JSON_AMPHOE[_val][_x];
				if (('rowid' in _ea) && ('name_th' in _ea)) _elemSelAmphoe.append('<option value=' + _ea['rowid'] + '>' + _ea['name_th'] + '</option>');
			}
			_setEnableElem(_elemSelAmphoe, isEnable($('.sel-province', _prnt)));
		}
	}
}

function _doCommitBuildingMapPosition(rowid) {
	if (! _map.hasLayer(_marker)) {
		alert('ยังไม่ได้กำหนดตำแหน่ง');
		return false;
	}
	var _rowid = rowid || 0;
	var _latlng = _marker.getLatLng() || false;
	if ((! _latlng) || (_rowid < 1)) {
		alert('Invalid parameters ( rowid = ' + _rowid + ', LatLng = ' + _latlng + ' )');
		return false;
	}
	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_COMMIT + "</p>")
		.dialog('option', 'title', MSG_DLG_TITLE_COMMIT)
		.dialog( "open" );
	var _params = {"rowid": _rowid, "lat": _latlng.lat, "lng": _latlng.lng};
	_doAjaxRequest(1, "./" + CONTROLLER_NAME + "/commit_map_position", _params, MSG_DLG_TITLE_COMMIT
		, function(data, textStatus, jqXHR) { //success callback
			if (data.success == true) {
				alert(MSG_ALERT_COMMIT_SUCCESS.replace(/v_XX_1/g, ''));
				doSearch(true);
				$('.cls-map-canvas-container').dialog("close");
			} else {
				alert(MSG_ALERT_COMMIT_FAILED.replace(/v_XX_1/g, data.error));
			}				
		}
		, function() { //done callback (no matter success or fail)
			$("#dialog-modal").dialog("close");
		}
	);
}