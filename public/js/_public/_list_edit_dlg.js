if (typeof _doBindEditDialog == 'undefined') {
	var _currEditTr, _currEditData;
	var _arrCurrEditData = [];
	var _ARR_EditDialogButtons = [];
	var _doBindEditDialog = function(elem) {
		_elem = _toJQObj(elem);
		_elem.dialog({
			height:600,
			width:'80%',
			show: {effect:"puff",duration: 1000},
			hide: {effect:"fade",duration:1000},
			resizable:true,
			modal:true,
			closeOnEscape:false,
			autoOpen:false,
			close: function() {
				if (typeof _doClearForm == 'function') _doClearForm($('.cls-frm-edit', this));
				/* ++ return currEditData to master set */
				var _master = $('.cls-frm-edit:visible');
				if (_master.length > 0) {
					var _index = $(_master).attr('index') || -1;
					if ((_index > -1) && (_index in _arrCurrEditData)) {
						_currEditData = _arrCurrEditData[_index];
					}
				}
				/* -- return currEditData to master set */
			}
		}).parent('.ui-dialog').css('zIndex', 1000);
	};
}