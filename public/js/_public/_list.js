	$(function() {
		$("#divSelectableFields").dialog({
			height:150,
			width:800,
			show: {effect:"flip",duration:1000},
			hide: {effect:"fade",duration: 1000},
			title: MSG_DLG_TITLE_SELECT_FIELDS,
			resizable:false,
			modal:true,
			closeOnEscape:true,
			autoOpen:false,
			open: function () {
				if (!_objDataTable) return false;
				var _bVis = false;
				for (i = 0;i < _objDataTable.fnSettings().aoColumns.length;i++) {
					_bVis = _objDataTable.fnSettings().aoColumns[i].bVisible;
					$( "#li_col_" + i ).prop("checked", _bVis);
				}
			},
			buttons: {
				'Apply': function() {
					if (! _objDataTable) return false;
					$( "#divSelectableFields ul li input[type='checkbox']" ).each(function () {
						//console.log($( this ).prop('id').substr(7) + " is " + $(this).prop('checked'));
						_idx = $( this ).prop('id').substr(7) || 0;
						_objDataTable.fnSetColumnVis(_idx, $(this).prop('checked'));
					});
					$(this).dialog('close');
				},
				'Cancel': function() {
					$(this).dialog('close');
				}
			}
		});

		$('.cls-div-list').on('click', "table tbody td img",
			function () {
				_doCommandClick(this);
				return false;
			}
		);
	});

	function _doCommandClick(img) {
		if ($( img ).is('.link-disabled') || $( img ).is('.disabled') || ($( img ).parents('.link-disabled').length > 0) || ($( img ).parents('.disabled').length > 0) ) return false;
		var _command = $( img ).attr('command');
		var _tr = $( img ).parents('tr')[0];
		var _index = $($( img ).parents('.cls-div-list')[0]).attr("index");
		var _divEditDlg = $(".cls-div-form-edit-dialog[index=" + _index + "]");
		var _datatable = $( img ).parents('.cls-tbl-list')[0];
		var _aData = $(_datatable).dataTable().fnGetData(_tr);
		if (_aData) {
			switch (_command) {
				case 'view':
					$('*.eventView-hide').addClass('hidden');
					if (typeof doView == 'function') doView(_aData, _divEditDlg);
					break;
				case 'edit':
					$('*.eventView-hide').removeClass('hidden');
					if (typeof doEdit == 'function') doEdit(_aData, _tr, _divEditDlg);
					break;
				case 'delete':
					if (typeof doDelete == 'function') {
						if (confirm(MSG_CONFIRM_DELETE_ROW.replace('v_XX_1', ''))) {
							doDelete(_aData, _tr, _divEditDlg);
						}
					}
					break;
				case 'cancel':
					if (typeof doCancel == 'function') {
						if (confirm(MSG_CONFIRM_CANCEL_ROW.replace('v_XX_1', ''))) {
							doCancel(_aData, _tr, _divEditDlg);
						}
					}
					break;
				case 'link':
					var _target = $(img).attr('target') || 'new';
					var _href = $(img).attr('href') || '';
					if (_href !== '') {
						if (_target == 'new') {
							window.open(_href);
						} else if (_target == 'self') {
							window.location.href = _href;
						}
					}
					break;
				case 'link_post':
					var _target = $(img).attr('target') || 'new';
					var _href = $(img).attr('href') || '';
					var _strParams = $( img ).attr('params') || '';
					var _params = {};
					_target = _target.toLowerCase();
					if (_strParams.trim().length > 0) {
						_strParams = _strParams.trim();
						_params = JSON.parse(_strParams);
					}
					var _frm = document.createElement('FORM');
					_frm.setAttribute('method', 'post');
					_frm.setAttribute('action', _href);
					if (_target == 'new') _frm.setAttribute('target', '_blank');
					if (typeof _params == 'string') {
						_elem = document.createElement('INPUT');
						_elem.setAttribute('type', 'hidden');
						_elem.setAttribute('name', 'params');
						_elem.value = _params;
						_frm.appendChild(_elem);
					} else if (typeof _params == 'object') {
						_elem = document.createElement('INPUT');
						_elem.setAttribute('type', 'hidden');
						_elem.setAttribute('name', 'params');
						_elem.value = JSON.stringify(_params);
						_frm.appendChild(_elem);
					}
					_frm.submit();
					break;
				default:
					if (typeof eval(_command) == 'function') {
						var _strParams = $( img ).attr('params') || '';
						if (_strParams.trim().length > 0) {
							_strParams = _strParams.trim();
							if (_strParams.substr(0, 1) != '[') _strParams = '[' + _strParams;
							if (_strParams.substr(-1) != ']') _strParams = _strParams + ']';
						}
						var _params = eval(_strParams) || [];
						eval(_command).apply(this, _params);
					}
					break;
			}
		}
	}

	function doSelectDisplayFields() {
		$("#divSelectableFields").dialog( "open" );
	}

	function _fncDTmRenderFormat(data, type, full, format) {
		if (data) {
			var _fmt = format || 'default_number';
			switch (_fmt.toLowerCase()) {
				case 'default_number':
					var _data = data.toString().replace(/\,/g, '');
					if (isNaN(_data)) {
						return data;
					} else {
						return formatNumber(parseFloat(_data), 2, true, true);
					}
					break;
				case 'default_integer':
					var _data = data.toString().replace(/\,/g, '');
					if (isNaN(_data)) {
						return data;
					} else {
						return formatNumber(parseFloat(_data), 0, true, true);
					}
					break;
			}
		}
		return data;
	}
