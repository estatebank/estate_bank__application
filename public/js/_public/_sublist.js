if (typeof _Sublist_arrDtColumns == 'undefined') {
	var _Sublist_arrDtColumns = [];
	var _Sublist_aoColumns = [];
	var _arrSublistDataTable = [];
	var _Sublist_tableToolButtons = [];
/*
	$(function() {
		$(".cls-div-sub-list").on("subTableLoaded", function(indx, objDataTable, arrMasterLink) {
			setTimeout(_doResizeDatatableHeader, 1200);
		});
	});
	function _doResizeDatatableHeader() {
		if (_arrSublistDataTable) {
			if (_arrSublistDataTable.length > 0) {
				for (i in _arrSublistDataTable) {
					if ($.isFunction(_arrSublistDataTable[i].fnAdjustColumnSizing)) _arrSublistDataTable[i].fnAdjustColumnSizing();
				}
			}
		}
	}
*/
	var _masterLink = {};
	function populateSublist(blnEditable, objLink, opt_fncCallback) {
		if (objLink) _masterLink = objLink;
		$(".cls-div-sub-list").each(function() {
			var _controller = $(this).attr('controller');
			var _datastring = $(this).attr('main-search');
			var _index = $(this).attr('index');
			var _divEditDlg = $('.cls-div-form-edit-dialog[index=' + _index + ']').get(0);
			var _this = this;
			doClearDisplayInfo(_index);
			doClearDisplayError('frmSublistFailed', _index);
			$.ajax({
				type: "POST", 
				url: "./" + _controller + "/json_search",
				contentType: "application/json;charset=utf-8",
				dataType: "json",
				data: _datastring,
				success: function(data, textStatus, jqXHR) {
					if (data.success == false) {
						var _msg = MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, data.error);
						doDisplayError('frmSublistFailed', _msg, true, _index);
					} else {
						data = data.data;
						var _arrayData = [];
						if (data.length > 0) {
							for (var i = 0; i < data.length; i++) {
								_arrayData[i] = {'client_temp_id':i};
								for (j=0;j<_Sublist_arrDtColumns[_index].length;j++) {
									_arrayData[i][_Sublist_arrDtColumns[_index][j][0]] = (data[i][_Sublist_arrDtColumns[_index][j][0]] == null)?'':data[i][_Sublist_arrDtColumns[_index][j][0]];
								}
							}
						}
						_doPopulateSublistTable(_this, _arrayData, blnEditable);
					}
					if (typeof opt_fncCallback == 'function') opt_fncCallback.apply(this, arguments);
					$('.cls-div-sub-list[index="' + _index + '"]').trigger("DataTableLoaded", [_index, _arrSublistDataTable[_index], _masterLink]);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					doDisplayInfo(MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, textStatus + ' ( ' + errorThrown + ' )'), "ErrorMessage", _index);
				},
				statusCode: {
					404: function() {
						doDisplayInfo("Page not found", "ErrorMessage", _index);
					}
				}
			});
		});
	}
	
	function _doPopulateSublistTable(div, arrayData, blnEditable) {
		var _div = div || $(".cls-div-sub-list").get(0);
		var _index = $(_div).attr('index');
		var _arrayData = arrayData || [];
		var _blnEditable = (blnEditable === false)?false:true;
		if (_arrSublistDataTable.length > 0) {
			if (_index in _arrSublistDataTable) if ($.isFunction(_arrSublistDataTable[_index].fnDestroy)) {
				_arrSublistDataTable[_index].fnDestroy(true);
				_arrSublistDataTable.splice(_index, 1);
			}
		}
		$(_div).html('<table id="tblSubList' + _index + '" class="cls-tbl-list cls-tbl-sub-list"></table>');
		var _arrColumns = _Sublist_aoColumns[_index].slice(0);
		var _arrButtons = (_Sublist_tableToolButtons[_index] != undefined)?_Sublist_tableToolButtons[_index]:[];

		_arrSublistDataTable[_index] = $('#tblSubList' + _index).dataTable(
			{
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"bDeferRender": true,
				"bAutoWidth": false,
				"aaData": _arrayData,
				"aaSorting":[],
				"sScrollY": "50%",
				"sScrollX": "95%",
				"sScrollXInner": "100%",
				"bScrollCollapse": true,
				"aLengthMenu": [[5, 15, 35, -1], [5, 15, 35, "all"]],
				"iDisplayLength": 5,
				"bStateSave": false,
				"fnStateLoadParams": false,
				"aoColumns": _arrColumns,
				"sDom": "<'row-fluid'<'span6'T><'span6'lf>r>t<'row-fluid'<'span6'i><'span6'p>><'clear'><'span6'T>",
				"oTableTools": {
					"aButtons": _arrButtons,
					"sSwfPath": "public/js/jquery/dataTable/TableTools/2.1.5/swf/copy_csv_xls_pdf.swf"
				},
				"fnInitComplete": function(oSettings, json) {
					oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
					setTimeout(function () { _arrSublistDataTable[_index].fnAdjustColumnSizing(true); }, 1200);
					return false;
					//more delay time because of animation load dialog
				}
			}
		);
		_doSetEnableSubList(_div, _blnEditable);
	}
	
	function _doSetEnableSubList(div, bln) {
		var _elem_div = div || $(".cls-div-sub-list").get(0);
		var _div = $(_elem_div);
		var _index = _div.attr('index') || -1;
		var _dt;
		if (_index >= 0) _dt = _arrSublistDataTable[_index];
		var _bln = true;
		if (bln === false) _bln = false;
		if (_bln) {
			if (_dt) {
				_len = _dt.fnSettings().aoColumns.length || 0;
				if (_len > 0) {
					_dt.fnSetColumnVis((_len - 1), true);
					_dt.fnSetColumnVis((_len - 2), true);
				}
			}
//			$('.cls-tbl-sub-list td img[command="edit"]', _div).css('display', '');
//			$('.cls-tbl-sub-list td img[command="delete"]', _div).css('display', '');
			if ((_div.attr('main-search') || '').trim() == '') {
				_div.find('.DTTT_button_space').css('display', 'none');
				_div.find('.DTTT_button_add_row').css('display', 'none');		
			} else {
				_div.find('.DTTT_button_space').css('display', '');
				_div.find('.DTTT_button_add_row').css('display', '');				
			}
		} else {
			if (_dt) {
				_len = _dt.fnSettings().aoColumns.length || 0;
				if (_len > 0) {
					_dt.fnSetColumnVis((_len - 1), false);
					_dt.fnSetColumnVis((_len - 2), false);
				}
			}
//			$('.cls-tbl-sub-list td img[command="edit"]', _div).css('display', 'none');
//			$('.cls-tbl-sub-list td img[command="delete"]', _div).css('display', 'none');
			_div.find('.DTTT_button_space').css('display', 'none');
			_div.find('.DTTT_button_add_row').css('display', 'none');
		}
	}
	
	function _clearSubListData(index) {
		var _p_index = index || -1;
		if (_p_index >= 0) {
			_dt = _arrSublistDataTable[_p_index];
			if (_dt) _dt.fnClearTable();
		} else {
			for (_i in _arrSublistDataTable) {
				_arrSublistDataTable[_i].fnClearTable();
			}
		}
	}
	
	function clearSubList() {
		$(".cls-div-sub-list").each(function() {
			var _index = $(this).attr('index');
			if (_index in _arrSublistDataTable) {
				_arrSublistDataTable[_index].fnDestroy(true);
			}
			$(this).html('<table id="tblSubList' + _index + '" class="cls-tbl-list cls-tbl-sub-list"></table>');
		});
		_arrSublistDataTable.length = 0;
	}
}