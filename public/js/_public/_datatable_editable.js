$.editable.addInputType('datepicker', {
	element: function(settings, original) {
		var input = $('<input />');
		input.datepicker({
			dateFormat: 'dd/mm/yy',
			changeYear: true,
			changeMonth: true,
			onSelect: function(dateText, inst) {
				$(this).parents("form").submit();
				inst.submit = true;
			},
			onClose: function(dateText, inst) {
				if ('submit' in inst) {
					delete inst['submit'];
				} else {
					$(this).parents("form").submit();
				}
			}
		});
		$(this).append(input);
		
		if (settings.maxDate) $('input', this).datepicker("option", "maxDate", settings.maxDate);
		
		settings.onblur = function (value, settings) {
			if ($('input', this).datepicker( "widget" ).is(":visible")) {
				return false;
			} else {
				return value;
			}
		};

		return (input);
	},
	plugin: function(settings, original) {
		var form = this;
		$("input", this).filter(":text").datepicker({
			onSelect: function(dateText) { $(this).hide(); $(form).trigger("submit"); }
		});
	}
});

var _objChangedData = {};
$(function() {
	//++ Warning leaving page without save
	$(window).bind('beforeunload', function(){
		if (_fnCheckDisplayDataChanged()) return MSG_CONFIRM_LEAVE_PAGE_WITHOUT_SAVE;
	});
	//-- Warning leaving page without save

	//++ Add special datatable properties and tools
	if (_tableToolButtons) {
		_tableToolButtons.push({"sExtends": "text", "sButtonText": "", "sButtonClass": "DTTT_button_space"});
		_tableToolButtons.push({"sExtends": "text",	"sButtonText": "Commit Data", "sButtonClass": "DTTT_button_commit_page DTTT_button_disabled",	
			"fnClick": function ( nButton, oConfig, oFlash ) {
				if (! _fnCheckDisplayDataChanged()) return false;
				if ($('td.edit form').length > 0) return false; //Editing
				if (typeof _doCommitPage == 'function') {
					_doCommitPage();
				} else {
					console.error('No funtion "_doCommitPage"');
				}
			}
		});
	}
	//-- Add special datatable properties and tools
	
	var _fnDefault_doPopulateTable = doPopulateTable;
	var _fnDefault_doSearch = doSearch;
	var _fnDefault__doResize = _doResize;

	doSearch = function (blnChangeSearchCriteria) {
		if (_fnCheckDisplayDataChanged()) {
			if ( ! confirm(MSG_CONFIRM_LEAVE_PAGE_WITHOUT_SAVE)) return false;
		}
		_fnDefault_doSearch.apply(this, [blnChangeSearchCriteria]);
	};
	
	_doResize = function () {
		_objDataTable.fnAdjustColumnSizing(false);
		$('#tblSearchResult thead').css('visibility', 'hidden');
	};

});

function _fnCheckDisplayDataChanged() {
	var nButton = $('.DTTT_button_commit_page');
	if ($(nButton).hasClass('DTTT_button_disabled') || (JSON.stringify(_objChangedData) == '{}')) {
		_objChangedData = {};
		$(nButton).addClass('DTTT_button_disabled');
		return false;
	} else {
		$(nButton).removeClass('DTTT_button_disabled');
		return true;
	}
}
