if (typeof __IS_LOAD_FORM_JS != 'undefined') exit();
__IS_LOAD_FORM_JS = true;
$(function() {
	$('#frm_edit').on('click', '[command=link]', function() {
		if ($(this).hasClass('link-disabled')) return false;
		var _target = $(this).attr('target') || 'new';
		var _href = $(this).attr('href') || '';
		if (_href != '') {
			_index = $($(this).parents('.cls-frm-edit').get(0)).attr('index') || -1;
			if ((typeof _arrPanelLinkFields == 'object') && (_index in _arrPanelLinkFields)) {
				var _objEditPanelData = _arrPanelLinkFields[_index];
				var _match;
				var _Regexp = /\{([^\}]+)\}/g;
				var _newHref = _href;
				_match = _Regexp.exec(_href);
				while (_match != null) {
					if (_match[1] in _objEditPanelData) _newHref = _newHref.replace(_match[0], _objEditPanelData[_match[1]]);
					_match = _Regexp.exec(_href);
				}
				_href = _newHref;
			}
			if (_target == 'new') {
				window.open(_href);
			} else if (_target == 'self') {
				window.location.href = _href;
			}
		}
	});
});
function _clickSubmit(btn) {
	var _frm = $('form', $(btn).parents('.ui-dialog')).get(0);
	$(_frm).attr('state', 'submit');
	if (blnDataChanged(_frm)) {
		if (blnValidateContainer(true, _frm, '.user-input')) {
			doSubmit(_frm);
		}
	} else {
		alert(MSG_ALERT_COMMIT_NO_CHANGE);
	}
}
function _clickReset(btn) {
	var _frm = $('form', $(btn).parents('.ui-dialog')).get(0);
	if (blnDataChanged(_frm)) {
		var _divDialog = $(_frm).parents(".cls-div-form-edit-dialog").get(0);
		if (confirm(MSG_CONFIRM_CANCEL_EDITED_PANEL)) doEdit("", "", _divDialog);
	} else {
		alert(MSG_ALERT_COMMIT_NO_CHANGE);
	}
}
function _clickCancel(btn) {
	var _frm = $('form', $(btn).parents('.ui-dialog')).get(0);
	$(_frm).attr('state', '');
	var _divDialog = $(_frm).parents(".cls-div-form-edit-dialog").get(0);
	if ($(".cls-btn-form-submit", $(btn).parents('.ui-dialog')).css('display') == 'none') { //View
		if ($(_divDialog).dialog("isOpen")) $(_divDialog).dialog("close");
	} else {
		if (blnDataChanged(_frm)) {
			if (confirm(MSG_CONFIRM_CANCEL_EDITED_PANEL)) if ($(_divDialog).dialog("isOpen")) $(_divDialog).dialog("close");
		} else {
			if ($(_divDialog).dialog("isOpen")) $(_divDialog).dialog("close");
		}
	}
}
function doInsert(divEditDlg) {
	_currEditTr = undefined;
	_currEditData = undefined;
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	$(_frm).attr('state', 'insert');
	if (typeof _doClearForm == 'function') _doClearForm(_frm);
	if ($(divEditDlg).find('.cls-div-sub-list').length > 0) {
		var _divSublist = $(divEditDlg).find('.cls-div-sub-list').get(0);
		if (typeof _doPopulateSublistTable == 'function') _doPopulateSublistTable(_divSublist);
	}
	doSetEnableUserInput(_frm, true);
	if (!$(divEditDlg).dialog("isOpen")) $(divEditDlg).dialog('option', 'title', MSG_DLG_TITLE_INSERT_FORM.replace(/v_XX_1/g, '')).dialog("open");
}

function doView(dataRowObj, divEditDlg) {
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	$(_frm).attr('state', 'view');
	if (typeof _doClearForm == 'function') _doClearForm(_frm);		
	if ((dataRowObj) && (_frm)) {
		if ($(divEditDlg).find('.cls-div-sub-list').length > 0) {
			var _arrSearchParams = {};
			var _dummy1 = $('.cls-div-sub-list').attr('master_cols');
			var _dummy2 = $('.cls-div-sub-list').attr('map_cols');
			if ((_dummy1.trim().length > 0) && (_dummy2.trim().length > 0)) {
				var _arr1 = _dummy1.split(',');
				var _arr2 = _dummy2.split(',');
				if ((_arr1.length > 0) && (_arr2.length > 0)) {
					for (var _i=0;_i<_arr2.length;_i++) {
						if ((_arr1.length > _i) && (_arr2.length > _i) && (_arr1[_i].trim().length > 0)) {
							var _str = _arr1[_i].trim();
							if (_str in dataRowObj) _arrSearchParams[_arr2[_i].trim()] = dataRowObj[_str];
						}
					}
				}
				var _strSearch = JSON.stringify(_arrSearchParams);
				$('.cls-div-sub-list').attr('main-search', _strSearch);
				if (typeof populateSublist == 'function') populateSublist(false, _arrSearchParams);
			}
		}
		doSetEnableUserInput(_frm, false);
		_doFetchFormData(_frm, dataRowObj);	

		var _strShowTitle = ('rowid' in dataRowObj)?' ( rowid: ' + dataRowObj['rowid'] + ' )':'';
		if (!$(divEditDlg).dialog("isOpen")) $(divEditDlg).dialog('option', 'title', MSG_DLG_TITLE_VIEW_FORM.replace(/v_XX_1/g, _strShowTitle)).dialog("open");
	}
}

function doEdit(dataRowObj, trObj, divEditDlg) {
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	var _index = $(_frm).attr('index') || 2;
	$(_frm).attr('state', 'edit');
	if (typeof _doClearForm == 'function') _doClearForm(_frm);
	if (dataRowObj && _frm) {
		_currEditTr = trObj;
		_currEditData = dataRowObj;
		_arrCurrEditData[_index] = _currEditData;
	}
	if (_currEditData && _frm) {		
		if ($(divEditDlg).find('.cls-div-sub-list').length > 0) {
			var _arrSearchParams = {};
			var _dummy1 = $('.cls-div-sub-list').attr('master_cols');
			var _dummy2 = $('.cls-div-sub-list').attr('map_cols');
			if ((_dummy1.trim().length > 0) && (_dummy2.trim().length > 0)) {
				var _arr1 = _dummy1.split(',');
				var _arr2 = _dummy2.split(',');
				if ((_arr1.length > 0) && (_arr2.length > 0)) {
					for (var _i=0;_i<_arr2.length;_i++) {
						if ((_arr1.length > _i) && (_arr2.length > _i) && (_arr1[_i].trim().length > 0)) {
							var _str = _arr1[_i].trim();
							if (_str in _currEditData) _arrSearchParams[_arr2[_i].trim()] = _currEditData[_str];
						}
					}
				}
				var _strSearch = JSON.stringify(_arrSearchParams);
				$('.cls-div-sub-list').attr('main-search', _strSearch);
				if (typeof populateSublist == 'function') populateSublist(true, _arrSearchParams);
			}
		}
		doSetEnableUserInput(_frm, true);
		_doFetchFormData(_frm, _currEditData);		

		var _strShowTitle = ('rowid' in dataRowObj)?' ( rowid: ' + dataRowObj['rowid'] + ' )':'';
		if (!$(divEditDlg).dialog("isOpen")) $(divEditDlg).dialog('option', 'title', MSG_DLG_TITLE_EDIT_FORM.replace(/v_XX_1/g, _strShowTitle)).dialog("open");
	}
}

function doCancel(dataRowObj, trObj, divEditDlg, opt_fncCallback) {
	var _index = $(divEditDlg).attr('index');
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	var _url = "./" + $(_frm).attr("controller") + "/cancel";
	var _params = '{"rowid":' + dataRowObj['rowid'] + '}';
	
	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_CANCEL + "</p>");
	$("#dialog-modal").dialog('option', 'title', MSG_DLG_TITLE_CANCEL);
	$("#dialog-modal").dialog("open");
	_doAjaxRequest(_index, _url, _params, 'frmCancel', 
		function (data, textStatus, jqXHR) {
			if (data.success == false) {
				doDisplayError('frmCancelFailed', MSG_ALERT_CANCEL_FAILED.replace(/v_XX_1/g, data.error), true, _index);
			} else {
				if (_index <= 2) {
					if (typeof doSearch == 'function') doSearch(false);
				} else {
					if (typeof populateSublist == 'function') populateSublist(true);
				}
				alert(MSG_ALERT_CANCEL_SUCCESS.replace(/v_XX_1/g, ''));
			}
		}, 
		function () {
			if (typeof opt_fncCallback == 'function') opt_fncCallback.apply(this, arguments);
			if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
		}
	);
}

function doDelete(dataRowObj, trObj, divEditDlg, opt_fncCallback) {
	var _index = $(divEditDlg).attr('index');
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	var _url = "./" + $(_frm).attr("controller") + "/delete";
	var _params = '{"rowid":' + dataRowObj['rowid'] + '}';

	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_DELETE + "</p>")
		.dialog('option', 'title', MSG_DLG_TITLE_DELETE)
		.dialog("open");
	
	_doAjaxRequest(_index, _url, _params, 'frmDelete', 
		function (data, textStatus, jqXHR) {
			if (data.success == false) {
				doDisplayError('frmDeleteFailed', MSG_ALERT_DELETE_FAILED.replace(/v_XX_1/g, data.error), true, _index);
			} else {
				if (_index <= 2) {
					if (typeof doSearch == 'function') doSearch(false);
				} else {
					if (typeof populateSublist == 'function') populateSublist(true);
				}
				alert(MSG_ALERT_DELETE_SUCCESS.replace(/v_XX_1/g, ''));
			}
		}, 
		function () {
			if (typeof opt_fncCallback == 'function') opt_fncCallback.apply(this, arguments);
			if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
		}
	);
}

function doGenPDF(dataRowObj, divEditDlg) {
	var _frm = $(divEditDlg).find(".cls-frm-edit").get(0);
	var _index = $(divEditDlg).attr('index');
	if (dataRowObj && _frm) {
		window.open("./" + $(_frm).attr("controller") + "/get_pdf/" + dataRowObj['rowid']);
	}
}

function doSubmit(form, opt_fncCallback) {
	var _index = $(form).attr('index') || 0;
	doClearDisplayError('frmSubmitFailed', _index);
	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_COMMIT + "</p>")
		.dialog('option', 'title', MSG_DLG_TITLE_COMMIT)
		.dialog("open");

	var _update = {};
	if (_currEditData !== undefined) _update = $.extend({}, _currEditData);
	$(form).find(".user-input:not(.no-commit)").each(
		function () {
			_tag = this.tagName.toLowerCase();
			_type = this.type;
			if (_tag == 'input' && (_type == 'radio')) {
				_name = $(this).prop('name');
				_val =_getElemValue(this);
				if (_val) _update[_name] = _val;
			} else {
				_name = $(this).prop('id').substr(4);
				_update[_name] = _getElemValue(this);
			}
		}
	);
	//++add master link (foreign key) if master details style
	if (typeof _masterLink != 'undefined') {
		for (_key in _masterLink) {
			if (_key in _update) {
				_update[_key] = _masterLink[_key];
			}
		}
	}
	//--add master link (foreign key) if master details style
	_str = JSON.stringify(_update);
	$.ajax({
		type:"POST",
		url:"./" + $(form).attr("controller") + "/commit",
		contentType: "application/json;charset=utf-8",
		dataType:"json",
		data: _str,
		success: function(data, textStatus, jqXHR) {
			if (data.success == false) {
				var _msg = MSG_ALERT_COMMIT_FAILED.replace(/v_XX_1/g, data.error);
				doDisplayError('frmSubmitFailed', _msg, true, _index);
				if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
			} else {
				if (typeof fncAfterSubmitSuccess == 'function') fncAfterSubmitSuccess(data);
				_currEditTr = undefined;
				_currEditData = undefined;
				//if (typeof _doClearForm == 'function') _doClearForm(form); //moved to on close dialog (_list_edit_dlg.js)
				var _divDialog = $(form).parents(".cls-div-form-edit-dialog").get(0);
				if ($(_divDialog).dialog("isOpen")) $(_divDialog).dialog("close");
/*
				if (_index <= 2) {
					if (typeof doSearch == 'function') doSearch(false);
				} else {
					if (typeof populateSublist == 'function') populateSublist(true);
				}
*/
				if (typeof _divDialog != 'undefined') {
					if ($(_divDialog).attr("id").indexOf("Sublist") >= 0) {
						if (typeof populateSublist == 'function') populateSublist(true, null, opt_fncCallback);
					} else {
						if (typeof doSearch == 'function') doSearch(false, opt_fncCallback);
					}
					if ($(_divDialog).dialog( "isOpen" )) $(_divDialog).dialog( "close" );
				}
				alert(MSG_ALERT_COMMIT_SUCCESS.replace(/v_XX_1/g, ''));
				if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			//doDisplayInfo(textStatus + ' : ' + errorThrown, "ErrorMessage", _index);
			doDisplayError('frmSubmitFailed', textStatus + ' : ' + errorThrown, true, _index);
			if ($("#dialog-modal").dialog("isOpen"))  $("#dialog-modal").dialog("close");
		},
		statusCode: {
			404: function() {
				//doDisplayInfo("Page not found", "ErrorMessage", _index);
				doDisplayError('frmSubmitFailed', "Page not found", true, _index);
				if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
			}
		}
	});
}

function _doClearFormError(form) {
	var _form = form || $('form');
	var _div = $( _form ).parents('.cls-div-form-edit-dialog')[0];
	doClearVldrError(_div);
}
function _doClearForm(form) {
	var _form = form || $('form');
	var _div = $( _form ).parents('.cls-div-form-edit-dialog')[0];
	doClearVldrError(_div);
	if ($( _div ).find(".cls-div-sub-list").length > 0) {
		$( _div ).find('.cls-div-sub-list').attr('main-search', '');
		if ($.isFunction(clearSubList)) clearSubList();
	}
	doClearUserInput(_form);
	//++ Clear data value for special link
	_index = $(_form).attr('index') || -1;
	if ((typeof _arrPanelLinkFields == 'object') && (_index in _arrPanelLinkFields)) {
		var _objEditPanelData = _arrPanelLinkFields[_index];
		for (_key in _objEditPanelData) {
			$(_form).find('[href*="{' + _key + '}"]').each(function() { $(this).addClass('link-disabled'); });
			_objEditPanelData[_key] = "";
		}
	}
	//-- Clear data value for special link
}
function _doFetchFormData(form, arrData) {
	var _form = form || $('form');
	doFetchDataContainer(arrData, _form);
	//++ prepare data value for special link
	_index = $(_form).attr('index') || -1;
	if ((typeof _arrPanelLinkFields == 'object') && (_index in _arrPanelLinkFields)) {
		var _objEditPanelData = _arrPanelLinkFields[_index];
		for (_key in _objEditPanelData) {
			if (_key in arrData) {
				$(_form).find('[href*="{' + _key + '}"]').each(function() { $(this).removeClass('link-disabled'); });
				_objEditPanelData[_key] = arrData[_key];
			}
		}
	}
	//-- prepare data value for special link
}

function blnDataChanged(form) {
	var _isChanged = false;
	var _form = form || $('form');
	$('.user-input:not(".no-validate")', _form).each(
		function () {
			var _elem = _getJQUserInputElement(this);
			if (_elem.is('readonly') || _elem.is('disabled')) return true;
			var _value = _getElemValue(_elem);
			var _dataField = _getElemData(_elem);
			if ((typeof _currEditData != 'undefined') && (_dataField != '') && (_currEditData[_dataField])) {
				var _oldVal = _currEditData[_dataField];
				if (_elem.hasClass('hasDatepicker')) {
					var _format = _elem.datepicker("option", "dateFormat");
					var _old = '';
					var _curr = '';	
					_oldDate = new Date(_oldVal);
					if (Object.prototype.toString.call(_oldDate) === '[object Date]') {
						_old = $.datepicker.formatDate(_format, _oldDate);
					}
					var _currVal = _elem.val();
					var _currDate;
					if (_format == 'mm/yy') {
						_currDate = $.datepicker.parseDate('dd/' + _format, '01/' + _currVal);						
					} else {
						_currDate = $.datepicker.parseDate(_format, _currVal);
					}
					if (Object.prototype.toString.call(_currDate) === '[object Date]') {
						_curr = $.datepicker.formatDate(_format, _currDate);
					}
					if (_old != _curr) {
						_isChanged = true;
						return false;
					}
				} else {
						if (_elem.is('.input-integer, .input-double, .input-number')) _oldVal = _cleanNumericValue(_oldVal);
					if (this.type == 'checkbox') {
						switch (_oldVal.toLowerCase()) {
							case "t":
							case "1":
							case "true":
								_oldVal = 1;
								break;
							default:
								_oldVal = 0;
								break;
						}
					} else if ((this.type == "file") && (typeof _value == 'string')) {
						if (_value.toLowerCase() == 'unchange') {
							_value == _oldVal;
						//} else if (_value.toLowerCase() == 'remove') {
						}
					}
					if ((_value || 0) != (_oldVal || 0)) {
						_isChanged = true;
						return false;
					}					
				}
			} else {
				if ((_value || false) != false) {
					_isChanged = true;
					return false;
				}
			}
		}
	);
//console.debug('changed = ' + _isChanged);
	return _isChanged;
}