if (typeof _doCommitJSPN == 'undefined') {
	function _doCommitJSPN(dlg) {
		$("#dialog-modal").html("<p>" + MSG_DLG_HTML_COMMIT + "</p>").dialog('option', 'title', MSG_DLG_TITLE_COMMIT).dialog("open");
		var _toUpdate = {};
		if (_currEditData !== undefined) _toUpdate = $.extend({}, _currEditData);
		$(dlg).find(".data-container").each(
			function () {
				_toUpdate[getData(this)] = getValue(this);
			}
		);
		var _ctrl = $(dlg).attr('controller') || '';
		var _cmd = $(dlg).attr('command') || 'add';
		var _commit_path = $(dlg).attr('commit_path') || (_ctrl + "/commit_jspn_add");
		var _for = $(dlg).attr('for') || '';
		var _parent_form_index = $(dlg).attr('form_index') || -1;
		var _parent_form_selector = $(dlg).attr('form_selector') || 'form';
		var _curr_form_index = $('.cls-frm-edit', dlg).attr('index') || -1;
		_params = JSON.stringify(_toUpdate);
		_doAjaxRequest(
			_curr_form_index, 
			_commit_path, 
			_params,
			'CommitJSPN',
			function (data, textStatus, jqXHR) { //on success
				if ('data' in data) {
					var _data = data['data'];
					var _value = -1;
					if ('added_rowid' in _data) {
						_value = _data['added_rowid'];
					}
					if ((_for != '') && ('list' in _data) && (_data['list'].length > 0)) {
						var _frm;
						var _elem_to_update;
						if (_parent_form_index > 0) {
							_frm = $(_parent_form_selector + '[index="' + _parent_form_index + '"]');
						} else {
							_frm = $($(_parent_form_selector).get(0));
						}
						if (_frm.length == 1) {
							_elem_to_update = $('#' + _for, _frm);
						} else {
							_elem_to_update = $('#' + _for);
						}
						if (_elem_to_update.length <= 0) return false;
						var _list = _data['list'];
						var _tag = _elem_to_update.get(0).tagName.toLowerCase();
						if (_tag == 'select') {
							__doUpdateElem_Select(_elem_to_update, _list, _value);
						}
					}
					$(dlg).removeAttr('controller');
					$(dlg).removeAttr('commit_path');
					$(dlg).removeAttr('for');
					$(dlg).removeAttr('form_index');
					$(dlg).removeAttr('form_selector');
					$(dlg).dialog('close');
				}
			}
			, function () {
				$("#dialog-modal").dialog("close");
			}
		);
	}
}

if (typeof __doUpdateElem_Select == 'undefined') {
	function __doUpdateElem_Select(elem, arrData, value) {
		var _elem = _toJQObj(elem);
		var _arrData = arrData || [];
		var _value = value || -1;
		
		if (! (_elem instanceof jQuery)) {
			console.error('Invalid object ( jQuery expected ): ' + _elem.toString());
			return false;
		}
		var _sel_val = _elem.attr('sel_val') || 'rowid';
		var _sel_text = _elem.attr('sel_text') || 'name';
		_elem.empty();
		for (var _i=0;_i<_arrData.length;_i++) {
			_strOpt = '<option value="' + _arrData[_i][_sel_val] + '">' + _arrData[_i][_sel_text] + '</option>';
			_elem.append(_strOpt);
		}
		
		if (_value >= 0) _setElemValue(_elem, _value);
	}
}

if (typeof _objJSPN_AddOptions == 'undefined') {
	var _objJSPN_AddOptions = {
		height:700,
		width:900,
		show:{effect:"flip",duration:1000},
		hide:{effect:"fade",duration:1000},
		resizable:true,
		modal:true,
		closeOnEscape:false,
		autoOpen:false,
		buttons:[
			{
				text:MSG_DLG_BUTTON_TEXT_SUBMIT,
				class:"cls-btn-form-submit",
				icons:{primary: "ui-icon-disk"},
				click: function(evnt) {
					if (blnValidateContainer(true, this, '.user-input')) _doCommitJSPN(this);
				}
			},
			{
				text:MSG_DLG_BUTTON_TEXT_CANCEL,
				class:"cls-btn-form-cancel",
				icons: { primary: "ui-icon-extlink"},
				click: function(evnt) {
					$( this ).dialog("close");
				}
			}
		],
		close: function(evnt) {
			if (typeof _doClearForm == 'function') _doClearForm($('form', this));
		}
	};
}

if (typeof _doBindJSPNButton == 'undefined') {
	var _OBJ_JSON_LOADED_FUNCTIONS = {
		_doClearForm:{}
		,doInsert:{}
		,doView:{}
		,doEdit:{}
		,doDelete:{}
		,_doCommitJSPN:{}
		,blnDataChanged:{}
	};
	var __ajaxJSPNRequest = function(index, ctrl, cmd, load_path, prm, commit_path, for_elem, frm_selector, trg) {
		var _index = (index || 0)
			, _ctrl = (_ctrl || CONTROLLER_NAME)
			, _cmd = (cmd || 'add')
			, _load_path = (load_path || (_ctrl + '/jspn_' + _cmd))
			, _prm = (prm || '')
			, _commit_path = (commit_path || (_ctrl + '/commit_jspn_' + _cmd))
			, _for = (for_elem || '')
			, _frm_selector = (frm_selector || 'form')
			, _trg = (trg || 'self')
		;
		if (_load_path.trim() != '') {
			var _hrf = _load_path + '/' + _index;
			$("#dialog-modal").html("<p>" + MSG_DLG_HTML_QUERY + "</p>").dialog('option', 'title', MSG_DLG_TITLE_QUERY).dialog("open");
			_doAjaxRequest(
				_index, 
				_hrf, 
				_prm,
				'RequestJSPNData',
				function (data, textStatus, jqXHR) { //on success
					if ('panel' in data) {
						var _html = ('html' in data['panel'])?data['panel']['html']:'';
						var _options = ('option' in data['panel'])?data['panel']['option']:{};
						if ('options' in data['panel']) _options = data['panel']['options'];
						var _jQDivId = 'div_jspn' + _index; //'#div_jspn_add'
						if ($('#'+_jQDivId).length > 0) {
							if ($('#'+_jQDivId).hasClass('ui-dialog-content')) $('#'+_jQDivId).dialog("destroy");
							$('#'+_jQDivId).remove('');
						}
						var _div = $('<div id="' + _jQDivId + '">').html(_html);
						if (_div.length > 0) {
							for (var _f in _OBJ_JSON_LOADED_FUNCTIONS) {
								if (typeof window[_f] == 'function') {
									if (typeof _OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] != 'function') {
										_OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] = window[_f];
									} else if (_OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] != window[_f]) {
										window[_f] = _OBJ_JSON_LOADED_FUNCTIONS[_f]['main'];
									}
								} else if (typeof _OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] != 'function') {
									delete _OBJ_JSON_LOADED_FUNCTIONS[_f]['main'];
								}
							}
							var _scripts = $('script', _div).detach();
							for (var _i=0;_i<_scripts.length;_i++) {
								if (_scripts[_i].src) {
									_doLoadJSPNScriptFile(_div, _scripts[_i].src);
								} else {
									_doLoadJSPNScriptFile(_div, _scripts[_i], 'custom');
								}
							}
							$('#middle_panel').append(_div);
							for (var _f in _OBJ_JSON_LOADED_FUNCTIONS) {
								if ((typeof window[_f] == 'function') && (typeof _OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] == 'function') && (_OBJ_JSON_LOADED_FUNCTIONS[_f]['main'] != window[_f])) {
									_OBJ_JSON_LOADED_FUNCTIONS[_f]['json'] = window[_f];
								}
							}
							var _objJSPNOpt = $.extend(true, {}, _objJSPN_AddOptions);
							if ($('span.error-message', _div).length > 0) {
								if ($('span.error-message', _div).html().trim().length > 0) {
									_objJSPNOpt.buttons[0]['disabled'] = true;
									_objJSPNOpt.buttons[1]['disabled'] = true;
								}
							} else {
								if ((_cmd == 'view') || (('editable' in _options) && (_options['editable'] == false))) {
									_objJSPNOpt.buttons.shift();
								}
							}
							_div.dialog(_objJSPNOpt);
							_div.attr('controller', _ctrl);
							_div.attr('command', _cmd);
							_div.attr('commit_path', _commit_path);
							_div.attr('for', _for);
							_div.attr('form_selector', _frm_selector);
							_div.attr('form_index', _index);
							for (_key in _options) {
								_div.dialog('option', _key, _options[_key]);
							}
							_div.dialog('open');
						}
					}
				},
				function () {
					$("#dialog-modal").dialog( "close" );
				}
			);
		}
		
	};
	var _doBindJSPNButton = function(form) {
		_jqForm = _toJQObj(form);
		var _index = _jqForm.attr('index') || -1;
		var _frm_selector = '';
		if (_index <= 0) {
			_parents = _jqForm.parents('form[index]');
			if (_parents.length > 0) {
				_index = $(_parents[0]).attr('index') || 2;
				_frm_selector = ($(_parents[0]).attr('id') || '').trim();
			}
		}
		if (_index <= 0) {
			_parents = _jqForm.parents('div[index]');
			if (_parents.length > 0) {
				_index = ($(_parents[0]).attr('index') || 1);
			}
		}
		_jqForm.on('click', 'img.jspn-button', function() {
			if ($(this).hasClass('link-disabled')) return false;
			if (_frm_selector.length > 0) {
				_frm_selector = '#' + _frm_selector.trim();
			} else {
				_frm_selector = 'form';
			}
			var _trg = $(this).attr('target') || 'self';
			var _for = $(this).attr('for') || '';
			var _ctrl = $(this).attr('controller') || '';
			var _cmd = $(this).attr('command') || 'add';
			var _load_path = $(this).attr('load_path') || (_ctrl + '/jspn_' + _cmd);
			var _commit_path = $(this).attr('commit_path') || (_ctrl + '/commit_jspn_' + _cmd);
			var _prm = JSON.parse($(this).attr('params') || '{}');
			__ajaxJSPNRequest(_ctrl, _cmd, _load_path, _prm, _commit_path, _for, _frm_selector, _trg);
		});
	};
}
//$.ajaxPrefilter(function( options, originalOptions, jqXHR ) { options.async = true; });
if (typeof _doLoadJSPNScriptFile == 'undefined') {
	var _JSPN_CNTR;
	if (typeof _objJSPNLoadScriptLogger == 'undefined') _objJSPNLoadScriptLogger = {};
	if (typeof _arrJSPNOnLoadCustomScriptBlock == 'undefined') _arrJSPNOnLoadCustomScriptBlock = [];
	_intJSPNPendingLoadFile = 0;
	var __fncJSPN_FileLoaded = function(strSrcName) {
		if (((strSrcName || '').trim() != '') && (strSrcName in _objJSPNLoadScriptLogger)) {
			_objJSPNLoadScriptLogger[strSrcName] = 'done';
			_intJSPNPendingLoadFile -= 1;
		}
		if (_intJSPNPendingLoadFile <= 0) __fncApplyOnLoadScriptBlock();
	};
	var __fncApplyOnLoadScriptBlock = function () {
		for (var _i=0;_i<_arrJSPNOnLoadCustomScriptBlock.length;_i++) {
			_JSPN_CNTR.append(_arrJSPNOnLoadCustomScriptBlock[_i]);
		}
	};
	var _doLoadJSPNScriptFile = function(container, script, strFileType) {
		_JSPN_CNTR = container || document;
		_JSPN_CNTR = _toJQObj(_JSPN_CNTR);
		if (_JSPN_CNTR.length < 1) return false;
		
		var _script = script || false;
		var _strType = (typeof strFileType == 'string')?strFileType:'js';
		var _sc = false;
		if (typeof _script == 'string') {
			if (_strType.toLowerCase() == "js") { //if is a external JavaScript file
				_sc = document.createElement('script');
				_sc.setAttribute("type","text/javascript");
				_sc.setAttribute("src", _script);
			} else if (_strType.toLowerCase() == "css") { //if is an external CSS file
				_sc = document.createElement("link");
				_sc.setAttribute("rel", "stylesheet");
				_sc.setAttribute("type", "text/css");
				_sc.setAttribute("href", _script);
			}
		} else {
			_sc = _script;
		}
		var _exists = false;
		if (_sc != false) {
			if ((_strType.toLowerCase() != 'custom')) {
				$('script', document).each(function() {
					if (this.src == _sc.src) {
						_exists = true;
						return false;
					}
				});
				if (! _exists) {
					$('script', _JSPN_CNTR).each(function() {
						if (this.src == _sc.src) {
							_exists = true;
							return false;
						}
					});
				}
			}
			if ((! _exists)) {
				if (_sc.src !== '') {
					//_JSPN_CNTR.append(_sc);
					_objJSPNLoadScriptLogger[_sc.src] = 'loading';
					_intJSPNPendingLoadFile += 1;
					$.getScript(_sc.src, function() { __fncJSPN_FileLoaded(_sc.src); });
				} else {
					_arrJSPNOnLoadCustomScriptBlock.push(_sc);
					//_JSPN_CNTR.append(_sc); //add to bottom of container since it will clear everytime close dialog
				}
			}
		}
	};
}