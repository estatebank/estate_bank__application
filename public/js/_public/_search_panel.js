	var _blnDataChanged = false;
	var _objDataTable;
	var _currentDataString; //use in re-query after change data

	$(function() {
		$("#btnSearch").button().click(function() {
			doSearch(true);
		});
		$("#btnReset").button().click(function() {
			_clearByJQSelector($('#frmSearch'), '.search-param:not(".data-constant")');
		});
		$("#dialog-modal").html("<p>" + MSG_DLG_HTML_QUERY + "</p>");
		$("#dialog-modal").dialog({
			height:100,
			width:400,
			resizable:false,
			modal:true,
			closeOnEscape:false,
			title: MSG_DLG_TITLE_QUERY,
			autoOpen:false
		});
/*
		// add back buttons
		if (($('#frmSearch').length > 0) && (window.opener == null) && (window.history.length > 1)) {
			$('<a onclick="window.history.back();" class="btn-page-back">Back</a>')
				.button({icons:{primary: 'ui-icon-arrowthick-1-w'}})
				.addClass('cls-navigator')
				.insertBefore($('#frmSearch'));
		}
*/
/*		
		if ((typeof _autoSearch_OnLoad == 'boolean') && (_autoSearch_OnLoad == false)) {
			doPopulateTable([], true);
		} else {
			$("#btnSearch").trigger('click');
		}
*/
		doPopulateTable([], true);
	});

	function doSearch(blnChangeSearchCriteria) {
		var _index = $('#divDisplayQueryResult').attr('index') || 0;
		if (_aoColumns.length == 0) return;
		if (typeof _currEditTr != 'undefined') _currEditTr = undefined;
		if (typeof _currEditData != 'undefined') _currEditData = undefined;

		var _blnChangeSearchCriteria = typeof blnChangeSearchCriteria != undefined?blnChangeSearchCriteria:true;
		doClearDisplayInfo();
		doClearDisplayError();
		if (_objDataTable) _objDataTable.fnClearTable();
		if (_blnChangeSearchCriteria) {
			$("#dialog-modal").dialog( "open" );
			_currentDataString = "";			
			$(".search-param").each(
				function() {
					var _name = getData(this);
					var _val = getValue(this);
					if ((_val || '') != '') _currentDataString += '"' + _name + '":"' + _val + '",';
				}
			);
			if (_currentDataString.length > 0) _currentDataString = "{" + _currentDataString.substr(0, _currentDataString.length - 1) + "}";
		}
		$.ajax({
			type: "POST", 
			url: "./" + CONTROLLER_NAME + "/json_search",
			contentType: "application/json;charset=utf-8",
			dataType: "json",
			data: _currentDataString,
			success: function(data, textStatus, jqXHR) {
				if (data.success == false) {
					var _msg = MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, data.error);
					doDisplayError('frmSearchFailed', _msg, true, _index);
				} else {
					data = data.data;
					var _arrayData = [];
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							_arrayData[i] = {'client_temp_id':i};
							for (j=0;j<_arrDtColumns.length;j++) {
								_arrayData[i][_arrDtColumns[j][0]] = (data[i][_arrDtColumns[j][0]] == null)?'':data[i][_arrDtColumns[j][0]];
							}
						}
					} else {
						doDisplayInfo(MSG_ALERT_QUERY_NO_DATA_FOUND, 'Info', _index);
					}
					doPopulateTable(_arrayData, _blnChangeSearchCriteria);
				}
				$('#divDisplayQueryResult').trigger("DataTableLoaded", [_index, _objDataTable]);
				if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog( "close" );
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$("#dialog-modal").dialog( "close" );
				doDisplayInfo(MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, textStatus + ' ( ' + errorThrown + ' )'), "ErrorMessage", _index);
			},
			statusCode: {
				404: function() {
					$("#dialog-modal").dialog( "close" );
					doDisplayInfo("Page not found", "ErrorMessage", _index);
				}
			}
		});
		return false;
	}

	function doPopulateTable(arrData, blnChangeSearchCriteria) {
		var _blnChangeSearchCriteria = typeof blnChangeSearchCriteria != undefined?blnChangeSearchCriteria:true;
		if (_objDataTable) _objDataTable.fnDestroy(true);
		$('#divDisplayQueryResult').html('<table id="tblSearchResult" class="cls-tbl-list"></table>');
		_objDataTable = $('#tblSearchResult').dataTable(
			{
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"bDeferRender": true,
				"bAutoWidth": false,
				"aaData": arrData,
				"aaSorting":[],
				"sScrollY": "85%",
				"sScrollX": "95%",
				"sScrollXInner": "100%",
				"aLengthMenu": [[15, 25, 35, 50, -1], [15, 25, 35, 50, "all"]],
				"iDisplayLength": 15,
				"bStateSave": true,
				"fnStateLoadParams": function (oSettings, oaData) {
					if (_blnChangeSearchCriteria) { //Destroy state saving if requery
						_blnChangeSearchCriteria = false;
						return false;
					}
				},
				"aoColumns": _aoColumns,
				"sDom": "<'row-fluid'<'span6'T><'span6'lf>r>t<'row-fluid'<'span6'i><'span6'p>><'clear'><'span6'T>",
				"oTableTools": {
					"aButtons": (_tableToolButtons != undefined)?_tableToolButtons:[],
					"sSwfPath": "public/js/jquery/dataTable/TableTools/2.1.5/swf/copy_csv_xls_pdf.swf"
				},
				"fnInitComplete": function(oSettings, json) {
					oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
					setTimeout(_doResize, 1000);
				}
			}
		);
	}
	function _doResize() {
		_objDataTable.fnAdjustColumnSizing(true);
	}
	function _visibleButtonColumns(blnVis) {
		_bln = (blnVis || false);
		_colLength = 0;
		if (_aoColumns) _colLength = _aoColumns.length;
		if ((_objDataTable) && (_colLength > 3)) {
			_objDataTable.fnSetColumnVis(_colLength - 3, _bln);		
			_objDataTable.fnSetColumnVis(_colLength - 2, _bln);		
			_objDataTable.fnSetColumnVis(_colLength - 1, _bln);		
		}
		return false;
	}
