$(function() {
	$('#divPanelHandler').click(function() {
		doToggleLeftPanel();
	});
	$('#divPanelHandler').css('left', $('#left_panel').width());
});
function doToggleLeftPanel() {
	if ($('#left_panel').css('display') !== 'none') {
		$('#left_panel').css('display', 'none');
		$('#divPanelHandler').css('left', '1em');
	} else {
		$('#left_panel').css('display', 'block');
		$('#divPanelHandler').css('left', $('#left_panel').width());
	}
	if ((typeof _objDataTable != 'undefined') && (typeof(_objDataTable.draw) == 'function')) _objDataTable.draw();
}
