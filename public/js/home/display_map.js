var _MAP_CANVAS_ID = 'disp_layout_map';

var _DEF_ICON = new L.Icon({iconUrl: 'public/images/marker-red.png', iconSize: [21, 25], iconAnchor: [10, 25]});
var _OBJ_BT_ICONS = {
	'11': new L.Icon({iconUrl: 'public/images/marker-blue.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'21': new L.Icon({iconUrl: 'public/images/marker-gold.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'31': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
};
var _LNK_FULLSCREEN;

/* ++ little hack starts here, for display multiple popup */
L.Map = L.Map.extend({
    openPopup: function(popup) {
		this._popup = popup;

        return this.addLayer(popup).fire('popupopen', {
            popup: this._popup
        });
    }
});
/* -- end of hack */
function _doInitMap(geoJsonData) {
	var _geoJsonData = geoJsonData || {};

	var map = L.map(_MAP_CANVAS_ID, {
		center: L.latLng(_DEFAULT_LAT, _DEFAULT_LNG)
		,minZoom: _MIN_ZOOM_LEVEL
		,maxZoom: _MAX_ZOOM_LEVEL
		,zoom: _DEFAULT_ZOOM_LEVEL
		,tap: false
		,touchZoom: false
		,closePopupOnClick: false
	})
		.on("contextmenu", function (e) {alert("Context menu disabled")})
		//.on("moveend", function (e) { _doUpdateDisplay(); }) //Activate over speed notification and update view
	;
	
	//Add Scale Control
	L.control.scale( { maxWidth:200 } ).addTo(map);
	//Add Mouse Position Control
	L.control.mousePosition( { 
		position: 'bottomright',
		latFormatter: formatLat,
		lngFormatter: formatLon,
	}).addTo(map);
	
	/* ++ Tuneup map for mobile device */
	if (L.Browser.touch) {
		L.Util.setOptions(map, {
			tap: true
			,zoomAnimation: false
			,markerZoomAnimation: false
		});
	}
	L.Util.emptyImageUrl = "public/images/blank.gif";
	/*-- Buff 20130807 Tuneup map for mobile device*/

	var layerControl = false;
	if (typeof __fncGetLeafletLayerControl == 'function') layerControl = __fncGetLeafletLayerControl.apply(this, [map]);
	if (! layerControl) {
		alert('ERROR: Map init function not found! ( __fncGetLeafletLayerControls )');
		return false;
	}
	map.addControl(layerControl);
	
	/*++ Create toggle fullscreen control */
	var _myControl = L.control( {position: "topleft"} );
	_myControl.onAdd = function (map) {
		var _container = map.zoomControl._container; //L.DomUtil.create("div", "leaflet-control-zoom-fullscreen leaflet-bar");
		_LNK_FULLSCREEN = L.DomUtil.create('a', 'leaflet-control-zoom-fullscreen', _container);
		_LNK_FULLSCREEN.href = '#';
		_LNK_FULLSCREEN.title = 'Toggle Full Screen Mode';
		L.DomEvent
			.addListener(_LNK_FULLSCREEN, 'click', L.DomEvent.stopPropagation)
			.addListener(_LNK_FULLSCREEN, 'click', L.DomEvent.preventDefault)
			.addListener(_LNK_FULLSCREEN, 'click', fnc_toggleFullScreen, map)
			.addListener(_LNK_FULLSCREEN, 'dblclick', L.DomEvent.stopPropagation);
		return _container;
	}
	map.addControl(_myControl);
	/*-- Create toggle fullscreen control */

	/*++ Create toggle measure control */
	ctrlMeasure = L.control.measure();
	ctrlMeasure.onAdd = function (map) {
		/*
		var _className = 'leaflet-control-zoom leaflet-bar leaflet-control',
		var _container = L.DomUtil.create('div', _className);
		*/
		var _container = map.zoomControl._container;
		this._createButton('&#8674;', 'Measure', 'leaflet-control-measure', _container, this._toggleMeasure, this);
		return _container;
	};
	ctrlMeasure.addTo(map);
	/*-- Create toggle measure control */
	//&& ('type' in _geoJsonData) && (_geoJsonData['type'] == 'FeatureCollection')) {
	if ((typeof _geoJsonData == 'object') && ('features' in _geoJsonData) && (_geoJsonData['features'].length > 0)) {
		L.geoJson(_geoJsonData, {
			pointToLayer: function (feature, latlng) {
				var _opt = { riseOnHover:true, icon: _DEF_ICON};
				var _b_type_rowid = parseInt(feature.properties.b_type_rowid || 0);
				if (_b_type_rowid in _OBJ_BT_ICONS) _opt['icon'] = _OBJ_BT_ICONS[_b_type_rowid.toString()];
				return L.marker(latlng, _opt);
			}
			, onEachFeature: function (feature, layer) {
				layer.bindPopup("", { offset: [0, -15], className: 'cls-my-popup-content' })
					.bindLabel( feature.properties.code, { offset: [15, -30] })
					.on('popupopen', function(e) {
						e.popup.setContent(fnc_strDisplayPopup(feature));
					})
					.on("popupclose", function (e) {
						
					})
				;
			}
		}).addTo(map);
	}
}

function fnc_strDisplayPopup(feature) {
	var _b_type_rowid = parseInt(feature.properties.b_type_rowid || 0);
	var _rowid = parseInt(feature.properties.rowid || 0);
	var _code = feature.properties.code || '';
	var _disp_b_type = feature.properties.disp_b_type || '';
	var _disp_name = feature.properties.disp_name || '';
	var _descrp = feature.properties.description || '';
	if (_descrp.length > 20) _descrp = _descrp.substring(0, 20) + '...';
	var _html = '';
	switch (_b_type_rowid) {
		case 1:
			//break;
		default:
			_html += '<table class="cls-tbl-popup-bldng"><thead><tr><th colspan="3">' + _disp_name + '</th></tr></thead>';
			_html += '<tfoot><tr><td colspan="3">&nbsp;</td></tr></tfoot><tbody>';
			_html += '<tr><td class="cls-td-popup-title">รหัสอาคาร:</td><td class="cls-td-popup-label">' + _code  + '</td>';
			_html += '<td class="cls-td-popup-image" rowspan="4">';
			_html += '<img id="img_popup_' + _rowid + '" class="cls-popup-image" src="home/get_building_display_image/' + _rowid + '"></td></tr>';
			_html += '<tr><td class="cls-td-popup-title">ประเภท:</td><td class="cls-td-popup-label">' + _disp_b_type + '</td></tr>';
			_html += '<tr><td class="cls-td-popup-title">รายละเอียด:</td><td class="cls-td-popup-label">' + _descrp + '</td></tr>';
			_html += '</tbody></table>';
			break;
	}
	return _html;
}

function fnc_toggleFullScreen() {
	// if content inside iFrame, fullscreen function have to call from iFrame element [ $( window.frameElement ).toggleFullScreen( true ); ]
	var _dest = $( window.frameElement );
	if ((_dest.length <= 0) || ((_dest.attr('src') || '') == '')) _dest = $( window.document );
	if (_dest.fullScreen()) {
		$( _LNK_FULLSCREEN ).removeClass('leaflet-control-zoom-fullscreen-on');
	} else {
		$( _LNK_FULLSCREEN ).addClass('leaflet-control-zoom-fullscreen-on');
	}
	_dest.toggleFullScreen( true );
}

function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

function formatLon(long) {
	var pos = Math.abs(long);
	var dir = (long>0 ? 'E' : 'W');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ew = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ew + ' (' + padLeft((Math.round(long * 10000) / 10000).toFixed(4), 8) + ')';
}

function formatLat(lat) {
	var pos = Math.abs(lat);
	var dir = (lat>0 ? 'N' : 'S');
	var deg = padLeft(Math.floor(pos), 3);
	var min = padLeft(Math.floor((pos-deg)*60), 3);
	var sec = padLeft(Math.floor((pos-deg-min/60)*3600), 3);
	var ns = deg + '\u00B0' + min + "'" + sec + '"' + dir;
	return ns + ' (' + padLeft((Math.round(lat * 10000) / 10000).toFixed(4), 8) + ')';
}
