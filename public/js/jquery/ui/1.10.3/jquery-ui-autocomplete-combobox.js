(function( $ ) {
	$.widget( "ui.combobox", {
		_create: function() {
			var self = this;
			this.element.hide();
			this.wrapper = $( "<span>" )
				.addClass( "ui-combobox" )
				.insertAfter( this.element );

			this._createAutocomplete();
			this._createShowAllButton();
		},
		_createAutocomplete: function() {
			var selected = this.element.children( ":selected" ),
				value = selected.val() ? selected.text() : "";
			var _self = this;
			/*
			$.ui.autocomplete.prototype._renderItem = function( ul, item) {
				var li = $( "<li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + item.label + "</a>" );
				if (item.is_new) li.addClass('combobox-newitem');
				if (item.class) li.addClass(item.class);
				li.appendTo( ul );
				return li;
			};
			*/
			this.input = $( "<input>" )
				.appendTo( this.wrapper )
				.val( value )
				.attr( "title", "" )
				.addClass( "ui-combobox-input ui-widget ui-widget-content ui-corner-left" ) //ui-state-default 
				.autocomplete({
					delay: 0
					, minLength: 0
					, source: $.proxy( this, "_source" )
					, open: function(event) {
						$(this).autocomplete('widget').css('z-index', 5000);
						event.preventDefault();
						return false;
					}
				})
				.tooltip({
					tooltipClass: "ui-state-highlight",
					position: { my: "left+15 center", at: "right center" }
				})
			;
			this.input.data('ui-autocomplete')._renderItem = function(ul, item) {
				var _li = $("<li>")
					.data("item.autocomplete", item)
					.append("<a>" + item.label + "</a>");
				if (item.is_new) _li.addClass('combobox-newitem');
				if (item.class) _li.addClass(item.class);
				_li.appendTo(ul);
				return _li;
			};
			/* ++ Inferno 20160401: add placeholder */
			if ($(this.element).attr('placeholder')) this.input.attr('placeholder', $(this.element).attr('placeholder'));
			/* -- Inferno 20160401: add placeholder */				
			this._on( this.input, {
				autocompleteselect: function( event, ui ) {
					if (typeof this.options.blnBeforeChange == 'function') {
						if (! this.options.blnBeforeChange(value, event, ui)) return false;
					}
					if (! ((typeof ui == 'object') && (typeof ui.item == 'object') && ('option' in ui.item)) ) return true;
					var _uiObj = {item: ui.item.option};
					ui.item.option.selected = true;
					this._trigger( "select", event, _uiObj);
					/* ++ Buff : Add for allow add new to ui.combobox */
					if (this.options.is_allow_add) {
						this._displayNew();
					}
					/* -- Buff : Add for allow add new to ui.combobox */
					if (typeof this.options.changed == 'function') {
						this.options.changed( ui.item.value, jQuery.Event( "combobox.change" ), _uiObj );
					}
				},
				autocompletechange: "_checkSelectValue"
			});
		},
		_createShowAllButton: function() {
			var wasOpen = false;
			var input = this.input;
			this.btnButton = $( "<a>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Show All Items" )
				.tooltip()
				.appendTo( this.wrapper )
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "ui-corner-right ui-combobox-toggle" )
				.mousedown(function(event) {
					wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					return false;
				})
				.click(function(event) {
					if ( wasOpen ) {
						return false;
					}
					// Pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
					input.focus();
					return false;
				});
		},
		_source: function( request, response ) {
			var _term = (request.term)?$.ui.autocomplete.escapeRegex(request.term):'';
			var _matcher;
			var _blnHasTerm = false;
			if ((typeof _term == 'string') && (_term.trim().length > 0)) {
				_blnHasTerm = true;
				_matcher = new RegExp(_term, "i");
			}
			var _self = this;
			response(this.element.children( "option" ).map(function() {
				var _text = $( this ).text();
				var _isNew = $( this ).attr('new');
				var _class = $( this ).attr('class');
				var _label = _text;
				var _blnIsValid = true;
				if (_blnHasTerm) {
					_label = _label.replace(_matcher, "<strong>" + request.term + "</strong>");
					_blnIsValid = _matcher.test(_text);
					if ((! _blnIsValid) && (_self.options.arr_add_matching) && (_self.options.arr_add_matching.length > 0)) {	
						for (var _i=0;_i < _self.options.arr_add_matching.length;_i++) {
							var _ea = _self.options.arr_add_matching[_i];
							var _str = '';
							switch (_ea.type) {
								case 'attr': 
									_str = $( this ).attr(_ea.data) || '';
									break;
							}
							if (_matcher.test(_str)) {
								_blnIsValid = true;
								break;
							}
						}
					}
				}
				if (_blnIsValid) {
					return {
						label: _label,
						value: _text,
						is_new: _isNew,
						class: _class,
						option: this
					};
				}
			}) );
		},
		_checkSelectValue: function( event, ui ) {
			var thisInput = this.input;
			var self = this;
			// Selected an item, nothing to do
			if ( ui.item ) {
				event.preventDefault();
				return false;
			}
			// Search for a match (case-insensitive)
			var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false,
				_blnInterupt = false;
			if (valueLowerCase.trim().length == 0) {
				this.clearValue();
				return false;
			}
			this.element.children( "option" ).each(function() {
				if ($(this).text().toLowerCase() === valueLowerCase ) {
					event.preventDefault();
					this.selected = valid = true;
					if (typeof self.options.blnBeforeChange == 'function') {
						if (! self.options.blnBeforeChange(value, jQuery.Event( "combobox.beforechange" ), {item: this})) {
							_blnInterupt = true;
							return false;
						}
					}
					thisInput.val($(this).text());
					self._trigger( "select", event, { item: this });
					/* ++Buff : Add for allow add new to ui.combobox */
					// Case already added and select it again
					if (self.options.is_allow_add) {
						self._displayNew();
					}
					/* --Buff : Add for allow add new to ui.combobox */
					if (self.options.changed) {
						if (typeof self.options.changed == 'function') {
							self.options.changed(value, jQuery.Event( "combobox.change" ), { item: this });
						}
					}
					return false; //break out from loop each function
				}
			});
			if (_blnInterupt) return false;
			// Found a match, nothing to do
			if ( ! valid ) {
				/* ++Buff : Add for allow add new to ui.combobox */
				if (typeof this.options.blnBeforeChange == 'function') {
					if (! this.options.blnBeforeChange(value, jQuery.Event( "combobox.beforechange" ), ui)) return false;
				}
				if (this.options.is_allow_add) {
					var newOption = $('<option></option>').val(value).html(value).attr('new', true);
					newOption.selected = true;
					this.element.append(newOption);
					this._addNew();
					if (self.options.changed) {
						if (typeof self.options.changed == 'function') {
							self.options.changed(value, jQuery.Event( "combobox.change" ), { item: newOption });
						}
					}	
				} else {
					// Remove invalid value
					this.input
						.val( "" )
						.attr( "title", '"' + value + '"' + " didn't match any item" )
						.tooltip( "open" );
					this.element.val( "" );
					this._delay(function() {
						this.input.tooltip( "close" ).attr( "title", "" );
					}, 2500 );
					this.input.data( "ui-autocomplete" ).term = "";
				}
				/* --Buff : Add for allow add new to ui.combobox */
			}				
			event.preventDefault();
			return false;
		},
		/* ++Buff : Add for allow add new to ui.combobox */
		_displayNew: function() {
			this._clearNew();
			if (this.element.children( ":selected" ).attr('new')) {
				this._addNew();
			}
		},
		_clearNew: function() {
			this.input
				.removeClass('ui-combobox-newitem')
				.tooltip( "close" )
				.attr( "title", "")
			;
		},
		_addNew: function() {
			this.input
				.addClass('ui-combobox-newitem')
				.attr( "title", "Add new")
				.tooltip( "open" )
			;
		},
		/* --Buff : Add for allow add new to ui.combobox */
		_destroy: function() {
			this.wrapper.remove();
			this.element.show();
			return false;
		},
		clearValue: function() {
			var _formerTextValue = (this.input.val() + this.element.val()).trim();
			this.input.val("");
			this.element.val("");

			var _uiObj = {item: this.input};
			this._trigger('select', jQuery.Event("select"), _uiObj);
			if ((_formerTextValue != "") && (typeof this.options.changed == 'function')) {
				this.options.changed.apply(this, ["", jQuery.Event( "combobox.change" ), _uiObj]);
			}				
			return false;
		},
		setValue: function(value, fn_Callback) {
			var thisInput = this.input;
			var self = this;
			var _formerTextValue = $(thisInput).val();
			this.element.children( "option" ).each(function() {
				if ($(this).val() == value) {
					this.selected = true;
					var _strText = $(this).text();
					$(thisInput).val(_strText);
					var _uiObj = {item: this, fnCallback: fn_Callback};
					self._trigger('select', jQuery.Event("select"), _uiObj);
					if ((_formerTextValue != _strText) && (typeof self.options.changed == 'function')) {
						self.options.changed.apply(this, [_strText, jQuery.Event( "combobox.change" ), _uiObj]);
					}
					return false;
				}
			});
		},
		setText: function(text, fn_Callback) {
			var _text = (typeof text == 'string')?text.trim():'';
			var thisInput = this.input;
			var self = this;
			var _formerTextValue = $(thisInput).text();
			this.element.children( "option" ).each(function() {
				if ($(this).text() == _text) {
					this.selected = true;
					$(thisInput).val(_text);
					var _uiObj = {item: this, fnCallback: fn_Callback};
					self._trigger('select', jQuery.Event("select"), _uiObj);
					if ((_formerTextValue != _text) && (typeof self.options.changed == 'function')) {
						self.options.changed.apply(this, [_text, jQuery.Event( "combobox.change" ), _uiObj]);
					}
					return false;
				}
			});
		},
		setSelectOption: function(obj, fn_Callback) {
			var thisInput = this.input;
			var self = this;
			var _formerTextValue = $(thisInput).val();
			this.element.children( "option" ).each(function() {
				if (this == obj) {
					this.selected = true;
					var _strText = $(this).text();
					$(thisInput).val(_strText);
					var _uiObj = {item: this, fnCallback: fn_Callback};
					self._trigger('select', jQuery.Event("select"), _uiObj);
					if ((_formerTextValue != _strText) && (typeof self.options.changed == 'function')) {
						self.options.changed.apply(this, [_strText, jQuery.Event( "combobox.change" ), _uiObj]);
					}
					return false;
				}
			});
		},
		setFocus: function() {
			this.input.focus();
			return false;
		},
		enable: function(is_enable) {
			this.input.attr('readonly', !is_enable);
			if (is_enable) {
				this.element.removeClass('ui-combobox-input-disabled');
				this.input.removeClass('ui-combobox-input-disabled');
				this.btnButton.css('display', '');
			} else {
				this.element.addClass('ui-combobox-input-disabled');
				this.input.addClass('ui-combobox-input-disabled');
				this.btnButton.css('display', 'none');
			}
			return false;
		},
		setChanged: function (fnc) {
			if (typeof fnc == 'function') this.options.changed = fnc;
		},
		addMatching: function (params, type) {
			var _type = type || 'attr';
			_type = _type.toLowerCase();
			if (typeof params == 'string') {
				if (! this.options.arr_add_matching) this.options.arr_add_matching = [];
				this.options.arr_add_matching.push({"type": _type, "data": params});
			} else if (Object.prototype.toString.call(params) === '[object Array]') {
				for (var _i=0;_i<params.length;_i++) {
					_ea = params[_i];
					this.options.arr_add_matching.push({"type": _type, "data": _ea});
				}
			}
		}
	});
	
	$.extend($.ui.combobox, {
		is_allow_add: false,
		arr_add_matching: [],
		changed: function(value, event, ui) {
			return false;
		},
		blnBeforeChange: function (value, event, ui) {
			return true;
		}
	});
})( jQuery );
