﻿/*++ default map center, Bangkok */
var _DEFAULT_LAT = 13.75;
var _DEFAULT_LNG = 100.52;
var _MIN_ZOOM_LEVEL = 1;
var _MAX_ZOOM_LEVEL = 16;
var _DEFAULT_ZOOM_LEVEL = 10;
/*-- default map center, use when no view added or first time login to system */
var _DEF_ICON = new L.Icon({iconUrl: 'public/images/marker-red.png', iconSize: [21, 25], iconAnchor: [10, 25]});
var _OBJ_BT_ICONS = {
	'1': new L.Icon({iconUrl: 'public/images/marker-blue.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'2': new L.Icon({iconUrl: 'public/images/marker-gold.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'3': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'4': new L.Icon({iconUrl: 'public/images/marker-blue.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'5': new L.Icon({iconUrl: 'public/images/marker-gold.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'6': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'7': new L.Icon({iconUrl: 'public/images/marker-blue.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'8': new L.Icon({iconUrl: 'public/images/marker-gold.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'9': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'10': new L.Icon({iconUrl: 'public/images/marker-blue.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'11': new L.Icon({iconUrl: 'public/images/marker-gold.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'12': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
	,'31': new L.Icon({iconUrl: 'public/images/marker-green.png', iconSize: [21, 25], iconAnchor: [10, 25]})
};