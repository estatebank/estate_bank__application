﻿String.prototype.trim=function(){return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');};
String.prototype.escapeHtml = function () {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
	};
	return this.replace(/[&<>"']/g, function(m) { return map[m]; });
};
String.prototype.escapeQuotes = function () {
	var map = {
		'"': '&quot;',
		"'": '&#039;'
	};
	return this.replace(/["']/g, function(m) { return map[m]; });
};
function escapeQuotes(str) {
	var map = {
		'"': '&quot;',
		"'": '&#039;'
	};
	if (typeof str == "string") {
		return str.replace(/["']/g, function(m) { return map[m]; });
	} else {
		return "";
	}
}
String.prototype.escapePostgresQuoteIdent = function () {
	return this.replace(/\"\"/g, '\\\"');
};
function escapePostgresQuoteIdent(str) {
	if (typeof str == "string") {
		return str.replace(/\"\"/g, '\\\"');
	} else {
		return "";
	}
}
if (!Array.remove) {
	Array.prototype.remove = function(from, to) {
		var rest = this.slice((to || from) + 1 || this.length);
		this.length = from < 0 ? this.length + from : from;
		return this.push.apply(this, rest);
	};
}
if (!Array.indexOf) {
	Array.prototype.indexOf = function(obj){
		for(var i=0; i<this.length; i++){
			if(this[i]==obj){
				return i;
			}
		}
		return -1;
	}
}
if (!Array.removeByValue) {
	Array.prototype.removeByValue = function() {
		var what, a = arguments, L = a.length, ax;
		while (L && this.length) {
			what = a[--L];
			while ((ax = this.indexOf(what)) !== -1) {
				this.splice(ax, 1);
			}
		}
		return this;
	};
}

Date.prototype.strCurrentDate = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}
Date.prototype.strCurrentTime = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes();
	 // +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds()
}
function _isValidTimeString(str){
	var _str = str || '';
	var _val = new String(_str.trim());
	if (_val.length != 5) {
		return false;
	} else {
		var _hour = _val.substring(0,2);
		var _min = _val.substring(3,5);
		if (_val.substring(2,3) !== ":") return false;
		if (isNaN(_hour)) return false;
		if (isNaN(_min)) return false;
		if (parseInt(_hour) < 24){
			if (parseInt(_min) < 60){
				return true;
			} else return false;
		} else return false;
	}
}
function datParseTime(strTime, dat) {
	if (!dat) dat = new Date();
	var _time = strTime.match(/(\d+)(?::(\d\d))?\s*(p?)/i);
	if (!_time) return NaN;
	var _hours = parseInt(_time[1], 10);
	if (_time[3]) {
		_hours += (_hours < 12 && _time[3]) ? 12 : 0;
	}
	dat.setHours(_hours);
	dat.setMinutes(parseInt(_time[2], 10) || 0);
	dat.setSeconds(0, 0);
	return dat;
}

function _doSortArrObjByPropVal(arrObj, strPropName, blnDesc) {
	var _strName = strPropName || false;
	if (! _strName) return false;
	if ((typeof arrObj != 'object') || (typeof arrObj.sort != 'function')) return false;
	var _blnDesc = blnDesc || false;
	var _compare = function(a, b) {
		if (((!(_strName in a)) && (!(_strName in b))) || ((a[_strName] < 0) && (b[_strName] < 0))) {
			return 0;			
		} else if ((! (_strName in a)) || (a[_strName] < 0)) {
			return 1;
		} else if ((! (_strName in b)) || (b[_strName] < 0)) {
			return -1;
		} else {
			if (_blnDesc == false) {
				if (a[_strName] < b[_strName]) return -1;
				if (a[_strName] > b[_strName]) return 1;
			} else {
				if (a[_strName] < b[_strName]) return 1;
				if (a[_strName] > b[_strName]) return -1;
			}
		}
		return 0;
	};
	arrObj.sort(_compare);
}

function _doFocusNext(strSelector, src) {
	var _elem = _toJQObj(src);
	var _selecter = strSelector || '';
	if ((_elem.length <= 0) || (_selecter.trim() == '')) return false;
	var _elemNext = $(_selecter + ':gt(' + ($(_selecter).index(_elem)) + '):visible');
	if (_elemNext.length > 0) _elemNext.each(function() {
		if (isEnable(this)) {
			$(this).focus();
			return false;
		}
	});
}

$(document).ready(function () {
	//first set disabled 
	$(".user-input.set-disabled").each(function () {
		_setEnableElem(this, false);
	});
    //validation on change
    $(document).on('change', '.input-integer:not(.no-validate)', function (ev) {
		//doClearVldrErrorElement(this);
		blnValidateElem_TypeInt(this);
	});
    $(document).on('change', '.input-double:not(.no-validate)', function (ev) {
		//doClearVldrErrorElement(this);
		blnValidateElem_TypeDouble(this);
	});
	$(document).on('change', '.user-input.input-invalid', function() {
		//doClearVldrErrorElement(this);
		blnValidateElem(this);
	});
	// time type input key event (auto fill format)
	$(document).on('keyup', '.input-time:not(.no-validate)', function() {
		var _elem = _getJQUserInputElement(this);
		if (_elem.length <= 0) return false;
		doClearVldrErrorElement(_elem);
		var _dummy = _elem[0].value.toString().trim() || '';
		if ((_dummy.length < 4)) {
			return true;
		} else if ((_dummy.length == 4)) {
			if (! _isInt(_dummy)) { //still correct value possible ( 4 char but not all numeric )
				return true;
			} else {
				var _val = _dummy.substr(0, 2) + ':' + _dummy.substr(2, 2);
				_elem[0].value = _val;				
			}
		} else if ((_dummy.length == 5)) {
			if (isNaN(_dummy.substr(2, 1))) {
				var _val = _dummy.substr(0, 2) + ':' + _dummy.substr(3, 2);
				_elem[0].value = _val;
			}
		}
		if (blnValidateElem_TypeTime(this)) {
			_doFocusNext('.user-input', this);
		}
		return false;
	});
	$(document).on('change', '.input-time:not(.no-validate)', function() {
		blnValidateElem_TypeTime(this);
	});
});

function _findObjectInList(objList, objKey, objValue) {
	var _objList = objList || [];
	var _objKey = objKey || '';
	var _objVal = objValue || '';
	for (var _x in _objList) {
		if ((typeof _objList[_x] == null) || (typeof _objList[_x] == 'function') || (typeof _objList[_x] == 'undefined')) {
			continue;
		} else if ((typeof _objKey == 'object') && (_objVal == '')) {
			var _ea = _objList[_x];
			var _blnInvalid = false;
			for (var _y in _objKey) {
				if ((! _blnInvalid) && (! ((_y in _ea) && (_ea[_y] == _objKey[_y])))) _blnInvalid = true;
			}
			if (! _blnInvalid) return _ea;
		} else if (((_objKey.toLowerCase() == '__index') || (_objKey.toLowerCase() == '__key')) && (_isInt(_x))) {
			if ((_objList[_x] == _objVal)) return _objList[_x];
		} else if ((_objKey in _objList[_x]) && (_objList[_x][_objKey] == _objVal)) {
			return _objList[_x];
		}
	}
}

function _removeObjectFromList(objList, objKey, objValue) {
	var _objList = objList || [];
	var _objKey = objKey || '';
	var _objVal = objValue || '';
	for (var _x in _objList) {
		if ((typeof _objList[_x] == 'function')) {
			continue;
		} else if ((typeof _objKey == 'object')) {
			var _ea = _objList[_x];
			var _blnInvalid = false;
			for (var _y in _objKey) {
				if ((! _blnInvalid) && (! ((_y in _ea) && (_ea[_y] == _objKey[_y])))) _blnInvalid = true;
			}
			if (! _blnInvalid) {
				if (Array.isArray(objList)) { //array
					objList.splice(_x, 1);
				} else { //object
					delete objList[_x];
				}
			}
		} else if (((_objKey.toLowerCase() == '__index') || (_objKey.toLowerCase() == '__key')) && (_isInt(_x)) && (_objList[_x] == _objVal)) {
			if (Array.isArray(objList)) { //array
				objList.splice(_x, 1);
			} else { //object
				delete objList[_x];
			}
		} else if ((_objKey in _objList[_x]) && (_objList[_x][_objKey] == _objVal)) {
			if (Array.isArray(objList)) { //array
				objList.splice(_x, 1);
			} else { //object
				delete objList[_x];
			}
		}
	}
	return true;
}

/*
function datGetDate(str) {
	var parts = str.split("/");
	if (parts.length < 3) return false;
	return parseInt(parts[2], 10) + '-' + pad(parseInt(parts[1], 10), 2, '0') + '-' +  pad(parseInt(parts[0], 10), 2, '0');
}
*/
function datGetDate(str) {
	var parts = str.split("/");
	if (parts.length < 3) return false;
	return new Date(parseInt(parts[2], 10), (parseInt(parts[1], 10) - 1), parseInt(parts[0], 10));
}

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc, dispFormat) {
		var dF = dateFormat;
		/* ++ buff added */
		var _dispFormat = dispFormat || '';
		if ((_dispFormat != '') && (_dispFormat in dateFormat.dispFormat)) {
			dF.i18n = dateFormat.dispFormat[_dispFormat];
		}
		/* -- buff added */

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};
dateFormat.dispFormat = {
	th:{
		dayNames: [
			"อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส.",
			"อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"
		],
		monthNames: [
			"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.",
			"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
		]
	}
};
// For convenience...
Date.prototype.format = function (mask, utc, dispFormat) {
	return dateFormat(this, mask, utc, dispFormat);
};
//--

function _toJQObj(obj) {
	if (obj instanceof jQuery) {
		return obj
	} else if (typeof obj == 'string') {
		if ($('#' + obj).length > 0) {
			return $('#' + obj);
		} else if ($('[data="' + obj + '"]').length > 0) {
			return $('[data="' + obj + '"]');
		} else {
			return;
		}
	} else {
		return $(obj);
	}
}

function _getJQUserInputElement(elem) {
	var _elem = _toJQObj(elem);
//	if (_elem.get(0).tagName.toLowerCase() == 'select') {
//		return _elem;
//	} else {
	if (_elem.children(':not(label,option)').length > 0) _elem = $(_elem.children(':not(label,option)').get(0));
	return _elem;
//	}
}

function _isInt(n) {
	return parseFloat(n) == parseInt(n, 10) && !isNaN(n);
}

function formatNumber(nStr, digit, blnComma, blnBracketMinus) {
    if (digit === undefined) digit = 2;
    if (blnComma === undefined) blnComma = true;
	var _blnBracket = blnBracketMinus || false;
    fl = parseFloat(nStr);
	if ((_blnBracket == true) && (fl < 0)) {
		fl = Math.abs(fl);
	} else {
		_blnBracket = false;
	}
    pow = Math.pow(10, digit);
    fl = Math.round(fl * pow) / pow;
    fx = fl.toFixed(digit);
    nStr = fx + '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? x[1] : '';
    if ((x2.length) < digit) {
        for (i = (digit - (x2.length)) ; i < digit; i++) {
            x2 = x2 + '0';
        }
    }
    if (digit > 0) x2 = '.' + x2;

    if (blnComma) {
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
    }
	
	if (_blnBracket == true) {
		return '( ' + x1 + x2 + ' )';
	} else {
		return '  ' + x1 + x2 + '  ';
	}
}

function doSetVldrError(obj, data_field, type, msg, index) {
	var _elem = _toJQObj(obj);
	var _dataContainer = jqObjDataContainer(obj);
	if (_elem.lenght <= 0) return false;
	var _idx = index || false;
	var _dataField = data_field || _getElemData(_elem) || false;
	var _ulValidateResult = false;
	if (_idx === false) {
		$(_elem).parents('div').each(function() {
			var _ul = $(this).find('.ul-vldr-error-msg');
			if (_ul.length > 0) {
				_ulValidateResult = $(_ul[0]);
				return false;
			}
		});
		if (_ulValidateResult === false) {
			$(_dataContainer).parents('div').each(function() {
				var _ul = $(this).children('.ul-vldr-error-msg'); // $(this).siblings('.ul-vldr-error-msg');// $(this).find('.ul-vldr-error-msg');
				if (_ul.length > 0) {
					_ulValidateResult = $(_ul[0]);
					return false;
				}
			});
		}
	} else {
		var _strIndexFilter = (_idx)?'[index="' + _idx + '"]':'';
		_ulValidateResult = $('.ul-vldr-error-msg' + _strIndexFilter);
		if (_ulValidateResult.length <= 0) _ulValidateResult = false;
	}
	if (_dataContainer.length <= 0) _dataContainer = _elem;
	if (_dataContainer.length > 0) {
		_dataField = data_field || _getElemData(obj);
		_strErrMsg = ((_dataContainer.attr('invalid-msg') && (_dataContainer.attr('invalid-msg') != msg))?_dataContainer.attr('invalid-msg') + ', ':'') + msg;
		_dataContainer.addClass('input-invalid').attr('invalid-msg', _strErrMsg).attr('title', _strErrMsg);
/*
		$('<span class="cls-elem-validate-popup"><span>' + _strErrMsg + '</span></span>')
			.insertAfter(_dataContainer)
			.show().fadeTo(1000, 0.15).fadeTo(200, 1).fadeTo(1000, 0.15).fadeTo(200, 1).fadeOut(1000, function() { $(this).remove(); });
		;
*/
		var _anchor = (_dataContainer.is(':visible')) ? _dataContainer : $(_dataContainer.siblings(':visible')[0]);
		if (_dataContainer.data('ui-tooltip')) _dataContainer.tooltip("disable").tooltip("close");
		_dataContainer.tooltip({
				content: _strErrMsg
				,position: { my: "left+20 center", at: "right center", of: _anchor, collision: "flipfit" }
				,tooltipClass: "cls-elem-validate-popup"
				,show: {effect: "heightlight"}//{ effect: "puff", duration: 200 }
				,hide: {effect: "puff"}//{ effect: "puff", duration: 600 }
			}).tooltip("enable").tooltip("open")
		;
	} else {
		_dataField = data_field || 'UnknownObject';
		_strErrMsg = msg || 'Unknown error!';
	}
	if (_ulValidateResult) _ulValidateResult.append('<li id="li_' + _dataField + '__' + type + '" data="' + _dataField + '">' + _strErrMsg + '</li>');
}

function doClearVldrErrorElement(elem, dataField, index) {
	var _elem = _toJQObj(elem);
	var _dataContainer = jqObjDataContainer(elem);
	if (_elem.lenght <= 0) return false;
	var _idx = index || false;
	var _dataField = dataField || _getElemData(_elem) || false;
	var _ulValidateResult = false;

	if (_dataContainer.is('.input-invalid')) {
		if (_dataContainer.data('ui-tooltip')) _dataContainer.tooltip("disable").tooltip("close");
		_dataContainer.removeClass('input-invalid');
		_dataContainer.removeAttr('invalid-msg');
		_dataContainer.removeAttr('title');
	}
	if (_elem.is('.input-invalid')) {
		if (_elem.data('ui-tooltip')) _elem.tooltip("disable").tooltip("close");
		_elem.removeClass('input-invalid');
		_elem.removeAttr('invalid-msg');
		_elem.removeAttr('title');
	}

	if (_idx === false) {
		$(_elem).parents('div').each(function() {
			var _ul = $(this).find('.ul-vldr-error-msg');
			if (_ul.length > 0) {
				_ulValidateResult = $(_ul[0]);
				return false;
			}
		});
		if (_ulValidateResult === false) {
			$(_dataContainer).parents('div').each(function() {
				var _ul = $(this).children('.ul-vldr-error-msg'); // $(this).siblings('.ul-vldr-error-msg');// $(this).find('.ul-vldr-error-msg');
				if (_ul.length > 0) {
					_ulValidateResult = $(_ul[0]);
					return false;
				}
			});
		}
	} else {
		var _strIndexFilter = (_idx)?'[index="' + _idx + '"]':'';
		_ulValidateResult = $('.ul-vldr-error-msg' + _strIndexFilter);
		if (_ulValidateResult.length <= 0) _ulValidateResult = false;
	}
	if ((typeof _dataField == 'string') && (_ulValidateResult) && (_dataField.trim())) {
		_dataField = _dataField.trim();
		$('li[data="' + _dataField + '"]', _ulValidateResult).remove();
	}
}

function _doClearVldrErrorByDataField(dataField, container, index) {
	var _dataField = dataField || '';
	var _container = _toJQObj(container);
	var _idx = index || false;
	var _ulValidateResult = false;
	var _elem;
	if (typeof _dataField != 'string') return false;
	
	if (_container.length > 0) {
		_elem = $('[data="' + _dataField + '"]', _container);
	} else {
		_elem = $('[data="' + _dataField + '"]');
	}
	_dataField = _dataField.trim();
	if (_idx === false) {
		$(_elem).parents('div').each(function() {
			var _ul = $(this).find('.ul-vldr-error-msg');
			if (_ul.length > 0) {
				_ulValidateResult = $(_ul[0]);
				return false;
			}
		});
	} else {
		var _strIndexFilter = (_idx)?'[index="' + _idx + '"]':'';
		_ulValidateResult = $('.ul-vldr-error-msg' + _strIndexFilter);
		if (_ulValidateResult.length <= 0) _ulValidateResult = false;
	}
	if ((_ulValidateResult) && (_dataField)) $('li[data="' + _dataField + '"]', _ulValidateResult).remove();
	
	if (_elem.is('.input-invalid')) {
		if (_elem.data('ui-tooltip')) _elem.tooltip("disable").tooltip("close");
		_elem.removeClass('input-invalid');
		_elem.removeAttr('invalid-msg');
		_elem.removeAttr('title');
	}
}

function blnValidateContainer(blnFullTest, container, strJqSelector) {
	var _blnFullLoop = blnFullTest || false;
    var _container = container || $('body');
	var _strJqSelector = strJqSelector || '.user-input';
    var _blnIsValid = true;
	if (($(_strJqSelector + ' .input-invalid', _container).length > 0) || (($(_strJqSelector + '.input-invalid', _container).length > 0))) {
		if (_blnIsValid == true) _blnIsValid = false;
		if (_blnFullLoop == false) return false;
	}
	doClearVldrError(_container);
	$(_strJqSelector, _container).each(
		function () {
			if (blnValidateElem(this) == false) {
				if (_blnIsValid == true) _blnIsValid = false;
				if (_blnFullLoop == false) return false;
			}
		}
	 );
	 return _blnIsValid;
}

function blnValidateElem(elem) {
	var _elem = _toJQObj(elem);

	if (_elem.is('.input-required') && ( ! blnValidateElem_TypeRequired(_elem))) return false;
	if (_elem.is('.input-integer') && ( ! blnValidateElem_TypeInt(_elem))) return false;
	if (_elem.is('.input-double') && ( ! blnValidateElem_TypeDouble(_elem))) return false;
	
    return true;
}

function blnValidateElem_TypeInt(obj) {
	var _elem = _getJQUserInputElement(obj);
	var _dataField = _getElemData(obj);
	var _label = getLabel(obj);
	doClearVldrErrorElement(_elem);
	var _value = _elem[0].value.toString().trim().replace(/,/g, '');
	if ((_value != '') && (! _isInt(_value))) {
		var _strErrMsg = MSG_VLDR_INVALID_DATATYPE.replace(/v_XX_1/g, '( ' + ((_label != '')?_label:_dataField) + ': integer )');
		doSetVldrError(_elem, _dataField, "typeInt", _strErrMsg);
		return false;
	}
	if (_elem.is('.input-format-number') && (_value != '') && (! isNaN(_value))) _elem.val(formatNumber(parseInt(_value), 0));
	return true;
}

function blnValidateElem_TypeDouble(obj) {
	var _elem = _getJQUserInputElement(obj);
	var _dataField = _getElemData(obj);
	var _label = getLabel(obj);
	doClearVldrErrorElement(_elem);
	var _value = _elem[0].value.toString().trim().replace(/,/g, '');
	if ((_value != '') && isNaN(_value)) {
		var _strErrMsg = MSG_VLDR_INVALID_DATATYPE.replace(/v_XX_1/g, '( ' + ((_label != '')?_label:_dataField) + ': double )');
		doSetVldrError(_elem, _dataField, "typeDouble", _strErrMsg);
		return false;
	}
	if (_elem.is('.input-format-number') && (_value != '') && (! isNaN(_value))) _elem.val(formatNumber(parseFloat(_value)));
	return true;
}

function blnValidateElem_TypeRequired(obj) {
	var _elem = _getJQUserInputElement(obj);
	var _label = getLabel(obj);
	var _dataField = _getElemData(obj);
	doClearVldrErrorElement(_elem);
	var _value = _elem[0].value.toString().trim();
	if (_value == "") {
		var _strErrMsg = MSG_VLDR_INVALID_REQUIRED.replace(/v_XX_1/g, '( ' + ((_label != '')?_label:_dataField) + ' )');
		doSetVldrError(_elem, _dataField, "required", _strErrMsg);
		return false;
	}
	return true;
}

function blnValidateElem_TypeTime(obj) {
	var _elem = _getJQUserInputElement(obj);
	var _label = getLabel(obj);
	var _dataField = _getElemData(obj);
	doClearVldrErrorElement(_elem);
	var _value = _elem[0].value.toString().trim();
	var _dat;
	if (_value.match(/^\d{2,}:(?:[0-5]\d)$/)) _dat = datParseTime(_value);
	if (!(_dat instanceof Date)) {
		var _strErrMsg = MSG_VLDR_INVALID_DATATYPE.replace(/v_XX_1/g, ((_label != '') ? _label : _dataField) + ' = "' + _value + '" ( HH24:MI, eg. 12:30, 09:15 )');
		doSetVldrError(_elem, _dataField, "typeTime", _strErrMsg);
		return false;
	}
	return true;
}

function doClearVldrError(container) {
    var _container = container || $('form');
    $('.ul-vldr-error-msg', _container).empty();
    $('.input-invalid', _container).each(
		function () {
		    doClearVldrErrorElement(this);
		}
	);
}

function _doFetchDataByJQSelector(arrData, container, selectText) {
	var _arrData = arrData || [];
    $(selectText, container).each(
        function () {
			_setElemValue(this, _arrData, false);
        }
    );
}

function doFetchDataUserInput(arrData, container) {
    var _container = container || $('form');
    var _arrData = arrData || [];
    if (_arrData == []) {
        return false;
    } else {
        _doFetchDataByJQSelector(_arrData, _container, '.user-input');
    }
}

function doFetchDataContainer(arrData, container) {
    var _container = container || $('form');
    var _arrData = arrData || [];
    if (_arrData == []) {
        return false;
    } else {
        _doFetchDataByJQSelector(_arrData, _container, '.data-container');
    }
}

function _clearByJQSelector(container, selectText) {
    $(selectText, container).each(
        function () {
			_clearElemValue(this);
        }
    );
}
function doClearUserInput(container) {
    var _container = container || $('form');
    doClearVldrError(_container);
    _clearByJQSelector(_container, '.user-input:not(".data-constant")');
}
function doClearDataContainer(container) {
    var _container = container || $('form');
    doClearVldrError(_container);
    _clearByJQSelector(_container, '.data-container:not(".data-constant")');
}

function jqObjDataContainer(obj) {
	var _elem = _toJQObj(obj);
	if (_elem) {
		if (_elem.hasClass('data-container')) {
			return _elem;
		} else if (_elem.parents('.data-container').length > 0) {
			return $(_elem.parents('.data-container').get(0));
		} else if ($('.data-container', _elem).length > 0) {
			return $($('.data-container', _elem).get(0));
		} else {
			return _elem;
		}
	}
}
function getData(obj) {
	return _getElemData(obj);
}
function getLabel(obj) {
	return _getElemLabel(obj);
}
function getValue(obj, defReturn) {
	return _getElemValue(obj, defReturn);
}
function setValue(obj, arrData, blnStrictDataType) {
	return _setElemValue(obj, arrData, blnStrictDataType);
}
function clearValue(obj) {
	_clearElemValue(obj);
}
function isEnable(obj) {
	return _enableElem(obj);
}

function _getElemData(elem) {
	var _elem = _toJQObj(elem);
	if (_elem.length <= 0) return;
	var _data = (_elem.attr('data'))?_elem.attr('data'):'';
	if (_data.trim() != '') return _data;

	if (_elem.parents('.data-container').length > 0) _data = $(_elem.parents('.data-container').get(0)).attr('data') || '';
	if (_data.trim() != '') return _data;

	var _tag = _elem.get(0).tagName.toLowerCase();
	var _type = _elem.get(0).type;
	if (_tag == 'input' && _type == 'radio') {
		_data = _elem.attr('name');
	} else if ((_elem.attr('id')) && (_elem.attr('id').length > 4)) {
		_data = _elem.attr('id').substr(4);
	}
	return _data;
}
function _getElemLabel(elem) {
	var _elem = _toJQObj(elem);
	if (_elem.length <= 0) return;
	var _label = (_elem.find('label').length > 0)?$(_elem.find('label').get(0)).html():'';
	if (_label == '') {
		if (_elem.parents('.data-container').length > 0) {
			if ($(_elem.parents('.data-container').get(0)).find('label').length > 0) _label = $($(_elem.parents('.data-container').get(0)).find('label').get(0)).html() || '';
		}
	}
	_label = _label.trim();
	if ((_label != '') && (_label.substr(-1) == ':')) _label = _label.substr(0, (_label.length-1)).trim();
	
	return _label;
}
function _getElemValue(elem, defReturn) {
	var _elem = _getJQUserInputElement(elem);
	var _defRet = defReturn;
	var _valRet;
	if (_elem.length <= 0) return _defRet;
	_tag = _elem.get(0).tagName.toLowerCase();
	_type = _elem.get(0).type;
	if ((_elem) && (_tag == 'input' && (_type == 'text' || _type == 'hidden')) || (_tag == 'textarea') || (_tag == 'select')) {
		if (_elem.data('ech-multiselect')) {
			var _arrRet = [];
			if (_elem.is('.input-integer, .input-double, .input-number')) {
				var _arrVal = _elem.val();
				for (var _i in _arrVal) {
					_dummy = _cleanNumericValue(_arrVal[_i]);
					if (_dummy) _arrRet.push(_dummy);
				}
			} else {
				_arrRet = _elem.val() || false;
			}
			if ((_arrRet) && (_arrRet.length > 0)) return _arrRet.join(',');
		} else if (_elem.is('.input-integer, .input-double, .input-number')) {
			_valRet = _cleanNumericValue(_elem.val());
			//if ((_valRet) && (_elem.val() != _valRet)) _elem.val(_valRet); //change to refined numeric data // removed after implement class .format-number
		} else {
			_valRet = _elem.val();
		}
	} else if (_tag == 'input' && (_type == 'checkbox')) {
		if (_elem.is(':checked') == true) {
			return '1';
		} else {
			return '0';
		}
	} else if (_tag == 'input' && (_type == 'radio')) {
		var _elem = $('input[type="radio"][name="' + _elem.attr('name') + '"]:checked');
		if (_elem.length > 0) {
			return _elem.val();
		} else {
			return false;
		}
    } else if ((_tag == 'span') || (_tag == 'div')) {
		if (_elem.is('.input-integer, .input-double, .input-number')) {
			_valRet = _cleanNumericValue(_elem.html());
		} else {
			_valRet = _elem.html();
		}
	} else if ((_tag == 'input') && (_type == 'file')) {
		var _prnt = $(_elem.parents('div.display-upload').get(0));
		var _hdn = $($('input[type="hidden"].fmg-value', _prnt).get(0));
		if (_prnt.is('.clsCtrl-deactivate')) {
			_valRet = 'remove';
		} else if (! _elem.is('.clsCtrl-valueChanged')) {
			_valRet = 'unchange';
		} else {
			if (_hdn.length > 0) _valRet = _hdn.val();
		}
	} else {
		_valRet = _elem.val();
	}
	if ((_valRet == null) || (_valRet.toString().trim() == '')) {
		return _defRet;
	} else {
		return _valRet;		
	}
}
function _setElemValue(elem, data, blnStrictDataType) {
	var _data = (typeof data == 'undefined')?0:data;
	var _blnChangeType = true;
	if ((typeof blnStrictDataType != 'undefined') && (blnStrictDataType == false)) _blnChangeType = false;
	var _value;
	var _elem = _getJQUserInputElement(elem);
	if (_elem.length == 0) return false;
	var _tag = _elem.get(0).tagName.toLowerCase();
	var _type = _elem.get(0).type;
	if (_tag && (typeof _data != 'undefined')) {
		if ((_tag == 'input') && (_type == 'file')) {
			var _fileName, _pathUrl;
			if (typeof _data == 'object') {
				if (typeof _data["name"] == 'string') _fileName = _data["name"];
				if (typeof _data["url"] == 'string') _pathUrl = _data["url"];
			} else {
				_fileName = _data;
			}
			var _prnt = $(_elem.parents('div.display-upload').get(0));
			var _hdn = $($('input[type="hidden"].fmg-value', _prnt).get(0));
			var _isChange = false;
			if ((_hdn.length > 0)) {
				_hdn.val(_fileName);
				var _oldVal = _hdn.val() || false;
				if ((_oldVal) && (_oldVal != _fileName)) _isChange = true;
			}
			if ((_prnt.length > 0) && (typeof _pathUrl == 'string')) {
				var _bg = 'url(' + _pathUrl + ')';
				_prnt.css('background-image', _bg);
				if (! _isChange) {
					var _oldBg = _prnt.css('background-image') || false;
					if ((_oldBg) && (_oldBg != _bg)) _isChange = true;
				}
			}
			if (_isChange) {
				if ((_prnt.length > 0) && (! _prnt.hasClass('clsCtrl-valueChanged'))) _prnt.addClass('clsCtrl-valueChanged');
				if ((! _elem.hasClass('clsCtrl-valueChanged'))) _elem.addClass('clsCtrl-valueChanged');
				if ((_hdn.length > 0) && (! _hdn.hasClass('clsCtrl-valueChanged'))) _hdn.addClass('clsCtrl-valueChanged');
			}
		} else {
			if ((_data) && (typeof _data == 'object')) { //case pass array of values
				var _dataField = getData(_elem);
				if (_dataField in _data) _value = _data[_dataField];
			} else {
				_value = _data;
			}
			if (_elem.is('.input-integer, .input-double, .input-number') && (typeof _value == 'string')) _value = _value.trim();
			if (_blnChangeType === true) {
				if (_elem.is('.input-double, .input-number')) {
					if (! isNaN(_value)) {
						_value = parseFloat(_value);
					} else {
						doSetVldrError(_elem, '', 'SetInvalidDataType', 'Invalid data type on _SetElemValue');
						return false;
					}
				} else if (_elem.is('.input-integer')) {
					if (! isNaN(_value)) {
						_value = parseInt(_value);				
					} else {
						doSetVldrError(_elem, '', 'SetInvalidDataType', 'Invalid data type on _SetElemValue');
						return false;
					}
				}
			}
			if (_elem.hasClass('hasDatepicker')) {
				//var _strFormat = _elem.datepicker('option', 'dateFormat');
				if ((typeof _value == 'string') && (_value.trim() == '')) return false;
				var _datVar = new Date(_value);
				_elem.datepicker('setDate', _datVar);
			} else if ((_tag == 'input' && (_type == 'text' || _type == 'hidden')) || (_tag == 'textarea')) {
				_elem.val(_value);
			} else if (_tag == 'select') {
				if (_elem.data("ui-combobox")) {
					_elem.combobox('setValue', _value);
				} else if (_elem.data("ui-groupingcombobox")) {
					_elem.groupingcombobox('setValue', _value);
				} else if (_elem.data('ech-multiselect')) {
					_elem.val(_value);
				} else {
					_elem.val(_value);
				}
			} else if (_tag == 'input' && _type == 'checkbox') {
				if ((_value == 1) || (_value == 't') || (_value == true)) {
					_elem.prop('checked', true);
				} else {
					_elem.prop('checked', false);
				}
			} else if (_tag == 'input' && _type == 'radio') {
				if (_value == _elem.val()) {
					_elem.prop('checked', true);
				} else {
					_elem.prop('checked', false);
				}
			} else if ((_tag == 'span') || (_tag == 'div')) {
				_elem.html(_value);
			}
		}
	}
}
function _clearElemValue(elem) {
	_elem = _getJQUserInputElement(elem);
	if (_elem.length == 0) return false;
	var _tag = _elem.get(0).tagName.toLowerCase();
	var _type = _elem.get(0).type;
	if ((_tag == 'input' && (_type == 'text' || _type == 'hidden')) || (_tag == 'textarea')) {
		_elem.val('');
	} else if (_tag == 'select') {
		if (_elem.data("ui-combobox")) {
			_elem.combobox("clearValue");
		} else if (_elem.data("ui-groupingcombobox")) {
			_elem.groupingcombobox("clearValue");
		} else if (_elem.data('ech-multiselect')) {
			_elem.multiselect("uncheckAll");
		} else {
			_elem.val('');
		}
	} else if (_tag == 'input' && (_type == 'checkbox' || _type == 'radio')) {
		_elem.prop('checked', false);
	} else if ((_tag == 'input') && (_type == 'file')) {
		_elem.removeClass('clsCtrl-deactivate').removeClass('clsCtrl-valueChanged').removeAttr('ctrl_changeValue');
		var _prnt = $(_elem.parents('div.display-upload').get(0));
		var _remove = $($('input[type="checkbox"].fmg-no-image', _prnt).get(0));
		var _hdn = $($('input[type="hidden"].fmg-value', _prnt).get(0));
		if (_prnt.length > 0) _prnt.css('background-image', '').removeAttr('old_bg').removeClass('clsCtrl-deactivate').removeClass('clsCtrl-valueChanged').removeAttr('ctrl_changeValue');
		if (_remove.length > 0) _clearElemValue(_remove);
		if (_hdn.length > 0) _hdn.val('').removeClass('clsCtrl-deactivate').removeClass('clsCtrl-valueChanged').removeAttr('ctrl_changeValue');
	} else {
		if ($.isFunction(_elem.html)) _elem.html('');
	}
}

function _cleanNumericValue(val) {
	if ((typeof val == 'undefined') || (val == null)) return;
	var _val = val;
	_val = _val.toString().trim();
	var _blnMinus = (_val.match(/^\(.+\)$/) || false) !== false;
	_val = _val.replace(/[a-zA-Z_,\( \)]/g, '');
	if ((_val.length <= 0) || isNaN(_val)) {
		return '';
	} else {
		return (_blnMinus)?(parseFloat(_val) * -1):parseFloat(_val);
	}
}

function _enableElem(elem, blnEnabled) {
	if (typeof blnEnabled != 'undefined') {
		return _setEnableElem(elem, blnEnabled);
	} else {
		var _elem = _getJQUserInputElement(elem);
		var _tag = _elem.get(0).tagName.toLowerCase();
		var _type = _elem.get(0).type;
		if ((_tag == 'input' && (_type == 'text' || _type == 'hidden')) || (_tag == 'textarea')) {
			if (_elem.hasClass('hasDatepicker')) {
				return (! _elem.datepicker("option", "disabled"));
			} else {
				var _attr = _elem.attr('readonly') || false;
				return ( ! ((_attr == true) || (_attr == 'readonly')));
			}
		} else if (_tag == 'select') {
			if (_elem.data("ui-combobox")) {
				return (! _elem.is('.ui-combobox-input-disabled'));
			} else if (_elem.data("ui-groupingcombobox")) {
				return (! _elem.is('.ui-combobox-input-disabled'));
			} else {
				var _attr = _elem.attr('disabled') || false;
				return ( ! ((_attr == true) || (_attr == 'disabled')));
			}
		} else if (_tag == 'input' && (_type == 'checkbox' || _type == 'radio' || _type == 'file')) {
			var _attr = _elem.attr('disabled') || false;
			return ( ! ((_attr == true) || (_attr == 'disabled')));
		}
	}
}

function _setEnableElem(elem, blnEnabled) {
    var _bln = blnEnabled || false;
    var _elem = _getJQUserInputElement(elem);
	if (_elem.length == 0) return false;
    var _tag = _elem.get(0).tagName.toLowerCase();
    var _type = _elem.get(0).type;
    if ((_tag == 'input' && (_type == 'text' || _type == 'hidden')) || (_tag == 'textarea')) {
        if (_elem.hasClass('hasDatepicker')) {
            _elem.datepicker("option", "disabled", (!_bln));
        } else {
			if (_bln == true) {
				_elem.removeAttr("readonly");
			} else {
				_elem.attr("readonly", "readonly");
			}
        }
    } else if (_tag == 'select') {
		if (_elem.data("ui-combobox")) {
			_elem.combobox('enable', _bln);
		} else if (_elem.data("ui-groupingcombobox")) {
            _elem.groupingcombobox('enable', _bln);
        } else {
			if (_bln == true) {
				_elem.removeAttr("disabled");
			} else {
				_elem.attr("disabled", "disabled");
			}
        }
    } else if (_tag == 'input' && (_type == 'checkbox' || _type == 'radio')) {
		if (_bln == true) {
			_elem.removeAttr("disabled");
		} else {
			_elem.attr("disabled", "disabled");
		}
	} else if ((_tag == 'input') && (_type == 'file')) {
		var _prnt = $(_elem.parents('div.display-upload').get(0));
		if (_bln == true) {
			_elem.removeAttr("disabled");
			if ((_prnt.length > 0)) {
				_prnt.removeAttr("disabled");
				//var _oldBg = _prnt.attr('old_bg') || 'none';
				//if (_oldBg != 'none') _prnt.css('background-image', _oldBg).removeAttr('old_bg');
			}
		} else {
			_elem.attr("disabled", "disabled");
			if ((_prnt.length > 0)) {
				_prnt.attr("disabled", "disabled");
				//var _bg = _prnt.css('background-image') || 'none';
				//if (_bg != 'none') _prnt.css('background-image', '').attr('old_bg', _bg);
			}
		}
    }
	return _bln;
}

function doSetEnableUserInput(container, blnEnabled) {
    var _bln = blnEnabled || false;
	var _container = container || $('body');
	$(".user-input.set-disabled", _container).each(function() { _setEnableElem(this, false); });
	$(".user-input:not(.set-disabled)", _container).each(function () { _setEnableElem(this, _bln); });
    if (_bln) {
        $('.cls-btn-form-submit', $(_container).parents('.ui-dialog').get(0)).css('display', '');
        $('.cls-btn-form-reset', $(_container).parents('.ui-dialog').get(0)).css('display', '');
    } else {
        $('.cls-btn-form-submit', $(_container).parents('.ui-dialog').get(0)).css('display', 'none');
        $('.cls-btn-form-reset', $(_container).parents('.ui-dialog').get(0)).css('display', 'none');
    }
}

var _currInputValue = [];
function doSetCurrentQueriedData(arrData, container) {
    _container = container || $('form');
    _arrData = arrData || [];
    if (_arrData == []) return false;
    $('.data-container', _container).each(
        function () {
            _elem = $(this).children().get(0);
            _dataField = $(this).attr('data');
            if (_elem.tagName && _dataField) {
                _dataField = _dataField.toLowerCase();
                switch (_elem.tagName.toLowerCase()) {
                    case 'input':
                        _currInputValue[_dataField] = _arrData[_dataField] || '';
                        break;
                }
            }
        }
    );
}
function doResetInput() {
    doFetchDataContainer(_currInputValue);
}

function doClearDisplayError(strErrCategory, index) {
	var _index = index || -1;
	var _strSelector = (strErrCategory)?'#li_' + strErrCategory:'li';
	var _strIndexFiler = (_index == -1)?'':'[index="' + _index + '"]';
	$(_strSelector, $('.ul-vldr-error-msg' + _strIndexFiler)).remove();
}
function doDisplayError(strErrCategory, strErrorMessage, blnDoAlert, index) {
	var _index = index || -1;
	var _blnDoAlert = true;
	if (typeof blnDoAlert == 'boolean') _blnDoAlert = blnDoAlert;
	var _strErrMsg = strErrorMessage || 'Unknown Error!';
	var _strID = 'li_' + strErrCategory;
	var _strIndexFiler = (_index == -1)?'':'[index="' + _index + '"]';
	$('.ul-vldr-error-msg' + _strIndexFiler).append('<li id="' + _strID + '">' + _strErrMsg + '</li>');
	if (_blnDoAlert) alert(_strErrMsg);
}
function doClearDisplayInfo(index) {
	var _indx = index || -1;
	var _strIndexFiler = (_indx == -1)?'':'[index="' + _indx + '"]';
	$(".cls-div-info" + _strIndexFiler).html("");
}

function doDisplayInfo(msg, title, index) {
	var _indx = index || 0;
	$(".cls-div-info[index=" + _indx + "]").html(title + ': ' + msg);
	$(".cls-div-info[index=" + _indx + "]").show(500).fadeIn(500).fadeOut(200).fadeIn(500).fadeOut(200).fadeIn(500);
}

function _doAjaxRequest_Search(index, url, params, fncOnSuccessWithTrue, fncOnStatusSuccess, fncOnDone) {
	var _index = index || -1;
	var _url = url || '';
	var _params = (typeof params == 'object')?JSON.stringify(params):params;

	doClearDisplayInfo(_index);	
	doClearDisplayError(false, _index);
	
	if (_url.trim() == '') {
		doDisplayError('frmSearchFailed', 'Request URL is required!', true, _index);
		return false;
	}
	$.ajax({
		type: "POST", 
		url: _url,
		contentType: "application/json;charset=utf-8",
		dataType: "json",
		data: _params,
		success: function(data, textStatus, jqXHR) {
			if ((!('success' in data)) || (data.success == false)) {
				var _msg = MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, data.error);
				if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
				doDisplayError('frmSearchFailed', _msg, true, _index);
			} else {
				if (typeof fncOnSuccessWithTrue == 'function') fncOnSuccessWithTrue.apply(this, arguments);
			}
			if (typeof fncOnStatusSuccess == 'function') fncOnStatusSuccess.apply(this, arguments);
			if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
			doDisplayError('frmSearchFailed', MSG_ALERT_QUERY_FAILED.replace(/v_XX_1/g, textStatus + ' ( ' + errorThrown + ' )'), true, _index);
		},
		statusCode: {
			404: function() {
				if (typeof fncOnDone == 'function') fncOnDone.apply(this);
				doDisplayError('frmSearchFailed', "Page not found", false, _index);
			}
		}
	});
}
function _doAjaxRequest(index, url, params, user_notify_title, fncOnStatusSuccess, fncOnDone) {
	var _index = index || -1;
	var _url = url || '';
	var _user_notify_title = user_notify_title || '';
	var _params = (typeof params == 'object')?JSON.stringify(params):params;

	doClearDisplayInfo(_index);	
	doClearDisplayError(_user_notify_title + 'Failed', _index);
	if (_url == '') {
		doDisplayError(_user_notify_title + 'Failed', 'Request URL is required!', true, _index);
		if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
		return false;
	}
	$.ajax({
		type:"POST", 
		url:_url,
		contentType:"application/json;charset=utf-8",
		dataType:"json",
		data:_params,
		success: function(data, textStatus, jqXHR) {
			if (typeof fncOnStatusSuccess == 'function') fncOnStatusSuccess.apply(this, arguments);
			if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (typeof fncOnDone == 'function') fncOnDone.apply(this, arguments);
			doDisplayError(_user_notify_title + 'Failed', _user_notify_title + ' ' + textStatus + ' ( ' + errorThrown + ' )', true, _index);
		},
		statusCode: {
			404: function() {
				if (typeof fncOnDone == 'function') fncOnDone.apply(this);
				doDisplayError(_user_notify_title + 'Failed', "Page not found", false, _index);
			}
		}
	});
}
function doExportExcel( nButton, oConfig, oFlash ) {
	var uri = 'data:application/vnd.ms-excel;base64,'
		, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
		, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
	if (_objDataTable) {
		var ctx = {worksheet: name || 'export', table: strConvertDataToTableObject()}
		window.location.href = uri + base64(format(template, ctx));
	}
}
function strConvertDataToTableObject() {
	var jsonReturn = [];
	var _cols = _objDataTable.fnSettings().aoColumns;
	var _data = _objDataTable.fnGetData();
	var _each;
	//_data = _objDataTable.fnGetData();//TableTools.fnGetMasters()[0].fnGetTableData( oConfig );
	for (i=0;i<_data.length;i++) {
		_each = _data[i];
		_new = {};
		for (j=0;j<_cols.length;j++) {
			_col = _cols[j];
			if ((_col.bVisible) && (typeof _col.mData !== 'function')) _new[_col.sTitle] = _each[_col.mData];
		}
		jsonReturn.push(_new);
	}
	if (jsonReturn.length > 0) {
		var _strHead = '';
		$.each(jsonReturn[0], function (k, v) {
			_strHead += "<th>"+k+"</th>";
		});
		_strHead = '<tr>' + _strHead + '</tr>';
		var _strBody = '';
		$.each(jsonReturn, 
			function () {
				var _row = "";
				$.each(this, function (k , v) {
					_row += "<td>"+v+"</td>";
				});
				_strBody += "<tr>"+_row+"</tr>";                 
			}
		);
		var _strHTMLTable = '<table id="tblExportExcel">' + _strHead + _strBody + '</table>';
		return _strHTMLTable;
	} else {
		return '';
	}
}

function openExcelFile(strFilePath) {
    /*if (window.ActiveXObject) {
        try {
            var objExcel;
            objExcel = new ActiveXObject("Excel.Application");
            objExcel.Visible = true;
            objExcel.Workbooks.Open(location.protocol + '//' + window.location.host + strFilePath, false); //, true/false ( third paramenter = readonly )
        } catch (e) {
            alert (e.message);
        }
    } else {*/
        //window.location.href = location.protocol + '//' + window.location.host + strFilePath;
        window.open(location.protocol + '//' + window.location.host + strFilePath);
        //alert("Your browser does not support excel browsing.");
    //}
}

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

        $('body').append('<a id="xxshadowlink" style="display:none;"></a>');
        document.getElementById("xxshadowlink").href = uri + base64(format(template, ctx));
        document.getElementById("xxshadowlink").download = filename;
        document.getElementById("xxshadowlink").click();
    }
})();

function executeFunctionByName(functionName, context, args) {
	//var args = [].slice.call(arguments).splice(2);
	var namespaces = functionName.split(".");
	var func = namespaces.pop();
	for(var i = 0; i < namespaces.length; i++) {
		context = context[namespaces[i]];
	}
	if (typeof context[func] == 'function') {
		return context[func].apply(this, args);
	} else {
		console.error('No function "' + functionName + '"');
	}
}
function __performInitTemplateControls(jqContainer, dialogOptions) {
	_container = jqContainer || $('form');
	_options = dialogOptions || undefined;
	if (_container && (_container.length > 0)) {
		_container.each(function () {
			for (var _i=0;_i<this.elements.length;_i++){
				var _el = this.elements[_i];
//console.log(_elem.name+"="+_elem.value);
				var _elem = _toJQObj(_el);
				var _id = _elem.attr('id') || '';
				if (_id.length > 4) {
					var _pref = _id.substr(0, 4).toLowerCase();
					switch (_pref) {
						case 'dpk-':
							_elem.datepicker({
								showOn: "both",
								buttonText: 'เลือกวันที่',
								changeYear: true,
								changeMonth: true,
								buttonImage: "public/images/select_day.png",
								buttonImageOnly: true,
								dateFormat: 'dd/mm/yy'
							});
							var _def_val = _elem.attr('def_val') || '';
							if (_def_val.length >= 8) _elem.datepicker("setDate", new Date(_def_val));
							break;
						case 'sel-':
							_elem.combobox();
							break;
					}
				}
			}
			for (var _i=0;_i<this.getElementsByTagName('a').length;_i++){
				var _aHref = this.getElementsByTagName('a')[_i];
				var _elem = _toJQObj(_aHref);
				var _id = _elem.attr('id') || '';
				if (_id.length > 4) {
					var _pref = _id.substr(0, 4).toLowerCase();
					if (_pref == 'btn-') {
						var _icon = _elem.attr('icon').trim() || '';
						_elem.button();
						if (_icon !== '') _elem.button("option", "icons", {primary: _icon});
						_elem.on('click', 
							function (evnt) {
								var _func = $(this).attr('function').trim() || '';
								var _strParams = $(this).attr('params') || '';
								var _params = _strParams.split(',');
								if (_func !== '') executeFunctionByName(_func, window, _params); 
							}
						);					
					}
				}
			}
			//first set disabled 
			$(".user-input.set-disabled", _container).each(
				function () {
					_setEnableElem(this, false);
				}
			);			
		});
	}
}

function doPostToURL(path, params, method, target) {
	var _target = target || 'new';
	var _method = method || "post"; // Set method to post by default, if not specified.

	var _frm = document.createElement("form");
	_frm.setAttribute("method", _method);
	_frm.setAttribute("action", path);
	if (_target == 'new') _frm.setAttribute('target', '_blank');

	var _field = document.createElement("input");
	_field.setAttribute("type", "hidden");
	_field.setAttribute("name", 'data');
	_field.setAttribute("value", JSON.stringify(params));

	//alert(_field.value);
	_frm.appendChild(_field);
	_frm.submit();
	_frm.remove();
}


function _checkAccessImg(url, fnc_Callback_onSuccess, fnc_Callback_onError, fnc_Callback_onDone) {
	var _url = url || '';
	if (_url.trim().length <= 0) {
		if (typeof fnc_Callback_onError == 'function') fnc_Callback_onError.apply(this, ['Empty url path']);
		if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
	} else {
		var _el = document.createElement('img');
		var _timStart = new Date();
		var _fnc_idleChecker = function() {
			if (document.body.contains(_el)) {
//				alert('Timeout!!!');
				document.body.removeChild(_el);
				if (typeof fnc_Callback_onError == 'function') fnc_Callback_onError.apply(this, ['"' + this.src + '" failed, timeout']);
				if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
			}
		};
		_el.id = "lfchkr" + new Date().getTime() + Math.floor(Math.random() * 1000000);
		_el.onerror = function (ev) {
//			alert(this.id + ' failed to load');
			if ( ! document.body.contains(_el)) return false;
			document.body.removeChild(_el);
			if (typeof fnc_Callback_onError == 'function') fnc_Callback_onError.apply(this, ['"' + this.src + '" unaccessable']);
			if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
		};
		_el.onload = function (ev) {
//			alert(this.id + ' success');
			if ( ! document.body.contains(_el)) return false;
			document.body.removeChild(_el);
			if (typeof fnc_Callback_onSuccess == 'function') fnc_Callback_onSuccess.apply(this);
			if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
		};
		// + "?" + new Date().getTime() + Math.floor(Math.random() * 1000000); //case want to check if cached as well
		_el.src = _url;
		_el.setAttribute("style", "display:none;");
		document.body.appendChild(_el);
		setTimeout(_fnc_idleChecker, 5000); //5 seconds to check else return timeout
	}
}

function _checkAccessImgList(arrUrl, fnc_Callback_onSuccess, fnc_Callback_onError, fnc_Callback_onDone) {
	var _arrUrl = (typeof arrUrl == 'object') && (arrUrl.length > 0) ? arrUrl : '';
	if (typeof _arrUrl == 'string') {
		_checkAccessImg(_arrUrl, fnc_Callback_onSuccess, fnc_Callback_onError, fnc_Callback_onDone);
		return false;
	}
	var _blDone = false;
	var _arrLoadCheckList = [];
	var _arrObjErrorList = false;
	var _fncCheckLoadCompleted = function () {
		if (_blDone) return false;
		if (_arrLoadCheckList.length <= 0) {
			if (_arrObjErrorList) {
				if (typeof fnc_Callback_onError == 'function') fnc_Callback_onError.apply(this, [_arrObjErrorList]);
				if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
			} else {
				if (typeof fnc_Callback_onSuccess == 'function') fnc_Callback_onSuccess.apply(this);
				if (typeof fnc_Callback_onDone == 'function') fnc_Callback_onDone.apply(this);
			}
			_blDone = true;
		}
	};
	for (var _i=0;_i<_arrUrl.length;_i++) {
		var _strUrl = ((typeof _arrUrl[_i] == 'string') && (_arrUrl[_i].trim().length > 0)) ? _arrUrl[_i].trim() : '';
		if (_strUrl != '') {
			_arrLoadCheckList.push(_strUrl);
			_checkAccessImg(_strUrl
				, function() { _arrLoadCheckList.removeByValue(_strUrl) }
				, function(errMsg) { _arrObjErrorList.append({"url": _strUrl, "msg": (errMsg || 'unknown')}); }
				, _fncCheckLoadCompleted);
		}
	}
}