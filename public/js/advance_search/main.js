﻿var _ARR_PRICE_LIST = [];
var _ARR_PRICE_TYPE_MULTIPLIER = {'1': 1, '2': 100};
var _STR_CURR_PRICELIST = '';
var _CURR_PRICE_START = -1;
var _CURR_PRICE_LIMIT = -1;
var _currentDataString = '';
var _ulSearchInfo;
var _MAP;
var _OBJ_MAP_MRKS = {};

/*
price_type: 1=rent, 2=buy
formula = [each raw_price in _ARR_PRICE_LIST] * ([exact value in _ARR_PRICE_TYPE_MULTIPLIER] * [selected ex_rate ratio])
*/
// ++ init pricelist options
_ARR_PRICE_LIST.push(2000, 5000, 7000);
for (var _i=1;_i<=10;_i++) {
	_ARR_PRICE_LIST.push((10000 * _i));
}
_ARR_PRICE_LIST.push(150000, 200000, 300000);
// -- init pricelist options

$(function() {
	_ulSearchInfo = $('#ul_search_info');
	
	$('#left_panel').css('width', '40%');
	$('#divPanelHandler').css('left', $('#left_panel').width());
	$("#dialog-modal").html("<p>" + MSG_DLG_HTML_QUERY + "</p>")
		.dialog({
			height:100,
			width:400,
			resizable:false,
			modal:true,
			closeOnEscape:false,
			title: MSG_DLG_TITLE_QUERY,
			autoOpen:false
		})
	;
	$('#frmSearch input[type=radio][name=price_type]').on('change', function() {
		_fncRefreshPriceList();
		return false;
	});
	$('#frmSearch #sel-price_unit').on('change', function() {
		_fncRefreshPriceList();
		return false;
	});

	var _cmbPlace = $('#frmSearch #sel-search_place').combobox().combobox("setChanged", function() {
		if (getValue($('#frmSearch #sel-search_place'), -1) == -1) {
			_enableElem('sel-search_area', true);
			return false;
		} else {
			clearValue($('#frmSearch #sel-search_area'));
			_enableElem('sel-search_area', false);
		}
	});
	// ++ hijacking ui.combobox ++
	//var _fnc_tmplComboboxAutoComplete_renderItem = _cmbPlace.data('uiCombobox').input.data('uiAutocomplete')._renderItem;
	_cmbPlace.data('uiCombobox').input.data('uiAutocomplete')._renderItem = function (ul, item) {
		var _li = $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a>" + item.label + "</a>" );

		var _grName = '';
		var _grId = '';
		if (item.class) _grName = (item.class || '');
		if (_grName.trim().length > 0) {
			_grId = _grName.toLowerCase().trim().replace(/[^a-zA-Z0-9]/gi, '_');
			_li.attr("group", _grName).addClass('cls-combobox-group cls-group-child cls-group__' + _grId);
			// add new li group header if not exists yet
			if ($('#' + _grId, ul).length == 0) $('<li id="' + _grId + '" class="cls-combobox-group cls-group-title">')
				.on('click', function () {
					_fnc_toggleGroup(this);
					return false;
				})
				//.html(_grName)
				.append('<a>' + _grName + '</a>')
				.appendTo(ul);
		}
		_li.appendTo(ul);
		return _li;
	};
	/*
	_cmbPlace.data('uiCombobox').input.on('autocompleteresponse', function(event, ui) { console.log(this); });
	_cmbPlace.data('uiCombobox').input.on('autocompleteopen', function(event, ui) { console.log(this); });
	_cmbPlace.data('uiCombobox').input.on('autocompleteselect', function(event, ui) { console.log(this); });
	_cmbPlace.data('uiCombobox').input.data('uiAutocomplete')._renderItem = function (ul, item) {};
	_cmbPlace.data('uiCombobox').input.data('uiAutocomplete')._renderMenu = function(ul, items) {};
	_cmbPlace.data('uiCombobox').options.blnBeforeChange =  function (value, event, ui) {};
	*/
	// -- hijacking ui.combobox --
	
	$('#frmSearch #sel-search_area').combobox().combobox("setChanged", function() {
		if (getValue($('#frmSearch #sel-search_area'), -1) == -1) {
			_enableElem('sel-search_place', true);
			//$('#frmSearch #sel-search_place').combobox('enable', true);
			return false;
		} else {
			clearValue($('#frmSearch #sel-search_place'));
			_enableElem('sel-search_place', false);
			//$('#frmSearch #sel-search_place').combobox('enable', false);
		}
	});
	$('#frmSearch #sel-search_place').parent().find('input.ui-combobox-input').attr('placeholder', 'Nearest Station');
	$('#frmSearch #sel-search_area').parent().find('input.ui-combobox-input').attr('placeholder', 'District');
	
	$('#frmSearch #sel-price_start').on('change', function() {
		var _val = getValue($('#frmSearch #sel-price_start'), -1);
		if (_val == _CURR_PRICE_START) return false;
		_CURR_PRICE_START = parseFloat(_val);
		_CURR_PRICE_LIMIT = parseFloat(getValue($('#frmSearch #sel-price_limit'), -1));
		$('#frmSearch #sel-price_start option[list]').remove();
		$('#frmSearch #sel-price_limit option[list]').remove();
		$('#frmSearch #sel-price_start').append(_STR_CURR_PRICELIST);
		if (_CURR_PRICE_START < 0) {
			$('#frmSearch #sel-price_limit option[last]').before(_STR_CURR_PRICELIST);
		} else {
			var _newLimitValue = -1;
			$('#frmSearch #sel-price_start option[list]').each(function() {
				_opt = $(this);
				if ((parseFloat(_opt.val()) >= _CURR_PRICE_START)) $('#frmSearch #sel-price_limit option[last]').before(_opt.clone());
				if (_opt.val() == _CURR_PRICE_LIMIT) _newLimitValue = _CURR_PRICE_LIMIT;
			});
			_CURR_PRICE_LIMIT = _newLimitValue;
		}
		setValue($('#frmSearch #sel-price_start'), _CURR_PRICE_START);
		setValue($('#frmSearch #sel-price_limit'), _CURR_PRICE_LIMIT);
		return false;
	});
	$('#frmSearch #sel-price_limit').on('change', function() {
		var _val = getValue($('#frmSearch #sel-price_limit'), -1);
		if (_val == _CURR_PRICE_LIMIT) return false;
		_CURR_PRICE_LIMIT = parseFloat(_val);
		_CURR_PRICE_START = parseFloat(getValue($('#frmSearch #sel-price_start'), -1));
		$('#frmSearch #sel-price_start option[list]').remove();
		$('#frmSearch #sel-price_limit option[list]').remove();
		$('#frmSearch #sel-price_limit option[last]').before(_STR_CURR_PRICELIST);
		if (_CURR_PRICE_LIMIT < 0) {
			$('#frmSearch #sel-price_start').append(_STR_CURR_PRICELIST);
		} else {
			var _newStartValue = -1;
			$('#frmSearch #sel-price_limit option[list]').each(function() {
				_opt = $(this);
				if ((parseFloat(_opt.val()) <= _CURR_PRICE_LIMIT)) $('#frmSearch #sel-price_start').append(_opt.clone());
				if (_opt.val() == _CURR_PRICE_START) _newStartValue = _CURR_PRICE_START;			
			});
			_CURR_PRICE_START = _newStartValue;
		}
		setValue($('#frmSearch #sel-price_start'), _CURR_PRICE_START);
		setValue($('#frmSearch #sel-price_limit'), _CURR_PRICE_LIMIT);
		return false;
	});
	
	$('#frmSearch #aac-search_name').autocomplete({
		delay:500
		,minLength:3
		,source: function( request, response ) {
			_enableElem($('#aac-search_name'), false);
			var _strName = getValue($('#aac-search_name'), '');
			var _params = {};
			$(".search-param").each(function() {
				var _name = getData(this);
				var _val = getValue(this);
				if ((_val || '') != '') _params[_name] = _val; //_searchParams += '"' + _name + '":"' + _val + '",';
			});
			$.ajax({
				dataType:"json"
				,type: 'POST'
				,data: _params //JSON.stringify(_params)
				,url: 'advance_search/ajax_aac_search_name'
				,success: function(data) {
					__doClearSearchNotice('search_name');

					_enableElem($('#aac-search_name'), true);
					$('#aac-search_name').removeClass('ui-autocomplete-loading');
					if ((data.success == true) && (data.data.length > 0)) {
						var data = data.data;
						var matcher = new RegExp( $.ui.autocomplete.escapeRegex(_strName), "i" );
						var _resp = $.map( data, function(item) {
								_text = item.name_en;
								if ( ! _strName || matcher.test(_text) ) return {
										label: _text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(_strName) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>" ),
										value: _text,
										hdn_value: item.rowid
									};
							})
						if (_resp.length > 0) _resp.unshift({label:"&nbsp;", value:"", hdn_value:""});
						response(_resp);
					} else {
						var _str = '';
						for (_x in _params) {
							_str += _x + ': ' + _params[_x] + ', ';
						}
						_str = _str.substr(0, (_str.length - 2));
						__doAddSearchNotice('search_name', 'No data found with current condition ( ' + _str + ' )', 'cls-caution');
					}
				}
				,error: function(data) {
					_enableElem($('#aac-search_name'), true);
					$('#aac-search_name').removeClass('ui-autocomplete-loading');
					__doAddSearchNotice('search_name', (data.error || ''), 'cls-error');
				}
			});
		}
		,select: function(event, ui) {
			var _aac_text = ui.item.value || '';
			var _aac_hdn_val = ui.item.hdn_value || '';
			_aac_text = _aac_text.toString().trim();
			_aac_hdn_val = _aac_hdn_val.toString().trim();
			$('#hdn-search_name').val(_aac_hdn_val);
		}
	});

	$('#frmSearch #btnSearch')//.button()
		.on('click', function() {
			doSearch(true);
		})
	;
	$('#frmSearch #btnReset')//.button()
		.on('click', function() {
			$('#frmSearch')[0].reset();
			_CURR_PRICE_START = -1;
			_CURR_PRICE_LIMIT = -1;
			setValue($('#frmSearch #sel-price_unit'), $('#frmSearch #sel-price_unit option')[0].value);
			$('#frmSearch #sel-search_place').combobox('enable', true);
			$('#frmSearch #sel-search_area').combobox('enable', true);
			setValue($('#frmSearch #sel-price_start'), _CURR_PRICE_START);
			setValue($('#frmSearch #sel-price_limit'), _CURR_PRICE_LIMIT);
			return false;
		})
	;

/*
	// ++ populate exchange_rate info list
	var _strExRateToolTip = '<ul>';
	$('#frmSearch #sel-price_unit option').each(function() {
		_opt = $(this);
		_strDisp = _opt.val().toUpperCase() || '';
		_floRateRatio = (! isNaN(_opt.attr('rate'))) ? parseFloat(_opt.attr('rate')) : -1;
		_strExRateToolTip += '<li>THB/' + _strDisp + ' = ' + formatNumber(_floRateRatio, 4) + '</li>';
	});
	$('#frmSearch #img_ex_rate_info')
		.attr("title", "")
		.tooltip({content: _strExRateToolTip, tooltipClass: 'cls-ex-rate-info-tooltip', show: {effect: 'heightlight'}, position: { my: "left+10 center", at: "right center" }})
//		.tooltip("open")
		;
	// -- populate exchange_rate info list
*/
	// populate price list
	_fncRefreshPriceList();
	
	_MAP = L.map("divDispMap", {
		center: L.latLng(_DEFAULT_LAT, _DEFAULT_LNG)
		,minZoom: _MIN_ZOOM_LEVEL
		,maxZoom: _MAX_ZOOM_LEVEL
		,zoom: _DEFAULT_ZOOM_LEVEL
		,tap: false
		,touchZoom: false
		,closePopupOnClick: false
	})
		.on("contextmenu", function (e) {
			alert("Context menu disabled");
		})
	;
	var layerControl = false;
	if (typeof __fncGetLeafletLayerControl == 'function') layerControl = __fncGetLeafletLayerControl.apply(this, [_MAP]);
	if (! layerControl) {
		alert('ERROR: Map init function not found! ( __fncGetLeafletLayerControls )');
		return false;
	}
	_MAP.addControl(layerControl);

	$('#divDisplayQueryResult').hide();
});

function _fnc_toggleGroup(uiGroup) {
	var _elem = $(uiGroup);
	if (_elem.length <= 0) return false;
	var _grId = uiGroup.id || false;
	var _ul = _elem.parent('ul') || false;
	if ((_grId) && (_ul)) {
		if (_elem.hasClass('cls-groupState__collapse')) {
			_elem.removeClass('cls-groupState__collapse');
			$('li.cls-group__' + _grId, _ul).css('display', '');
		} else {
			_elem.addClass('cls-groupState__collapse');
			$('li.cls-group__' + _grId, _ul).css('display', 'none');
		}
	}
}
function __doClearSearchNotice(name) {
	var _name = name || '';
	if ((_ulSearchInfo.length > 0) && (_name != '')) $('li.cls-sinfo-' + _name, _ulSearchInfo).remove();
}
function __doAddSearchNotice(name, msg, cls) {
	var _name = name || '';
	var _msg = msg || '';
	var _cls = cls || 'cls-info';
	if ((_ulSearchInfo.length > 0) && (_name != '') && (_msg != '')) {
		_ulSearchInfo.append('<li class="cls-sinfo-' + _name + ' ' + _cls + '" title="' + _msg + '">' + _msg + '</li>');
	}
}

function doSearch(blnChangeSearchCriteria) {
	var _index = $('#divDisplayQueryResult').attr('index') || 0;
	var _blnChangeSearchCriteria = typeof blnChangeSearchCriteria != undefined?blnChangeSearchCriteria:true;

	$('#divDisplayQueryResult').hide();

	doClearDisplayInfo(_index);
	doClearDisplayError(false, _index);
	$("#dialog-modal").dialog( "open" );
	if (_blnChangeSearchCriteria) {
		_currentDataString = "";
		var _params = {};
		$(".search-param").each(
			function() {
				var _name = getData(this);
				var _val = getValue(this);
				if ((_val || '') != '') _params[_name] = _val; //_currentDataString += '"' + _name + '":"' + _val + '",';
			}
		);
		//**!! Inferno add for separate lease and purchasing search
		_params['price_type'] = _getElemValue('rdo-price_type_1', 1);
		_currentDataString = JSON.stringify(_params);
	}
	_doAjaxRequest_Search(
		_index
		,_baseUrl + CONTROLLER_NAME + "/json_search"
		,_currentDataString
		,function(data, textStatus, jqXHR) { //fncOnSuccessWithTrue
			data = data.data;
			if (data.length > 0) {
				doPopulateResultPanel(data, _blnChangeSearchCriteria);
				if ($('#left_panel').css('display') == 'block') {
					if (typeof(doToggleLeftPanel) == 'function') doToggleLeftPanel.call();
				}
			} else {
				doDisplayInfo(MSG_ALERT_QUERY_NO_DATA_FOUND, 'Info', _index);
			}
		}
		,false //fncOnStatusSuccess
		,function() { //fncOnDone
			if ($("#dialog-modal").dialog("isOpen")) $("#dialog-modal").dialog("close");
		}
	);
	return false;
}

function _doManagerSearchCriteriaInfo() {
	if ((_enableElem('sel-search_place')) && (getValue('sel-search_place', 0) > 0)) {
		$('#info_search_criterial').append('<span class="round label">Location nearest to "' + $('#sel-search_place option:selected').text() + '"</span>');
	} else if ((_enableElem('sel-search_area')) && (getValue('sel-search_area', 0) > 0)) {
		$('#info_search_criterial').append('<span class="round label">Location in "' + $('#sel-search_area option:selected').text() + '"</span>');
	}
	if ((getValue('aac-search_name', "").trim() != '')) {
		$('#info_search_criterial').append('<span class="round label">Building name "' + getValue('aac-search_name', "").trim() + '"</span>');		
	}
	_val1 = getValue('sel-price_start', 0);
	_val2 = getValue('sel-price_limit', 0);
	_str = '';
	if ((_val1 + _val2) > 0) {
		if ((_val1 > 0) && (_val2 > 0)) {
			_str += ' between ' + formatNumber(_val1) + ' - ' + formatNumber(_val2);
		} else if ((_val1 > 0)) {
			_str += ' greater than ' + formatNumber(_val1);
		} else if ((_val2 > 0)) {
			_str += ' lesser than ' + formatNumber(_val2);
		}
		if ((getValue('rdo-price_type_1'))) {
			_str = 'Rent ' + _str + '/month';
		} else {
			_str = 'Purchase ' + _str;			
		}
		var _sel = $('#sel-price_unit option:selected');
		if (_sel.lenght > 0) {
			_str += ' (' + _sel.text() + ')'
		} else {
			_str += ' (bht)'; //default
		}
		$('#info_search_criterial').append('<span class="round label">' + _str + '</span>');
	}
}
var _objDataTable;
function doPopulateResultPanel(_arrayData, blnChangeSearchCriteria) {
	$('#info_search_criterial').empty();
	
	$('#info_found').html(_arrayData.length);
	$('#info_location').html(_arrayData[0]['info_found_location']);
	
	var _tbl = $('#tblSearchResult');
	if (_objDataTable) _objDataTable.destroy(false);
	_doManagerSearchCriteriaInfo();
	var _tbody = $('tbody', _tbl).empty();
	
	$('th.cls-place-distance', _tbl).remove();
	var _blDistance = false;
	if ((_enableElem('sel-search_place')) && (getValue('sel-search_place', 0) > 0)) {
		_blDistance = true;
		$('thead tr', _tbl).append('<th class="cls-place-distance">DISTANCE</th>');
	}
	__clearMapMarkers();
	var _m = false;
	for (_i=0;_i<_arrayData.length;_i++) {
		var _row = _arrayData[_i];
		//var _jInfo = {};
		//if (('info' in _row) && (_row['info'].trim() != '')) _jInfo = JSON.parse(_row['info'].trim());
		var _tr = $('<tr>')
			//.append('<td><a href="../index.php?option=com_content&view=article&id=' + _row['building_link_article_id'] + '" target="_new">' + _row['disp_building_name'] + '</a></td>')
			//.append('<td><a href="../index.php?option=com_content&view=article&id=' + _row['link_article_id'] + '" target="_new">' + _row['disp_room_no'] + '</a></td>')
			//.append('<td><a href="javascript:;" onclick="_doDisplayLinkArticle(' + _row['building_link_article_id'] + ')">' + _row['disp_building_name'] + '</a></td>')
			.append('<td>' + _row['disp_building_name'] + '</td>')
			.append('<td><a href="javascript:;" onclick="_doManageDisplay(' + _row['rowid'] + ')">' + _row['disp_name'] + '</a></td>')
			.append('<td>' + _row['room'] + '</td>')
			.append('<td>' + _row['floor'] + '</td>')
			.append('<td>' + _row['disp_rent'] + '</td>')
			.append('<td>' + _row['disp_purchase'] + '</td>')
			.append('<td>' + _row['bed_room'] + '</td>')
			.append('<td>' + _row['bath_room'] + '</td>')
			.append('<td>' + _row['area_m2'] + ' m<sup>2</sup></td>')
			.append('<td><img src="public/images/icons/' + ((_row['owner_type_rowid'] == 2)?'owner-b':'owner-r') + '.png"></td>')
			.append('<td>' + _row['disp_rent_per_m2'] + '</td>')
			.append('<td>' + _row['disp_purchase_per_m2'] + '</td>')
			.append('<td>' + _row['rowid'] + '</td>')
		;
		if (_blDistance) _tr.append('<td>' + _row['place_distance'] + '</td>');
		_tbody.append(_tr);

		var _rowid = _row['rowid'] || -1;
		var _arr = JSON.parse(_row['disp_map_area']);
		if ((_rowid > 0) && (typeof _arr.length != 'undefined') && (_arr.length > 1)) {
			var _opt = { riseOnHover:true, icon: _DEF_ICON};
			var _b_type_rowid = parseInt(_row['b_type_rowid'] || -1);
			if (_b_type_rowid in _OBJ_BT_ICONS) _opt['icon'] = _OBJ_BT_ICONS[_b_type_rowid.toString()];
			_m = L.marker(L.latLng(_arr[1], _arr[0]), _opt);
			_OBJ_MAP_MRKS[_rowid.toString()] = _m;
			_MAP.addLayer(_OBJ_MAP_MRKS[_rowid.toString()]);
		}
	}
	if (typeof _m == 'object') _MAP.setView(_m.getLatLng(), parseInt((_MAX_ZOOM_LEVEL - 3)), {"animate": true});
	$('#divDisplayQueryResult').show();
	var _arrColDefs = [{ "targets": '_all', "className": 'center' }]; //{ targets: [0, 1], visible: true}

	if (_getElemValue('rdo-price_type_1', 1) == 1) { //rent
		_arrColDefs.push({targets: [5, 11, 12], visible: false});
	} else {
		_arrColDefs.push({targets: [4, 10, 12], visible: false});		
	}
	_objDataTable = _tbl.DataTable({
			"bDeferRender": true,
			"jQueryUI": false,
			"lengthChange": false,
			"paging": false,
			"info": false,
			"searching": true,
			"bStateSave": false
			,"columnDefs": _arrColDefs
			//,"scrollY": '20vh'
			//,"scrollCollapse": true
		});

	$('tbody', _tbl).on('click', 'tr', function() {
		var _data = _objDataTable.row(this).data();
		var _rowid = _data[12] || -1;
		if (_rowid > 0) {
			var _selMarker = _OBJ_MAP_MRKS[_rowid.toString()] || false;
			if (typeof _selMarker == 'object') _MAP.setView(_selMarker.getLatLng(), _MAX_ZOOM_LEVEL, {animate: true});
		}
	});
}
function __clearMapMarkers() {
	for (_r in _OBJ_MAP_MRKS) {
		if (typeof _OBJ_MAP_MRKS[_r] == 'object') {
			_MAP.removeLayer(_OBJ_MAP_MRKS[_r]);
			delete _OBJ_MAP_MRKS[_r];
		}
	}
}
function _doDisplayLinkArticle(id) {
	var _id = id || 0;
	if (_id) {
		var _href = '../index.php?option=com_content&view=article&id=' + _id;
		window.open(_href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
		return false;
	}
}
function _doManageDisplay(row_id) {
	var _rowid = row_id || false;
	if (_rowid) {
		//var _href = '../index.php?option=com_content&view=article&id=' + link_article_id; //&tmpl=component
		var _href = 'room/display/' + _rowid;
		var _strOptions = 'location=0,menubar=0,status=0,titlebar=0,toolbar=0,resizebar=1,width=' + (screen.width * 0.9) + ',height=' + screen.height + ',left=' + (screen.width * 0.05) + ',top=0,fullscreen=1';
		window.open(_href, '_blank', _strOptions);
	}
}

function _fncRefreshPriceList(intPriceType, strUnitValue) {
	var _elemPriceType = _toJQObj('rdo-price_type_1');
	var _elemPriceUnit = $('#frmSearch #sel-price_unit');
	var _elemPriceStart = $('#frmSearch #sel-price_start');
	var _elemPriceLimit = $('#frmSearch #sel-price_limit');
	var _intType = (_isInt(intPriceType)) ? parseInt(intPriceType) : -1;
	var _strUnitVal = (typeof strUnitValue == 'string') ? strUnitValue.trim() : '';
	var _curType = getValue(_elemPriceType, 1);
	var _curUnitVal = getValue(_elemPriceUnit, 'thb');
	_curUnitVal = _curUnitVal.trim();

	var _strDispUnit = 'BHT';
	var _floRateRatio = 1;
	var _floTypeMultiplier = -1;
	if ((_intType != _curType)) {
		if (_intType > 0) {
			setValue(_elemPriceType, _intType);
		} else if (_curType > 0) {
			_intType = _curType;
		}
	}
	_floTypeMultiplier = (_intType in _ARR_PRICE_TYPE_MULTIPLIER) ? _ARR_PRICE_TYPE_MULTIPLIER[_intType] : -1;
	
	var _opt = $('option[value="' + _strUnitVal + '"]', _elemPriceUnit);
	if ((_strUnitVal != _curUnitVal)) {
		if ((_opt.length > 0)) {
			setValue(_elemPriceUnit, _opt.val());
		} else if ((_curUnitVal != '')) {
			_opt = $('option[value="' + _curUnitVal + '"]', _elemPriceUnit);
		}
	}
	if ((_opt.length > 0)) {
		_opt = $(_opt[0]);
		_strDispUnit = _opt.text() || '';
		_floRateRatio = (! isNaN(_opt.attr('rate'))) ? parseFloat(_opt.attr('rate')) : -1;
	}
	if ((_floTypeMultiplier > 0) && (_floRateRatio > 0)) {
		$('option[list]', _elemPriceStart).remove();
		$('option[list]', _elemPriceLimit).remove();
		_STR_CURR_PRICELIST = '';
		for (var _i=0;_i<_ARR_PRICE_LIST.length;_i++) {
			var _v = (! isNaN(_ARR_PRICE_LIST[_i])) ? _ARR_PRICE_LIST[_i] : -1;
			if ((! isNaN(_v)) && (_v > 0)) {
				var _val = (_v * _floTypeMultiplier) * _floRateRatio;
/*
				var _dispVal = -1;
				if (_val >= 1000000) {
					_dispVal = ((_val % 1000000) > 0) ? (_val / 1000000).toFixed(1) + 'M' : (_val / 1000000).toFixed() + 'M';					
				} else if (_val >= 10000) {
					_dispVal = ((_val % 1000) > 0) ? (_val / 1000).toFixed(1) + 'K' : (_val / 1000).toFixed() + 'K';
				} else {
					_dispVal = _val.toFixed();
				}
*/
				_STR_CURR_PRICELIST += '<option list="list" value="' + _v + '">' + formatNumber(_val, 0) + ' ' + _strDispUnit + '</option>';
			}
		}
		if (_STR_CURR_PRICELIST != '') {
			_elemPriceStart.append(_STR_CURR_PRICELIST);			
			$('option[last]', _elemPriceLimit).before(_STR_CURR_PRICELIST);
			setValue(_elemPriceStart, _CURR_PRICE_START);
			setValue(_elemPriceLimit, _CURR_PRICE_LIMIT);
		}
	}
}
