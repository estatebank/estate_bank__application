<h4>PURCHASE</h4>
<form id="frmSearch" controller="search__purchase" action="post">
	<input type="hidden" class="data-container" data="rdo-price_type_1" value="2">
	<div class="estate_bank_tmpl">
		<div class="items-row cols-2">
			<div class="column-1 control-group">
				<span class="label">SELECT NEAREST STATION</span>
				<div class="controlers">
				<select id="sel-search_place" class="data-container search-param">
					<option first="first" value=""> -- </option>
<?php if (isset($lst_landmark) && is_array($lst_landmark)): ?>
<?php 	foreach ($lst_landmark as $_row): ?>
					<option value="<?php echo $_row['rowid']; ?>" class="<?php echo $_row['disp_group_name']; ?>"><?php echo $_row['disp_text']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
				</select>
				</div>
				<span class="text-right">or</span>
				<span class="label">SELECT DISTRICT</span>
				<div class="controlers">
				<select id="sel-search_area" class="data-container search-param">
					<option first="first" value=""> -- </option>
<?php if (isset($lst_area) && is_array($lst_area)): ?>
<?php 	foreach ($lst_area as $_row): ?>
					<option value="<?php echo $_row['rowid']; ?>"><?php echo $_row['disp_text']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
				</select>
				</div>
			</div>
			<div class="column-2 control-group">
				<span class="label">MIN. COST</span>
				<select id="sel-price_start" class="data-container search-param">
					<option first="first" value=""> -- </option>
				</select>
				<span class="text-right">&nbsp;</span>
				<span class="label">MAX. COST</span>
				<select id="sel-price_limit" class="data-container search-param">
					<option last="last" value=""> -- </option>
				</select>
			</div>
		</div>
		<hr>
		<div class="items-row cols-2">
			<div class="column-1 control-group">
				<span class="label secondary">BUILDING NAME</span>
				<input type="text" id="aac-search_name" placeholder="( Optional )" class="data-container search-param">
				<ul id="ul_search_info"></ul>
			</div>
			<div class="column-2 control-group">
				<a id="btnSearch" class="clsFormButton expand radius button">SUBMIT</a>
				<a id="btnReset" class="clsFormButton expand radius button">RESET</a>
				<a class="expand radius secondary button">(Advanced Search)</a>
			</div>
		</div>
	</div>
</form>