<?php $_index = isset($index)?$index:-1; ?>
<div id="divInfo" class="cls-div-info" index="<?php echo $_index; ?>" ><?php echo isset($info)?$info:''; ?></div>
<div class="cls-list-container">
	<ul class="ul-vldr-error-msg" index="<?php echo $_index; ?>" ></ul>
	<div id="divDisplayQueryResult" class="cls-div-list" index="<?php echo $_index; ?>" >
		<div class="cls-div-result-header-cntr cols-2">
			<div id="divTitlePanel" class="panel">
				<h4>SEARCH RESULTS - PURCHASE</h4>
				<hr>
				Search Criteria: <div id="info_search_criterial">
					<!--span class="round label">Location Nearest to BTS-Siam</span>
					<span class="round label">Rent between THB10,000-20,000/month</span-->
				</div>
				<br>
				Found&nbsp;<span class="found label"><span id="info_found">0</span>&nbsp;Rooms</span>&nbsp;from&nbsp;<span class="found label"><span id="info_location">0</span>&nbsp;Locations</span>
			</div>
			<div id="divDispMap" class="leaflet-container leaflet-retina leaflet-fade-anim">&nbsp;</div>
		</div>
		<div id="divResultPanel" class="panel">
			<h4>RECOMMENDED LISTINGS</h4><hr>
			<small style="position:absolute;">( click header to sort )</small>
			<table id="tblSearchResult" class="cls-tbl-list">
				<thead>
					<tr>
						<th>BUILDING</th>
						<th>NAME</th>
						<th>ROOM NO.</th>
						<th>FLOOR</th>
						<th>RENT</th>
						<th>PRICE</th>
						<th>BED R.</th>
						<th>BATH R.</th>
						<th>AREA</th>
						<th>LIST BY</th>
						<th>RENT/m<sup>2</sup></th>
						<th>PRICE/m<sup>2</sup></th>
						<th>_rowid</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td colspan="12"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<?php if (isset($edit_dlg) && is_array($edit_dlg) && isset($edit_dlg['template'])): ?>
<div id="<?php echo $_CONTROLLER_GENERATED__EDIT_DLG_ID; ?>" class="cls-div-form-edit-dialog" index="<?php echo $_index; ?>">
	<?php echo $edit_dlg['template']; ?>
</div>
<?php endif; ?>
<div id="divSelectableFields" class="cls-div-select-list-fields" index="<?php echo $_index; ?>" >
<?php echo isset($_CONTROLLER_GENERATED__SELECT_ELEM)?$_CONTROLLER_GENERATED__SELECT_ELEM:''; ?>
</div>
