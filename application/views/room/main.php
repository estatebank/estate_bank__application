<?php $_index = isset($index)?$index:-1; ?>
<ul class="ul-vldr-error-msg" index="<?php echo $_index; ?>"></ul>
<form id="frm_edit" controller="<?php echo isset($crud_controller)?$crud_controller:$this->uri->rsegment(1); ?>" class="cls-frm-edit" index="<?php echo $_index; ?>" enctype="multipart/form-data">
	<?php echo isset($_CONTROL_ELEMENT["rowid"]) ? $_CONTROL_ELEMENT["rowid"] : ''; ?>
	<div class="row">
		<div class="medium-8 columns panel content">
			<h4 style="line-height:1.5em;">
				<div class="cls-control-container" style="width:40%;">
					<?php echo isset($_CONTROL_ELEMENT["building_rowid"]) ? $_CONTROL_ELEMENT["building_rowid"] : ''; ?>
				</div>
				<span>: Room</span>
				<div class="cls-control-container" style="width:40%;">
					<?php echo isset($_CONTROL_ELEMENT["name_en"]) ? $_CONTROL_ELEMENT["name_en"] : ''; ?>
				</div>
			</h4>
			<hr/>
			<div class="panel">
				<p><?php echo isset($_CONTROL_ELEMENT["description"]) ? $_CONTROL_ELEMENT["description"] : ''; ?></p>
				<hr>
<?php if (isset($_ACR["admin"]) && ($_ACR["admin"] != FALSE)): ?>
				<h5>Owner :</h5>
				<select id="sel-owner_type_rowid" class="user-input input-required data-container" data="owner_type_rowid">
					<option value="1" checked="checked">Room Owner</option>
					<option value="2">Building Owner</option>
				</select>
				<?php echo isset($_CONTROL_ELEMENT["owner_rowid"]) ? $_CONTROL_ELEMENT["owner_rowid"] : ''; ?>
				<!--span class="user-input data-container" data="disp_owner_tel"></span-->
<?php endif; ?>
			</div>
		</div>
		<div id="div_building_image" class="medium-4 panel columns">
			<div class="cls-image-container"></div>
		</div>
		<!--div class="medium-4 panel columns">
			<div class="frm-edit-row-value display-upload">
				<span class="spn-image-select eventView-hide">เพิ่ม/แก้ไขรูปภาพประกอบตึก<input id="fil-image0" type="file" name="image"></span>
				<input type="hidden" id="hdn-file_image0" class="user-input" >
			</div>
		</div-->
		<div class="medium-12 panel columns">
			<b>INFORMATION</b>
			<table class="cls-room-details">
				<tbody>
					<tr>
						<th><span>ROOM NO.</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["room"]) ? $_CONTROL_ELEMENT["room"] : ''; ?></td>
						<th><span>TYPE</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["room_type_rowid"]) ? $_CONTROL_ELEMENT["room_type_rowid"] : ''; ?></td>
					</tr>
					<tr>
						<th><span>BED ROOM</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["bed_room"]) ? $_CONTROL_ELEMENT["bed_room"] : ''; ?></td>
						<th><span>BATH ROOM</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["bath_room"]) ? $_CONTROL_ELEMENT["bath_room"] : ''; ?></td>
					</tr>
					<tr>
						<th><span>FLOOR</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["floor"]) ? $_CONTROL_ELEMENT["floor"] : ''; ?></td>
						<th><span>AREA</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["area_m2"]) ? $_CONTROL_ELEMENT["area_m2"] : ''; ?> m<sup>2</sup></td>
					</tr>
					<tr>
						<th><span>RENT</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["rent_price"]) ? $_CONTROL_ELEMENT["rent_price"] : ''; ?></td>
						<th><span>DEPOSIT</span></th>
						<td><?php echo isset($_CONTROL_ELEMENT["purchase_price"]) ? $_CONTROL_ELEMENT["purchase_price"] : ''; ?></td>
					</tr>
				</tbody>
			</table>
			<!-- hr/>
			<b>FACILITIES / SERVICES</b>
			<br>
			<img src="/real_estate/app/public/images/icons/building_services/faci-clean.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-conv.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-gym.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-laundry.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-pets.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-play.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-pool.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-sauna.png" width="50" />
			<img src="/real_estate/app/public/images/icons/building_services/faci-shuttle.png" width="50" />
			</dl-->
		</div>
		<hr/>
		<!-- Thumbnails -->
		<div id="div_room_images" class="row">
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid1"]) ? $_CONTROL_ELEMENT["img_rowid1"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image1"]) ? $_CONTROL_ELEMENT["image1"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title1"]) ? $_CONTROL_ELEMENT["img_title1"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid2"]) ? $_CONTROL_ELEMENT["img_rowid2"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image2"]) ? $_CONTROL_ELEMENT["image2"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title2"]) ? $_CONTROL_ELEMENT["img_title2"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid3"]) ? $_CONTROL_ELEMENT["img_rowid3"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image3"]) ? $_CONTROL_ELEMENT["image3"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title3"]) ? $_CONTROL_ELEMENT["img_title3"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid4"]) ? $_CONTROL_ELEMENT["img_rowid4"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image4"]) ? $_CONTROL_ELEMENT["image4"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title4"]) ? $_CONTROL_ELEMENT["img_title4"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid5"]) ? $_CONTROL_ELEMENT["img_rowid5"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image5"]) ? $_CONTROL_ELEMENT["image5"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title5"]) ? $_CONTROL_ELEMENT["img_title5"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid6"]) ? $_CONTROL_ELEMENT["img_rowid6"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image6"]) ? $_CONTROL_ELEMENT["image6"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title6"]) ? $_CONTROL_ELEMENT["img_title6"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid7"]) ? $_CONTROL_ELEMENT["img_rowid7"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image7"]) ? $_CONTROL_ELEMENT["image7"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title7"]) ? $_CONTROL_ELEMENT["img_title7"] : ''; ?>
				</div>
			</div>
			<div class="medium-3 columns">
				<?php echo isset($_CONTROL_ELEMENT["img_rowid8"]) ? $_CONTROL_ELEMENT["img_rowid8"] : ''; ?>
				<?php echo isset($_CONTROL_ELEMENT["image8"]) ? $_CONTROL_ELEMENT["image8"] : ''; ?>
				<div class="panel">
					<?php echo isset($_CONTROL_ELEMENT["img_title8"]) ? $_CONTROL_ELEMENT["img_title8"] : ''; ?>
				</div>
			</div>
		</div>
	</div>
</form>