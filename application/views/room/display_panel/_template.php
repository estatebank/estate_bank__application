<?php 
	$_BILD_IMG_PATH = base_url() . 'building/get_building_image/';
	$_ROOM_IMG_PATH = base_url() . 'room/get_room_image/';
	$_BILD_DEF_IMG_PATH = "none";
	$_ROOM_DEF_IMG_PATH = "none"; //base_url() . 'room/get_room_image/-1';
?>
	<div class="row">
		<div class="medium-8 columns panel content">
			<h4><?php echo isset($disp_room_name) ? $disp_room_name : ''; ?></h4>
			<hr/>
			<div class="panel">
				<p style="min-height:3.5em;"><?php echo isset($description) ? $description : ''; ?></p>
				<hr>
				<h5>Contact Owner :</h5>
				<?php echo isset($disp_owner_tel) ? '<span class="button success">' .$disp_owner_tel . '</span>' : ''; ?>
			</div>
		</div>
		<div id="div_building_image" class="medium-4 panel columns">
			<div class="cls-image-container" style="background-image:<?php echo isset($disp_b_img_rowid) ? "url(" . $_BILD_IMG_PATH .  $disp_b_img_rowid . ")" : $_BILD_DEF_IMG_PATH; ?>;" alt="<?php echo isset($disp_b_img_title) ? $disp_b_img_title : ''; ?>"></div>
		</div>
		<div class="medium-12 panel columns">
			<b>INFORMATION</b>
			<table class="cls-room-details">
				<tbody>
					<tr>
						<th><span>ROOM NO.</span></th>
						<td><?php echo isset($room) ? $room : ''; ?></td>
						<th><span>TYPE</span></th>
						<td><?php echo isset($disp_room_type) ? $disp_room_type : ''; ?></td>
					</tr>
					<tr>
						<th><span>BED ROOM</span></th>
						<td><?php echo isset($bed_room) ? $bed_room : ''; ?></td>
						<th><span>BATH ROOM</span></th>
						<td><?php echo isset($bath_room) ? $bath_room : ''; ?></td>
					</tr>
					<tr>
						<th><span>FLOOR</span></th>
						<td><?php echo isset($floor) ? $floor : ''; ?></td>
						<th><span>AREA</span></th>
						<td><?php echo isset($area_m2) ? $area_m2 : ''; ?> m<sup>2</sup></td>
					</tr>
					<tr>
						<th><span>RENT</span></th>
						<td><?php echo isset($disp_rent_price) ? $disp_rent_price : ''; ?></td>
						<th><span>DEPOSIT</span></th>
						<td><?php echo isset($disp_deposit_price) ? $disp_deposit_price : ''; ?></td>
					</tr>
				</tbody>
			</table>
			<hr/>
			<b>FACILITIES / SERVICES</b>
			<br>
		<?php if (isset($is_cleaner) && ($is_cleaner > 0)): ?>
			<img src="public/images/icons/building_services/faci-clean.png" class="cls-fac-srvc-icon" title="Cleaner Services"/>
		<?php endif; ?>
		<?php if (isset($nrfc_super_rowid) && ($nrfc_super_rowid > 0)): ?>
			<img src="public/images/icons/building_services/faci-conv.png" class="cls-fac-srvc-icon" title="Convenience Stores"/>
		<?php endif; ?>
		<?php if (isset($is_gym) && ($is_gym > 0)): ?>
			<img src="public/images/icons/building_services/faci-gym.png" class="cls-fac-srvc-icon" title="Gym Services"/>
		<?php endif; ?>
		<?php if (isset($srvc_laundry_rowid) && ($srvc_laundry_rowid > 0)): ?>
			<img src="public/images/icons/building_services/faci-laundry.png" class="cls-fac-srvc-icon" title="Laundry Services"/>
		<?php endif; ?>
		<?php if (isset($is_pet) && ($is_pet > 0)): ?>
			<img src="public/images/icons/building_services/faci-pets.png" class="cls-fac-srvc-icon" title="Pet Allowed"/>
		<?php endif; ?>
		<?php if (isset($is_playground) && ($is_playground > 0)): ?>
			<img src="public/images/icons/building_services/faci-play.png" class="cls-fac-srvc-icon" title="Children Playground"/>
		<?php endif; ?>
		<?php if (isset($is_pool) && ($is_pool > 0)): ?>
			<img src="public/images/icons/building_services/faci-pool.png" class="cls-fac-srvc-icon" title="Swimming Pool"/>
		<?php endif; ?>
		<?php if (isset($is_sauna) && ($is_sauna > 0)): ?>
			<img src="public/images/icons/building_services/faci-sauna.png" class="cls-fac-srvc-icon" title="Sauna Services"/>
		<?php endif; ?>
		<?php if (isset($is_shuttle) && ($is_shuttle > 0)): ?>
			<img src="public/images/icons/building_services/faci-shuttle.png" class="cls-fac-srvc-icon" title="Shuttle Bus Services"/>
		<?php endif; ?>
			</dl>
		</div>
		<hr/>
		<!-- Thumbnails -->
		<div id="div_room_images" class="row">
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid1) ? "url(" . $_ROOM_IMG_PATH . $img_rowid1 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title1) ? $img_title1 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title1) ? $img_title1 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid2) ? "url(" . $_ROOM_IMG_PATH . $img_rowid2 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title2) ? $img_title2 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title2) ? $img_title2 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid3) ? "url(" . $_ROOM_IMG_PATH . $img_rowid3 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title3) ? $img_title3 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title3) ? $img_title3 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid4) ? "url(" . $_ROOM_IMG_PATH . $img_rowid4 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title4) ? $img_title4 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title4) ? $img_title4 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid5) ? "url(" . $_ROOM_IMG_PATH . $img_rowid5 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title5) ? $img_title5 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title5) ? $img_title5 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid6) ? "url(" . $_ROOM_IMG_PATH . $img_rowid6 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title6) ? $img_title6 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title6) ? $img_title6 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid7) ? "url(" . $_ROOM_IMG_PATH . $img_rowid7 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title7) ? $img_title7 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title7) ? $img_title7 : ''; ?></p>
				</div>
			</div>
			<div class="medium-3 columns">
				<div class="cls-image-container" style="background-image:<?php echo isset($img_rowid8) ? "url(" . $_ROOM_IMG_PATH . $img_rowid8 . ")" : $_ROOM_DEF_IMG_PATH; ?>;" alt="<?php echo isset($img_title8) ? $img_title8 : ''; ?>"></div>
				<div class="panel">
					<p><?php echo isset($img_title8) ? $img_title8 : ''; ?></p>
				</div>
			</div>
		</div>
	</div>
