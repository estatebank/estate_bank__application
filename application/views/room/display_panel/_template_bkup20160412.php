<style>
	.cls-bld-srvc-icon {width:60px;margin:0px 1px;}
</style>
<div class="items-row" style="padding:2%;margin-bottom:20px;max-height:600px;">
	<div class="panel" style="width:70%;float:left;max-height:100%;">
		<p><?php echo isset($name_en)?$name_en:''; ?></p>
		<div class="cls-div-contents">
		<?php
			//echo isset($content)?$content:'';
			if (isset($link_article_id) && (is_numeric($link_article_id))) {
				echo '<iframe style="border:0px;width:100%;height:500px;" src="' . APPNAME . '/index.php?option=com_content&view=article&tmpl=component&id='.(int)$link_article_id.'"></iframe>';
			}
			//index.php?option=com_content&view=form&layout=edit&id=1
		?>
		</div>
		<div>{sub table rooms}</div>
	</div>
	<div class="panel" style="width:25%;float:right;max-height:100%;">
		<dl style="height:55%;max-height:55%;">
			<dt>BUILDING</dt>
			<dd><?php echo isset($name_en)?$name_en:''; ?></dd>
			<dt>LOCATION</dt>
			<dd><?php echo isset($address)?$address:''; ?></dd>
			<dt>YEAR OPENED</dt>
			<dd><?php echo (isset($year) && (! empty($year)))?($year . ' ( ' . ((int)(new DateTime())->format('Y') - ((int)$year)) . ' year(s) ago )'):''; ?></dd>
			<!--dt>NEAREST STATION</dt>
			<dd>BTS-Asoke (10 mins)</dd>
			<dt>NEAREST SUPERMARKET</dt>
			<dd>Tops Supermarket</dd-->
			<?php
				if (isset($details) && (! empty($details))) {
					$_dtl = json_decode($details);
					$_serviceIcons = '';
					$_ICON_PATH = base_url() . 'public/images/icons/building_services/';
					foreach ($_dtl AS $_obj) {
						if (! is_array($_obj)) {
							switch (strtolower($_obj)) {
								case 'child':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'pg-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'cleaning':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'cl-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'coin_laundry':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'ld-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'gym':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'gm-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'minimart':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'cv-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'pet':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'pt-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'pool':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'pl-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'japanese_service':
									$_serviceIcons .= '';
									break;
								case 'sauna':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'sn-01.png" class="cls-bld-srvc-icon">';
									break;
								case 'shuttle':
									$_serviceIcons .= '<img src="' . $_ICON_PATH . 'sh-01.png" class="cls-bld-srvc-icon">';
									break;
							}
						}
					}
					if (! empty(trim($_serviceIcons))) {
						echo <<<SRVC
			<dt>FACILITIES / SERVICES</dt>
			<dd>
				<div class="cls-bld-srvcs">$_serviceIcons</div>
			</dd>
			
SRVC;

						
					}
				}
			?>
		</dl>
		<hr>
		<div id="disp_map_<?php echo $rowid; ?>" style="width:100%;height:30%;margin-left:-10px;padding:0px 10px;"></div>
	</div>
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/leaflet/0.7.3/leaflet.css" charset="utf-8" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>public/js/leaflet/0.7.3/leaflet.js" charset="utf-8"></script>
	<script>
		_latLng = L.latLng(<?php echo isset($lat)?(float)$lat:0; ?>, <?php echo isset($lng)?(float)$lng:0; ?>);
		L.map("disp_map_<?php echo $rowid; ?>", {
			center: _latLng
			,minZoom: 12
			,maxZoom: 16
			,zoom: 15
			,tap: false
			,touchZoom: false
			,closePopupOnClick: false
		})
			.addLayer(new L.TileLayer('http://c.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 17}))
			.addLayer(L.marker(_latLng, {icon: new L.Icon({iconUrl: '<?php echo base_url(); ?>public/images/marker-red.png', iconSize: [21, 25], iconAnchor: [10, 25]})}))
			.on("contextmenu", function (e) {
				alert("Context menu disabled");
			})
		;
	</script>
</div>