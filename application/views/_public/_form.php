<?php
	$_index = isset($index)?$index:-1;
?>
<ul class="ul-vldr-error-msg" index="<?php echo $_index; ?>" ></ul>
<form id="frm_edit" controller="<?php echo isset($crud_controller)?$crud_controller:$this->uri->rsegment(1); ?>" class="cls-frm-edit" index="<?php echo $_index; ?>" enctype="multipart/form-data">
	<table id="tbl_edit" class="rounded-corner cls-tbl-edit" autofocus > <!-- to prevent focus on first input that cause problem when have scroll bar (back to top after blur lower elements) -->
	<thead>
		<tr>
			<th class="rounded-top-left" style="width:30%"></th>
			<th style="width:30%">&nbsp;</th>
			<th class="rounded-top-right" style="width:40%"></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="2" class="rounded-foot-left"></td>
			<td class="rounded-foot-right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
<?php
	if (isset($_CONTROLLER_GENERATED_CONTENTS)) {
		echo $_CONTROLLER_GENERATED_CONTENTS;
	}
?>
	</tbody>
	</table>
</form>
<br style="clear:both" />
<div id="sublist"><?php echo isset($sublist)?$sublist:''; ?></div>
