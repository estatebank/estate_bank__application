<?php
	$_index = isset($index)?$index:-1
?>
<div id="divSubListInfo" class="cls-div-info cls-div-sub-info" index="<?php echo $_index; ?>" ><?php echo isset($info)?$info:''; ?></div>
<div id="divSubList" class="cls-div-list cls-div-sub-list" controller="<?php echo isset($crud_controller)?$crud_controller:$this->uri->rsegment(1); ?>" index="<?php echo $_index; ?>" master_cols="<?php echo isset($master_cols)?$master_cols:''; ?>" map_cols="<?php echo isset($map_cols)?$map_cols:''; ?>" >
	<table id="tblSubList" class="cls-tbl-list cls-tbl-sub-list"></table>
</div>
<?php if (isset($edit_dlg['template'])): ?>
<div id="<?php echo $_CONTROLLER_GENERATED__EDIT_DLG_ID; ?>" class="cls-div-form-edit-dialog" index="<?php echo $_index; ?>">
	<?php echo $edit_dlg['template']; ?>
</div>
<?php endif; ?>
