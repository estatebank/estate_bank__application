<form id="frmSearch" controller="<?php echo $this->uri->rsegment(1); ?>" id="frmSearch" action="post">
	<table id="tblSearchPanel" class="rounded-corner cls-tbl-search">
	<thead>
		<tr>
			<th class="rounded-top-left" style="height:24px;"></th>
			<th></th>
			<th class="rounded-top-right"></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
		<td colspan="2" class="rounded-foot-left"></td>
		<td class="rounded-foot-right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
<?php
	if (isset($_CONTROLLER_GENERATED_CONTENTS)) {
		echo $_CONTROLLER_GENERATED_CONTENTS;
	}
?>
		<tr>
			<td colspan="3" style="height:20px;"></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;"><a id="btnSearch" class="clsFormButton">SEARCH</a> <a id="btnReset" class="clsFormButton">RESET</a></td>
		</tr>
	</tbody>
	</table>
</form>
<br style="clear:both" />
