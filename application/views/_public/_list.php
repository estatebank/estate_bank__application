<?php $_index = isset($index)?$index:-1; ?>
<div id="divInfo" class="cls-div-info" index="<?php echo $_index; ?>" ><?php echo isset($info)?$info:''; ?></div>
<div class="cls-list-container">
	<ul class="ul-vldr-error-msg" index="<?php echo $_index; ?>" ></ul>
<?php if (isset($div_title)) : ?>
	<div id="div_list_title"><?php echo $div_title; ?></div>
<?php endif; ?>
	<div id="divDisplayQueryResult" class="cls-div-list" index="<?php echo $_index; ?>" >
		<table id="tblSearchResult" class="cls-tbl-list"></table>
	</div>
</div>
<?php if (isset($edit_dlg) && is_array($edit_dlg) && isset($edit_dlg['template'])): ?>
<div id="<?php echo $_CONTROLLER_GENERATED__EDIT_DLG_ID; ?>" class="cls-div-form-edit-dialog" index="<?php echo $_index; ?>">
	<?php echo $edit_dlg['template']; ?>
</div>
<?php endif; ?>
<div id="divSelectableFields" class="cls-div-select-list-fields" index="<?php echo $_index; ?>" >
<?php echo isset($_CONTROLLER_GENERATED__SELECT_ELEM)?$_CONTROLLER_GENERATED__SELECT_ELEM:''; ?>
</div>
