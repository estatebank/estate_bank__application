<!DOCTYPE html>
<html>
	<head>
        <?php echo '<base href="' . base_url() . '" />' . "\n"; ?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Last-Modified" content="Fri, 13 Feb 2004 09:49:40 GMT">
		<meta http-equiv="Expires" content="Fri, 13 Feb 2004 09:49:40 GMT">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;"/>
        <title><?php echo isset($title)?$title:DEFAULT_TITLE; ?></title>
		<link rel="shortcut icon" href="<?php echo APPNAME; ?>/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="<?php echo APPNAME; ?>/favicon.ico" type="image/x-icon">
		<?php echo isset($_scripts_include)?$_scripts_include:''; ?>
 	</head>
	<body>
		<div id="content">
			<div id="top_wrapper">
				<div id="disp_message">
					<div class="cls-div-info" index="0" ></div>
					<ul class="ul-vldr-error-msg" index="0"></ul>
				</div>
			</div>
			<div id="mid_wrapper">
				<div id="middle_panel"><?php echo isset($middle_panel)?$middle_panel:'';?></div>
			</div>
			<div id="bot_wrapper">
				<div id="bottom_panel"><?php echo isset($bottom_panel)?$bottom_panel:'';?></div>
			</div>
		</div>
<?php echo isset($template_add_html)?$template_add_html:''; ?>
		<div id="dialog-modal" title=""></div>
	</body>
</html>