<?php
	$_index = isset($index)?$index:0;
	$_strDtColumns = '';
	$_strDtDisplay = '';
	$_strSelectElem = '';
	$_strEditDialogID = 'divFormEditDialog';
	if (isset($dataview_fields)) {
		if (is_array($dataview_fields)) {
			$_arr = array();
			$_count = 1;
			foreach ($dataview_fields as $_id => $_obj) {
				$_lbl = '';
				if (array_key_exists("form_edit", $_obj)) {
					$_lbl = (array_key_exists("label", $_obj["form_edit"]))?$_obj["form_edit"]["label"]:'';
				} else if (array_key_exists("list_item", $_obj)) {
					$_lbl = (array_key_exists("label", $_obj["list_item"]))?$_obj["list_item"]["label"]:'';
				}
				$_strDtColumns .= '["' . $_id . '","' . $_lbl . '"],';
				if (array_key_exists("list_item", $_obj)) {
					$_str1 = '';
					$_title = (array_key_exists("label", $_obj["list_item"]))?$_obj["list_item"]["label"]:'';
					$_str1 .= '{"sTitle":"' . $_title . '", "mData":"' . $_id . '"';
					if (array_key_exists('width', $_obj["list_item"])) $_str1 .= ',"sWidth":"' . $_obj["list_item"]['width'] . '"';
					if (array_key_exists('class', $_obj["list_item"])) {
						$_class = strtolower($_obj["list_item"]['class']);
						if (strpos($_class, 'default_number') !== FALSE) {
							$_str1 .= ',"mRender": function(data, type, full) { if ($.isFunction(_fncDTmRenderFormat)) { return _fncDTmRenderFormat(data, type, full, "default_number"); } else { return data } }';
							$_class = trim(str_replace('default_number', 'right', $_class));
						} else if (strpos($_class, 'default_integer') !== FALSE) {
							$_str1 .= ',"mRender": function(data, type, full) { if ($.isFunction(_fncDTmRenderFormat)) { return _fncDTmRenderFormat(data, type, full, "default_integer"); } else { return data } }';
							$_class = trim(str_replace('default_integer', 'right', $_class));
						}
						$_str1 .= ',"sClass":"' . $_class . '"';
					}
					if (array_key_exists('default', $_obj["list_item"]) && ((boolean)($_obj["list_item"]["default"]) == FALSE)) $_str1 .= ',"bVisible":false';
					$_str1 .= '},';
					
					$_str = '';
					if (array_key_exists("selectable", $_obj["list_item"]) && ((boolean)($_obj["list_item"]["selectable"]) == TRUE)) {
						$_str .= '<li><input type="checkbox" id="li_col_XXXX" ';
						if (array_key_exists('default', $_obj["list_item"]) && ((boolean)($_obj["list_item"]["default"]) == FALSE)) $_str .= 'checked';
						$_str .= ' />' . $_title . '</li>';							
					}
					
					if (array_key_exists("order", $_obj["list_item"]) && (! array_key_exists($_obj["list_item"]["order"], $_arr))) {
						$_arr[$_obj["list_item"]["order"]] = array($_str, $_str1);
					} else {
						$_arr[90 + $_count] = array($_str, $_str1);
						$_count++;
					}
				}
			}
			if (isset($custom_columns)) {
				if (is_array($custom_columns)) {
					foreach ($custom_columns as $_col) {
						if (array_key_exists("order", $_col) && (! array_key_exists($_col["order"], $_arr))) {
							$_str1 = $_col["column"];
							if (substr(trim($_str1), -1, 1) != ',') $_str1 = trim($_str1) . ',';
							$_arr[$_col["order"]] = array('', $_str1);
						} else {
							$_arr[90 + $_count] = array('', $_str1);
							$_count++;
						}
					}
				} else { //string
					if ((trim($custom_columns) != '')) {
						if (substr(trim($custom_columns), -1, 1) != ',') $custom_columns = trim($custom_columns) . ',';
						$_arr[90 + $_count] = array('', $custom_columns);
					}
				}
			}
			ksort($_arr);
			$_i = 0;
			foreach ($_arr as $_elems) {
				$_strSelectElem .= str_replace("XXXX", $_i, $_elems[0]);
				$_strDtDisplay .= $_elems[1];
				$_i ++;
			}
			
			if (strlen($_strDtColumns) > 0) $_strDtColumns = substr($_strDtColumns, 0, -1);
			if (strlen($_strDtDisplay) > 0) {
				if (!(isset($list_viewable) && ($list_viewable == FALSE))) $_strDtDisplay .= '{"sTitle":"เรียกดู", "sWidth":"50","sClass": "center","mData": function() { return \'<img class="list-row-button" command="view" src="./public/images/b_view.png" alt="view" title="\' + MSG_ICON_TITLE_VIEW + \'" />\';},"bSortable":false},';
				if (!(isset($list_editable) && ($list_editable == FALSE))) $_strDtDisplay .= '{"sTitle":"แก้ไข", "sWidth":"50","sClass": "center","mData": function() { return \'<img class="list-row-button" command="edit" src="./public/images/b_edit.png" alt="edit" title="\' + MSG_ICON_TITLE_EDIT + \'" />\';},"bSortable":false},';
				if (isset($list_cancelable) && ($list_cancelable == TRUE)) $_strDtDisplay .= '{"sTitle":"ยกเลิก","sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { if (full.is_cancel == 1) { return ""; } else { return \'<img class="list-row-button" command="cancel" src="./public/images/details_close.png" alt="cancel" title="\' + MSG_ICON_TITLE_CANCEL + \'" />\'; } },"bSortable":false}';
				if (!(isset($list_deleteable) && ($list_deleteable == FALSE))) $_strDtDisplay .= '{"sTitle":"ลบ", "sWidth":"50","sClass":"center","mData": function() { return \'<img class="list-row-button" command="delete" src="./public/images/b_delete.png" alt="delete" title="\' + MSG_ICON_TITLE_DELETE + \'" />\';},"bSortable":false},';
				
				if (substr(trim($_strDtDisplay), -1, 1) == ',') $_strDtDisplay = substr(trim($_strDtDisplay), 0, -1);				
				if (substr(trim($_strDtColumns), -1, 1) == ',') $_strDtColumns = substr(trim($_strDtColumns), 0, -1);				
			}
			$_strSelectElem = '<ul>' . $_strSelectElem . '</ul>';
		}
	}
	$_strTableToolButtons = '';
	if (!(isset($list_select_columns) && ($list_select_columns == FALSE))) {
		$_strTableToolButtons .= <<<TBLT
						{
							"sExtends": "text",
							"sButtonText": "เลือกแสดงข้อมูล",
							"sButtonClass": "cls_button_select",
							"fnClick": function ( nButton, oConfig, oFlash ) {
								doSelectDisplayFields();
							}
						},
TBLT;
	}
	
	$_strTableToolButtons .= <<<TBLT
						{
							"sExtends": "copy",
							"sButtonText": "คัดลอก",
							"bShowAll": true,
							"bHeader": true,
							"bFooter": false,
							"mColumns": "visible",
							"fnCellRender": function ( sValue, iColumn, nTr, iDataIndex ) {
								if (sValue.length > 4) if (sValue.substr(0, 4) == "<img") return '';
								return sValue;
							}
						}
						, {
							"sExtends": "print",
							"sButtonText": "พิมพ์",
							"bShowAll": true,
							"bHeader": true,
							"bFooter": false,
							//"sInfo": "Please press escape when done",
							"mColumns": "visible",
							"fnClick": function (nButton, oConfig, oFlash) {
								_blnLeft = false;
								if ($('#left_panel').css('display') !== 'none') {
									_blnLeft = true;
									if (typeof doToggleLeftPanel == 'function') doToggleLeftPanel();
								}
								if (typeof _visibleButtonColumns == 'function') _visibleButtonColumns(false);
								$(window).keyup(function() {
									if (typeof _visibleButtonColumns == 'function') _visibleButtonColumns(true);
									if (_blnLeft && (typeof doToggleLeftPanel == 'function')) doToggleLeftPanel();
								});
								this.fnPrint( true, oConfig );
							}
						}
						, {
							"sExtends": "text",
							"sButtonText": "Excel",
							"sButtonClass": "DTTT_button_xls",
							"bShowAll": true,
							"bHeader": true,
							"bFooter": false,
							"mColumns": "visible",
							"fnClick": function ( nButton, oConfig, oFlash ) {
								doExportExcel( nButton, oConfig, oFlash );
							}
						}
TBLT;
	if (!(isset($list_insertable) && ($list_insertable == FALSE))) {
		$_strTableToolButtons .= <<<TBLT
						, {
							"sExtends": "text"
							, "sButtonText": ""
							, "sButtonClass": "DTTT_button_space"
							
						}
						, {
							"sExtends": "text",
							"sButtonText": "เพิ่ม",
							"sButtonClass": "DTTT_button_add_row",
							"fnClick": function ( nButton, oConfig, oFlash ) {
								doInsert($("#$_strEditDialogID"));
							}
						}
TBLT;
	}
	
	echo <<<EOT
	<script language='javascript'>
		var _arrDtColumns = [$_strDtColumns];
		var _aoColumns = [$_strDtDisplay];
		var _tableToolButtons = [$_strTableToolButtons];
EOT;
	if (isset($edit_dlg) && is_array($edit_dlg)) {
		echo "\n\t\t\$(function() {\n";
		if (isset($edit_dlg['option']) || isset($edit_dlg['options'])) {
			$_strJsonOpts = '';
			$_arrOptions;
			if (isset($edit_dlg['option'])) {
				$_arrOptions = $edit_dlg['option'];
			} else if (isset($edit_dlg['options'])) {
				$_arrOptions = $edit_dlg['options'];
			}
			if (is_array($_arrOptions)) {
				foreach ($_arrOptions as $_key=>$_val) {
					$_strJsonOpts .= $_key . ':' . $_val . ",";
				}
				$_strJsonOpts = '{'.substr($_strJsonOpts, 0, -1).'}';
			}
			echo "\t\t\t\$('#" . $_strEditDialogID . "').dialog('option', " . $_strJsonOpts . ");\n";
		}
		if (isset($edit_dlg['close_tool']) && ($edit_dlg['close_tool'] == TRUE)) {
			echo "\t\t\t\$('#" . $_strEditDialogID . "').parents('.ui-dialog').find('.ui-dialog-titlebar-close').css('visibility', 'visible');\n";
		} else {
			echo "\t\t\t\$('#" . $_strEditDialogID . "').parents('.ui-dialog').find('.ui-dialog-titlebar-close').css('visibility', 'hidden');\n";			
		}
		$_arr_default_buttons = array();
		$_arr_default_buttons['submit'] = <<<'SUBMIT'
			{
				text:"บันทึก",
				class:"cls-btn-form-submit",
				icons: { primary: "ui-icon-disk"},
				click:function() {
					_clickSubmit(this);
				}
			}
SUBMIT;
		$_arr_default_buttons['reset'] = <<<'RESET'
			{
				text:"ค่าเริ่มต้น",
				class:"cls-btn-form-reset",
				icons: { primary: "ui-icon-refresh"},
				click:function() {
					_clickReset(this);
				}
			}
RESET;
		$_arr_default_buttons['cancel'] = <<<'CANCEL'
			{
				text:"ยกเลิก",
				class:"cls-btn-form-cancel",
				icons: { primary: "ui-icon-extlink"},
				click:function() {
					_clickCancel(this);
				}
			}
CANCEL;

		if (isset($edit_dlg['buttons']) && is_array($edit_dlg['buttons'])) {
			if (isset($edit_dlg['buttons']['submitable']) && ($edit_dlg['buttons']['submitable'] == FALSE)) $_arr_default_buttons['submit'] = '';
			if (isset($edit_dlg['buttons']['resetable']) && ($edit_dlg['buttons']['resetable'] == FALSE)) $_arr_default_buttons['reset'] = '';
			if (isset($edit_dlg['buttons']['cancelable']) && ($edit_dlg['buttons']['cancelable'] == FALSE)) $_arr_default_buttons['cancel'] = ''; 
		}
		
		$_strButtons = (strlen($_arr_default_buttons['submit'])>0)?$_arr_default_buttons['submit'].','."\n":'';
		$_strButtons .= (strlen($_arr_default_buttons['reset'])>0)?$_arr_default_buttons['reset'].','."\n":'';
		$_strButtons .= (strlen($_arr_default_buttons['cancel'])>0)?$_arr_default_buttons['cancel'].','."\n":'';
		if (strlen($_strButtons) > 0) {
			$_strButtons = "[\n" . substr($_strButtons,0,-1) . "]\n";
			echo "\t\t\t\$('#" . $_strEditDialogID . "').dialog('option', 'buttons', " . $_strButtons . ");\n";
		}
		echo "\t\t});\n";
	}
	echo "\t</script>\n";
?>
<div id="divInfo" class="cls-div-info" index="<?php echo $_index; ?>" ><?php echo isset($info)?$info:''; ?></div>
<div class="cls-list-container">
	<ul class="ul-vldr-error-msg" index="<?php echo $_index; ?>" ></ul>
<?php if (isset($div_title)) : ?>
	<div id="div_list_title"><?php echo $div_title; ?></div>
<?php endif; ?>
	<div id="divDisplayQueryResult" class="cls-div-list" index="<?php echo $_index; ?>" >
		<table id="tblSearchResult" class="cls-tbl-list"></table>
	</div>
</div>
<?php if (isset($edit_dlg) && is_array($edit_dlg) && isset($edit_dlg['template'])): ?>
<div id="<?php echo $_strEditDialogID; ?>" class="cls-div-form-edit-dialog" index="<?php echo $_index; ?>">
	<?php echo $edit_dlg['template']; ?>
</div>
<?php endif; ?>
<div id="dialog-modal" title="">
</div>
<div id="divSelectableFields" class="cls-div-select-list-fields" index="<?php echo $_index; ?>" >
<?php
	echo $_strSelectElem;
?>
</div>
