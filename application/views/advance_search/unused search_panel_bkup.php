<form id="frmSearch" controller="advance_search" action="post">
	<table id="tblSearchPanel" class="rounded-corner cls-tbl-search">
	<thead>
		<tr>
			<th class="rounded-top-left" style="height:24px;"></th>
			<th></th>
			<th class="rounded-top-right"></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
		<td colspan="2" class="rounded-foot-left"></td>
		<td class="rounded-foot-right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td colspan="3" class="td-align-center">
				<div class="cls-row">
					<div class="cls-row-value" style="width:40%;text-align:center;">
						<input type="radio" id="rdo-price_type_1" value="1" name="price_type" class="data-container search-param" checked="checked">
						<label class="cls-radio-label" for="rdo-price_type_1">เช่า</label>
						<input type="radio" id="rdo-price_type_2" value="2" name="price_type" class="data-container search-param">
						<label class="cls-radio-label" for="rdo-price_type_2">ซื้อ</label>
					</div>
					<div class="table-title cls-row-title" style="width:20%"><img src="public/images/icons/16/preview.png" id="img_ex_rate_info">หน่วยเงิน</div>
					<div class="table-value cls-row-value" style="width:40%">
						<select id="sel-price_unit" class="data-container search-param">
<?php if (isset($lst_ex_rate) && is_array($lst_ex_rate)): ?>
<?php 	foreach ($lst_ex_rate as $_row): ?>
							<option value="<?php echo $_row['value']; ?>" rate="<?php echo $_row['rate']; ?>"><?php echo $_row['name']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
						</select>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="td-align-center">
				<div class="cls-row">
					<div class="table-title cls-row-title" style="width:20%;">สถานที่</div>
					<div class="table-value cls-row-value" style="width:35%;">
						<select id="sel-search_place" class="data-container search-param">
							<option></option>
							<option value=1>TEST 1</option>
							<option value=2>TEST 2</option>
							<option value=3>TEST 3</option>
							<option value=4>TEST 4</option>
						</select>
					</div>
					<div class="table-title" style="width:10%;text-align:center;">หรือ</div>
					<div class="table-value cls-row-value" style="width:35%;">
						<select id="sel-search_area" class="data-container search-param">
							<option></option>
							<option value=1>TEST 1</option>
							<option value=2>TEST 2</option>
							<option value=3>TEST 3</option>
							<option value=4>TEST 4</option>
						</select>
					</div>
				</div>
				<div class="cls-row">
					<div class="table-title cls-row-title" style="width:10%;">งบ</div>
					<div class="table-value cls-row-value" style="width:40%;">
						<select id="sel-price_start" style="float:right;width:95%;" class="data-container search-param">
							<option first="first" value="-1">ไม่จำกัดขั้นต่ำ</option>
						</select>
					</div>
					<div class="table-title" style="width:10%;text-align:center;">～</div>
					<div class="table-value cls-row-value" style="width:40%;">
						<select id="sel-price_limit" style="width:95%;" class="data-container search-param">
							<!--option first="first" value="">&nbsp;</option-->
							<option last="last" value="-1">ไม่จำกัด</option>
						</select>
					</div>
				</div>
				<div class="cls-row">
					<div class="table-title cls-row-title" style="width:25%">ชื่อคอนโด</div>
					<div class="table-value cls-row-value" style="width:75%">
						<input type="text" id="aac-search_name" value="" placeholder="（เว้นได้）" class="data-container search-param">
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height:20px;"></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;"><a id="btnSearch" class="clsFormButton">ค้นหา</a><a id="btnReset" class="clsFormButton">ล้าง</a></td>
		</tr>
	</tbody>
	</table>
</form>