<h4>ADVANCE SEARCH</h4>
<form id="frmSearch" controller="advance_search" action="post">
	<div class="estate_bank_tmpl">
		<div class="items-row cols-2">
			<div class="column-1 control-group">
				<span class="label">SEARCH TYPE</span>
				<div class="ctrl-group-row" style="margin-top:.5em;">
					<input type="radio" id="rdo-price_type_1" value="1" name="price_type" class="cls-radio data-container search-param" checked="checked">
					<label class="cls-radio-label" for="rdo-price_type_1">LEASE/RENT</label>
					<input type="radio" id="rdo-price_type_2" value="2" name="price_type" class="cls-radio data-container search-param">
					<label class="cls-radio-label" for="rdo-price_type_2">PURCHASE</label>
				</div>
			</div>
			<div class="column-2 control-group">
				<span class="label"><img src="public/images/icons/16/preview.png" id="img_ex_rate_info">UNIT</span>
				<select id="sel-price_unit" class="data-container search-param">
<?php if (isset($lst_ex_rate) && is_array($lst_ex_rate)): ?>
<?php 	foreach ($lst_ex_rate as $_row): ?>
					<option value="<?php echo $_row['value']; ?>" rate="<?php echo $_row['rate']; ?>"><?php echo $_row['name']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
				</select>
			</div>
		</div>
		<hr>
		<div class="items-row cols-2">
			<div class="column-1 control-group">
				<span class="label">SELECT NEAREST STATION</span>
				<div class="controlers">
				<select id="sel-search_place" class="data-container search-param">
					<option first="first" value=""> -- </option>
<?php if (isset($lst_landmark) && is_array($lst_landmark)): ?>
<?php 	foreach ($lst_landmark as $_row): ?>
					<option value="<?php echo $_row['rowid']; ?>" class="<?php echo $_row['disp_group_name']; ?>"><?php echo $_row['disp_text']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
				</select>
				</div>
				<span class="text-right">or</span>
				<span class="label">SELECT DISTRICT</span>
				<div class="controlers">
				<select id="sel-search_area" class="data-container search-param">
					<option first="first" value=""> -- </option>
<?php if (isset($lst_area) && is_array($lst_area)): ?>
<?php 	foreach ($lst_area as $_row): ?>
					<option value="<?php echo $_row['rowid']; ?>"><?php echo $_row['disp_text']; ?></option>
<?php 	endforeach; ?>
<?php endif; ?>
				</select>
				</div>
			</div>
			<div class="column-2 control-group">
				<span class="label">MIN. RENT COST</span>
				<select id="sel-price_start" class="data-container search-param">
					<option first="first" value=""> -- </option>
				</select>
				<span class="text-right">&nbsp;</span>
				<span class="label">MAX. RENT COST</span>
				<select id="sel-price_limit" class="data-container search-param">
					<option last="last" value=""> -- </option>
				</select>
			</div>
		</div>
		<hr>
		<div class="items-row cols-2">
			<div class="column-1 control-group">
				<span class="label secondary">BUILDING NAME</span>
				<input type="text" id="aac-search_name" placeholder="( Optional )" class="data-container search-param">
				<ul id="ul_search_info"></ul>
			</div>
			<div class="column-2 control-group">
				<a id="btnSearch" class="clsFormButton expand radius button">SUBMIT</a>
				<a id="btnReset" class="clsFormButton expand radius button">RESET</a>
				<a class="expand radius secondary button">(Advanced Search)</a>
			</div>
		</div>
	</div>
</form>