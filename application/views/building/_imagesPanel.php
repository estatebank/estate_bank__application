<style>
	div.display-upload span.spn-image-select { height:170px;line-height:170px; }
	.cls-image-title { text-align:center; }
	.panel input[type=text] {width:auto !important;}
</style>
<div id="div_room_images" class="row">
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid1"]) ? $_CONTROL_ELEMENT["img_rowid1"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image1"]) ? $_CONTROL_ELEMENT["image1"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title1"]) ? $_CONTROL_ELEMENT["img_title1"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid2"]) ? $_CONTROL_ELEMENT["img_rowid2"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image2"]) ? $_CONTROL_ELEMENT["image2"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title2"]) ? $_CONTROL_ELEMENT["img_title2"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid3"]) ? $_CONTROL_ELEMENT["img_rowid3"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image3"]) ? $_CONTROL_ELEMENT["image3"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title3"]) ? $_CONTROL_ELEMENT["img_title3"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid4"]) ? $_CONTROL_ELEMENT["img_rowid4"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image4"]) ? $_CONTROL_ELEMENT["image4"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title4"]) ? $_CONTROL_ELEMENT["img_title4"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid5"]) ? $_CONTROL_ELEMENT["img_rowid5"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image5"]) ? $_CONTROL_ELEMENT["image5"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title5"]) ? $_CONTROL_ELEMENT["img_title5"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid6"]) ? $_CONTROL_ELEMENT["img_rowid6"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image6"]) ? $_CONTROL_ELEMENT["image6"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title6"]) ? $_CONTROL_ELEMENT["img_title6"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid7"]) ? $_CONTROL_ELEMENT["img_rowid7"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image7"]) ? $_CONTROL_ELEMENT["image7"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title7"]) ? $_CONTROL_ELEMENT["img_title7"] : ''; ?>
		</div>
	</div>
	<div class="medium-3 columns">
		<?php echo isset($_CONTROL_ELEMENT["img_rowid8"]) ? $_CONTROL_ELEMENT["img_rowid8"] : ''; ?>
		<?php echo isset($_CONTROL_ELEMENT["image8"]) ? $_CONTROL_ELEMENT["image8"] : ''; ?>
		<div class="panel">
			<?php echo isset($_CONTROL_ELEMENT["img_title8"]) ? $_CONTROL_ELEMENT["img_title8"] : ''; ?>
		</div>
	</div>
</div>