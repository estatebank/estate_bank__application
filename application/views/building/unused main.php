	<div class="row">
		<div class="large-8 medium-8 columns panel content">
			<h4>Condominium A: Room 1004</h4><!-- t_building.name_en && ": Room " && t_room.room_number -->
			<hr>
			<div class="panel"><!-- t_room.description -->
				<p>The most prominent musical theater in the region was built in 1801 in Trieste and was named the Teatro Giuseppe Verdi in 1901. The city is the site of the Giuseppe Tartini music conservatory. It is also the home base for the internationally acclaimed Trieste Trio chamber music ensemble.</p>
				<hr>
				<h5>Contact Owner :</h5>
				<span class="button success">089-888-9999</span><!-- *ADD* t_room.owner_tel -->
			</div>
		</div>
		<div class="large-4 medium-4 panel columns">
			<img src="img\01-1004-01.jpg"/>
		</div>
			<div class="large-12 panel columns">
				<b>INFORMATION</b>
				<table class="table-room_detail">
					<tbody>
						<tr>
							<th><span>FLOOR</span></th>
							<td>10F</td><!-- t_room.floor -->
						</tr>
						<tr>
							<th><span>TYPE</span></th>
							<td>1LK</td><!-- t_room.room_type_rowid -->
						</tr>
						<tr>
							<th><span>AREA</span></th>
							<td>30m<sup>2</sup><!-- t_room.area_m2 -->
						</td>
						</tr>
						<tr>
							<th><span>RENT</span></th>
							<td>THB15,000 / mo.</td><!-- t_room.rent_price and/or t_room.purchase_price -->
						</tr>
						<tr>
							<th><span>DEPOSIT</span></th>
							<td>THB30,000</td><!-- *ADD* t_room.deposit_price -->
						</tr>					
					</tbody>
				</table>
				</dl>
				<hr/>
				<b>FACILITIES / SERVICES</b>
				<br>
				<img src="img/icon/faci-clean.png" width="50" /><!-- *ADD* t_building.is_cleaner -->
				<img src="img/icon/faci-conv.png" width="50" /><!-- t_building.nrfc_super_rowid -->
				<img src="img/icon/faci-gym.png" width="50" /><!-- t_building.is_gym -->
				<img src="img/icon/faci-laundry.png" width="50" /><!-- t_building.srvc_laundry_rowid -->
				<img src="img/icon/faci-pets.png" width="50" /><!-- t_building.is_pet -->
				<img src="img/icon/faci-play.png" width="50" /><!-- t_building.is_playground -->
				<img src="img/icon/faci-pool.png" width="50" /><!-- t_building.is_pool -->
				<img src="img/icon/faci-sauna.png" width="50" /><!-- t_building.is_sauna -->
				<img src="img/icon/faci-shuttle.png" width="50" /><!-- t_building.is_shuttle -->
				</dl>
			</div>
		<hr/>
		<!-- Thumbnails -->
		<div class="row">
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+1">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			 <div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+2">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+3">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+4">
				<div class="panel">
					<p>Floor Plan</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+5">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+6">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+7">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
			<div class="large-3 small-4 columns">
				<img src="http://placehold.it/400x300?text=Thumbnail+8">
				<div class="panel">
					<p>Interior</p>
				</div>
			</div>
		</div>