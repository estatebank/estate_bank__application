<span class="spn-group-title">รายละเอียดเพิ่มเติม</span>
<div class="cls-building-detail-container">
	<div class="cls-row cls-building-detail">
		<div class="cls-row-title" style="width:20%">Internet Service :</div>
		<div class="cls-row-value" style="width:20%">
			<?php echo isset($_CONTROL_ELEMENT["srvc_internet_rowid"]) ? $_CONTROL_ELEMENT["srvc_internet_rowid"] : ''; ?>
		</div>
		<div class="cls-row-title" style="width:20%">Laundry Service :</div>
		<div class="cls-row-value" style="width:20%">
			<?php echo isset($_CONTROL_ELEMENT["srvc_laundry_rowid"]) ? $_CONTROL_ELEMENT["srvc_laundry_rowid"] : ''; ?>
		</div>
	</div>
	<div class="cls-row cls-building-detail">
		<div class="cls-row-title" style="width:20%">Nearby Facilities :</div>
		<div class="cls-row-value" style="width:20%">
			<?php echo isset($_CONTROL_ELEMENT["nrfc_super_rowid"]) ? $_CONTROL_ELEMENT["nrfc_super_rowid"] : ''; ?>
		</div>
	</div>
	<div class="cls-row" style="height:1em;"></div>
	<div class="cls-row cls-building-detail">
		<input type="checkbox" id="chk-shuttle" data="shuttle" class="user-input data-container" name="detail[]"/>
		<label for="chk-shuttle">Shuttle</label>
		<input type="checkbox" id="chk-pet" data="pet" class="user-input data-container" name="detail[]"/>
		<label for="chk-pet">Pet</label>
		<input type="checkbox" id="chk-child" data="child" class="user-input data-container" name="detail[]"/>
		<label for="chk-child">Child</label>
		<input type="checkbox" id="chk-pool" data="pool" class="user-input data-container" name="detail[]"/>
		<label for="chk-pool">Pool</label>
	</div>
	<div class="cls-row cls-building-detail">
		<input type="checkbox" id="chk-gym" data="gym" class="user-input data-container" name="detail[]"/>
		<label for="chk-gym">Gym</label>
		<input type="checkbox" id="chk-sauna" data="sauna" class="user-input data-container" name="detail[]"/>
		<label for="chk-sauna">Sauna</label>
		<input type="checkbox" id="chk-japanese_service" data="japanese_service" class="user-input data-container" name="detail[]"/>
		<label for="chk-japanese_service">Japanese Service</label>
		<!--input type="checkbox" id="chk-coin_laundry" data="coin_laundry" class="user-input data-container" name="detail[]"/>
		<label for="chk-coin_laundry">Coin Laundry</label-->
	</div>
	<!--div class="cls-building-detail">
		<input type="checkbox" id="chk-" data="" class="user-input data-container"/>
		<label></label>
	</div-->
</div>