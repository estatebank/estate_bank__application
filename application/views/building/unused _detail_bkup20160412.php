<span class="spn-group-title">รายละเอียดเพิ่มเติม</span>
<div class="cls-building-detail-container">
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-minimart" data="minimart" class="user-input data-container" name="detail[]"><label for="chk-minimart">Minimart</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-cleaning" data="cleaning" class="user-input data-container" name="detail[]"><label for="chk-cleaning">Cleaning</label>
	</div>

	<div class="cls-building-detail">
		<input type="checkbox" id="chk-shuttle" data="shuttle" class="user-input data-container" name="detail[]"/>
		<label for="chk-shuttle">Shuttle</label>
	</div>
	<div class="cls-building-detail">
	<input type="checkbox" id="chk-pet" data="pet" class="user-input data-container" name="detail[]"/>
	<label for="chk-pet">Pet</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-child" data="child" class="user-input data-container" name="detail[]"/>
		<label for="chk-child">Child</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-pool" data="pool" class="user-input data-container" name="detail[]"/>
		<label for="chk-pool">Pool</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-gym" data="gym" class="user-input data-container" name="detail[]"/>
		<label for="chk-gym">Gym</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-sauna" data="sauna" class="user-input data-container" name="detail[]"/>
		<label for="chk-sauna">Sauna</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-coin_laundry" data="coin_laundry" class="user-input data-container" name="detail[]"/>
		<label for="chk-coin_laundry">Coin Laundry</label>
	</div>
	<div class="cls-building-detail">
		<input type="checkbox" id="chk-japanese_service" data="japanese_service" class="user-input data-container" name="detail[]"/>
		<label for="chk-japanese_service">Japanese Service</label>
	</div>
	<!--div class="cls-building-detail">
		<input type="checkbox" id="chk-" data="" class="user-input data-container"/>
		<label></label>
	</div-->
</div>