<?php
class Mdl_advance_search extends MY_Model {
	function __construct() {
		//parent::__construct();
	}
	function search__purchase($arrObj = array()) {
		$_arrObj = $arrObj;
		if (! is_array($arrObj)) $_arrObj = array();
		$_arrObj['price_type'] = 2;
		return $this->search_with_ex_rate(array(), $_arrObj);
	}
	function search__lease_rent($arrObj = array()) {
		$_arrObj = $arrObj;
		if (! is_array($arrObj)) $_arrObj = array();
		$_arrObj['price_type'] = 2;
		return $this->search_with_ex_rate(array(), $_arrObj);
	}

	function search_with_ex_rate($arrExRate = array(), $arrObj = array()) {
		$_addWhere = '';
		$_placeQuery = '';
		$_addSelect = ', NULL AS place_distance';
		/* price type */
		$_priceType = 1;
		if (isset($arrObj['price_type'])) {
			if (is_numeric($arrObj['price_type'])) $_priceType = (int) $arrObj['price_type'];
		}
		/* ++ compare with exchange rate */
		$__cRent = '';
		$__cPurchase = '';
		$_compareRent = '';
		$_comparePurchase = '';
		$_where = '';
		$_ratio = 1;
		if (is_array($arrExRate)) {
			foreach ($arrExRate as $order=>$obj) {
				if (is_array($obj) && isset($obj["value"]) && isset($obj["name"]) && isset($obj["rate"]) && is_numeric($obj["rate"])) {
					if (isset($arrObj['price_unit']) && (trim($arrObj['price_unit']) == trim($obj["value"]))) {
						$_ratio = (float) $obj["rate"];
					}
					$__cRent .= "\nWHEN LOWER(t.price_unit) = LOWER('" . trim($obj["value"]) . "') THEN (t.rent_price * " . (float) $obj["rate"] . " )";
					$__cPurchase .= "\nWHEN LOWER(t.price_unit) = LOWER('" . trim($obj["value"]) . "') THEN (t.purchase_price * " . (float) $obj["rate"] . " )";
				}
			}
		} else {
			$__cRent .= "\nWHEN TRUE THEN t.rent_price ";
			$__cPurchase .= "\nWHEN TRUE THEN t.purchase_price ";			
		}
		if (! empty(trim($__cRent))) {
			$_compareRent = ", FORMAT(CASE" . $__cRent . "\nEND, 2) AS compare_rent\n";
			$_compareRent .= ", FORMAT(CASE" . $__cRent . "\nEND, 2) || CASE WHEN t.rent_type_rowid = 1 THEN ' per month' END AS disp_rent\n";
			$_compareRent .= ", FORMAT(CASE WHEN COALESCE(t.area_m2, 0) > 0 THEN (CASE" . $__cRent . "\nEND / t.area_m2) END, 2) AS disp_rent_per_m2\n";
			if ($_priceType == 1) {
				if (isset($arrObj['price_start'])) {
					if (is_numeric($arrObj['price_start'])) {
						$_addWhere .= "\nAND CASE" . $__cRent . "\nEND >= " . ((float) $arrObj['price_start'] * $_ratio);
					}
				}
				if (isset($arrObj['price_limit'])) {
					if (is_numeric($arrObj['price_limit'])) {
						$_addWhere .= "\nAND CASE" . $__cRent . "\nEND <= " . ((float) $arrObj['price_limit'] * $_ratio);
					}
				}
			}
		}
		if (! empty(trim($__cPurchase))) {
			$_comparePurchase = ", FORMAT(CASE" . $__cPurchase . "\nEND, 2) AS compare_purchase\n";
			$_comparePurchase .= ", FORMAT(CASE" . $__cPurchase . "\nEND, 2) || CASE WHEN t.rent_type_rowid = 1 THEN ' per month' END AS disp_purchase\n";
			$_comparePurchase .= ", FORMAT(CASE WHEN COALESCE(t.area_m2, 0) > 0 THEN (CASE" . $__cPurchase . "\nEND / t.area_m2) END, 2) AS disp_purchase_per_m2\n";
			if ($_priceType == 2) {
				if (isset($arrObj['price_start'])) {
					if (is_numeric($arrObj['price_start'])) {
						$_addWhere .= "\nAND CASE" . $__cPurchase . "\nEND >= " . ((float) $arrObj['price_start'] * $_ratio);
					}
				}
				if (isset($arrObj['price_limit'])) {
					if (is_numeric($arrObj['price_limit'])) {
						$_addWhere .= "\nAND CASE" . $__cPurchase . "\nEND <= " . ((float) $arrObj['price_limit'] * $_ratio);
					}
				}
			}
		}
		/* -- compare with exchange rate */
		
		/* ++ search place / search area */
		if (isset($arrObj['search_place'])) {
			$_id = (int) $arrObj['search_place'];
			$_placeQuery =<<<PQR
	INNER JOIN t_landmark l 
		ON l.rowid = {$_id} 
		AND fnc_distanceKm(l.geom_4326, b.map_position) <= 5
PQR;
//		AND ST_Within(b.map_position, ST_GeomFromText(ST_AsText(ST_Buffer(ST_GeomFromText(ST_AsText(l.geom_4326), 32647), 3000)), 4326))
			$_addSelect = ", FORMAT(ST_Distance(ST_GeomFromText(ST_AsText(b.map_position), 32647), ST_GeomFromText(ST_AsText(l.geom_4326), 32647)) / 1000, 3) || ' km.' AS place_distance";
		} else if (isset($arrObj['search_area'])) {
			$_id = (int) $arrObj['search_area'];
			$_placeQuery =<<<PQR
	INNER JOIN m_amphoe_polygon l 
		ON l.amphoe_rowid = {$_id} 
		AND ST_Within(b.map_position, l.geom)

PQR;
		}
		/* -- search place / search area */
		
		if (isset($arrObj['price_type'])) unset($arrObj['price_type']);
		if (isset($arrObj['price_unit'])) unset($arrObj['price_unit']);
		if (isset($arrObj['price_start'])) unset($arrObj['price_start']);
		if (isset($arrObj['price_limit'])) unset($arrObj['price_limit']);
		if (isset($arrObj['search_place'])) unset($arrObj['search_place']);
		if (isset($arrObj['search_area'])) unset($arrObj['search_area']);
//COALESCE(t.room, ' - ') AS disp_room_no,
		$_params = array();
		$_sql = <<<QUERY
SELECT t.rowid, t.building_rowid, t.room_type_rowid, t.code, t.name_en, t.name_th, t.name_jp, t.room, t.floor
, t.bed_room, t.bath_room, t.area_m2, t.info, t.details, t.rent_price, t.rent_type_rowid, t.deposit_price, t.purchase_price, t.price_unit
, t.owner_type_rowid, t.description, t.remark, t.link_article_id, t.is_cancel, t.create_by, t.create_date, t.update_by, t.update_date
, COALESCE(t.name_en, t.name_jp, t.name_th, ' - ') AS disp_name
$_compareRent
$_comparePurchase
, COALESCE(b.b_type_rowid, -1) AS b_type_rowid, COALESCE(b.name_en, b.name_jp, b.name_th, ' - ') AS disp_building_name
, ST_AsText(b.map_position) AS map_position, b.link_article_id AS building_link_article_id
, CASE 
	WHEN b.map_position IS NOT NULL THEN CONCAT('[', ST_X(b.map_position), ',', ST_Y(b.map_position), ']')
	ELSE 'false'
END AS disp_map_position
, CASE 
	WHEN b.map_position IS NOT NULL THEN CONCAT('[', ST_X(b.map_position), ',', ST_Y(b.map_position), ']')
	WHEN ag.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(ag.geom)), ',', ST_Y(ST_Centroid(ag.geom)), ']')
	WHEN pg.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(pg.geom)), ',', ST_Y(ST_Centroid(pg.geom)), ']')
	ELSE 'false'
END AS disp_map_area
, bt.name_en AS b_type_name_en, a.name_th AS city, p.name_th AS province
FROM t_room t
	LEFT OUTER JOIN t_building b ON b.rowid = t.building_rowid
$_placeQuery	
	LEFT OUTER JOIN m_province p ON b.province_rowid = p.rowid 
	LEFT OUTER JOIN m_province_polygon pg ON p.rowid = pg.province_rowid 
	LEFT OUTER JOIN m_amphoe a ON b.city_rowid = a.rowid 
	LEFT OUTER JOIN m_amphoe_polygon ag ON a.rowid = ag.amphoe_rowid 
	LEFT OUTER JOIN m_building_type bt ON b.b_type_rowid = bt.rowid 
WHERE TRUE
$_addWhere

QUERY;
//, b.name_en AS building_name_en, b.name_th AS building_name_th, b.name_en AS building_name_jp

		$_specSearch = array();
		if (isset($arrObj['search_name'])) {
			$_specSearch['search_name'] = array(
				"type"=>"raw"
				,"dbcol"=>"CONCAT(COALESCE(t.name_en, ''), ' ', COALESCE(t.name_th, ''), ' ', COALESCE(t.name_jp, ''), ' ', COALESCE(b.name_en, ''), ' ', COALESCE(b.name_th, ''), ' ', COALESCE(b.name_jp, ''))"
				,"operand"=>"LIKE"
				,"val"=>"CONCAT('%', '" . $this->db->escape_str(trim($arrObj['search_name'])) . "', '%')"
			);
		}
		$_sql .= $this->_getSearchConditionSQL_params($_params, $arrObj, $_specSearch);
//		$_sql .= $this->_getCheckAccessRight("t.create_by", "customer");
		$_sql .= "ORDER BY t.code";
//echo $_sql;exit;
		$_arrResult = $this->arr_execute($_sql, $_params);
		if (is_array($_arrResult) && (count($_arrResult) > 0)) {
			$_arrResult[0]['info_found_location'] = count($_arrResult);
		}
//var_dump($_arrResult);exit;
		return $_arrResult;
	}
	
	function list_stations() {
//, IFNULL(CASE WHEN CHAR_LENGTH(m.name_en) > 7 THEN SUBSTR(m.name_en, 1, 7) ELSE m.name_en END, ' - unknown group - ') AS disp_group_name

		$_sql = <<<QUERY
SELECT t.rowid
, REPLACE(UPPER(t.name_en), ' STATION', '') AS disp_text
, IFNULL(m.name_en, ' - unknown group - ') AS disp_group_name
FROM t_landmark t
	INNER JOIN m_landmark_type m ON m.rowid = t.lm_type_rowid
WHERE t.lm_type_rowid < 7 
AND t.name_en NOT LIKE '%GATE %'
AND t.name_en NOT LIKE '%EXIT %'
ORDER BY m.rowid, t.name_en

QUERY;
		return $this->arr_execute($_sql);
	}
	function list_districts() {
		$_sql = <<<QUERY
SELECT * 
, INITCAP(name_en) AS disp_text
FROM m_amphoe 
WHERE province_rowid = 10 
ORDER by name_en

QUERY;
		return $this->arr_execute($_sql);
	}

}