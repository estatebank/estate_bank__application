<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_test_landmark extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_TABLE_NAME = 't_landmark';
		$this->_AUTO_FIELDS = array(
			'rowid' => ''
		);
		$this->_FIELDS = array(
			'lm_type_rowid' => NULL
			,'object_id' => NULL
			,'name_en' => NULL
			,'name_th' => NULL
			,'name_jp' => NULL
			,'create_by' => NULL
			,'create_date' => NULL
			,'update_by' => NULL
			,'update_date' => NULL
		);
	}
	
	function search($arrObj = array()) {
		$_params = array();
		$_sql = <<<QUERY
SELECT t.*
FROM {$this->_TABLE_NAME} t
WHERE TRUE

QUERY;
		return $this->arr_execute($_sql, $_params);
	}
	
}