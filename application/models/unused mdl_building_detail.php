<?php
class Mdl_building_detail extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_TABLE_NAME = 't_building_detail';
		$this->_AUTO_FIELDS = array(
			'rowid' => ''
		);
		$this->_FIELDS = array(
			'building_rowid' => NULL
			,'remark' => NULL
//			,'is_cancel' => NULL
			,'create_by' => NULL
			,'create_date' => NULL
			,'update_by' => NULL
			,'update_date' => NULL
		);
	}
/*
	function search($arrObj = array()) {
		$_sql = <<<QUERY
SELECT t.* 
, CONCAT(c.name, ' ( ', t.name, ' )') AS full_name
, c.name AS customer_name, c.name_th AS customer_name_th
, CASE WHEN CHAR_LENGTH(t.address) > 50 THEN CONCAT(SUBSTRING(t.address FROM 1 FOR 47), '...') ELSE t.address END AS abbr_address
, a.name_th AS amphoe, p.name_th AS province 
, CONCAT(t.address, COALESCE(' อ. ' || a.name_th, ''), COALESCE(' จ. ' || p.name_th, '')) AS full_address
FROM t_building_detail t 
	INNER JOIN ef_m_customer c ON t.customer_rowid = c.rowid 
	LEFT OUTER JOIN ef_m_amphoe a ON t.amphoe_rowid = a.rowid 
	LEFT OUTER JOIN ef_m_province p ON t.province_rowid = p.rowid 
WHERE 1=1 

QUERY;
		$_sql .= $this->_getSearchConditionSQL($arrObj);
//		$_sql .= $this->_getCheckAccessRight("t.create_by", "customer");
		$_sql .= "ORDER BY t.name, t.name_th";
		return $this->arr_execute($_sql);
	}
*/
}