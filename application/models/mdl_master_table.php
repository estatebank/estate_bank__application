﻿<?php
class Mdl_master_table extends MY_Model {
	function __construct() {
		parent::__construct();
	}

	function list_all($tableName, $orderBy = '', $tablePrefix = 'm_') {
		$_db = $this->db;
		if ($orderBy) {
			$_db = $this->db->order_by($orderBy);
		}
		return $_db->get($tablePrefix . $tableName)->result_array();
	}
	
	function list_where($tableName, $where, $orderBy = '', $tablePrefix = 'm_') {
		$_db = $this->db->where($where); 
		if ($orderBy) {
			$_db = $this->db->order_by($orderBy);
		}
		return $_db->get($tablePrefix . $tableName)->result_array();
	}
}