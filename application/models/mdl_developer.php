<?php
class Mdl_developer extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_TABLE_NAME = 't_developer';
		$this->_AUTO_FIELDS = array(
			'rowid' => ''
		);
		$this->_FIELDS = array(
			'code' => NULL
			,'name_en' => NULL
			,'name_th' => NULL
			,'name_jp' => NULL
			,'description' => NULL
			,'remark' => NULL
//			,'is_cancel' => NULL
			,'create_by' => NULL
			,'create_date' => NULL
			,'update_by' => NULL
			,'update_date' => NULL
		);
	}

}