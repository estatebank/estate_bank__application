<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_building extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_TABLE_NAME = 't_building';
		$this->_AUTO_FIELDS = array(
			'rowid' => ''
		);
		$this->_FIELDS = array(
			'b_type_rowid' => NULL
			,'code' => NULL
			,'name_en' => NULL
			,'name_th' => NULL
			,'name_jp' => NULL
			,'address' => NULL
			,'city_rowid' => NULL
			,'province_rowid' => NULL
			,'postal_code' => NULL
			,'email' => NULL
			,'tel' => NULL
			,'fax' => NULL
			,'description' => NULL
			,'units' => NULL
			,'floors' => NULL
			,'year' => NULL
			,'srvc_internet_rowid' => NULL
			,'srvc_laundry_rowid' => NULL
			,'nrfc_super_rowid' => NULL
			,'is_pet' => NULL
			,'is_pool' => NULL
			,'is_sauna' => NULL
			,'is_gym' => NULL
			,'is_conv_store' => NULL
			,'is_nhk' => NULL
			,'is_jp_srvc' => NULL
			,'is_playground' => NULL
			,'is_shuttle' => NULL
			,'is_cleaner' => NULL
			,'dev_rowid' => NULL
			,'remark' => NULL
/* ++ these columns are unediatable via normal auto generated controls
			,'details' => NULL
			,'map_position' => NULL
			,'owner_rowid' => NULL
			,'link_article_id' => NULL
			,'is_cancel' => NULL
*/
			,'create_by' => NULL
			,'create_date' => NULL
			,'update_by' => NULL
			,'update_date' => NULL
		);
	}
	
/*t.rowid, t.b_type_rowid, t.code, t.name_en, t.name_th, t.name_jp, t.address, t.city_rowid
, t.province_rowid, t.postal_code, t.email, t.tel, t.fax, t.description, t.units, t.floors, t.year
, t.dev_rowid, t.remark, t.details, t.link_article_id, t.is_cancel, t.create_by, t.create_date, t.update_by, t.update_date
*/	
	function search($arrObj = array()) {
		$_params = array();
		$_sql = <<<QUERY
SELECT t.rowid, t.b_type_rowid, t.code, t.name_en, t.name_th, t.name_jp, t.address, t.city_rowid
, t.province_rowid, t.postal_code, t.email, t.tel, t.fax, t.description, t.units, t.floors, t.year
, t.srvc_internet_rowid, t.srvc_laundry_rowid, t.nrfc_super_rowid, t.is_pet, t.is_pool, t.is_sauna
, t.is_gym, t.is_conv_store, t.is_nhk, t.is_jp_srvc, t.is_playground, t.is_shuttle, t.is_cleaner
, t.dev_rowid, t.remark, t.details, t.link_article_id, t.is_cancel, t.create_by, t.create_date, t.update_by, t.update_date
, CASE 
	WHEN t.map_position IS NOT NULL THEN CONCAT('[', ST_X(t.map_position), ',', ST_Y(t.map_position), ']')
	WHEN ag.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(ag.geom)), ',', ST_Y(ST_Centroid(ag.geom)), ']')
	WHEN pg.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(pg.geom)), ',', ST_Y(ST_Centroid(pg.geom)), ']')
	ELSE 'false'
END AS disp_map_position
, CASE 
	WHEN t.map_position IS NOT NULL THEN CONCAT('[', ST_X(t.map_position), ',', ST_Y(t.map_position), ']')
	WHEN ag.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(ag.geom)), ',', ST_Y(ST_Centroid(ag.geom)), ']')
	WHEN pg.geom IS NOT NULL THEN CONCAT('[', ST_X(ST_Centroid(pg.geom)), ',', ST_Y(ST_Centroid(pg.geom)), ']')
	ELSE 'false'
END AS disp_map_area
, bt.name_en AS b_type_name_en
, a.name_en AS city
, p.name_en AS province
, img.img_rowid1, img.img_title1, img.img_rowid1 AS image1
, img.img_rowid2, img.img_title2, img.img_rowid2 AS image2
, img.img_rowid3, img.img_title3, img.img_rowid3 AS image3
, img.img_rowid4, img.img_title4, img.img_rowid4 AS image4
, img.img_rowid5, img.img_title5, img.img_rowid5 AS image5
, img.img_rowid6, img.img_title6, img.img_rowid6 AS image6
, img.img_rowid7, img.img_title7, img.img_rowid7 AS image7
, img.img_rowid8, img.img_title8, img.img_rowid8 AS image8
FROM t_building t
	LEFT OUTER JOIN m_province p ON t.province_rowid = p.rowid 
	LEFT OUTER JOIN m_province_polygon pg ON p.rowid = pg.province_rowid 
	LEFT OUTER JOIN m_amphoe a ON t.city_rowid = a.rowid 
	LEFT OUTER JOIN m_amphoe_polygon ag ON a.rowid = ag.amphoe_rowid 
	LEFT OUTER JOIN m_building_type bt ON t.b_type_rowid = bt.rowid 
	LEFT OUTER JOIN v_building_images img ON img.building_rowid = t.rowid
WHERE COALESCE(t.is_cancel, 0) < 1

QUERY;
		$_specSearch = array();
		if (array_key_exists('building_name', $arrObj)) {
			$_specSearch['building_name'] = array(
				"type"=>"raw"
				,"dbcol"=>"CONCAT(COALESCE(t.name_en, ''), ' ', COALESCE(t.name_th, ''), ' ', COALESCE(t.name_jp, ''))"
				,"operand"=>"LIKE"
				,"val"=>"CONCAT('%', '" . $this->db->escape_str(trim($arrObj['building_name'])) . "', '%')"
			);
		}
		$_sql .= $this->_getSearchConditionSQL($arrObj, $_specSearch);
//		$_sql .= $this->_getCheckAccessRight("t.create_by", "customer");
		$_sql .= "ORDER BY t.name_en";
//echo $_sql;exit;
		return $this->arr_execute($_sql, $_params);
	}
	
	function commit($arrObj, $blnUpdateEmpty = TRUE) { //override
		$this->db->trans_begin();
		$this->error_message = 'Error on function "Mdl_building.commit"';
		try {
			if (array_key_exists('new_details', $arrObj) && (is_array($arrObj['new_details'])) && (count($arrObj['new_details']) > 0)) {
				$this->db->set('details', "'" . json_encode($arrObj['new_details']) . "'", FALSE); //,JSON_NUMERIC_CHECK, JSON_FORCE_OBJECT
			} else {
				$this->db->set('details', 'NULL', FALSE);
			}
			unset($arrObj['details']);
			unset($arrObj['new_details']);

			/* ++ UNUSED:: add joomla article to link_article_rowid
			if ((!isset($arrObj['rowid'])) || empty($arrObj['rowid'])) { //only insert
				$this->load->helper('joomla_content_helper');
				$_result = createNewArticleContent(0, array("title"=>isset($arrObj['name_en'])?$arrObj['name_en']:'-'));
				if (is_array($_result) && (isset($_result['id'])) && (((int)$_result['id']) > 0)) {
					$this->db->set('link_article_id', (int)$_result['id']);
				}
			}
			*/
			$_aff_rows = parent::commit($arrObj, $blnUpdateEmpty);
		} catch (Exception $e) {
			$this->error_message .= '::Catch exception error, ' . $e->getMessage();
			$this->error_number = $e->getCode();
			$this->last_insert_id = -1;
		}
		if (($this->db->trans_status() === FALSE) || ($this->error_message != "")) {
			$this->error_message .= "::DB Transaction rollback";
			$this->db->trans_rollback();
		} else {
			$this->db->trans_complete();
			$this->error_message = '';
		}			
		return $this->error_message;
	}

	function getImageByBuildingImageID($rowid) {
		$_params = array($rowid);
		$_sql = <<<EOT
SELECT t.*
FROM t_building_images t
WHERE t.rowid = ?
-- AND IFNULL(m.is_cancel, 0) < 1
EOT;
		//$_result = $this->db->query($_sql, $_params)->result();
		$_result = $this->arr_execute($_sql, $_params);
//var_dump($_result);exit;
		if (is_array($_result) && (count($_result) > 0)) {
			return $_result[0];
		} else {
			return FALSE;
		}
	}
	
	function getImageByBuildingRowID($rowid) {
		$_params = array($rowid);
		$_sql = <<<EOT
SELECT t.*
FROM v_default_building_images v
	INNER JOIN t_building_images t ON t.rowid = v.rowid 
WHERE v.building_rowid = ?
EOT;
		$_result = $this->arr_execute($_sql, $_params);
		if (is_array($_result) && (count($_result) > 0)) {
			return $_result[0];
		} else {
			return FALSE;
		}		
	}

	function commit_map_position($arrObj) {
		$this->error_message = 'Error on function "Mdl_building.commit_map_position"';
		if (is_array($arrObj) && array_key_exists('rowid', $arrObj) && (trim($arrObj['rowid']) > '0')) {
			$_lat = 0;
			$_lng = 0;
			$_rowid = $this->db->escape((int)$arrObj['rowid']);
			if (array_key_exists('lat', $arrObj)) $_lat = $this->db->escape((float)$arrObj['lat']);
			if (array_key_exists('lng', $arrObj)) $_lng = $this->db->escape((float)$arrObj['lng']);
			
			if (($_lat == 0) && ($_lng == 0)) {
				$this->error_message .= '::Both lat and lng are empty, update canceled.';
			} else {
				$this->last_insert_id = -1;
				$this->db->set('map_position', "ST_GeomFromText('POINT(" . $_lng . " " . $_lat . ")', 4326)", FALSE);
				$this->db->where('rowid', $_rowid);
				$this->db->update($this->_TABLE_NAME);
				if ($this->db->_error_message() == '') {
					$this->error_message = '';
					$this->error_number = 0;
				} else {
					$this->error_message .= $this->db->_error_message();
					$this->error_number = $this->db->_error_number();
				}
			}
		}
		return $this->error_message;		
	}

	function list_map_objects() {
/*
		$_sql = <<<EOT

SELECT geojson AS geo_json
FROM v_geojson_buildings

EOT;
		$_arr = $this->arr_execute($_sql);
var_dump($_arr);exit;
		if (is_array($_arr) && (count($_arr) > 0)) {
			$_arrReturn = json_decode($_arr[0]['geo_json']);
		}
*/
		$_sql = <<<EOT

SELECT each_feature
FROM v_geojson_each_building
WHERE each_feature IS NOT NULL
ORDER BY rowid

EOT;
		$_arrReturn = array(
			"type"=>"FeatureCollection"
		); 
		$_arrFeatures = $this->arr_execute($_sql);
		if (is_array($_arrFeatures)) {
			$_arr = array();
			foreach($_arrFeatures as $_row) {
				$_arr[] = json_decode($_row["each_feature"]);
			}
			$_arrReturn["features"] = $_arr;
		} else {
			$_arrReturn["features"] = array();
		}
		return $_arrReturn;
	}
	
	function get_by_id($rowid) {
		$_params = array($rowid);
		$_sql = <<<EOT

SELECT t.rowid, t.b_type_rowid, t.code, t.name_en, t.name_th, t.name_jp, t.address, t.city_rowid
, t.province_rowid, t.postal_code, t.email, t.tel, t.fax, t.description, t.units, t.floors, t.year
, t.dev_rowid, t.remark, t.details, t.link_article_id, t.is_cancel, t.create_by, t.create_date, t.update_by, t.update_date
, ST_X(t.map_position) AS lng, ST_Y(t.map_position) AS lat
FROM {$this->_TABLE_NAME} t
WHERE t.rowid = ?
AND COALESCE(t.is_cancel, 0) < 1

EOT;
		$_result = $this->arr_execute($_sql, $_params);
		if (is_array($_result) && (count($_result) > 0)) {
			return $_result[0];
		} else {
			return FALSE;
		}
	}
	//override
	function delete($RowID) {
		try {
			$this->db->set('is_cancel', 1);
			$this->db->where(array('rowid' => $this->db->escape((int) $RowID)));
			$this->db->update($this->_TABLE_NAME);
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
			return $this->db->affected_rows();
		} catch (Exception $e) {
			$this->error_message = $e->getMessage();
			$this->error_number = $e->getCode();
		}
	}

}