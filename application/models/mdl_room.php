<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_room extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_TABLE_NAME = 't_room';
		$this->_AUTO_FIELDS = array(
			'rowid' => ''
		);
		$this->_FIELDS = array(
			'building_rowid' => NULL
			,'room_type_rowid' => NULL
			,'code' => NULL
			,'name_en' => NULL
			,'name_th' => NULL
			,'name_jp' => NULL
			,'room' => NULL
			,'floor' => NULL
			,'bed_room' => NULL
			,'bath_room' => NULL
			,'area_m2' => NULL
			,'rent_price' => NULL
			//,'rent_type_rowid' => NULL
			,'deposit_price' => NULL
			,'purchase_price' => NULL
			//,'price_unit' => NULL
			,'owner_rowid' => NULL
			,'owner_type_rowid' => NULL
			,'description' => NULL
			,'remark' => NULL
/* ++ these column not ediatable via normal auto generated controls
			,'info' => NULL
			,'details' => NULL
			,'link_article_id' => NULL
			,'is_cancel' => NULL
*/
			,'create_by' => NULL
			,'create_date' => NULL
			,'update_by' => NULL
			,'update_date' => NULL
		);
	}
	
	function search($arrObj = array()) {
		$_params = array();
		$_sql = <<<QUERY
SELECT t.*
, CONCAT(IFNULL(CONCAT(COALESCE(b.name_en, b.name_jp, b.name_th), ': Room '), ''), COALESCE(t.name_en, t.name_jp, t.name_th, t.code, CONCAT('( rowid ', t.rowid, ' )'))) AS disp_name
, rt.code AS room_type, COALESCE(b.name_en, b.name_jp, b.name_th, b.code, CONCAT('( rowid ', b.rowid, ' )')) AS building_name
, COALESCE(t.name_en, t.name_jp, t.name_th, t.code, CONCAT('( rowid ', t.rowid, ' )')) AS room_name
, img.img_rowid1, img.img_title1, img.img_rowid1 AS image1
, img.img_rowid2, img.img_title2, img.img_rowid2 AS image2
, img.img_rowid3, img.img_title3, img.img_rowid3 AS image3
, img.img_rowid4, img.img_title4, img.img_rowid4 AS image4
, img.img_rowid5, img.img_title5, img.img_rowid5 AS image5
, img.img_rowid6, img.img_title6, img.img_rowid6 AS image6
, img.img_rowid7, img.img_title7, img.img_rowid7 AS image7
, img.img_rowid8, img.img_title8, img.img_rowid8 AS image8
FROM t_room t
	INNER JOIN m_room_type rt ON t.room_type_rowid = rt.rowid 
	LEFT OUTER JOIN t_building b ON b.rowid = t.building_rowid
	LEFT OUTER JOIN v_room_images img ON img.room_rowid = t.rowid
WHERE IFNULL(t.is_cancel, 0) < 1

QUERY;

		$_specSearch = array();
		if (array_key_exists('room_name', $arrObj)) {
			$_specSearch['room_name'] = array(
				"type"=>"raw"
				,"dbcol"=>"CONCAT_WS(t.name_en, ' ', t.name_th, ' ', t.name_jp)"
				,"operand"=>"LIKE"
				,"val"=>"CONCAT('%', '" . $this->db->escape_str(trim($arrObj['room_name'])) . "', '%')"
			);
		}
		$_sql .= $this->_getSearchConditionSQL($arrObj, $_specSearch);
//		$_sql .= $this->_getCheckAccessRight("t.create_by", "customer");
		$_sql .= "ORDER BY t.code";
//echo $_sql;exit;
		return $this->arr_execute($_sql, $_params);
	}
	
	function commit($arrObj, $blnUpdateEmpty = TRUE) { //override
		$this->db->trans_begin();
		$this->error_message = 'Error on function "Mdl_room.commit"';
		try {
			if (array_key_exists('info', $arrObj) && (is_array($arrObj['info'])) && (count($arrObj['info']) > 0)) {
				$this->db->set('info', "'" . json_encode($arrObj['info']) . "'", FALSE); //,JSON_NUMERIC_CHECK, JSON_FORCE_OBJECT
			} else {
				$this->db->set('info', 'NULL', FALSE);
			}
			unset($arrObj['info']);
			
			if (array_key_exists('details', $arrObj) && (is_array($arrObj['details'])) && (count($arrObj['details']) > 0)) {
				$this->db->set('details', "'" . json_encode($arrObj['details']) . "'", FALSE); //,JSON_NUMERIC_CHECK, JSON_FORCE_OBJECT
			} else {
				$this->db->set('details', 'NULL', FALSE);
			}
			unset($arrObj['details']);

			if ((!isset($arrObj['rowid'])) || empty($arrObj['rowid'])) { //only insert
				$this->load->helper('joomla_content_helper');
				$_result = createNewArticleContent(1, array("title"=>isset($arrObj['name_en'])?$arrObj['name_en']:'-'));
				if (is_array($_result) && (isset($_result['id'])) && (((int)$_result['id']) > 0)) {
					$this->db->set('link_article_id', (int)$_result['id']);
				}
			}
			$_aff_rows = parent::commit($arrObj, $blnUpdateEmpty);
		} catch (Exception $e) {
			$this->error_message .= '::Catch exception error, ' . $e->getMessage();
			$this->error_number = $e->getCode();
			$this->last_insert_id = -1;
		}
		if (($this->db->trans_status() === FALSE) || ($this->error_message != "")) {
			$this->error_message .= "::DB Transaction rollback";
			$this->db->trans_rollback();
		} else {
			$this->db->trans_complete();
			$this->error_message = '';
		}			
		return $this->error_message;
	}
	
	function getRoomImageByRoomImageID($rowid) {
		$_params = array($rowid);
		$_sql = <<<EOT
SELECT t.*
FROM t_room_images t
WHERE t.rowid = ?
-- AND IFNULL(m.is_cancel, 0) < 1
EOT;
		//$_result = $this->db->query($_sql, $_params)->result();
		$_result = $this->arr_execute($_sql, $_params);
//var_dump($_result);exit;
		if (is_array($_result) && (count($_result) > 0)) {
			return $_result[0];
		} else {
			return FALSE;
		}
	}

	function getDisplayRoomDetails($rowid) {
		$_params = array($rowid);
		$_sql = <<<EOT

SELECT CONCAT(IFNULL(CONCAT(COALESCE(b.name_en, b.name_jp, b.name_th), ': Room '), ''), COALESCE(t.name_en, t.name_jp, t.name_th, t.code, CONCAT('( rowid ', t.rowid, ' )'))) AS disp_room_name
, t.code, t.room, t.floor, t.bed_room, t.bath_room, t.area_m2, t.description, t.remark
, '-none-' AS disp_owner_tel, maxbi.rowid AS disp_b_img_rowid, bi.title AS disp_b_img_title, rt.code AS disp_room_type
, CONCAT(UPPER(t.price_unit), ' ', FORMAT(t.rent_price, 2), CASE 
		WHEN IFNULL(t.rent_type_rowid, 1) = 1 THEN ' / mo.'
	END) AS disp_rent_price
, FORMAT(t.deposit_price, 2) AS disp_deposit_price
, b.srvc_internet_rowid, b.srvc_laundry_rowid, b.nrfc_super_rowid, b.is_pet, b.is_pool, b.is_sauna, b.is_gym
, b.is_conv_store, b.is_nhk, b.is_jp_srvc, b.is_playground, b.is_shuttle, b.is_cleaner
, ri.img_rowid1, ri.img_title1, ri.img_rowid1 AS image1
, ri.img_rowid2, ri.img_title2, ri.img_rowid2 AS image2
, ri.img_rowid3, ri.img_title3, ri.img_rowid3 AS image3
, ri.img_rowid4, ri.img_title4, ri.img_rowid4 AS image4
, ri.img_rowid5, ri.img_title5, ri.img_rowid5 AS image5
, ri.img_rowid6, ri.img_title6, ri.img_rowid6 AS image6
, ri.img_rowid7, ri.img_title7, ri.img_rowid7 AS image7
, ri.img_rowid8, ri.img_title8, ri.img_rowid8 AS image8
FROM t_room t
	INNER JOIN m_room_type rt ON t.room_type_rowid = rt.rowid 
	INNER JOIN t_building b ON b.rowid = t.building_rowid 
	LEFT OUTER JOIN v_default_building_images maxbi ON maxbi.building_rowid = t.building_rowid 
	LEFT OUTER JOIN t_building_images bi ON bi.rowid = maxbi.rowid
	LEFT OUTER JOIN v_room_images ri ON ri.room_rowid = t.rowid
	LEFT OUTER JOIN j343_users u ON (t.owner_type_rowid = 1 AND u.id = t.owner_rowid) OR (t.owner_type_rowid = 2 AND u.id = b.owner_rowid)
WHERE t.rowid = ?
AND IFNULL(t.is_cancel, 0) < 1

EOT;
		$_result = $this->arr_execute($_sql, $_params);
		if (is_array($_result) && (count($_result) > 0)) {
			return $_result[0];
		} else {
			return FALSE;
		}
	}

	//override
	function delete($RowID) {
		try {
			$this->db->set('is_cancel', 1);
			$this->db->where(array('rowid' => $this->db->escape((int) $RowID)));
			$this->db->update($this->_TABLE_NAME);
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
			return $this->db->affected_rows();
		} catch (Exception $e) {
			$this->error_message = $e->getMessage();
			$this->error_number = $e->getCode();
		}
	}

}