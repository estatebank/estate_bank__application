<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Ctrl_crud extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->modelName = '';
		$this->_model_fncSearch = 'search';
		$this->_model_fncSearch__arrAppendArgs = array();
		$this->_model_fncCommit = 'commit';
		$this->_model_fncCancel = 'cancel';
		$this->_model_fncDelete = 'delete';
		$this->_defaultBaseIndex = 0; //default base index used in jQuery select to identify correct elements and objects 
		$this->_pageOptions = array("type"=>"left_main");
		$this->_selOptions = array();
		$this->_arrDataViewFields = array();
		$this->_editPanelWidth = 900; //default edit panel width
		$this->_editPanelHeight = 600; //default edit panel height

		$this->load->helper('crud_controller_helper');
	}
	function __prepareEditForm() { //for main panel
		$this->_prepareControlsDefault();
		$this->_setController("name", "name", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
	}
	function __getEditForm($intFormIndex = -1) { // for main panel
		$_intFrmIndx = ($intFormIndex >= 0)?$intFormIndex:($this->_defaultBaseIndex + 2);
		$this->__prepareEditForm();
		return $this->add_view('_public/_form'
				, array(
					'index' => $_intFrmIndx
					,'crud_controller' => $this->page_name
					,'controls' => $this->_arrGetEditControls()
				),TRUE
			);
	}
	function __arrSearchControls() { // default for left/top search panel
		$_strMdlName = $this->modelName;
		$this->load->model($_strMdlName);
		$_arrControls = array();
		$_intCount = 0;
		foreach ($this->$_strMdlName->_FIELDS AS $_key=>$_val) {
			if (((strpos($_key, 'is_') === FALSE) && (strpos($_key, '_rowid') === FALSE) && (strpos($_key, '_date') === FALSE) 
				&& (strpos($_key, '_datetime') === FALSE) && (strpos($_key, '_timestamp') === FALSE)) 
				&& ($_intCount < 3)) {
				array_push($_arrControls, array(
					"name" => $_key, "type" => "txt", "label" => $this->_getDisplayLabel($_key), "placeHolder"=> $this->_getDisplayLabel($_key)
				));
				$_intCount += 1;
			}
		}
		return $_arrControls;
	}
	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(($this->_defaultBaseIndex + 2));
		return $this->add_view(
			'_public/_list'
			,array(
				'index' => ($this->_defaultBaseIndex + 1)
				,'dataview_fields' => $this->_arrDataViewFields
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight) 
				)
			),TRUE
		);
	}
	function _getLeftPanel() { //override
		$_arrControls = $this->__arrSearchControls();
		if (is_array($_arrControls) && (count($_arrControls) > 0)) {
			return $this->add_view(
				'_public/_search_panel'
				,array(
					'controls' => $_arrControls
				),TRUE
			);
		} else {
			return '';
		}
	}
	function _getTopPanel() { //overwritable
		$_arrSrcControls = $this->__arrSearchControls();
		$_arrControls = array();
		if (is_array($_arrSrcControls) && (count($_arrSrcControls) > 0)) {
			$_arrLayoutHeadRow = array();
			$_arrLayoutCtrlRow = array();
			$_ctrlHiddens = array();
			foreach ($_arrSrcControls as $_ctrl) {
				$_eaH = FALSE;
				if (isset($_ctrl['name']) && (! empty($_ctrl['name']))) {
					$_new = array_merge(array(), $_ctrl);
					if (isset($_ctrl['label']) && (! empty($_ctrl['label']))) {
						if ((! (array_key_exists('placeHolder', $_ctrl) || array_key_exists('place_holder', $_ctrl))) 
							|| (empty($_ctrl['placeHolder']) && empty($_ctrl['place_holder']))
						) {
							$_new['placeHolder'] = $_ctrl['label'];
						}
						$_eaH = 'return <div class="top_search_title">' . (isset($_ctrl['label']) ? $_ctrl['label'] : '') . '</div>';
					} else if (($_ctrl['type'] != 'hdn')) {
						$_eaH = 'return <div class="top_search_title"></div>';
					}
					$_new['label'] = '';
					array_push($_arrControls, $_new);

					if ($_eaH !== FALSE) {
						array_push($_arrLayoutHeadRow, $_eaH);
						array_push($_arrLayoutCtrlRow, $_ctrl['name']);
					}
				}
			}
		}
		if (count($_arrControls) > 0) {
			return $this->add_view(
				'_public/_search_panel_top'
				,array(
					'controls' => $_arrControls
					, 'layout' => array($_arrLayoutHeadRow, $_arrLayoutCtrlRow)
				), TRUE
			);			
		} else {
			return '';
		}
	}
	public function index() {
		$this->add_css(
			array(
				//,'http://fonts.googleapis.com/css?family=Roboto+Condensed:700italic,400,300,700'
				'public/css/jquery/dataTable/1.10.0/jquery.dataTables.css'
				//,'public/css/jquery/dataTable/1.10.0/jquery.dataTables_themeroller.css'
				,'public/css/jquery/dataTable/TableTools/2.2.1/dataTables.tableTools.css'
				,'public/css/jquery/ui/1.10.4/cupertino/jquery-ui.min.css'
				,'public/css/jquery/dataTable/1.9.4/dataTables_jui.css'
				,'public/css/jquery/dataTable/TableTools/2.1.5/TableTools_JUI.css'
			)
		);
		$this->add_js(
			array(
				'public/js/jquery/1.11.0/jquery.js',
				'public/js/jquery/ui/1.10.4/jquery-ui.min.js',
				'public/js/jquery/ui/1.10.3/jquery-ui-autocomplete-combobox.js',
				'public/js/jquery/dataTable/1.10.0/jquery.dataTables.min.js',
				'public/js/jquery/dataTable/TableTools/2.2.1/dataTables.tableTools.min.js',
				'public/js/jsGlobal.js',
				'public/js/jsUtilities.js',
				'public/js/jsGlobalConstants.js'
			)
		);
		$pass_main['title'] = $this->_pageTitle;
		$pass_main['bottom_panel'] = $this->add_view('_public/_sub_template/_bottom_main', NULL, TRUE);
		if (array_key_exists('type', $this->_pageOptions)) {
			switch (strtolower($this->_pageOptions['type'])) {
				case "left_main":
					$_pass['main_panel'] = $this->_getMainPanel();
					$_pass['left_panel'] = $this->_getLeftPanel();
					$pass_main['middle_panel'] = $this->add_view('_public/_sub_template/_left_main', $_pass, TRUE);
					break;
				case "top_main":
					$_pass['main_tm_panel'] = $this->_getMainPanel();
					$_pass['top_tm_panel'] = $this->_getTopPanel();
					$pass_main['middle_panel'] = $this->add_view('_public/_sub_template/_top_main', $_pass, TRUE);
					break;
				default:
					$pass_main['middle_panel'] = $this->_getMainPanel();
					break;
			}
		}
		$this->add_view_with_script_header('_public/_template_main', $pass_main);
	}
	public function json_search() {
		$this->_serviceCheckRight('view');
		$blnSuccess = FALSE;
		$arrResult = array();
		$strError = 'Unknown Error';
		$_arrData = $this->__getAjaxPostParams();
		if (! is_array($_arrData)) $_arrData = array();
		if (!empty($this->_model_fncSearch__arrAppendArgs)) $_arrData = array_merge($_arrData, $this->_model_fncSearch__arrAppendArgs);
		$this->load->model($this->modelName, 'm');
		if (! empty($_arrData)) {
			$arrResult = call_user_func(array($this->m, $this->_model_fncSearch), $_arrData); //$this->m->search($_arrData);
		} else {
			$arrResult = call_user_func(array($this->m, $this->_model_fncSearch)); //$this->m->search();
		}
		$strError = $this->m->error_message;
		if ($strError == '') {
			$blnSuccess = TRUE;
			if (!is_array($arrResult)) {
				$arrResult = array();
			}
		}
		$json = json_encode(
			array(
				'success' => $blnSuccess,
				'error' => $strError,
				'data' => $arrResult
			)
		);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")":$json;
	}
	function commit() {
		$this->_serviceCheckRight(($this->_blnCheckRight('insert') || $this->_blnCheckRight('edit')));
		$_blnSuccess = FALSE;
		$_strError = '';
		$_strMessage = '';
		$_arrData = $this->__getAjaxPostParams();
		if ($_arrData != FALSE) {
			$this->load->model($this->modelName, 'm');
			$_aff_rows = call_user_func(array($this->m, $this->_model_fncCommit), $_arrData); //$this->m->commit($_arrData);
			$_strError = $this->m->error_message;
			if ($_strError == '') {
				$_blnSuccess = TRUE;
				$_strMessage = $_aff_rows;
			} else {
				$_blnSuccess = FALSE;
			}
		}
		$json = json_encode(
			array(
				'success' => $_blnSuccess,
				'error' => $_strError,
				'message' => $_strMessage
			)
		);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
	function cancel() {
		$this->_serviceCheckRight('cancel');
		$_blnSuccess = FALSE;
		$_strError = '';
		$_strMessage = '';
		$_arrData = $this->__getAjaxPostParams();
		if ($_arrData != FALSE) {
			$this->load->model($this->modelName, 'm');
			$_rowid = $_arrData['rowid'];
			$_aff_rows = call_user_func(array($this->m, $this->_model_fncCancel), $_rowid); //$this->m->cancel($_rowid);
			$_strError = $this->m->error_message;
			if ($_strError == '') {
				$_blnSuccess = TRUE;
				$_strMessage = $_aff_rows;
			} else {
				$_blnSuccess = FALSE;
			}
		}
		$json = json_encode(
			array(
				'success' => $_blnSuccess,
				'error' => $_strError,
				'message' => $_strMessage
			)
		);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
	function delete() {
		$this->_serviceCheckRight('delete');
		$_blnSuccess = FALSE;
		$_strError = '';
		$_strMessage = '';
		$_arrData = $this->__getAjaxPostParams();
		if ($_arrData != FALSE) {
			$this->load->model($this->modelName, 'm');
			$_rowid = $_arrData['rowid'];
			$_aff_rows = call_user_func(array($this->m, $this->_model_fncDelete), $_rowid); //$this->m->delete($_rowid);
			$_strError = $this->m->error_message;
			if ($_strError == '') {
				$_blnSuccess = TRUE;
				$_strMessage = $_aff_rows;
			} else {
				$_blnSuccess = FALSE;
			}
		}
		$json = json_encode(
			array(
				'success' => $_blnSuccess,
				'error' => $_strError,
				'message' => $_strMessage
			)
		);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
	function _prepareSelectOptions($_arrSelList) {	
		$this->_selOptions = hlpr_prepareMasterTableSelectOptions($_arrSelList, $this->_selOptions);
		return $this->_selOptions;
	}
	function _prepareControlsDefault() {
		$this->_arrDataViewFields = hlpr_prepareControlsDefault($this->modelName, $this->_selOptions);
		return $this->_arrDataViewFields;
	}
	function _setController($strDataColumn, $strLabel, $arrEditAttributes = NULL, $arrGridAttributes = NULL) {
		hlpr_setController($this->_arrDataViewFields, $strDataColumn, $strLabel, $arrEditAttributes, $arrGridAttributes);
	}
	function _getDisplayLabel($strDataColumn) {
		return hlpr_getDisplayLabel($this->_arrDataViewFields, $strDataColumn);
	}
	function _arrGetEditControls($arr = array()) {
		return hlpr_arrGetEditControls($this->_arrDataViewFields, $arr);
	}
	function _getPostObjValue($strName, $arrObjSource, $default = NULL) {
		if (isset($arrObjSource[$strName]) && (strlen(trim($arrObjSource[$strName])) > 0)) {
			return $this->db->escape_str($arrObjSource[$strName]);
		} else {
			return $this->db->escape_str($default);
		}
	}
}
//---------------------------------------------------
