<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->_pageTitle = '';
		date_default_timezone_set("Asia/Bangkok");
		
		$this->page_name = $this->page_path = strtolower($this->router->fetch_class()); //strtolower(get_class($this));
		$this->page_method = strtolower($this->router->fetch_method());
		if ($this->page_method != 'index') $this->page_path = $this->page_path . '/' . $this->page_method;
	
		$this->load->library('access_control', '', '_AC');
		$this->load->helper('joomla_auth_helper');
		$_ac_name = '';
		switch ($this->page_name) {
			case 'property':
			case 'layout_setup':
				$_ac_name = 'setup_layout';
				break;
			default:
				if (substr($this->page_name, 0, 11) == 'map_rental_') {
					$_ac_name = 'setup_formula';
				} else {
					$_ac_name = $this->page_name;
				}
				break;
		}
		$this->_AC_NAME = $_ac_name;
// ++TESTTESTTEST **** 
		$this->_is_test = TRUE;
		//_checkSessionAuth($this->page_name, $this->_AC);
		//if ($this->_blnCheckRight() === FALSE) exit(str_replace('v_XX_1', '', $this->_AC->_MSG_ACCESS_NOT_ALLOWED));
		$this->_AC->_ACR = array("admin"=>TRUE);
		$this->session->set_userdata(
			array(
				'user_id' => 1,
				'user_name' => 'test',
				'user_display' => 'test period',
				'user_email' => 'test@period.com',
				'user_groups' => array(),
				'_AC' => json_encode($this->_AC->_ACR)
			)
		);
//--TESTTESTTEST****
		
		$this->_arrPrependHTML = array();
		$this->_arrAttachHTML = array();
		
		/*++ temp file use for sorting include file during assign step in derived class, before include it in final add view step 
			index: 0 = initial, 1 = declaration 1, 2 = declaration 2, 3 = process step 1, 4 = process step 2
		*/
		$this->_tmp_header_js = array(
			'file' => array(array(), array(), array(), array(), array()),
			'ie' => array(array(), array(), array(), array(), array()),
			'ie6' => array(array(), array(), array(), array(), array()),
			'custom' => array(array(), array(), array(), array(), array()),
			'ie_custom' => array(array(), array(), array(), array(), array()),
			'ie6_custom' => array(array(), array(), array(), array(), array())
		);
		$this->_tmp_header_css = array(
			'file' => array(array(), array(), array(), array(), array()),
			'ie' => array(array(), array(), array(), array(), array()),
			'ie6' => array(array(), array(), array(), array(), array()),
			'custom' => array(array(), array(), array(), array(), array()),
			'ie_custom' => array(array(), array(), array(), array(), array()),
			'ie6_custom' => array(array(), array(), array(), array(), array())
		);
		/*-- temp file use for sorting include file during assign step in derived class, before include it in final add view step */

		$this->_header_js = array(
			'file' => array(),
			'ie' => array(),
			'ie6' => array(),
			'custom' => array(),
			'ie_custom' => array(),
			'ie6_custom' => array()
		);
		$this->_header_css = array(
			'file' => array(),
			'ie' => array(),
			'ie6' => array(),
			'custom' => array(),
			'ie_custom' => array(),
			'ie6_custom' => array()
		);
		/*++ Mostly use in JSPN to add special css and js along with panel to enable them on the fly */
		$this->_onload_css_files = array();
		$this->_onload_js_files = array();
		$this->_onload_css_files_append = array();
		$this->_onload_js_files_append = array();
		/*-- Mostly use in JSPN to add special css and js along with panel to enable them on the fly */
		$this->_onload_scripts = array(
			0 => '', //step 0 - declaration
			1 => '', //step 1 - initial
			2 => '',
			3 => '',
			4 => ''
		);

		$this->_add_css('public/css/main.css', 'file', 0);
		$this->add_js(
			array(
				array('public/js/ie.js', 'ie'),
				array('public/js/ie6.js', 'ie6'),
				array('alert("You are using IE");', 'ie_custom'),
				array('alert("Stop using IE6 (test period :D)");', 'ie6_custom'),
				array('var _baseUrl = "' . base_url() .'";', 'custom'),
				array('var CONTROLLER_NAME = "' . $this->page_name .'";', 'custom'),
				array('var _arrPanelLinkFields = [];', 'custom')
			)
		);
	}
	
	function __performPrependContents(&$params) {
		$_returnStr = '';
		if (is_array($params) && is_array($this->_arrPrependHTML)) {
			foreach ($this->_arrPrependHTML as $_key=>$_html) {
				if (! is_integer($_key)) {
					if (array_key_exists($_key, $params)) $params[$_key] .= $_html;
				} else {
					$_returnStr .= "\n" . $_html;		
				}
				unset($this->_arrPrependHTML[$_key]);
			}
		}
		return $_returnStr;
	}

	function __performAppendContents(&$params) {
		$_returnStr = '';
		if (is_array($params) && is_array($this->_arrAttachHTML)) {
			foreach ($this->_arrAttachHTML as $_key=>$_html) {
				if (! is_integer($_key)) {
					if (array_key_exists($_key, $params)) $params[$_key] .= $_html;
				} else {
					$_returnStr .= "\n" . $_html;
				}
				unset($this->_arrAttachHTML[$_key]);
			}
		}
		return $_returnStr;
	}

	public function add_view($path, $params = null, $return = FALSE, $with_script_header = FALSE) {
		if ((strpos($path, '_public/_search_panel') === 0) && ((isset($params['list_viewable'])) && ($params['list_viewable'] == FALSE))) {
			return '';
		}
		if (! (isset($params['auto_load_css']) && (($params['auto_load_css'] == FALSE) || ($params['auto_load_css'] == 'n')))) {
			if (file_exists('public/css/' . $path . '.css')) {
				if ((strpos($path, '_public/') === 0)) {
					$this->add_onload_css_file('public/css/' . $path . '.css');
				} else {
					$this->add_onload_css_file('public/css/' . $path . '.css', TRUE);					
				}
			}
		}
		if (! (isset($params['auto_load_js']) && (($params['auto_load_js'] == FALSE) || ($params['auto_load_js'] == 'n')))) {
			$strPath = '';
			if (defined('ENVIRONMENT') && (ENVIRONMENT != 'development') && file_exists('public/js/' . $path . '.obf.js')) {
				$strPath = 'public/js/' . $path . '.obf.js';
			} else if (file_exists('public/js/' . $path . '.js')) {
				$strPath = 'public/js/' . $path . '.js';
			}
			if (!empty($strPath)) {
				if ((strpos($path, '_public/') === 0)) {
					$this->add_onload_js_file($strPath);
				} else {
					$this->add_onload_js_file($strPath, TRUE);
				}
			}
		}
		$_strFormSelector = '';
		$_strControlBaseType = 'data-container';
		$_index = (isset($params['index']) && (strlen(trim($params['index'])) > 0)) ? trim($params['index']) : -1;
		$_viewType = 0;
		if ($path == '_public/_form') {
			$_viewType = 1;
			$_strFormSelector = '#frm_edit';
			$_strControlBaseType .= ' user-input';
		} elseif (strpos($path, '_public/_search_panel') === 0) {
			$_viewType = 2;
			$_strFormSelector = '#frmSearch';
			$_strControlBaseType .= ' search-param';
		} elseif ($path == '_public/_list') {
			$_viewType = 3;
		} elseif ($path == '_public/_sublist') {
			$_viewType = 4;
		//++ 20160404 ::Buff add special parameter
		} elseif (isset($params['_gen_control_elements']) && ($params['_gen_control_elements'] == TRUE)) {
			$_viewType = 5;
			$_strFormSelector = 'form';
			$_strControlBaseType .= ' user-input';
		}
		//-- 20160404 ::Buff add special parameter
		if ((! empty($_strFormSelector)) && ($_index > -1)) $_strFormSelector .= '[index="' . $_index . '"]';

		if (($_viewType > 0) && is_array($params)) {
			$params['_CONTROLLER_GENERATED_CONTENTS'] = '';
			switch ($_viewType) {
				//special gen control for template
				case 5:
					if (isset($params['controls']) && is_array($params['controls'])) {
						$_arrControls = array();
						$this->___performGetControlElements($_strFormSelector, $_strControlBaseType, $params, $_arrControls);
						$_arr_template_controls_elements = array();
						foreach ($_arrControls as $_id=>$_arrElem) {
							if (is_array($_arrElem) && (count($_arrElem) > 2)) $_arr_template_controls_elements[$_id] = $_arrElem[1];
						}
						$params["_CONTROL_ELEMENT"] = $_arr_template_controls_elements;
					}
					break;
				//form and search_panel
				case 1:
				case 2:
					if (array_key_exists('controls', $params)) {
						if (is_array($params['controls'])) {
							$_arrControls = array();
							$this->___performGetControlElements($_strFormSelector, $_strControlBaseType, $params, $_arrControls);
							$_display = '';
							$_arrEditPanelDataKey = array();
							if (array_key_exists('layout', $params) && isset($params['layout']) && is_array($params['layout'])) {
								$_display .= _getLayoutItemDisplay($params['layout'], $_arrControls, $_arrEditPanelDataKey);
							}
							foreach ($_arrControls as $_item) { //left over from layout (if passed, otherwise generate all)
								if (($_item[0] == 'hidden') || ($_item[2] == 'info')) {
									$_display .= $_item[1];
								} elseif ($_item[2] == 'chk') {
									$_display .= <<<EOT
	<tr>
		<td class="table-value table-value-checkbox">{$_item[1]}</td>
		<td class="table-title table-title-checkbox" colspan="2">{$_item[0]}</td>
	</tr>

EOT;
								} else {
									$_display .= <<<EOT
	<tr>
		<td class="table-title">{$_item[0]} :</td>
		<td class="table-value" colspan="2">{$_item[1]}</td>
	</tr>

EOT;
								}
							}
						}
						if (strlen(trim($_display)) > 0) {
							$params['_CONTROLLER_GENERATED_CONTENTS'] = str_replace('[[FRMID]]', $_strFormSelector, $_display);
						}				
						$_strArrEditPanelData = '';
						foreach($_arrEditPanelDataKey as $_key) {
							if (strpos($_strArrEditPanelData, '"' . $_key . '":') == FALSE) $_strArrEditPanelData .= '"' . $_key . '":"",';
						}
						if ($_strArrEditPanelData != '') {
							$_strArrEditPanelData = trim(substr($_strArrEditPanelData, 0, -1));
							$this->_onload_scripts[0] .= "\t\t_arrPanelLinkFields[$_index] = { $_strArrEditPanelData };\n";
						}
					}
					if (array_key_exists('end_script', $params) && (strlen(trim($params['end_script'])) > 0)) {
						$this->_onload_scripts[1] .= trim($params['end_script']) . "\n";
					}
					//Clear params after use
					unset($params['controls']);
					
					break;
				//list and sublist
				case 3:
				case 4:
					if (! $this->_blnCheckRight('view')) $params['list_viewable'] = FALSE;
					if (! $this->_blnCheckRight('insert')) $params['list_insertable'] = FALSE;
					if (! $this->_blnCheckRight('edit')) $params['list_editable'] = FALSE;
					if (! $this->_blnCheckRight('cancel')) $params['list_cancelable'] = FALSE;
					if (! $this->_blnCheckRight('delete')) $params['list_deleteable'] = FALSE;
					
					$_strDtColumns = '';
					$_strDtDisplay = '';
					$_strSelectElem = '';
					$_strEditDialogID = ($_viewType == 3)?'divFormEditDialog':'divSublistFormEditDialog';
					$_strEditDialogSelector = '.cls-div-form-edit-dialog[index="' . $_index . '"]';
					if (isset($params['dataview_fields'])) {
						$dataview_fields = $params['dataview_fields'];
						if (is_array($dataview_fields)) {
							$_arr = array();
							$_count = 1;
							foreach ($dataview_fields as $_id => $_obj) {
								$_lbl = '';
								if (array_key_exists("form_edit", $_obj)) {
									$_lbl = (array_key_exists("label", $_obj["form_edit"]))?$_obj["form_edit"]["label"]:'';
								} else if (array_key_exists("list_item", $_obj)) {
									$_lbl = (array_key_exists("label", $_obj["list_item"]))?$_obj["list_item"]["label"]:'';
								}
								$_strDtColumns .= '["' . $_id . '","' . $_lbl . '"],';
								if (array_key_exists("list_item", $_obj)) {
									$_str1 = '';
									$_title = (array_key_exists("label", $_obj["list_item"]))?$_obj["list_item"]["label"]:'';
									$_str1 .= '{"sTitle":"' . $_title . '", "mData":"' . $_id . '"';
									if (array_key_exists('width', $_obj["list_item"])) $_str1 .= ',"sWidth":"' . $_obj["list_item"]['width'] . '"';
									if (array_key_exists('class', $_obj["list_item"])) {
										$_class = strtolower($_obj["list_item"]['class']);
										if (strpos($_class, 'default_number') !== FALSE) {
											$_str1 .= ',"mRender": function(data, type, full) { if ($.isFunction(_fncDTmRenderFormat)) { return _fncDTmRenderFormat(data, type, full, "default_number"); } else { return data } }';
											$_class = trim(str_replace('default_number', 'right', $_class));
										} else if (strpos($_class, 'default_integer') !== FALSE) {
											$_str1 .= ',"mRender": function(data, type, full) { if ($.isFunction(_fncDTmRenderFormat)) { return _fncDTmRenderFormat(data, type, full, "default_integer"); } else { return data } }';
											$_class = trim(str_replace('default_integer', 'right', $_class));
										}
										$_str1 .= ',"sClass":"' . $_class . '"';
									}
									if (array_key_exists('default', $_obj["list_item"]) && ((boolean)($_obj["list_item"]["default"]) == FALSE)) $_str1 .= ',"bVisible":false';
									$_str1 .= '},';

									$_str = '';
									if (array_key_exists("selectable", $_obj["list_item"]) && ((boolean)($_obj["list_item"]["selectable"]) == TRUE)) {
										$_str .= '<li><input type="checkbox" id="li_col_XXXX" ';
										if (array_key_exists('default', $_obj["list_item"]) && ((boolean)($_obj["list_item"]["default"]) == FALSE)) $_str .= 'checked';
										$_str .= ' />' . $_title . '</li>';
									}
									
									if (array_key_exists("order", $_obj["list_item"]) && (! array_key_exists($_obj["list_item"]["order"], $_arr))) {
										$_arr[$_obj["list_item"]["order"]] = array($_str, $_str1);
									} else {
										$_arr[90 + $_count] = array($_str, $_str1);
										$_count++;
									}
								}
							}
							if (isset($params['custom_columns'])) {
								$custom_columns = $params['custom_columns'];
								if (is_array($custom_columns)) {
									foreach ($custom_columns as $_col) {
										if (is_array($_col)) {
											$_str1 = $_col["column"];
											if (substr(trim($_str1), -1, 1) != ',') $_str1 = trim($_str1) . ',';
											if (array_key_exists("order", $_col) && (! array_key_exists($_col["order"], $_arr))) {
												$_arr[$_col["order"]] = array('', $_str1);
											} else {
												$_arr[90 + $_count] = array('', $_str1);
												$_count++;
											}
										} else {
											$_arr[90 + $_count] = array('', $_col);
											$_count++;
										}
									}
								} else { //string
									if ((trim($custom_columns) != '')) {
										if (substr(trim($custom_columns), -1, 1) != ',') $custom_columns = trim($custom_columns) . ',';
										$_arr[90 + $_count] = array('', $custom_columns);
									}
								}
							}
							ksort($_arr);
							$_i = 0;
							foreach ($_arr as $_elems) {
								$_strSelectElem .= str_replace("XXXX", $_i, $_elems[0]);
								$_strDtDisplay .= $_elems[1];
								$_i ++;
							}
							if (strlen($_strDtColumns) > 0) $_strDtColumns = substr($_strDtColumns, 0, -1);
							if (strlen($_strDtDisplay) > 0) {
								if (!(isset($params['list_viewable']) && ($params['list_viewable'] == FALSE))) $_strDtDisplay .= '{"sTitle":"View", "sWidth":"50","sClass": "center","mData": function() { return \'<img class="list-row-button" command="view" src="./public/images/b_view.png" alt="view" title="\' + MSG_ICON_TITLE_VIEW + \'" />\';},"bSortable":false},';
								if (!(isset($params['list_editable']) && ($params['list_editable'] == FALSE))) $_strDtDisplay .= '{"sTitle":"Edit", "sWidth":"50","sClass": "center","mData": function() { return \'<img class="list-row-button" command="edit" src="./public/images/b_edit.png" alt="edit" title="\' + MSG_ICON_TITLE_EDIT + \'" />\';},"bSortable":false},';
								if (isset($params['list_cancelable']) && ($params['list_cancelable'] == TRUE)) $_strDtDisplay .= '{"sTitle":"Cancel","sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { if (full.is_cancel == 1) { return ""; } else { return \'<img class="list-row-button" command="cancel" src="./public/images/details_close.png" alt="cancel" title="\' + MSG_ICON_TITLE_CANCEL + \'" />\'; } },"bSortable":false}';
								if (!(isset($params['list_deleteable']) && ($params['list_deleteable'] == FALSE))) $_strDtDisplay .= '{"sTitle":"Del.", "sWidth":"50","sClass":"center","mData": function() { return \'<img class="list-row-button" command="delete" src="./public/images/b_delete.png" alt="delete" title="\' + MSG_ICON_TITLE_DELETE + \'" />\';},"bSortable":false},';
								
								if (substr(trim($_strDtDisplay), -1, 1) == ',') $_strDtDisplay = substr(trim($_strDtDisplay), 0, -1);
								if (substr(trim($_strDtColumns), -1, 1) == ',') $_strDtColumns = substr(trim($_strDtColumns), 0, -1);
							}
							$_strSelectElem = '<ul autofocus >' . $_strSelectElem . '</ul>';
						}
					}
					$_strTableToolButtons = '';
					if (($_viewType == 3) && (!(isset($params['list_select_columns']) && ($params['list_select_columns'] == FALSE)))) {
						$_strTableToolButtons .= <<<TBLT
		{
			"sExtends": "text",
			"sButtonText": "Select columns",
			"sButtonClass": "cls_button_select",
			"fnClick": function ( nButton, oConfig, oFlash ) {
				doSelectDisplayFields();
			}
		},
TBLT;
					}
					
					$_strTableToolButtons .= <<<TBLT
		{
			"sExtends": "copy",
			"sButtonText": "Copy",
			"bShowAll": true,
			"bHeader": true,
			"bFooter": false,
			"mColumns": "visible",
			"fnCellRender": function ( sValue, iColumn, nTr, iDataIndex ) {
				if (sValue.length > 4) if (sValue.substr(0, 4) == "<img") return '';
				return sValue;
			}
		}
		, {
			"sExtends": "print",
			"sButtonText": "Print",
			"bShowAll": true,
			"bHeader": true,
			"bFooter": false,
			//"sInfo": "Please press escape when done",
			"mColumns": "visible",
			"fnClick": function (nButton, oConfig, oFlash) {
				_blnLeft = false;
				if ($('#left_panel').css('display') !== 'none') {
					_blnLeft = true;
					if (typeof doToggleLeftPanel == 'function') doToggleLeftPanel();
				}
				if (typeof _visibleButtonColumns == 'function') _visibleButtonColumns(false);
				$(window).keyup(function() {
					if (typeof _visibleButtonColumns == 'function') _visibleButtonColumns(true);
					if (_blnLeft && (typeof doToggleLeftPanel == 'function')) doToggleLeftPanel();
				});
				this.fnPrint( true, oConfig );
			}
		}

TBLT;
					if ($_viewType == 3) {
						$_strTableToolButtons .= <<<TBLT
		, {
			"sExtends": "text",
			"sButtonText": "Excel",
			"sButtonClass": "DTTT_button_xls",
			"bShowAll": true,
			"bHeader": true,
			"bFooter": false,
			"mColumns": "visible",
			"fnClick": function ( nButton, oConfig, oFlash ) {
				if (! $(nButton).is('.DTTT_button_disabled')) doExportExcel( nButton, oConfig, oFlash );
			}
		}

TBLT;
					}
					if (!(isset($params['list_insertable'])) && (isset($params['list_addable']))) $params['list_insertable'] = $params['list_addable'];
					if (!(isset($params['list_insertable']) && ($params['list_insertable'] == FALSE))) {
						$_strTableToolButtons .= <<<TBLT
		, {
			"sExtends": "text"
			, "sButtonText": ""
			, "sButtonClass": "DTTT_button_space"
			
		}
		, {
			"sExtends": "text",
			"sButtonText": "Insert",
			"sButtonClass": "DTTT_button_add_row",
			"fnClick": function ( nButton, oConfig, oFlash ) {
				if (! $(nButton).is('.DTTT_button_disabled')) doInsert($('$_strEditDialogSelector'));
			}
		}
TBLT;
					}
					
					if ($_viewType == 3) {
						$this->_onload_scripts[0] .= <<<EOT
	var _arrDtColumns = [$_strDtColumns];
	var _aoColumns = [$_strDtDisplay];
	var _tableToolButtons = [$_strTableToolButtons];

EOT;
					} else {
						$this->_onload_scripts[0] .= <<<EOT
	_Sublist_arrDtColumns[$_index] = [$_strDtColumns];
	_Sublist_aoColumns[$_index] = [$_strDtDisplay];
	_Sublist_tableToolButtons[$_index] = [
$_strTableToolButtons
	];

EOT;
					}
					if (isset($params['edit_dlg']) && is_array($params['edit_dlg'])) {
						$this->add_onload_js_file('public/js/_public/_list_edit_dlg.js');
						$edit_dlg = $params['edit_dlg'];
						$this->_onload_scripts[1] .= "\t\t_doBindEditDialog($('" . $_strEditDialogSelector . "'));\n";
						if (isset($edit_dlg['option']) || isset($edit_dlg['options'])) {
							$_strJsonOpts = '';
							$_arrOptions;
							if (isset($edit_dlg['option'])) {
								$_arrOptions = $edit_dlg['option'];
							} else if (isset($edit_dlg['options'])) {
								$_arrOptions = $edit_dlg['options'];
							}
							if (is_array($_arrOptions)) {
								foreach ($_arrOptions as $_key=>$_val) {
									if (strpos($_val, '__RAW__')) {
										$_val = str_replace('__RAW__', '', $_val);
										$_strJsonOpts .= $_key . ":" . $_val . ",";
									} else {
										$_strJsonOpts .= $_key . ":'" . $_val . "',";
									}
								}
								$_strJsonOpts = '{'.substr($_strJsonOpts, 0, -1).'}';
							}
							$this->_onload_scripts[1] .= "\t\t$('" . $_strEditDialogSelector . "').dialog('option', " . $_strJsonOpts . ");\n";
						}
						if (isset($edit_dlg['close_tool']) && ($edit_dlg['close_tool'] == TRUE)) {
							$this->_onload_scripts[1] .= "\t\t$('" . $_strEditDialogSelector . "').parents('.ui-dialog').find('.ui-dialog-titlebar-close').css('visibility', 'visible');\n";
						} else {
							$this->_onload_scripts[1] .= "\t\t$('" . $_strEditDialogSelector . "').parents('.ui-dialog').find('.ui-dialog-titlebar-close').css('visibility', 'hidden');\n";			
						}
						$_arr_default_buttons = array();
						$_arr_default_buttons['submit'] = <<<'SUBMIT'
							{
								text:MSG_DLG_BUTTON_TEXT_SUBMIT,
								class:"cls-btn-form-submit",
								icons: { primary: "ui-icon-disk"},
								click:function() {
									_clickSubmit(this);
								}
							}
SUBMIT;
						$_arr_default_buttons['reset'] = <<<'RESET'
							{
								text:MSG_DLG_BUTTON_TEXT_DEFAULT,
								class:"cls-btn-form-reset",
								icons: { primary: "ui-icon-refresh"},
								click:function() {
									_clickReset(this);
								}
							}
RESET;
						$_arr_default_buttons['cancel'] = <<<'CANCEL'
							{
								text:MSG_DLG_BUTTON_TEXT_CANCEL,
								class:"cls-btn-form-cancel",
								icons: { primary: "ui-icon-extlink"},
								click:function() {
									_clickCancel(this);
								}
							}
CANCEL;

						if (isset($edit_dlg['buttons']) && is_array($edit_dlg['buttons'])) {
							if (isset($edit_dlg['buttons']['submitable']) && ($edit_dlg['buttons']['submitable'] == FALSE)) $_arr_default_buttons['submit'] = '';
							if (isset($edit_dlg['buttons']['resetable']) && ($edit_dlg['buttons']['resetable'] == FALSE)) $_arr_default_buttons['reset'] = '';
							if (isset($edit_dlg['buttons']['cancelable']) && ($edit_dlg['buttons']['cancelable'] == FALSE)) $_arr_default_buttons['cancel'] = ''; 
						}
						
						$_strButtons = (strlen($_arr_default_buttons['submit'])>0)?$_arr_default_buttons['submit'].','."\n":'';
						$_strButtons .= (strlen($_arr_default_buttons['reset'])>0)?$_arr_default_buttons['reset'].','."\n":'';
						$_strButtons .= (strlen($_arr_default_buttons['cancel'])>0)?$_arr_default_buttons['cancel'].','."\n":'';
						if (strlen($_strButtons) > 0) {
							$this->_onload_scripts[0] .= "\n\t\t_ARR_EditDialogButtons[" . $_index . "] = [" . substr($_strButtons,0,-2) . "];\n";
							$this->_onload_scripts[1] .= "\t\t$('" . $_strEditDialogSelector . "').dialog('option', 'buttons', _ARR_EditDialogButtons[" . $_index . "]);\n";
						}
					}
					if ($_viewType == 3) $params['_CONTROLLER_GENERATED__SELECT_ELEM'] = $_strSelectElem;
					$params['_CONTROLLER_GENERATED__EDIT_DLG_ID'] = $_strEditDialogID;
					break;
			}
		}
		//++ end scripts
		if (is_array($params) && array_key_exists('end_script', $params) && (strlen(trim($params['end_script'])) > 0)) {
			$this->_onload_scripts[1] .= trim($params['end_script']) . "\n";
			unset($params['end_script']);
		}
		//-- end scripts
		//++ Auto Search Control
		if (is_array($params) && isset($params['autosearch']) && ($params['autosearch'] == FALSE)) {
			//$this->_add_js('var _autoSearch_OnLoad = false;', "custom", 0);
			$this->_DISABLE_ON_LOAD_SEARCH = TRUE;
		}
		//-- Auto Search Control
		
		//Add _ACR to view params
		if (((substr($path, 0, 8) == '_public/') && ($path != '_public/_template_main') && (strpos($path, '_sub_template/') == FALSE)) && isset($this->_AC)) $params['_ACR'] = $this->_AC->_ACR;

		if ($with_script_header == TRUE) $params['_scripts_include'] = $this->_get_headers();
		
		$_prepToGlobalHTML = $this->__performPrependContents($params);
		$_appeToGlobalHTML = $this->__performAppendContents($params);
		$_content = $this->load->view($path, $params, TRUE);
		if (strlen(trim($_prepToGlobalHTML)) > 0) $_content = $_prepToGlobalHTML . $_content;
		if (strlen(trim($_appeToGlobalHTML)) > 0) $_content .= $_appeToGlobalHTML;

		/*++ clear inheritable params cache */
		if (isset($params['layout'])) unset($params['layout']);
		/*-- clear inheritable params cache */
		
		if ($return == TRUE) {
			return $_content;
		} else {
			echo $_content;
		}
	}

	function add_view_with_script_header($path, $params = null, $return = FALSE) {
		if (file_exists('public/css/' . $this->page_name . '/main.css')) $this->add_css('public/css/' . $this->page_name . '/main.css');
		if (file_exists('public/css/' . $this->page_path . '/main.css')) $this->add_css('public/css/' . $this->page_path . '/main.css');
		if (file_exists('public/js/' . $this->page_name . '/main.js')) $this->add_js('public/js/' . $this->page_name . '/main.js');
		if (file_exists('public/js/' . $this->page_path . '/main.js')) $this->add_js('public/js/' . $this->page_path . '/main.js');

		return $this->add_view($path, $params, $return, TRUE);
	}

	function __prepareHeaderFiles() {
		foreach ($this->_tmp_header_css as $_type=>$_arrValueSet) {
			if (is_array($_arrValueSet)) foreach ($_arrValueSet as $_eaValueSet) {
				if (is_array($_eaValueSet)) foreach ($_eaValueSet as $_item) {
					$_item = (isset($_item)) ? trim($_item) : NULL;
					if ((! empty($_item)) && (! in_array($_item, $this->_header_css[$_type]))) array_push($this->_header_css[$_type], $_item);
				}
			}
		}
		foreach ($this->_tmp_header_js as $_type=>$_arrValueSet) {
			if (is_array($_arrValueSet)) foreach ($_arrValueSet as $_eaValueSet) {
				if (is_array($_eaValueSet)) foreach ($_eaValueSet as $_item) {
					$_item = (isset($_item)) ? trim($_item) : NULL;
					if ((! empty($_item)) && (! in_array($_item, $this->_header_js[$_type]))) array_push($this->_header_js[$_type], $_item);
				}
			}
		}
	}
	
	function __isExistsJs($item, $type = 'file') {
		if (empty($item)) return FALSE;
		if (empty($type)) $type = 'file';
		$_arrHayStck = $this->_tmp_header_js[$type];
		if (is_array($_arrHayStck)) foreach ($_arrHayStck as $_i=>$_eaHayStck) {
			if (is_array($_eaHayStck)) foreach ($_eaHayStck as $_j=>$_eaItem) {
				if ($_eaItem == trim($item)) return 1;
			}
		}
		if (is_array($this->_onload_js_files) && (count($this->_onload_js_files) > 0)) foreach($this->_onload_js_files as $_ea) {
			if ($_ea == trim($item)) return 2;
		}
		if (is_array($this->_onload_js_files_append) && (count($this->_onload_js_files_append) > 0)) foreach($this->_onload_js_files_append as $_ea) {
			if ($_ea == trim($item)) return 3;
		}
		return FALSE;
	}
	
	function __isExistsCss($item, $type = 'file') {
		if (empty($item)) return FALSE;
		if (empty($type)) $type = 'file';
		$_arrHayStck = $this->_tmp_header_css[$type];
		if (is_array($_arrHayStck)) foreach ($_arrHayStck as $_i=>$_eaHayStck) {
			if (is_array($_eaHayStck)) foreach ($_eaHayStck as $_j=>$_eaItem) {
				if ($_eaItem == $item) return 1;
			}
		}
		if (is_array($this->_onload_css_files) && (count($this->_onload_css_files) > 0)) foreach($this->_onload_css_files as $_ea) {
			if ($_ea == trim($item)) return 2;
		}
		if (is_array($this->_onload_css_files_append) && (count($this->_onload_css_files_append) > 0)) foreach($this->_onload_css_files_append as $_ea) {
			if ($_ea == trim($item)) return 3;
		}
		return FALSE;
	}

	function __performRemoveExistsJs($item, $type = 'file') {
		if (empty($item)) return FALSE;
		if (empty($type)) $type = 'file';
		$_arrHayStck = $this->_tmp_header_js[$type];
		if (is_array($_arrHayStck)) foreach ($_arrHayStck as $_i=>$_eaHayStck) {
			if (is_array($_eaHayStck)) foreach ($_eaHayStck as $_j=>$_eaItem) {
				if ($_eaItem == $item) unset($this->_tmp_header_js[$type][$_i][$_j]);
			}
		}
	}
	
	function __performRemoveExistsCss($item, $type = 'file') {
		if (empty($item)) return FALSE;
		if (empty($type)) $type = 'file';
		$_arrHayStck = $this->_tmp_header_css[$type];
		if (is_array($_arrHayStck)) foreach ($_arrHayStck as $_i=>$_eaHayStck) {
			if (is_array($_eaHayStck)) foreach ($_eaHayStck as $_j=>$_eaItem) {
				if ($_eaItem == $item) unset($this->_tmp_header_css[$type][$_i][$_j]);
			}
		}
	}

	function _add_js($file='', $type='file', $phase=4) {
		if (empty($file)) return;
		if (empty($type)) $type = 'file';
		if (is_array($file)) {
			foreach($file AS $item) {
				if (is_array($item)) {
					if ((count($item) > 0)) {
						$_type = isset($item[1])?$item[1]:$type;
						$this->_add_js($item[0], $_type, $phase);
					}
				} else {
					$this->__performRemoveExistsJs($item, $type);
					array_push($this->_tmp_header_js[$type][$phase], $item);
				}
			}
		} else {
			$this->__performRemoveExistsJs($file, $type);
			array_push($this->_tmp_header_js[$type][$phase], $file);
		}
	}

	function _add_css($file='', $type='file', $phase=4) {
		if (empty($file)) return;
		if (empty($type)) $type = 'file';
		if (is_array($file)) {
			foreach($file AS $item) {
				if (is_array($item)) {
					if ((count($item) > 0)) {
						$_type = isset($item[1])?$item[1]:$type;
						$this->_add_css($item[0], $_type, $phase);
					}
				} else {
					$this->__performRemoveExistsCss($item, $type);
					array_push($this->_tmp_header_css[$type][$phase], $item);
				}
			}
		} else {
			$this->__performRemoveExistsCss($file, $type);
			array_push($this->_tmp_header_css[$type][$phase], $file);
		}
	}

	function _prepend_js($file='', $type='file') {
		$this->_add_js($file, $type, 2);
	}
	function _prepend_css($file='', $type='file') {
		$this->_add_css($file, $type, 2);
	}
	function _append_js($file='', $type='file') {
		$this->_add_js($file, $type, 4);
	}
	function _append_css($file='', $type='file') {
		$this->_add_css($file, $type, 4);
	}
	function add_js($file='', $type='file') {
		$this->_add_js($file, $type, 4);
	}
	function add_css($file='', $type='file') {
		$this->_add_css($file, $type, 4);
	}
	
	function add_onload_js_file($file='', $orderIndex = FALSE) {
		$file = trim($file);
		if ((! empty($file)) && file_exists($file)) {
			if ($this->__isExistsJs($file) == 1) $this->__performRemoveExistsJs($file);
			if ($orderIndex === FALSE) {
				if (! in_array($file, $this->_onload_js_files)) array_push($this->_onload_js_files, $file);
			} else {
				if (is_numeric($orderIndex) && (count($this->_onload_js_files_append) < intval($orderIndex))) {
					array_splice($this->_onload_js_files_append, intval($orderIndex), 0, $file);
				} else {
					$this->_onload_js_files_append[] = $file;					
				}
			}
		}
	}
	
	function add_onload_css_file($file='', $orderIndex = FALSE) {
		$file = trim($file);
		if ((! empty($file)) && file_exists($file)) {
			if ($this->__isExistsCss($file) == 1) $this->__performRemoveExistsCss($file);
			if ($orderIndex === FALSE) {
				if (! in_array($file, $this->_onload_css_files)) array_push($this->_onload_css_files, $file);
			} else {
				//if (! in_array($file, $this->_onload_css_files_append)) array_push($this->_onload_css_files_append, trim($file));
				if (is_numeric($orderIndex) && (count($this->_onload_css_files_append) < intval($orderIndex))) {
					array_splice($this->_onload_css_files_append, intval($orderIndex), 0, $file);
				} else {
					$this->_onload_css_files_append[] = $file;					
				}
			}
		}
	}

	function _getOnloadFiles_CSS() {
		$_str = "";
		$_strAppend = "";
		asort($this->_onload_css_files_append);
		foreach ($this->_onload_css_files_append as $_eaFile) {
			$_i = array_search($_eaFile, $this->_onload_css_files);
			if ($_i !== FALSE) unset($this->_onload_css_files[$_i]);
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_css['file']))) $_strAppend .= '<link rel="stylesheet" href="'.trim($_eaFile).'" charset="utf-8" type="text/css" />'."\n";
		}
		foreach ($this->_onload_css_files as $_eaFile) {
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_css['file']))) $_str .= '<link rel="stylesheet" href="'.trim($_eaFile).'" charset="utf-8" type="text/css" />'."\n";
		}
		return $_str . "\n" . $_strAppend;
	}
	
	function _getOnloadFiles_JS() {
		$_str = "";
		$_strAppend = "";
		asort($this->_onload_js_files_append);
		foreach ($this->_onload_js_files_append as $_eaFile) {
			$_i = array_search($_eaFile, $this->_onload_js_files);
			if ($_i !== FALSE) unset($this->_onload_js_files[$_i]);
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_js['file']))) $_strAppend .= '<script type="text/javascript" src="'.trim($_eaFile).'" charset="utf-8"></script>'."\n";
		}
		foreach ($this->_onload_js_files as $_eaFile) {
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_js['file']))) $_str .= '<script type="text/javascript" src="'.trim($_eaFile).'" charset="utf-8"></script>'."\n";
		}
		return $_str . "\n" . $_strAppend;
	}
	
	function _getOnloadScript() {
		$_strReturn = "\n";
		$_onPageLoad = '';
		ksort($this->_onload_scripts);
		foreach ($this->_onload_scripts as $_idx=>$_eaStep) {
			if (is_array($_eaStep)) {
				foreach ($_eaStep as $_eaScript) {
					if (strlen(trim($_eaScript)) > 0) {
						if ($_idx == 0) { //initial level
							$_strReturn .= $_eaScript . "\n";
						} else {
							$_onPageLoad .= $_eaScript . "\n";
						}
					}
				}
			} elseif (strlen(trim($_eaStep)) > 0) {
				if ($_idx == 0) { //initial level
					$_strReturn .= $_eaStep . "\n";
				} else {
					$_onPageLoad .= $_eaStep . "\n";					
				}
			}
		}
		if (strlen(trim($_onPageLoad)) > 0) $_strReturn .= "\t\t$(function() {\n" . $_onPageLoad . "\n});\n";
		return $_strReturn;
	}
	
	function _get_headers() {
		//++ Auto Search Control
		if (isset($this->_DISABLE_ON_LOAD_SEARCH) && ($this->_DISABLE_ON_LOAD_SEARCH == TRUE)) {
			$this->_onload_scripts[3] .= "\t\tif (typeof doPopulateTable == 'function') doPopulateTable([], true);\n";
		} else {
			$this->_onload_scripts[3] .= "\t\t$('#btnSearch').trigger('click');\n";
		}
		//-- Auto Search Control

		$str = '';
		$this->__prepareHeaderFiles();
		foreach($this->_header_css['file'] AS $item) {
			$str .= '<link rel="stylesheet" href="'.$item.'" charset="utf-8" type="text/css" />'."\n";
		}
		$_files = $this->_getOnloadFiles_CSS();
		if (! empty(trim($_files))) $str .= $_files;
		
		if (count($this->_header_css['ie6']) > 0) {
			$str .= '<!--[if IE 6]>'."\n";
			foreach($this->_header_css['ie6'] AS $item) {
				$str .= '<link rel="stylesheet" href="'.$item.'" media="screen" charset="utf-8" type="text/css" />'."\n";
			}
			$str .= '<![endif]-->'."\n";
		}
		if (count($this->_header_css['ie']) > 0) {
			$str .= '<!--[if IE]>'."\n";
			foreach($this->_header_css['ie'] AS $item) {
				$str .= '<link rel="stylesheet" href="'.$item.'" media="screen" charset="utf-8" type="text/css" />'."\n";
			}
			$str .= '<![endif]-->'."\n";
		}
		if (count($this->_header_css['custom']) > 0) {
			$str .= '<style type="text/css" media="screen">'."\n";
			foreach($this->_header_css['custom'] AS $item) {
				$str .= $item."\n";
			}
			$str .= '</style>'."\n";
		}

		foreach($this->_header_js['file'] AS $item){
			$str .= '<script type="text/javascript" src="' . $item . '" charset="utf-8"></script>'."\n";
		}
		$_files = $this->_getOnloadFiles_JS();
		if (! empty(trim($_files))) $str .= $_files;
		
		if (count($this->_header_js['ie6']) > 0) {
			$str .= '<!--[if IE 6]>'."\n";
			foreach($this->_header_js['ie6'] AS $item){
				$str .= '<script type="text/javascript" src="'.$item.'" charset="utf-8"></script>'."\n";
			}
			$str .= '<![endif]-->'."\n";
		}

		if (count($this->_header_js['ie']) > 0) {
			$str .= '<!--[if IE]>'."\n";
			foreach($this->_header_js['ie'] AS $item){
				$str .= '<script type="text/javascript" src="'.$item.'" charset="utf-8"></script>'."\n";
			}
			$str .= '<![endif]-->'."\n";
		}
		$str .= '<script type="text/javascript" charset="utf-8">'."\n";
		if (count($this->_header_js['custom']) > 0) {
			foreach($this->_header_js['custom'] AS $item){
				$str .= $item."\n";
			}			
		}
		$_script = $this->_getOnloadScript();
		if (strlen(trim($_script)) > 0) $str .= $_script;
		$str .= '</script>'."\n";

		return $str;
	}
	
	function _blnCheckRight($name = '', $group = NULL) {
		/*++ always return TRUE in test period */
		if (isset($this->_is_test) && ($this->_is_test === TRUE)) return TRUE;
		/*-- always return TRUE in test period */
		$_group_name = ( ! empty($group) )?strtolower($group):$this->_AC_NAME;
		return $this->_AC->blnCheckRight($name, $_group_name);
	}
	
	function _serviceCheckRight($param) {
		$_blnAllow = FALSE;
		if (is_bool($param)) {
			$_blnAllow = (boolean)$param;
		} elseif (is_string($param)) {
			$_blnAllow = $this->_blnCheckRight($param);
		}

		if ($_blnAllow == FALSE) {
			$_strPref = 'User "' . $this->session->userdata('user_name') . '"';
			$_json = json_encode(
				array(
					"success" => FALSE,
					"error" => str_replace('v_XX_1', $_strPref, $this->_AC->_MSG_FUNCTION_NOT_ALLOWED)
				)
			);
			header('content-type: application/json; charset=utf-8');
			echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$_json.")" : $_json;
			exit;
		}
	}

	function __getAjaxPostParams() {
		$json_input_data = json_decode(trim(file_get_contents('php://input')), true); //get json
		$_arrData = (isset($json_input_data))?$json_input_data:$this->input->post(); //or post data submit
		if (is_array($_arrData)) {
			return $_arrData;
		} else {
			return FALSE;
		}
	}
	
	function ___performGetControlElements($_strFormSelector, $_strControlBaseType, &$params, &$_arrControls) {
		$_indx = 0;
		$_blnUploadImage = FALSE;
		$_blnJSPNButton = FALSE;
		foreach ($params['controls'] as $_ctrl) {
			if (! is_array($_ctrl)) continue;
			$_indx += 1;
			$_type = array_key_exists('type', $_ctrl)?$_ctrl['type']:'txt';
			$_label = array_key_exists('label', $_ctrl)?$_ctrl['label']:'';
			$_name = array_key_exists('name', $_ctrl)?$_ctrl['name']:'';
			$_val = array_key_exists('value', $_ctrl)?$_ctrl['value']:'';
			$_class = $_strControlBaseType;
			$_class .= array_key_exists('class', $_ctrl)?' ' . $_ctrl['class']:'';
			$_class .= array_key_exists('add_class', $_ctrl)?' ' . $_ctrl['add_class']:'';
			$_maxlength = '';
			$_size = '';
			$_specStyle = '';
			$_data = ' data="' . $_name . '"';
			$_placeHolder = '';
			$_input_elem = '';
			if (array_key_exists('style', $_ctrl)) $_specStyle .= trim($_ctrl['style']);
			if (array_key_exists('add_style', $_ctrl)) $_specStyle .= trim($_ctrl['add_style']);
			if (strlen($_specStyle) > 0) {
				if (strpos($_specStyle, 'style="') !== 0) $_specStyle = 'style="' . str_replace(array("style=", '"', "'"), '', $_specStyle) . '" ';
			}
			if (array_key_exists('maxlength', $_ctrl)) {
				$_set_length = intval($_ctrl['maxlength']);
				if ($_set_length > 0) {
					$_maxlength = ' maxlength="' . $_set_length . '"';
					if (!(($_type == 'chk') || ($_type == 'rdo') || ($_type == 'hdn'))) { //textbox, select
						$_size = ' size="' . $_set_length . '"';
					}
				}
			}
			if (isset($_ctrl['placeHolder']) || isset($_ctrl['place_holder'])) {
				$_placeHolder = (isset($_ctrl['placeHolder'])) ? $_ctrl['placeHolder'] : $_ctrl['place_holder'];
				$_placeHolder = ' placeholder="' . $_placeHolder . '"';
			} else {
				$_placeHolder = ($_label != '') ? ' placeholder="' . $_label . '"' : '';
			}
			switch ($_type) {
				case 'txt':
					$_input_elem = '<input type="text" id="' . $_type . '-' . $_name . '" value="' . $_val . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . $_placeHolder . $_data . '/>';
					break;
				case 'dpk': //date picker
					$_input_elem = '<input type="text" id="txt-' . $_name . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . $_placeHolder . $_data . '/>';
					$this->_onload_scripts[2] .= <<<EOT

		$('$_strFormSelector #txt-$_name').datepicker({
			showOn: "both",
			changeYear: true,
			changeMonth: true,
			buttonImage: "public/images/select_day.png",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy'
		});

EOT;
					if ((! empty($_val)) && ($_val instanceof Datetime)) $_val = $_val->format('Y-m-d');
					if (! empty($_val)) $this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').datepicker('setDate', new Date('" . $_val . "'));\n";
					if (array_key_exists('option', $_ctrl) || array_key_exists('options', $_ctrl)) {
						$_arrOptions;
						if (is_array($_ctrl['option'])) {
							$_arrOptions = $_ctrl['option'];
						}else if (is_array($_ctrl['options'])) {
							$_arrOptions = $_ctrl['options'];
						}
						if (isset($_arrOptions)) {
							foreach ($_arrOptions as $_eao=>$_eav) {
								$this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').datepicker('option', '" . $_eao . "', '" . $_eav . "');\n";
							}
						}
					}
					break;
				case 'mpk': //month picker
					$_input_elem = '<input type="text" id="txt-' . $_name . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . $_placeHolder . $_data . ' />';
					$this->_onload_scripts[2] .= <<<EOT

		$('$_strFormSelector #txt-$_name').datepicker({
				showOn: "both",
				changeYear: true,
				changeMonth: true,
				buttonImage: "public/images/select_day.png",
				buttonImageOnly: true,
				dateFormat: 'mm/yy',
				showButtonPanel: true,
				beforeShow: function(input, inst) {
					var _txtDate = input.value || '';
					if ((_txtDate.length > 0) && (_txtDate.indexOf('/') > 0)) {
						var _arr = _txtDate.split("/");
						if (_arr.length == 2) {
							var _month = parseInt(_arr[0]);
							var _year = parseInt(_arr[1]);
							if ((_month >= 0) && (_year > 0)) {
								$("#ui-datepicker-div .ui-datepicker-month").val(_month - 1);
								$("#ui-datepicker-div .ui-datepicker-year").val(_year);
							}
						}
					}
				},
				onChangeMonthYear: function (year, month, inst) {
					var _strDate = $.datepicker.formatDate('mm/yy', new Date(year, month - 1, 1));
					$(this).val(_strDate);
					$(".ui-datepicker-calendar").hide();
				}
			}).focus(function () {
				$(".ui-datepicker-calendar").hide();
				$("#ui-datepicker-div").position({
					my: "center bottom",
					at: "center top",
					of: $(this)
				});
			});

EOT;
					if ((! empty($_val)) && ($_val instanceof Datetime)) $_val = $_val->format('Y-m-d');
					if (! empty($_val)) $this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').datepicker('setDate', new Date('" . $_val . "'));\n";
					if (array_key_exists('option', $_ctrl) || array_key_exists('options', $_ctrl)) {
						$_arrOptions;
						if (is_array($_ctrl['option'])) {
							$_arrOptions = $_ctrl['option'];
						}else if (is_array($_ctrl['options'])) {
							$_arrOptions = $_ctrl['options'];
						}
						if (isset($_arrOptions)) {
							foreach ($_arrOptions as $_eao=>$_eav) {
								$this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').datepicker('option', '" . $_eao . "', '" . $_eav . "');\n";
							}
						}
					}
					break;
				case 'dtp': //date_time picker
					$_input_elem = '<input type="text" id="txt-' . $_name . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . $_placeHolder . $_data . ' />';
					if (! $this->__isExistsJs('public/js/jquery/ui/timepicker/1.6.1/jquery-ui-timepicker-addon.min.js')) {
						$this->add_onload_css_file('public/css/jquery/ui/timepicker/1.6.1/jquery-ui-timepicker-addon.min.css');
						$this->add_onload_js_file('public/js/jquery/ui/timepicker/1.6.1/jquery-ui-timepicker-addon.min.js');
						$this->add_onload_js_file('public/js/jquery/ui/timepicker/1.6.1/jquery-ui-sliderAccess.js');
					}
					$this->_onload_scripts[2] .= <<<EOT

		$('$_strFormSelector #txt-$_name').datetimepicker({
				dateFormat: 'dd/mm/yy'
				,timeFormat: 'HH:mm'
				,timeInput: true
				,stepMinute: 10
				,maxDate: new Date()
			});

EOT;
					if ((! empty($_val)) && ($_val instanceof Datetime)) $_val = $_val->format('Y-m-d');
					if (! empty($_val)) $this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').datepicker('setDate', new Date('" . $_val . "'));\n";
					if (array_key_exists('option', $_ctrl) || array_key_exists('options', $_ctrl)) {
						$_arrOptions;
						if (is_array($_ctrl['option'])) {
							$_arrOptions = $_ctrl['option'];
						}else if (is_array($_ctrl['options'])) {
							$_arrOptions = $_ctrl['options'];
						}
						if (isset($_arrOptions)) {
							foreach ($_arrOptions as $_eao=>$_eav) {
								$this->_onload_scripts[2] .= "\n\t\t" . "$('" . $_strFormSelector . ' #txt-' . $_name . "').Timepicker('option', '" . $_eao . "', '" . $_eav . "');\n";
							}
						}
					}
					break;
				case 'sel':
					if (! $this->__isExistsJs('public/js/jquery/ui/1.10.3/jquery-ui-autocomplete-combobox.js')) $this->add_onload_js_file('public/js/jquery/ui/1.10.3/jquery-ui-autocomplete-combobox.js');
					$_sel_val = 'rowid';
					$_sel_text = 'name';
					$_sel_attr = array();
					if (array_key_exists('sel_val', $_ctrl)) $_sel_val = $_ctrl['sel_val'];
					if (array_key_exists('sel_text', $_ctrl)) $_sel_text = $_ctrl['sel_text'];
					if (array_key_exists('sel_attr', $_ctrl)) $_sel_attr = $_ctrl['sel_attr'];
					$_input_elem = '<select id="' . $_type . '-' . $_name . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . ' sel_val="' . $_sel_val . '" sel_text="' . $_sel_text . '"' . $_placeHolder . $_data . '>';
					if (array_key_exists('sel_options', $_ctrl) && (is_array($_ctrl['sel_options']))) {
						foreach ($_ctrl['sel_options'] as $_opt) {
							$_dummyText = isset($_opt[$_sel_text])?$_opt[$_sel_text]:'&nbsp;';
							$_dummyVal = isset($_opt[$_sel_val])?$_opt[$_sel_val]:'';
							$_input_elem .= '<option value="'.$_dummyVal.'" ';
							if (is_array($_sel_attr)) {
								foreach($_sel_attr as $_ea) {
									if (isset($_opt[$_ea])) $_input_elem .= ''.$_ea.'="'.$_opt[$_ea].'" ';
								}
							} else {
								if (isset($_opt[$_ea])) $_input_elem .= ''.$_ea.'="'.$_opt[$_ea].'" ';
							}
							if (($_val != '') && ($_dummyVal == $_val)) $_input_elem .= 'selected ';
							$_input_elem .= '>' . $_dummyText . '</option>';
						}
					}
					$_input_elem .= '</select>';
					$this->_onload_scripts[2] .= "\t$('" . $_strFormSelector . " #" . $_type . '-' . $_name . "').combobox({changed: function(str, ev, ui) { ";
					if (array_key_exists('hidden_name', $_ctrl)) {
						$_input_elem .= '<input type="hidden" id="hdn-' . $_ctrl['hidden_name'] . '" class="user-input no-validate" />';
						$this->_onload_scripts[2] .= " $('" . $_strFormSelector . " #hdn-" . $_ctrl['hidden_name'] . "').val(str);";
					}
					if (array_key_exists('on_change', $_ctrl)) {
						$this->_onload_scripts[2] .= $_ctrl['on_change'];
					}
					$this->_onload_scripts[2] .= "return false; }";
					if (array_key_exists('before_change', $_ctrl)) {
						$this->_onload_scripts[2] .= ', blnBeforeChange: function(str, ev, ui) { return ' . $_ctrl['before_change'] . ' }';
					}
					$this->_onload_scripts[2] .= "});\n";
					break;
				case 'aac': //Ajax auto complete
					$_url = './' . $_name . '/json_search';
					$_sel_val = 'rowid';
					$_sel_text = 'name';
					$_min_length = 3;
					$_on_select = <<<OSL

			, select: function(event, ui) {
				var _aac_text = ui.item.value || '';
				var _aac_hdn_val = ui.item.hdn_value || '';
				_aac_text = _aac_text.toString().trim();
				_aac_hdn_val = _aac_hdn_val.toString().trim();
				$('$_strFormSelector #hdn-$_name').val(_aac_hdn_val);

OSL;
					if (array_key_exists('url', $_ctrl)) $_url = $_ctrl['url'];
					if (array_key_exists('sel_val', $_ctrl)) $_sel_val = $_ctrl['sel_val'];
					if (array_key_exists('sel_text', $_ctrl)) $_sel_text = $_ctrl['sel_text'];
					if (array_key_exists('min_length', $_ctrl) && is_int($_ctrl['min_length'])) $_min_length = (int) $_ctrl['min_length'];
					if (array_key_exists('on_select', $_ctrl)) $_on_select .= $_ctrl['on_select'];
					$_input_elem = '<input type="text" id="' . $_type . '-' . $_name . '" value="' . $_val . '" class="ajax-autocomplete ' . $_class . '"' . $_maxlength . $_size . $_specStyle . '  sel_val="' . $_sel_val . '" sel_text="' . $_sel_text . '"' . $_placeHolder . $_data . '/>';
					$_input_elem .= '<input type="hidden" id="hdn-' . $_name . '" class="' . $_class . '" />';

					$_on_select .= "\n}";

					$_jqSel = $_strFormSelector . " #aac-" . $_name;
					
					$this->_onload_scripts[2] .= <<<AAC

		$('$_jqSel').autocomplete({
			delay : 0,
			source: function( request, response ) {
				$.ajax({
					dataType: "json"
					, type : 'POST'
					, data:{
						"$_sel_text": request.term
					}
					, url: '$_url'
					, success: function(data) {
						$('$_jqSel').removeClass('ui-autocomplete-loading');
						if ((data.success == true) && (data.data.length > 0)) {
							var data = data.data;
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							var _resp = $.map( data, function(item) {
									_text = item.$_sel_text;
									if ( !request.term || matcher.test(_text) ) return {
											label: _text.replace(
												new RegExp(
													"(?![^&;]+;)(?!<[^<>]*)(" +
													$.ui.autocomplete.escapeRegex(request.term) +
													")(?![^<>]*>)(?![^&;]+;)", "gi"
												), "<strong>$1</strong>" ),
											value: _text,
											hdn_value: item.$_sel_val
										};
								})
							if (_resp.length > 0) _resp.unshift({label:"&nbsp;", value:"", hdn_value:""});
							response(_resp);
						}
					}
					, error: function(data) {
						$('$_jqSel').removeClass('ui-autocomplete-loading');  
					}
				});
			}
			, minLength: $_min_length
$_on_select
		});

AAC;
/*
			, open: function() {
			}
			, close: function() {
			}
			, focus:function(event,ui) {
			}
*/
					break;
				case 'txa':
					$_input_elem = '<textarea id="' . $_type . '-' . $_name . '" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . $_placeHolder . $_data . '>' . $_val . '</textarea>';
					break;
				case 'hdn':
					$_label = 'hidden';
					$_input_elem = '<input type="hidden" id="' . $_type . '-' . $_name . '" value="' . $_val . '" class="' . $_class . ' no-validate"' . $_data . '/>';
					break;
				case 'chk':
					$_input_elem = '<input type="checkbox" id="'. $_type . '-' . $_name .'" ';
					if ($_val !== '') $_input_elem .= 'checked';
					$_input_elem .= ' class="' . $_class . '" ' . $_data . '/>';
					break;
				case 'rdo':
					if (array_key_exists('sel_options', $_ctrl) && (is_array($_ctrl['sel_options']))) {
						$_sel_val = 'rowid';
						$_sel_text = 'name';
						if (array_key_exists('sel_val', $_ctrl)) $_sel_val = $_ctrl['sel_val'];
						if (array_key_exists('sel_text', $_ctrl)) $_sel_text = $_ctrl['sel_text'];
						foreach ($_ctrl['sel_options'] as $_opt) {
							if (isset($_opt[$_sel_text])) {
								$_id = $_type . '-' . $_name . '_' . $_opt[$_sel_val];
								$_input_elem .= '<input type="radio" id="' . $_id . '" value="' . $_opt[$_sel_val] . '" ';
								if (($_val != '') && ($_opt[$_sel_val] == $_val)) $_input_elem .= 'checked ';
								$_input_elem .= 'name="' . $_name . '" class="' . $_class . '" ' . $_data . '>';
								$_input_elem .= '<label class="cls-radio-label" for="' . $_id . '" >' . $_opt[$_sel_text] . '</label>';
							}
						}
					}
					break;
				case 'msl': //multiselect
					if (! $this->__isExistsJs('public/js/jquery/ui/multiselect/1.13/jquery.multiselect.min.js')) {
						$this->add_onload_css_file('public/css/jquery/ui/multiselect/1.13/jquery.multiselect.css');
						$this->add_onload_js_file('public/js/jquery/ui/multiselect/1.13/jquery.multiselect.min.js');
					}
					$_sel_val = 'rowid';
					$_sel_text = 'name';
					$_sel_attr = array();
					$_arrAddOptions = array("selectedList"=>2, "height"=>"auto", "uncheckAllText"=>"Clear");
					if (array_key_exists('sel_val', $_ctrl)) $_sel_val = $_ctrl['sel_val'];
					if (array_key_exists('sel_text', $_ctrl)) $_sel_text = $_ctrl['sel_text'];
					if (array_key_exists('sel_attr', $_ctrl)) $_sel_attr = $_ctrl['sel_attr'];
					if (array_key_exists('add_options', $_ctrl) && (is_array($_ctrl['add_options']))) $_arrAddOptions = array_merge($_arrAddOptions, $_ctrl['add_options']);
					$_input_elem = '<select id="' . $_type . '-' . $_name . '" multiple="multiple" class="' . $_class . '"' . $_maxlength . $_size . $_specStyle . ' sel_val="' . $_sel_val . '" sel_text="' . $_sel_text . '">';
					if (array_key_exists('sel_options', $_ctrl) && (is_array($_ctrl['sel_options']))) {
						foreach ($_ctrl['sel_options'] as $_opt) {
							$_dummyText = isset($_opt[$_sel_text])?$_opt[$_sel_text]:'&nbsp;';
							$_dummyVal = isset($_opt[$_sel_val])?$_opt[$_sel_val]:'';
							$_input_elem .= '<option value="'.$_dummyVal.'" ';
							if (is_array($_sel_attr)) {
								foreach($_sel_attr as $_ea) {
									if (isset($_opt[$_ea])) $_input_elem .= ''.$_ea.'="'.$_opt[$_ea].'" ';
								}
							} else {
								if (isset($_opt[$_ea])) $_input_elem .= ''.$_ea.'="'.$_opt[$_ea].'" ';
							}
							if (($_val != '') && ($_dummyVal == $_val)) $_input_elem .= 'selected ';
							$_input_elem .= '>' . $_dummyText . '</option>';
						}
					}
					$_input_elem .= '</select>';
					$_strAddOpts = '';
					foreach ($_arrAddOptions as $_key=>$_val) {
						$_strAddOpts .= '"'.$_key.'":';
						if (is_numeric($_val)) {
							$_strAddOpts .= $_val.',';
						} else {
							$_strAddOpts .= '"'.(string)$_val.'",';
						}
					}
					if (strlen(trim($_strAddOpts)) > 0) {
						$_strAddOpts = "{".substr($_strAddOpts, 0, -1)."}";
					} else {
						$_strAddOpts = "";
					}
					$this->_onload_scripts[2] .= "\t$('" . $_strFormSelector . " #" . $_type . '-' . $_name . "').multiselect(" . $_strAddOpts . ");\n";
					$this->_onload_scripts[2] .= "\t$($('" . $_strFormSelector . " #" . $_type . '-' . $_name . "').multiselect('getButton')).css('width', '');\n";
					$this->_onload_scripts[2] .= "\t$($('" . $_strFormSelector . " #" . $_type . '-' . $_name . "').multiselect('widget')).css('width', 'auto');\n";
					break;
				case 'fmg':
					$_input_elem = <<<DIV

		<div class="div-disp-img-upload display-upload" id="div_fmg_{$_name}">
			<div class="input-controller fmg-controller eventView-hide" title="remove image, prevent add/edit.">
				<input type="checkbox" class="fmg-no-image input-controller no-commit no-validate"><label>remove</label>
			</div>
			<span class="spn-image-select" {$_maxlength}{$_size}{$_specStyle} title="add/edit [$_label]">
				add/edit [$_label]
				<input type="file" id="fmg-{$_name}" name="image" class="input-file-upload {$_class}" {$_data}/>
			</span>
			<input type="hidden" id="hdn-{$_name}" class="fmg-value"/>
		</div>

DIV;
					$_blnUploadImage = TRUE;
					$_label = 'hidden';
					break;								
				case 'spn':
					$_input_elem = '<span id="' . $_type . '-' . $_name . '" class="' . $_class . ' no-validate" ' . $_maxlength . $_size . $_specStyle . $_data . '></span>';
					break;
				case 'div':
					$_html = '';
					if (array_key_exists('html', $_ctrl)) $_html = $_ctrl['html'];
					$_input_elem = '<div id="' . $_type . '-' . $_name . '" class="' . $_class . ' no-validate" ' . $_maxlength . $_size . $_specStyle . $_data . '>' . $_html . '</div>';
					break;
				case 'info':
					$_name = 'dummy_' . $_indx;
					$_label = 'hidden';
					$_input_elem = '<tr><td colspan="3"><span class="cls-info">' . $_val . '</span></td></tr>';
					break;
			}
			if (strpos($_class, 'jspn-addable')) {
				$_dummy = $_name;
				if ((strlen($_dummy) > 6) && (substr($_dummy, -5) == 'rowid')) $_dummy = substr($_dummy, 0, -6);
				$_controller = $_dummy;
				if (array_key_exists('jspn-controller', $_ctrl)) $_controller = trim($_ctrl['jspn-controller']);
				$_input_elem .= '<img class="jspn-button" command="add" target="self" for="' . $_type . '-' . $_name . '" controller="' . $_controller . '" src="./public/images/icons/16/plus.png" title="' . $_label . '"/>';
				$_blnJSPNButton = TRUE;
			}
			if (strpos($_class, 'set-disabled')) {
				$_dummyId = '';
				if (($_type == 'dtp') || ($_type == 'dpk') || ($_type == 'mpk')) {
					$_dummyId = 'txt-' . $_name;
				} else {
					$_dummyId = $_type . '-' . $_name;
				}
				$this->_onload_scripts[1] .= "\t\t_setEnableElem($('" . $_strFormSelector . " #" . $_dummyId . "'), false);\n";
			}
			$_arrControls[$_name] = array($_label, $_input_elem, $_type, $_name);
		}
/*
		if ($_blnUploadImage) {
			$_dummy = <<<DIV

		<form id="frm_upload_image" action="upload_temp_image" method="post" enctype="multipart/form-data">
			<input type="hidden" id="element_id" name="element_id" >
		</form>

DIV;
			//array_push($this->_arrAttachHTML, $_dummy);
		}
*/
		if ($_blnJSPNButton) {
			$this->add_onload_js_file('public/js/_public/_jspn_dialog.js');
			$this->_onload_scripts[1] .= "\t\t_doBindJSPNButton($('$_strFormSelector'));\n";
		}
	}
}
//---------------------------------------------------

include('MY_Ctrl_crud.php');
include('MY_Ctrl_crud_jspn.php');