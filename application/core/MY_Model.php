<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
	var $_TABLE_NAME = '';
	var $_AUTO_FIELDS = array();
	var $_FIELDS = array();
	var $_DATE_FIELDS = array();
	var $_DATETIME_FIELDS = array();
	var $_JSON_FIELDS = array();
	var $last_insert_id = 0;
	var $error_message = '';
	var $error_number = 0;

	function __construct() {
        parent::__construct();
		if (! isset($this->db)) {
			$this->db = $this->load->database('default', TRUE);
		}
	}

	function search($arrObj = array()) {
		$_sql = <<<EOT
SELECT t.* 
FROM {$this->_TABLE_NAME} AS t 
ORDER BY t.rowid DESC
EOT;
		return $this->arr_execute($_sql);
	}

	function list_all() {
		$_result = $this->db->from($this->_TABLE_NAME)->get()->result_array();
		$this->error_message = $this->db->_error_message();
		$this->error_number = $this->db->_error_number();

		if (is_array($_result)) {
			$_items = array();
			$_arrObj = new ArrayObject($this->_FIELDS);
			foreach ($_result as $_obj) {
				$_row = $_arrObj->getArrayCopy();
				$_row = array_merge($_row, $_obj);
				$_items[] = $_row;
			}
			return $_items;
		} else {
			return FALSE;
		}
	}

	function get_by_id($RowID) {
		$_result = $this->db->get_where($this->_TABLE_NAME, array('rowid' => $RowID))->row();
		$this->error_message = $this->db->_error_message();
		$this->error_number = $this->db->_error_number();
		if ($_result == FALSE) { 
			return FALSE;
		} else {
			$_item = array();
			foreach ($_result as $_key => $_value) {
				$_item[$_key] = $_value;
			}
		}
		return $_item;
	}

	function commit($arrObj, $blnUpdateEmpty = TRUE) {
		foreach ($arrObj as $key=>$value) {
			if (array_key_exists($key, $this->_FIELDS)) {
				$this->_FIELDS[$key] = $value;
			}
		}
		$_blnIsInsert = TRUE;
		if (array_key_exists('rowid', $arrObj) && (trim($arrObj['rowid']) > 0)) $_blnIsInsert = FALSE;
		if ($_blnIsInsert) {
			foreach ($this->_AUTO_FIELDS as $key=>$value) {
				$this->_AUTO_FIELDS[$key] = null;
			}
			return $this->insert();
		} else {
			return $this->update(array('rowid' => $arrObj['rowid']), $blnUpdateEmpty);
		}
	}
	
	function insert() {
		$blnHasData = FALSE;
		foreach ($this->_FIELDS as $key=>$value) {
			switch (strtolower($key)) {
				case 'create_by':
					if ($this->_FIELDS['create_by'] == '') { 
						$this->db->set($key, $this->session->userdata('user_id'));
					} else {
						$this->db->set($key, $this->_FIELDS['create_by']);
					}
					break;
				case 'create_date':
					$this->db->set($key, 'CURRENT_TIMESTAMP', FALSE);
					break;
				case 'update_by':
					break;
				case 'update_date':
					break;
				default:
					if (is_string($value)) $value = trim($value);
					if (empty($value) && ($value != '0')) {
						$this->db->set($key, NULL);
					} else {
						$blnHasData = TRUE;
						if (in_array($key, $this->_DATE_FIELDS)) {
							if ($value instanceof DateTime) {
								$dat_value = $value;
							} else {
								$dat_value = $this->_datFromPost($value);
							}
							if ($dat_value instanceof DateTime) $this->db->set($key, "'" . $dat_value->format('Y/m/d')."'::DATE", FALSE);
						} elseif (in_array($key, $this->_DATETIME_FIELDS)) {
							if ($value instanceof DateTime) {
								$dat_value = $value;
							} else {
								$dat_value = $this->_datFromPost($value);
							}
							if ($dat_value instanceof DateTime) $this->db->set($key, "'" . $dat_value->format('Y/m/d H:i:s')."'::TIMESTAMP WITHOUT TIME ZONE", FALSE);
						} elseif ($value instanceof DateTime) {
							$this->db->set($key, "'" . $value->format('Y/m/d') . "'::DATE", FALSE);
						} elseif (in_array($key, $this->_JSON_FIELDS)) {
							$this->db->set($key, "'" . $value . "'::JSON", FALSE);
						} else {
							$this->db->set($key, $value);
						}
					}
					break;
			}
		}
		if ($blnHasData) {
//$_obj = $this->db->ar_set;echo $this->db->insert_string($this->_TABLE_NAME, $_obj);exit;
			try {
				$this->db->insert($this->_TABLE_NAME);
				$this->error_message = $this->db->_error_message();
				$this->error_number = $this->db->_error_number();
				$this->last_insert_id = $this->db->insert_id();
				return $this->last_insert_id;
			} catch (Exception $e) {
				$this->error_message = $e->getMessage();
				$this->error_number = $e->getCode();
				$this->last_insert_id = -1;
				return FALSE;
			}
		} else {
			$this->error_message = 'No value';
			$this->error_number = 701;
			return FALSE;
		}
	}
	
	function update($arrWhere, $blnUpdateEmpty = TRUE) {
		$blnHasData = FALSE;
		foreach ($this->_FIELDS as $key=>$value) {
			switch (strtolower($key)) {
				case 'create_by':
					break;
				case 'create_date':
					break;
				case 'update_by':
					if ($this->_FIELDS['update_by'] == '') { 
						$this->db->set($key, $this->session->userdata('user_id'));
					} else {
						$this->db->set($key, $this->_FIELDS['update_by']);
					}
					break;
				case 'update_date':
					$this->db->set($key, 'CURRENT_TIMESTAMP', FALSE);
					break;
				default:
					if (is_string($value)) $value = trim($value);
					if (empty($value) && ($value != '0')) {
						if ($blnUpdateEmpty) {
							$this->db->set($key, NULL);
							$blnHasData = TRUE;
						}
					} else {
						$blnHasData = TRUE;
						if (in_array($key, $this->_DATE_FIELDS)) {
							if ($value instanceof DateTime) {
								$dat_value = $value;
							} else {
								$dat_value = $this->_datFromPost($value);
							}
							if ($dat_value instanceof DateTime) $this->db->set($key, "'" . $dat_value->format('Y/m/d')."'::DATE", FALSE);
						} elseif (in_array($key, $this->_DATETIME_FIELDS)) {
							if ($value instanceof DateTime) {
								$dat_value = $value;
							} else {
								$dat_value = $this->_datFromPost($value);
							}
							if ($dat_value instanceof DateTime) $this->db->set($key, "'" . $dat_value->format('Y/m/d H:i:s')."'::TIMESTAMP WITHOUT TIME ZONE", FALSE);
						} elseif ($value instanceof DateTime) {
							$this->db->set($key, "'" . $value->format('Y/m/d') . "'::DATE", FALSE);
						} elseif (in_array($key, $this->_JSON_FIELDS)) {
							$this->db->set($key, "'" . $value . "'::JSON", FALSE);
						} else {
							$this->db->set($key, $value);
						}
					}
					break;
			}
		}
		try {
			$this->last_insert_id = -1;
			$this->db->where($arrWhere);
			$this->db->update($this->_TABLE_NAME);
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
			return $this->db->affected_rows();
		} catch (Exception $e) {
			$this->error_message = $e->getMessage();
			$this->error_number = $e->getCode();
			return FALSE;
		}
	}

	function cancel($RowID) {
		if ((array_key_exists('rowid', $this->_AUTO_FIELDS) || array_key_exists('rowid', $this->_FIELDS)) && array_key_exists('is_cancel', $this->_FIELDS)) {
			if (array_key_exists('update_by', $this->_FIELDS)) $this->db->set('update_by', $this->db->escape((int) $this->session->userdata('user_id')));
			if (array_key_exists('update_date', $this->_FIELDS)) $this->db->set('update_date', 'CURRENT_TIMESTAMP', FALSE);
			$this->db->set('is_cancel', 1);
			$this->db->where(array('rowid' => $this->db->escape((int) $RowID)));
			$this->db->update($this->_TABLE_NAME);
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
			return $this->db->affected_rows();
		} else {
			$this->error_message = 'Default "cancel" function error: columns "rowid" and "is_cancel" are not exists.';
			$this->error_number = 91;
			return FALSE;
		}
	}

	function delete($RowID) {
		if ((array_key_exists('rowid', $this->_AUTO_FIELDS) || array_key_exists('rowid', $this->_FIELDS))) {
			$this->db->delete($this->_TABLE_NAME, array('rowid' => $this->db->escape((int) $RowID)));
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
			return $this->db->affected_rows();
		} else {
			$this->error_message = 'Default "delete" function error: column "rowid" is not exists.';
			$this->error_number = 90;
			return FALSE;
		}
	}
	
    //escapes and adds single quotes
    //to each value of an array
    function safe_escape(&$data) {
		if (is_array($data)) {
			foreach($data as $node) {
				$node = $this->db->escape($node);
			}
		} 
		return $data;
	}

	function SP_execute($sp, $data = '') {
		//$CI = get_instance();
		//$CI->firephp->log($data);
		$_return;
		if ($data !== '') {
			$params = '';
			if (is_array($data))
			{
				for ($i=0;$i<count($data);$i++)
				{
					$params .= '?, ';
				}
				$params = substr($params, 0, -2);
			} 
			else
			{
				$params = '?';
			}
			//$CI->firephp->log($params);
			$_return = $this->db->query('select * from '.$sp.' ('.$params.')', $this->safe_escape($data));
		} else {
			$_return = $this->db->query('select * from '.$sp.'()');		
		}
		$this->error_message = $this->db->_error_message();
		$this->error_number = $this->db->_error_number();
		return $_return;
	}

	function arr_execute($sql, $params = NULL) {
		$result;
		try {
			if (isset($params) && is_array($params)) {
				$result = $this->db->query($sql, $params)->result();			
			} else {
				$result = $this->db->query($sql)->result();
			}
			$this->error_message = $this->db->_error_message();
			$this->error_number = $this->db->_error_number();
		} catch (Exception $e) {
			$this->error_message = $e->getMessage();
			$this->error_number = $e->getCode();
		}
//echo $this->db->last_query();exit;
		$return = array();
		if ($result) {
			foreach($result as $row) {
				$rowTemp = array();
				foreach ($row as $key => $val) {
					$rowTemp[$key] = $val;
				}
				$return[] = $rowTemp;
			}
		}
		return $return;
	}
/*	++ Old procedure arr_execute 20141020
	{
		$result = $this->db->query($sql)->result();
		$return = false;
		if ($result) {
			foreach($result as $row) {
				$rowTemp = array();
				foreach ($row as $key => $val) {
					$rowTemp[$key] = $val;
				}
				$return[] = $rowTemp;
			}
		}
		$this->error_message = $this->db->_error_message();
		$this->error_number = $this->db->_error_number();
		return $return;
	}
*/

	function arr_SP_execute($sp, $data = '')
	{
		$result = $this->SP_execute($sp, $data)->result();
		$return = false;
		if ($result) {
			foreach($result as $row) {
				$rowTemp = array();
				foreach ($row as $key => $val) {
					$rowTemp[$key] = $val;
				}
				$return[] = $rowTemp;
			}
		}
		$this->error_message = $this->db->_error_message();
		$this->error_number = $this->db->_error_number();
		return $return;
	}

	function _strConvertDisplayDateFormat($dtDate = null) 
	{
		if ($dtDate == null) {
			//$dtDate = new DateTime();
			return '';
		} else {
			return $dtDate->format('Y/m/d');
		}
	}
	
	function _explode_trim($delimiter, $str) { 
		if ( is_string($delimiter) ) { 
			$str = trim(preg_replace('|\\s*(?:' . preg_quote($delimiter) . ')\\s*|', $delimiter, $str)); 
			return explode($delimiter, $str); 
		} 
		return $str; 
	}

	function _datFromPost($strDateText, $date_delimeter = "/", $datetime_seperater = ' ', $time_delimeter = ':') { //default ui format = dd/mm/yyyy
		$strDateText = $this->db->escape_str($strDateText);
		$_strDate = '';
		$_strTime = '';
		if (strpos($strDateText, $datetime_seperater) > 0) {
			$_arr = $this->_explode_trim($datetime_seperater, $strDateText);
			if (count($_arr) >= 2) {
				$_strDate = $_arr[0];
				$_strTime = $_arr[1];
			}
		} else {
			$_strDate = trim($strDateText);
		}
		$datDummy = new DateTime();
		if (strpos($_strDate, $date_delimeter) > 0) {
			$arr = $this->_explode_trim($date_delimeter, $_strDate);
			if (count($arr) == 2) { //only MM/YYYY
				$arr[2] = $arr[1];
				$arr[1] = $arr[0];
				$arr[0] = 1;
			}
			if ($arr[2] >= 2500) {
				$arr[2] -= 543;
			}
			if (checkdate($arr[1], $arr[0], $arr[2])) {
				$datDummy->setDate($arr[2], $arr[1], $arr[0]);
			}
			if (strpos($_strTime, $time_delimeter) > 0) {
				$_arr = $this->_explode_trim($time_delimeter, $_strTime);
				if (count($_arr) == 2) {
					$datDummy->setTime($_arr[0], $_arr[1]);
				} elseif (count($_arr) > 2) {
					$datDummy->setTime($_arr[0], $_arr[1], $_arr[2]);
				}
			}
			return $datDummy;
		}
		return '';
	}

	function _getSearchConditionSQL($arrParams, $arrSpec = array()) {
		$_retSql = ' ';
		$_arrEaSpec = FALSE;
		foreach ($arrParams as $_name=>$_value) {
			$_arrEaSpec = FALSE;
			if (array_key_exists($_name, $arrSpec) && is_array($arrSpec[$_name])) $_arrEaSpec = $arrSpec[$_name];
			$_dbcol = ($_arrEaSpec && (array_key_exists('dbcol', $_arrEaSpec)))?$_arrEaSpec['dbcol']:'t.'.$_name;
			$_val = ($_arrEaSpec && (array_key_exists('val', $_arrEaSpec)))?$_arrEaSpec['val']:$_value;
			$_oper = ($_arrEaSpec && (array_key_exists('operand', $_arrEaSpec)))?$_arrEaSpec['operand']:'=';
			$_type = 'txt';
			if ($_arrEaSpec && (array_key_exists('type', $_arrEaSpec))) {
				$_type = $_arrEaSpec['type'];
			} else {
				if (($_name == 'rowid') || (substr($_name, -6) == '_rowid')) {
					$_type = 'int';
				} elseif (substr($_name, -5) == '_date') {
					$_type = 'dat';
				}
			}
			switch (strtolower($_type)) {
				case 'txt':
					if ($_oper == '=') {
						$_retSql .= sprintf("AND %s ILIKE CONCAT('%%', '%s', '%%') ", $_dbcol, $this->db->escape_like_str($_val));
					} else {
						$_retSql .= sprintf("AND %s %s '%s' ", $_dbcol, $_oper, $this->db->escape($_val));
					}
					break;
				case 'int':
				case 'dbl':
					if (is_numeric($_val)) $_retSql .= sprintf("AND %s %s %d ", $_dbcol, $_oper, $_val);
					break;
				case 'dat':
					$_dat = $this->_datFromPost($_val);
					if (($_dat !== '') && ($_dat instanceof Datetime)) $_retSql .= sprintf("AND %s %s TO_DATE('%s', 'YYYYMMDD') ", $_dbcol, $_oper, $_dat->format('Ymd'));
					break;
				case 'dtm': //datetime
					$_dat = $this->_datFromPost($_val);
					if (($_dat !== '') && ($_dat instanceof Datetime)) $_retSql .= sprintf("AND %s %s TO_TIMESTAMP('%s', 'YYYYMMDD HH24:MI') ", $_dbcol, $_oper, $_dat->format('Ymd H:i'));
					break;
				case 'raw':
					$_retSql .= sprintf("AND %s %s %s ", $_dbcol, $_oper, $_val);
					break;
			}
		}
		return $_retSql;
	}

	function _getSearchConditionSQL_params(&$arrParams, $arrSearchParams, $arrSpec = array()) {
		$_retSql = ' ';
		$_arrEaSpec = FALSE;
		foreach ($arrSearchParams as $_name=>$_value) {
			$_arrEaSpec = FALSE;
			if (array_key_exists($_name, $arrSpec) && is_array($arrSpec[$_name])) $_arrEaSpec = $arrSpec[$_name];
			$_dbcol = ($_arrEaSpec && (array_key_exists('dbcol', $_arrEaSpec)))?$_arrEaSpec['dbcol']:'t.'.$_name;
			$_val = ($_arrEaSpec && (array_key_exists('val', $_arrEaSpec)))?$_arrEaSpec['val']:$_value;
			$_oper = ($_arrEaSpec && (array_key_exists('operand', $_arrEaSpec)))?$_arrEaSpec['operand']:'=';
			$_type = 'txt';
			if (($_arrEaSpec != FALSE) && (array_key_exists('type', $_arrEaSpec))) {
				$_type = $_arrEaSpec['type'];
			} else {
				if (($_name == 'rowid') || (substr($_name, -6) == '_rowid')) {
					$_type = 'int';
				} elseif (substr($_name, -5) == '_date') {
					$_type = 'dat';
				}
			}
			switch (strtolower($_type)) {
				case 'txt':
					if ($_oper == '=') {
						$_retSql .= sprintf("AND %s ILIKE CONCAT('%%', ?, '%%') ", $_dbcol);
						array_push($arrParams, $_val);
					} else {
						$_retSql .= sprintf("AND %s %s ? ", $_dbcol, $_oper);
						array_push($arrParams, $_val);
					}
					break;
				case 'int':
				case 'dbl':
					if (is_numeric($_val)) {
						$_retSql .= sprintf("AND %s %s ? ", $_dbcol, $_oper);
						array_push($arrParams, $_val);
					}
					break;
				case 'dat':
					$_dat = $this->_datFromPost($_val);
					if (($_dat !== '') && ($_dat instanceof Datetime)) {
						$_retSql .= sprintf("AND %s %s TO_DATE(?, 'YYYYMMDD') ", $_dbcol, $_oper);
						array_push($arrParams, $_dat->format('Ymd'));
					}
					break;
				case 'dtm': //datetime
					$_dat = $this->_datFromPost($_val);
					if (($_dat !== '') && ($_dat instanceof Datetime)) $_retSql .= sprintf("AND %s %s TO_TIMESTAMP('%s', 'YYYYMMDD HH24:MI') ", $_dbcol, $_oper, $_dat->format('Ymd H:i'));
					break;
				case 'raw':
					$_retSql .= sprintf("AND %s %s %s ", $_dbcol, $_oper, $_val);
					break;
			}
		}
		return $_retSql;
	}

	function _getCheckAccessRight($userColumn = '', $groupname = '') {
/*		$CI = & get_instance();
		$_user_col = (!empty($userColumn))?$userColumn:'t.create_by';
		$_user_id = $this->db->escape((int) $CI->session->userdata('user_id'));
		$_retSql = ' ';
		if ($CI->_blnCheckRight('list_all', $groupname) == FALSE) {
			if ($CI->_blnCheckRight('list_branch', $groupname) == TRUE) {
				$_retSql .= <<<BRA

AND $_user_col IN (
	SELECT dest_user
	FROM pm_v_user_group_by_branch
	WHERE source_user = $_user_id
)

BRA;
			} else {
				$_retSql .= "AND $_user_col = $_user_id ";			
			}
		}
		return $_retSql;
*/
	}
}
//-------------------------------------------------------

