<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Ctrl_crud_jspn extends MY_Ctrl_crud {
	function __construct() {
		parent::__construct();
		$this->_defaultBaseIndex = 0; //default base index used in jQuery select to identify correct elements and objects 
		$this->_pageOptions = array("type"=>"left_main");
	}

	public function index() {
		parent::index();
	}
/*
	function __prepareEditForm() { //for main panel
		$this->_prepareControlsDefault();
	}
	
	function __getEditForm($intFormIndex = -1) { // for main panel
		$_intFrmIndx = ($intFormIndex >= 0)?$intFormIndex:($this->_defaultBaseIndex + 2);
		$this->__prepareEditForm();
		return $this->add_view('_public/_form'
				, array(
					'index' => $_intFrmIndx
					,'crud_controller' => $this->page_name
					,'controls' => $this->_arrGetEditControls()
				),TRUE
			);
	}

	function __arrSearchControls() { //for left panel
		$_arrControls = array();
		$this->load->model($this->modelName, 'm');
		if (array_key_exists('code', $this->m->_FIELDS)) {
			array_push($_arrControls, array(
					"type" => "txt"
					, "label" => $this->_getDisplayLabel('code')
					, "name" => "code"
				));
			
		}
		if (array_key_exists('name', $this->m->_FIELDS)) {
			array_push($_arrControls, array(
					"type" => "txt"
					, "label" => $this->_getDisplayLabel('name')
					, "name" => "name"
				));
			
		}
		return $_arrControls;
	}
	
	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(($this->_defaultBaseIndex + 2));
		return $this->add_view(
			'_public/_list'
			,array(
				'index' => ($this->_defaultBaseIndex + 1)
				,'dataview_fields' => $this->_arrDataViewFields
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight) 
				)
			),TRUE
		);
	}

	function _getLeftPanel() { //override
		$_arrControls = $this->__arrSearchControls();
		if (is_array($_arrControls) && (count($_arrControls) > 0)) {
			return $this->add_view(
				'_public/_search_panel'
				,array(
					'controls' => $_arrControls
				),TRUE
			);
		} else {
			return '';
		}
	}
*/
	function __JSPN_getOnloadFiles_CSS() {
		$_str = "";
		$_strAppend = "";
		foreach ($this->_onload_css_files_append as $_eaFile) {
			$_i = array_search($_eaFile, $this->_onload_css_files);
			if ($_i !== FALSE) unset($this->_onload_css_files[$_i]);
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_css['file']))) $_strAppend .= '_doLoadJSPNScriptFile("'.trim($_eaFile).'", "css");'."\n";
		}
		foreach ($this->_onload_css_files as $_eaFile) {
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_css['file']))) $_str .= '_doLoadJSPNScriptFile("'.trim($_eaFile).'", "css");'."\n";
		}
		return $_str . "\n" . $_strAppend;
	}
	function __JSPN_getOnloadFiles_JS() {
		$_str = "";
		$_strAppend = "";
		foreach ($this->_onload_js_files_append as $_eaFile) {
			$_i = array_search($_eaFile, $this->_onload_js_files);
			if ($_i !== FALSE) unset($this->_onload_js_files[$_i]);
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_js['file']))) $_strAppend .= '_doLoadJSPNScriptFile("'.trim($_eaFile).'", "js");'."\n";
		}
		foreach ($this->_onload_js_files as $_eaFile) {
			if ((strlen(trim($_eaFile)) > 0) && (! in_array(trim($_eaFile), $this->_header_js['file']))) $_str .= '_doLoadJSPNScriptFile("'.trim($_eaFile).'", "js");'."\n";
		}
		return $_str . "\n" . $_strAppend;
	}

	function jspn_add($intCurrentIndex = -1) {
		$this->_serviceCheckRight(($this->_blnCheckRight('insert') || $this->_blnCheckRight('edit')));

		$_frmIndex = ($intCurrentIndex > 0)?($intCurrentIndex + 1):($this->_defaultBaseIndex + 3);		
		$_strCp = $this->__getEditForm($_frmIndex);

		$_str = $this->_getOnloadFiles_CSS();
		$_str .= $this->_getOnloadFiles_JS();
		
		$_script = $this->_getOnloadScript();
		if (strlen(trim($_script)) > 0) $_str .= <<<OLSCR

<script type="text/javascript" charset="utf-8">
$_script
</script>

OLSCR;

		$_strCp = $_str . $_strCp;
		
		$_arrJson['success'] = TRUE;
		$_arrJson['panel'] = array(
				"html" => $_strCp
				,"option" => array(
					"title" => "เพิ่มข้อมูล " . $this->_pageTitle
					,"width" => $this->_editPanelWidth
					,"height" => $this->_editPanelHeight
				)
			);
		$json = json_encode($_arrJson);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
	
	function commit_jspn_add() {
		$this->_serviceCheckRight('insert');
		$_arrJson = array("success"=>FALSE, "message"=>"initial");
		$_arr = $this->__getAjaxPostParams();
		if ($_arr != FALSE) {
			$this->load->model($this->modelName, 'm');
			$this->m->commit($_arr);
			$_added_rowid = $this->m->last_insert_id;
			if (($_added_rowid !== FALSE) && ($_added_rowid > 0)) {
				$_arrList = $this->m->search(); //array("order-by"=>"t.rowid")
				$_arrJson['success'] = TRUE;
				$_arrJson['data'] = array("list" => $_arrList, "added_rowid" => $_added_rowid);
				$_arrJson['message'] = 'insert completed';
			} else {
				$_arrJson['success'] = FALSE;
				$_arrJson['message'] = 'ERROR: Invalid added rowid';
				$_arrJson['error'] = $this->m->error_message;
			}
		}
		$json = json_encode($_arrJson);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
}
//---------------------------------------------------