<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once ( JPATH_BASE.DS. 'includes'.DS.'defines.php' );
require_once ( JPATH_BASE.DS.'includes'.DS.'framework.php' );
require_once ( JPATH_BASE.DS.'libraries/joomla/factory.php' );

function getArticleIntro($params = NULL) {
	if ((empty($params)) || (! is_array($params))) return FALSE;
	$_tp = & JTable::getInstance('Content', 'JTable');
	$_tp->load($params);
//var_dump($_tp);exit();
	return preg_replace('/src=\"images\//i', 'src="../images/', $_tp->introtext);
}

function createNewArticleContent($type=0, $data=array()) { //type 0=building, 1=room
	if (! is_array($data)) $data = array();

	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();
	$_tp = & JTable::getInstance('Content', 'JTable');
	$_data = array(
		'catid' => (($type==0)?8:9),
		'title' => (isset($data['title'])?$data['title']:'-'),
		'introtext' => (isset($data['introtext'])?$data['introtext']:''),
		'fulltext' => (isset($data['fulltext'])?$data['fulltext']:''),
		'language' => '*',
		'state' => 1
	);
	// Bind data
	if ($_tp->bind($_data)) {
		// Check the data.
		if ($_tp->check()) {
			// Store the data.
			if ($_tp->store(TRUE)) {
				$_tp->reorder('catid = '.(int) $_tp->catid.' AND state >= 0');
				return array("success"=>TRUE, "id"=>$_tp->id);
			}
		}
	}
	return array("success"=>FALSE, "error"=>$_tp->getError());
}
/* End of file joomla_content_helper.php */ 
/* Location: ./application/helpers/joomla_content_helper.php */ 