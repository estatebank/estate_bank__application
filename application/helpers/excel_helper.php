<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

	function _url_excel_path() {
		$_return = base_url() . EXCEL_PATH;
		$_return = str_replace('\\', '/', $_return);
		if (substr($_return, -1) != "/") $_return .= '/';
		return $_return;
	}

	function _file_excel_path() {
		$_return = FCPATH . EXCEL_PATH;
		$_return = str_replace('/', '\\', $_return);
		if (substr($_return, -1) != "\\") $_return .= '\\';
		return $_return;
	}
	
	function _get_excel_file_path($filename = 'excel.xls') {
		$_fileName = trim($filename);
		$_str = str_replace('\\', '/', $_fileName);
		$_str = str_replace('//', '/', $_str);
		$_arr = explode('/', $_str);
		if (count($_arr) > 0) {
			$_fileName = $_arr[count($_arr) - 1];
		}
		$_return = _file_excel_path() . $_fileName;
		return $_return;
	}
	
	function _get_excel_url_path($filename = 'excel.xls') {
		$_fileName = trim($filename);
		$_str = str_replace('\\', '/', $_fileName);
		$_str = str_replace('//', '/', $_str);
		$_arr = explode('/', $_str);
		if (count($_arr) > 0) {
			$_fileName = $_arr[count($_arr) - 1];
		}
		$_return = _url_excel_path() . $_fileName;
		return $_return;
	}
	