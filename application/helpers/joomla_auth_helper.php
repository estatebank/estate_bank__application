<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once ( JPATH_BASE.DS. 'includes'.DS.'defines.php' );
require_once ( JPATH_BASE.DS.'includes'.DS.'framework.php' );
require_once ( JPATH_BASE.DS.'libraries/joomla/factory.php' );

/**
 * Program Name
 *
 * @author		Inferno
 */

// ------------------------------------------------------------------------

/**
 * Joomla Authentication Helper
 *
 * Provide helper functions for common display operations.
 *
 * @package		Polomaker
 * @subpackage	Helper
 * @category	Authentication
 * @author		Inferno
 */
function userinfo($name)
{
	if (validate_session())
	{
		$CI =& get_instance();
		return $CI->session->userdata($name);
	}
}

function _checkSessionAuth($pageName = '', $clsAccessControl = NULL) {
	$_page_name = strtolower(trim($pageName));
	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();
	$CI =& get_instance();
/* ++ TEST BYPASS */
			$CI->session->set_userdata(
				array(
					'user_id' => 0
					,'user_name' => 'testing'
					,'user_display' => 'TEST'
					,'user_email' => ''
				)
			);
/* -- TEST BYPASS */
/*
	$user = JFactory::getUser();
	if (empty($user->id)) {
		$CI->session->unset_userdata(
			array(
				'user_id' => "",
				'user_name' => "",
				'user_display' => "",
				'user_email' => "",
				'user_groups' => "",
				'_AC' => ""
			)
		);
		if ( $_page_name != 'home' ) exit('Session invalid, plrease login.');//redirect(prep_url(JOOMLA_URL));
	} else {
//		$_user_id = $CI->session->userdata('user_id');
//		if (empty($_user_id) || ($_user_id != $user->id)) {
			$CI->session->set_userdata(
				array(
					'user_id' => $user->id,
					'user_name' => $user->username,
					'user_display' => $user->name,
					'user_email' => $user->email,
					'user_groups' => $user->groups,
					'_AC' => ''
				)
			);
			$_user_id = $CI->session->userdata('user_id');
//		}
		if (isset($clsAccessControl)) $clsAccessControl->_loadPageAccessRight($_user_id);
//var_dump($CI->session->userdata);exit;
	}
*/	
}

//var_dump($user->getAuthorisedGroups());exit;
//var_dump($user->getAuthorisedViewLevels());exit;

/* End of file joomla_auth_helper.php */ 
/* Location: ./application/helpers/joomla_auth_helper.php */ 