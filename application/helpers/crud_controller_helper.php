<?php
	function hlpr_prepareMasterTableSelectOptions($_arrSelList, &$_currentSelOptions = NULL) {
		$_arrSelOptions = (isset($_currentSelOptions) && is_array($_currentSelOptions))?$_currentSelOptions:array();
		$CI = get_instance();
		$CI->load->model('mdl_master_table', 'mt');

		foreach ($_arrSelList as $_key=>$_item) {
			$_order = 'rowid';
			$_text = 'name';
			$_index = '';
			$_where = FALSE;
			$_blnDefaultBlank = TRUE;
			$_aliasIndex;
			$_tableName = NULL;
			$_tablePrefix = NULL;
			$_arr;
			if (is_array($_item)) {
				$_index = $_tableName = $_key;
				$_aliasIndex = $_index;
				if (array_key_exists('text', $_item)) $_text = $_item['text'];
				if (array_key_exists('where', $_item)) $_where = $_item['where'];
				if (array_key_exists('order', $_item)) $_order = $_item['order'];
				if (array_key_exists('default_null', $_item) && ($_item['default_null'] == FALSE)) $_blnDefaultBlank = FALSE;
				if (array_key_exists('alias', $_item)) $_aliasIndex = $_item['alias'];
				if (array_key_exists('table_name', $_item)) $_tableName = $_item['table_name'];
				if (array_key_exists('table_prefix', $_item)) $_tablePrefix = $_item['table_prefix'];
			} else {
				$_index = $_tableName = $_item;
				$_aliasIndex = $_index;
			}
			if ($_where !== FALSE) {
				if (!empty($_tablePrefix)) {
					$_arr = $CI->mt->list_where($_tableName, $_item['where'], $_order, $_tablePrefix);
				} else {
					$_arr = $CI->mt->list_where($_tableName, $_item['where'], $_order);
				}
			} else {
				if (!empty($_tablePrefix)) {
					$_arr = $CI->mt->list_all($_tableName, $_order, $_tablePrefix);
				} else {
					$_arr = $CI->mt->list_all($_tableName, $_order);
				}
			}
			if (is_array($_arr)) {
				if (($_blnDefaultBlank) && (count($_arr) > 0)) {
					array_unshift($_arr, array('rowid'=>'', $_text=>'&nbsp;'));
				}
				$_arrSelOptions[$_aliasIndex] = $_arr;
			}
		}
		return $_arrSelOptions;
	}

	function hlpr_prepareControlsDefault($modelName, $selOptions = array()) {
		$CI = null;
		if (!$CI) $CI = get_instance();
		$CI->load->model($modelName);
		$_arrDataViewFields = array();
		foreach ($CI->$modelName->_AUTO_FIELDS as $_name => $_value) {
			$_arrDataViewFields[$_name] = array("form_edit" => array("name" => $_name, "type" => "hdn"));
		}
		foreach ($CI->$modelName->_FIELDS as $_name => $_value) {
			$_arr = array();
			$_label = '';
			if (($_name == 'create_by') || ($_name == 'create_date') || ($_name == 'update_by') || ($_name == 'update_date')) continue;
			$_label = $_name;
			$_arr['type'] = 'txt';
			
			if (strlen($_name) > 3) {
				if (substr($_name, 0, 3) == 'is_') {
					$_arr['type'] = 'chk';
				} elseif (strlen($_name) > 6) {
					if (substr($_name, -5) == 'rowid') {
						$_label = substr($_name, 0, -6);
						$_arr['type'] = 'sel';
						$_arr['sel_text'] = 'name_en';
						$_arr['sel_val'] = 'rowid';
						if (array_key_exists($_label, $selOptions)) $_arr['sel_options'] = $selOptions[$_label];
						$_arr['hidden_name'] = $_label . '_disp';
					} elseif (substr($_name, -5) == '_date') {
						$_label = substr($_name, 0, -5);
						$_arr['type'] = 'dpk';
					}
				}
			}
			$_arr['label'] = str_replace('_', ' ', ucwords($_label));
			$_arr['name'] = $_name;
			$_arrDataViewFields[$_name] = array("form_edit" => $_arr);
		}
		return $_arrDataViewFields;
	}
	
	function hlpr_setController(&$destArray, $strDataColumn, $strLabel, $arrEditAttributes = NULL, $arrGridAttributes = NULL) {
		if (! is_string ($strLabel)) throw new Exception('hlpr_setController: strDataColumn invalid data type (string) for "'. $strDataColumn .'"');//trigger_error(, E_USER_ERROR);
		if (! array_key_exists($strDataColumn, $destArray)) {
			$destArray[$strDataColumn] = array("form_edit" => array("label"=>$strLabel, "name"=>$strDataColumn));
		} else {
			$destArray[$strDataColumn]['form_edit']["label"] = $strLabel;
		}
		if (is_array($arrEditAttributes)) foreach ($arrEditAttributes as $_key => $_itm) {
			$destArray[$strDataColumn]["form_edit"][$_key] = $_itm;
		}

		if (is_array($arrGridAttributes)) if (count($arrGridAttributes) > 0) {
			if (! array_key_exists("list_item", $destArray[$strDataColumn])) {
				$destArray[$strDataColumn]["list_item"] = array("label"=>$strLabel);
			} else {
				$destArray[$strDataColumn]['list_item']["label"] = $strLabel;
			}
			foreach ($arrGridAttributes as $_key => $_itm) {
				$destArray[$strDataColumn]["list_item"][$_key] = $_itm;
			}
		}
	}
	
	function hlpr_getDisplayLabel($srcArray, $strDataColumn) {
		if (array_key_exists($strDataColumn, $srcArray)) {
			if (array_key_exists("form_edit", $srcArray[$strDataColumn])) {
				if (array_key_exists("label", $srcArray[$strDataColumn]["form_edit"])) return $srcArray[$strDataColumn]["form_edit"]["label"];
			} else if (array_key_exists("list_item", $srcArray[$strDataColumn])) {
				if (array_key_exists("label", $srcArray[$strDataColumn]["list_item"])) return $srcArray[$strDataColumn]["list_item"]["label"];
			}
		}
		return str_replace('_', ' ', ucwords($strDataColumn));
	}
	
	function hlpr_arrGetEditControls($srcArray, $arr = array()) {
		if (count($arr) == 0) $arr = $srcArray;
		$_arrReturn = array();
		foreach ($arr as $_key => $_obj) {
			if (array_key_exists("form_edit", $_obj)) {
				//at least default from model will always have "type"
				if (array_key_exists("type", $_obj["form_edit"])) $_arrReturn[$_key] = $_obj["form_edit"];
			}
		}
		return $_arrReturn;
	}

/*++ View Helper */
	function _getLayoutItemDisplay($layout, &$arrControls, &$arrEditPanelDataKey) {
		$_return = '';
		$_rows = '';
		if (!(isset($arrEditPanelDataKey) && is_array($arrEditPanelDataKey))) $arrEditPanelDataKey = array();
		foreach ($layout as $_grp => $_item) {
			$_rows = '';
			if (is_array($_item)) {
				$_rows .= _getRow($_item, $arrControls, $arrEditPanelDataKey);
			}
			$_return .= '<tr><td colspan="3" class="td-align-center">';
			if ((! is_numeric($_grp)) && (strlen($_grp) > 0)) {
				$_return .= '<div class="cls-row-group">';
				$_return .= '<span class="cls-group-title">' . $_grp . '</span>';
				$_return .= $_rows;
				$_return .= '</div>';
			} else {
				$_return .= $_rows;
			}
			$_return .= '</td></tr>';
		}
		return $_return;
	}

	function _getRow($arrItems, &$arrControls, &$arrEditPanelDataKey, $intSubLevel = 0) {
		$_row = '';
		$_each = '';
		$_group = FALSE;
		$_width = 0;
		if (is_array($arrItems)) {
			if (count($arrItems) > 0) $_width = floor(100 / (count($arrItems) * 5));
			foreach ($arrItems as $_grp => $_item) {
				if ( ! is_array($_item)) {
					$_title = '';
					$_elem = '';
					$_type = '';
					if ((strlen($_item) > 7) && (substr($_item, 0, 7) == 'return ')) {
						$_elem = substr($_item, 7);
						if (preg_match_all('/\{([^\}]+)\}/', $_elem, $_matches)) {
							foreach($_matches[1] as $_col) {
								array_push($arrEditPanelDataKey, $_col);
							}
						}
					} else if (array_key_exists($_item, $arrControls)) {
						$_title = trim($arrControls[$_item][0]);
						$_elem = $arrControls[$_item][1];
						if (count($arrControls[$_item]) > 2) $_type = $arrControls[$_item][2];
						unset($arrControls[$_item]);
					}
					if (($_title == 'hidden') || ($_type == 'info')) {
						$_each = $_elem;
					} else if ($_title != '') {
						if ($_type == 'chk') {
							$_each = '<div class="table-value cls-row-value value-checkbox" style="width:' . ($_width * 3) . '%" >' . $_elem . '</div>';
							$_each .= '<div class="table-title cls-row-title title-checkbox" style="width:' . ($_width * 2) . '%" >' . $_title . '</div>';
						} else {
							$_each = '<div class="table-title cls-row-title" style="width:' . ($_width * 2) . '%" >' . $_title . ' :</div>';
							$_each .= '<div class="table-value cls-row-value" style="width:' . ($_width * 3) . '%" >' . $_elem . '</div>';
						}
					} else {
						$_each = '<div class="cls-row-value" style="width:' . ($_width * 5) . '%" >' . $_elem . '</div>';
					}
				} else {
					$_subLevel = $intSubLevel + 1;
					$_group = TRUE;
					$_each = _getRow($_item, $arrControls, $arrEditPanelDataKey, $_subLevel);
				}
				if ((! is_numeric($_grp)) && (strlen($_grp) > 0)) {
					$_row .= '<div class="cls-row-group">';
					$_row .= '<span class="cls-group-title">' . $_grp . '</span>';
					$_row .= $_each;
					$_row .= '</div>';
				} else {
					$_row .= $_each;
				}
			}
			if ($_group == FALSE) {
				$_row = '<div class="cls-row" >' . $_row . '</div>';
			}
		}
		return $_row;
	}
/*-- View Helper*/

/* End of file crud_controller_helper.php */ 
/* Location: ./application/helpers/crud_controller_helper.php */ 