<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('DEFAULT_TITLE', 'Real Estate Demo');
define('APPNAME', str_replace('/app', '', dirname($_SERVER['SCRIPT_NAME'])));

/* ++ Joomla 3.4.3 Vars */
define('JOOMLA_DB_PREFIX', 'j343_');

define('_JEXEC', 1);
define( 'DS','/' );
define('JPATH_BASE', $_SERVER['DOCUMENT_ROOT'].APPNAME);
define('JOOMLA_URL', $_SERVER['SERVER_NAME'].APPNAME);
/* -- Joomla 3.4.3 Vars */

/* ++ Path to upload and Excel */

define('TEMP_UPLOAD_PATH', 'public/uploads/temp/');
define('UPLOAD_PATH', 'public/uploads/');

define('EXCEL_PATH', 'public/excels/');

/* -- Path to upload and Excel */

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

/* End of file constants.php */
/* Location: ./application/config/constants.php */