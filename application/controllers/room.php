<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room extends MY_Ctrl_crud_jspn {
	function __construct() {
		parent::__construct();
		$this->modelName = 'Mdl_room';
	}
	public function index() {
		$this->_pageTitle = "Room";
		$this->_pageOptions = array("type"=>"left_main");
		$this->_editPanelWidth = "98%";
		$this->_editPanelHeight = 600;
		$this->_prepareSelectOptions(array('room_type'=>array("text"=>"name_en")));
		$this->load->model('Mdl_building', 'b');
		$this->_selOptions['building'] = $this->b->search();
		//array_unshift($this->_selOptions['building'], array("rowid"=>"", "name_en"=>"&nbsp;"));
		parent::index();
	}

	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(2);
		
		$_custom_columns = <<<CCLMN

{"sTitle":"Display", "sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { return '<img class="list-row-button" onclick="_doManageDisplay(this, '+ full.rowid + ', \'' + full.name_en + '\');" src="./public/images/icons/16/pictures_folder.png" title="Display content" />'; },"bSortable":false}

CCLMN;
		$_html = $this->add_view(
			'_public/_list'
			,array(
				'index' => 1
				,'dataview_fields' => $this->_arrDataViewFields
				,'custom_columns' => $_custom_columns //array(array("column"=>$_custom_columns, "order"=>12)) //
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight)
				)
			), TRUE
		);
		return $_html;
	}

	function __getEditForm($intFormIndex = 2) {
		$_intFrmIndex = ($intFormIndex > 0)?($intFormIndex):2;
		
		$this->add_onload_css_file('public/css/_public/_form.css', TRUE);
		$this->add_onload_css_file('public/css/estate_design.css', TRUE);
		
		$this->add_onload_js_file('public/js/jquery/fileupload/load-image.min.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/canvas-to-blob.min.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.iframe-transport.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload-process.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload-image.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.form.js');
		$this->add_onload_js_file('public/js/_public/_form.js');
		$this->add_onload_js_file('public/js/_public/_fmg_controller.js');
		
		//$this->add_onload_js_file('public/js/room/form.js', TRUE);

		$this->_prepareControlsDefault();
		$this->_setController("rowid", "", array("type"=>"hdn"));
		$this->_setController("code", "CODE", array("class"=>"input-required"));
		$this->_setController("building_rowid", "BUILDING", array("sel_text"=>"name_en", "on_change"=>"_evnt__onChangeBuildingRowID(str, ev, ui);"));
		$this->_setController("room_type_rowid", "TYPE", array("sel_text"=>"name_en"));
		$this->_setController("name_en", "NAME (EN)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
		//$this->_setController("name_th", "NAME (TH)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>1));
		//$this->_setController("name_jp", "NAME (日本語)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>2));
		$this->_setController("room", "ROOM NO.", array("class"=>"input-integer"), array("selectable"=>TRUE,"default"=>TRUE,"class"=>"right","order"=>0));
		$this->_setController("floor", "FLOOR", array("class"=>"input-integer"), array("selectable"=>TRUE,"default"=>TRUE,"class"=>"right","order"=>1));
		$this->_setController("bed_room", "BED ROOM", array("class"=>"input-integer"), array("selectable"=>TRUE,"default"=>TRUE,"class"=>"right","order"=>2));
		$this->_setController("bath_room", "BATH ROOM", array("class"=>"input-integer"), array("selectable"=>TRUE,"default"=>TRUE,"class"=>"right","order"=>3));
		$this->_setController("area_m2", "AREA (square m.)", array("class"=>"input-double", ""), array("selectable"=>TRUE,"default"=>TRUE,"class"=>"right","order"=>4));
		$this->_setController("rent_price", "RENT", array("class"=>"input-double"));
		$this->_setController("purchase_price", "DEPOSIT", array("class"=>"input-double"));
		$this->_setController("description", "DESCRIPTION", array("type"=>"txa"));
		$this->_setController("remark", "REMARK", array("type"=>"txa"));
		//$this->_setController("info", "", array("type"=>"hdn"));
		//$this->_setController("details", "", array("type"=>"hdn"));
		//$this->_setController("link_article_id", "", array("type"=>"hdn"));
		
		/* ++ special controller for display view list */
		$this->_setController("building_name", "Building", NULL, array("selectable"=>TRUE,"default"=>TRUE,"class"=>"center","order"=>5));
		/* -- special controllers for display view list */
		
		/* ++ custom controllers for edit form */
		$this->_setController("img_rowid1", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid2", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid3", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid4", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid5", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid6", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid7", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("img_rowid8", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		$this->_setController("image1", "รูปประกอบ 1", array("type"=>"fmg"));
		$this->_setController("image2", "รูปประกอบ 2", array("type"=>"fmg"));
		$this->_setController("image3", "รูปประกอบ 3", array("type"=>"fmg"));
		$this->_setController("image4", "รูปประกอบ 4", array("type"=>"fmg"));
		$this->_setController("image5", "รูปประกอบ 5", array("type"=>"fmg"));
		$this->_setController("image6", "รูปประกอบ 6", array("type"=>"fmg"));
		$this->_setController("image7", "รูปประกอบ 7", array("type"=>"fmg"));
		$this->_setController("image8", "รูปประกอบ 8", array("type"=>"fmg"));
		$this->_setController("img_title1", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title2", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title3", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title4", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title5", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title6", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title7", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		$this->_setController("img_title8", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		/* -- custom controllers for edit form */

		/*
		$_toReturn = $this->add_view(
			'_public/_form'
			,array(
				'index'=>$_intFrmIndex
				,'crud_controller'=>'room'
				,'controls'=>$this->_arrGetEditControls()
				,'layout'=> array(
					array("code", "building_rowid", "room_type_rowid")
					//,array("name_en", "name_th", "name_jp")
					,array("room_number", "floor", "area_m2")
					,array("", "bed_room", "bath_room")
					//,array("return " . $this->add_view('room/_detail', NULL, TRUE))
					,array("description")
					,array("remark")
					,array("")
				)
			), TRUE
		);
		*/
		$_toReturn = $this->add_view(
			'room/main'
			,array(
				'index'=>$_intFrmIndex
				,'crud_controller'=>'room'
				,'controls'=>$this->_arrGetEditControls()
				,'_gen_control_elements'=>TRUE
			), TRUE
		);
		return $_toReturn;
	}

	function _getLeftPanel() {
		array_unshift($this->_selOptions['building'], array("rowid"=>"", "name_en"=>"&nbsp;"));
		$_arrReturn = $this->add_view(
			'_public/_search_panel'
			,array(
				'controls' => array(
					array("type"=>"txt","label"=>'NAME',"name"=>"room_name")
					,array("type"=>"sel","label"=>'BUILDING',"name"=>"building_rowid","sel_options"=>$this->_selOptions['building'],"sel_text"=>"name_en")
					,array("type"=>"sel","label"=>'TYPE',"name"=>"room_type_rowid","sel_options"=>$this->_selOptions['room_type'],"sel_text"=>"name_en")
				)
			)
			,TRUE
		);
		array_shift($this->_selOptions['building']);

		return $_arrReturn;
	}
	
	function get_room_image($rowid = FALSE) {
		if ($rowid === FALSE) return FALSE;

		$_roomImgRowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_row = $this->m->getRoomImageByRoomImageID($_roomImgRowid);
		if (is_array($_row) && ($_row['image'])) {
			/*
			$_bln_304 = false;
			if (php_sapi_name() == 'apache') {
				// if our web server is apache
				// we get check HTTP
				// If-Modified-Since header
				// and do not send image
				// if there is a cached version
				$_ar = apache_request_headers();
				if (isset($_ar['If-Modified-Since']) && // If-Modified-Since should exists
					($_ar['If-Modified-Since'] != '') && // not empty
					(strtotime($_ar['If-Modified-Since']) >= $image_time)) // and grater than
					$_bln_304 = true;                                     // image_time
			}
			if ($_bln_304) {
				// Sending 304 response to browser
				// "Browser, your cached version of image is OK
				// we're not sending anything new to you"
				header('Last-Modified: '.gmdate('D, d M Y H:i:s', $ts).' GMT', true, 304);
				exit(); // bye-bye
			}
			
			// outputing Last-Modified header
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', $image_time).' GMT', true, 200);
			
			// Set expiration time +1 year
			// We do not have any photo re-uploading
			// so, browser may cache this photo for quite a long time
			header('Expires: '.gmdate('D, d M Y H:i:s',  $image_time + 86400*365).' GMT', true, 200);
			*/
			// outputing HTTP headers
			header('Content-Length: ' . strlen($_row['image']));
			header("Content-type: image/" . $_row['ext']);
			// outputing image
			echo $_row['image'];
			exit();
		}
	}
	
	function commit() {
		$this->_serviceCheckRight(($this->_blnCheckRight('insert') || $this->_blnCheckRight('edit')));
		$_arrResult = array('success' => FALSE, 'error' => '');
		$_arrData = $this->__getAjaxPostParams();
		if (is_array($_arrData)) {
			$this->load->model($this->modelName, 'm');
			$this->db->trans_begin();
			try {
				$_aff_rows = call_user_func(array($this->m, $this->_model_fncCommit), $_arrData); //$this->m->commit($_arrData);
				$_arrResult['error'] .= $this->m->error_message;

				$_roomRowID = -1;
				if (isset($_arrData['rowid']) && (trim($_arrData['rowid']) > 0)) {
					$_roomRowID = $this->db->escape((int) $_arrData['rowid']);
				} elseif ($this->m->last_insert_id > 0) {
					$_roomRowID = $this->m->last_insert_id;
				} else {
					$_arrResult['error'] .= ', Invalid rowid after commit "t_room".';
				}

				if (empty($_arrResult['error'])) {
					//$this->db->delete('t_room_images', array('room_rowid'=>$_roomRowID));
					$this->load->helper('upload_helper');
					$_strSQL = '';
					$_strFilePath = '';
					$this->db->flush_cache();
					foreach ($_arrData as $_key=>$_value) {
						if ((strlen($_key) > 5) && (strpos($_key, 'image') === 0)) {
							$_indx = (int) substr($_key, 5);
							$_img_rowid = (isset($_arrData['img_rowid' . $_indx]) && (! empty($_arrData['img_rowid' . $_indx]))) ? $_arrData['img_rowid' . $_indx] : -1;
							$_value = trim($_value);
							$_blnIsDelete = strtolower($_value) == 'remove';
							if ($_blnIsDelete) {
								$this->db->where('rowid', $_img_rowid);
								$this->db->delete('t_room_images');
							} else {
								$_title = (isset($_arrData['img_title' . $_indx]) && (! empty($_arrData['img_title' . $_indx]))) ? $_arrData['img_title' . $_indx] : '';
								$_userID = $this->db->escape((int) $this->session->userdata("user_id"));
								
								$this->db->set('room_rowid', $_roomRowID);
								$this->db->set('img_index', $_indx);
								if (! empty($_title)) {
									$this->db->set('title', $_title);
								} else {
									$this->db->set('title', NULL);
								}
								$_strFilePath = '';
								if ((! empty($_value)) && (is_string($_value)) && (strtolower($_value) != 'unchanged')) {
									$_strFilePath = _file_temp_upload_path() . $_value;
									if (file_exists($_strFilePath)) {
										$_strImgExt = pathinfo($_strFilePath, PATHINFO_EXTENSION);
										$_strImgBLOB = mysql_real_escape_string(file_get_contents($_strFilePath));
										//LOAD_FILE($_strFilePath)
										$this->db->set('image', "'" . $_strImgBLOB . "'", FALSE);
										$this->db->set('ext', $_strImgExt);
									} else {
										$_strFilePath = '';
									}
								}
								if ($_img_rowid <= 0) {
									$this->db->insert('t_room_images');
								} else {
									$this->db->where('rowid', $_img_rowid);
									$this->db->update('t_room_images');
								}
								if (! empty($_strFilePath)) { //remove temp files
									unlink($_strFilePath);
									clearstatcache();
								}
							}
							$this->db->flush_cache();
							if (! empty($this->db->_error_message())) {
								$_arrResult['error'] .= ", " . $this->db->_error_message();
								break;
							}
						}
					}
				}
			} catch (Exception $e) {
				$_arrResult['error'] .= $e->getMessage();
			}

			if (($this->db->trans_status() === FALSE) || (! empty($_arrResult['error']))) {
				$_arrResult['error'] .= "::DB Transaction rollback";
				$this->db->trans_rollback();
			} else {
				$this->db->trans_complete();
				unset($_arrResult['error']);
				$_arrResult['success'] = TRUE;
				$_arrResult['message'] = $_aff_rows;
			}
		}
		$_json = json_encode($_arrResult);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(" . $_json . ")": $_json;
	}
	function display($rowid = NULL) {
		$this->display_panel($rowid);
	}
	function display_panel($rowid = NULL) {
		if (! is_numeric($rowid)) return FALSE;
		
		$_rowid = (int) $rowid;
		$this->add_css(
			array(
				'http://fonts.googleapis.com/css?family=Roboto+Condensed:700italic,400,300,700',
				'public/css/jquery/ui/1.10.4/cupertino/jquery-ui.min.css',
				'public/css/jquery/dataTable/1.9.4/dataTables_jui.css',
				//'public/css/jquery/dataTable/1.10.0/jquery.dataTables.min.css',
				'public/css/jquery/dataTable/TableTools/2.1.5/TableTools_JUI.css'
			)
		);
		$this->add_js(
			array(
				'public/js/jquery/1.11.0/jquery.js'
				,'public/js/jquery/ui/1.10.4/jquery-ui.min.js'
				//,'public/js/jsGlobal.js'
				//,'public/js/jsUtilities.js'
				//,'public/js/jsGlobalConstants.js'
			)
		);
		$pass_main['middle_panel'] = $this->get_display_panel($_rowid, TRUE);
		$pass_main['bottom_panel'] = $this->add_view('_public/_sub_template/_bottom_main', NULL, TRUE);
		$pass_main['title'] = $this->_pageTitle;
		
		$this->add_view_with_script_header('_public/_template_main', $pass_main);
	}
	
	function get_display_panel($rowid = NULL, $return = FALSE) {
		if (! is_numeric($rowid)) return FALSE;
		
		$_rowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_params = $this->m->getDisplayRoomDetails($_rowid);
		if (empty($_params)) return FALSE;

		$this->_pageTitle = (isset($_params['disp_room_name'])) ? $_params['disp_room_name'] : '';
		
		$this->add_onload_css_file('public/css/_public/_form.css', TRUE);
		$this->add_onload_css_file('public/css/estate_design.css', TRUE);
		$this->add_onload_css_file('public/css/room/main.css', TRUE);
		
		$_html = $this->add_view('room/display_panel/_template', $_params, TRUE);
		if ($return) {
			return $_html;
		} else {
			echo $_html;
		}
	}
}
/* // ++ old display_panel
		if ((isset($_params['link_article_id'])) && (! empty($_params['link_article_id'])) && is_numeric($_params['link_article_id'])) {
			$this->load->helper('joomla_content_helper');
			$_params['content'] = getArticleIntro(array("id"=>(int)$_params['link_article_id']));
		} else {
			$_params['content'] = $_params['description'];
		}
*/
/*
		$_joomlaUrl = APPNAME;
		echo <<<HTML
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/layout.css" charset="utf-8" type="text/css">
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/personal.css" charset="utf-8" type="text/css">
		$_html
HTML;
// -- ++ old display_panel */


/* ++ PostgreSQL upload binary file codes 

					$_tmpUploadPath = FCPATH . TEMP_UPLOAD_PATH;
					$this->db->delete('t_room_images', array('room_rowid'=>$_rowid));
					$_strBulk = '';
					foreach ($_arrData as $_key=>$_value) {
						if ((strlen($_key) > 5) && (strpos($_key, 'image') === 0) && (! empty($_value)) && (is_string($_value))) {
							$_indx = (int) substr($_key, 5);
							$_strBulk .= ",(fnc_bytea_import('" . $_tmpUploadPath . $_value . "'),'";
							$_strBulk .= (isset($_arrData['img_title' . $_indx]) && (! empty($_arrData['img_title' . $_indx]))) ? $_arrData['img_title' . $_indx] : '';
							$_strBulk .= "'," . $this->session->userdata("user_id") . ",CURRENT_TIMESTAMP)";
						}
					}
					$_strSQL = "INSERT INTO t_room_images(image,title";
					$_strSQL .= ($_isInsert) ? ',create_by,create_date' : ',update_by,update_date';
					$_strSQL .= ") VALUES " . $_strBulk.substr(1);
echo $_strSQL;exit;
					if ($this->db->_error_message() !== "") {
						$_arrResult['error'] .= ", " . $this->db->_error_message();
						break;
					}

*/