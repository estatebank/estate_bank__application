<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_landmark extends MY_Ctrl_crud_jspn {
	function __construct() {
		parent::__construct();
		$this->modelName = 'Mdl_test_landmark';
	}
/*
	public function index() {
		$this->_pageTitle = "Room";
		$this->_pageOptions = array("type"=>"left_main");
		$this->_editPanelWidth = 900;
		$this->_editPanelHeight = 600;
		$this->_prepareSelectOptions(array(
			'room_type'=>array("text"=>"name_en")
		));
		$this->load->model('Mdl_building', 'b');
		$this->_selOptions['building'] = $this->b->search();

		array_unshift($this->_selOptions['building'], array("rowid"=>"", "name_en"=>"&nbsp;"));
		
		parent::index();
	}

	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(2);
		$_custom_columns = <<<CCLMN

{"sTitle":"Display", "sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { return '<img class="list-row-button" onclick="_doManageDisplay(this, '+ full.link_article_id + ', \'' + full.name_en + '\');" src="./public/images/icons/16/pictures_folder.png" title="Display content" />'; },"bSortable":false}

CCLMN;

		$_html = $this->add_view(
			'_public/_list'
			,array(
				'index' => 1
				,'dataview_fields' => $this->_arrDataViewFields
				,'custom_columns' => $_custom_columns //array(array("column"=>$_custom_columns, "order"=>12)) //
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight)
				)
			), TRUE
		);
		return $_html;
	}

	function __getEditForm($intFormIndex = 2) {
		$_intFrmIndex = ($intFormIndex > 0)?($intFormIndex):2;

		$this->_prepareControlsDefault();
		$this->_setController("rowid", "", array("type"=>"hdn"));
		$this->_setController("code", "CODE", array("class"=>"input-required"));
		$this->_setController("building_rowid", "BUILDING", array("sel_text"=>"name_en"));
		$this->_setController("room_type_rowid", "TYPE", array("sel_text"=>"name_en"));
		//$this->_setController("name_en", "NAME (EN)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
		//$this->_setController("name_th", "NAME (TH)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>1));
		//$this->_setController("name_jp", "NAME (日本語)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>2));
		$this->_setController("room_number", "ROOM NO.", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
		$this->_setController("floor", "FLOOR", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>1));
		$this->_setController("bed_room", "BED ROOM", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>2));
		$this->_setController("bath_room", "BATH ROOM", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>3));
		$this->_setController("area_m2", "AREA (m.<sup>2</sup>)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>4));
		$this->_setController("description", "DESC.", array("type"=>"txa"));
		$this->_setController("remark", "REMARK", array("type"=>"txa"));
		//$this->_setController("info", "", array("type"=>"hdn"));
		//$this->_setController("details", "", array("type"=>"hdn"));
		
		$this->_setController("link_article_id", "", array("type"=>"hdn"));
		$this->_setController("building_name", "Building", NULL, array("selectable"=>TRUE,"default"=>TRUE,"class"=>"center","order"=>5));

		$_toReturn = $this->add_view(
			'_public/_form'
			,array(
				'index'=>$_intFrmIndex
				,'crud_controller'=>'room'
				,'controls'=>$this->_arrGetEditControls()
				,'layout'=> array(
					array("code", "building_rowid", "room_type_rowid")
					//,array("name_en", "name_th", "name_jp")
					,array("room_number", "floor", "area_m2")
					,array("", "bed_room", "bath_room")
					//,array("return " . $this->add_view('room/_detail', NULL, TRUE))
					,array("description")
					,array("remark")
					,array("")
				)
			), TRUE
		);
		$this->add_onload_js_file('public/js/room/form.js'); //add latest (below _form.js)
		return $_toReturn;
	}

	function _getLeftPanel() {
		$_arrReturn = $this->add_view('_public/_search_panel'
			, array(
				'controls' => array(
					array(
						"type" => "txt"
						,"label" => 'NAME'
						,"name" => "room_name"
					)
					,array(
						"type" => "sel"
						,"label" => 'BUILDING'
						,"name" => "building_rowid"
						,"sel_options"=>$this->_selOptions['building']
						,"sel_text"=>"name_en"
					)
					,array(
						"type" => "sel"
						,"label" => 'TYPE'
						,"name" => "room_type_rowid"
						,"sel_options"=>$this->_selOptions['room_type']
						,"sel_text"=>"name_en"
					)
				)
			), TRUE
		);
		return $_arrReturn;
	}
	
	function display_panel($rowid = NULL) {
		if (! is_numeric($rowid)) return FALSE;
		
		$_rowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_params = $this->m->get_by_id($_rowid);
		if (empty($_params)) return FALSE;
		
		if ((isset($_params['link_article_id'])) && (! empty($_params['link_article_id'])) && is_numeric($_params['link_article_id'])) {
			$this->load->helper('joomla_content_helper');
			$_params['content'] = getArticleIntro(array("id"=>(int)$_params['link_article_id']));
		} else {
			$_params['content'] = $_params['description'];
		}
		
		$_html = $this->add_view(
			'building/display_panel/_template'
			,$_params
			,TRUE
		);
		$_joomlaUrl = APPNAME;
		echo <<<HTML
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/layout.css" charset="utf-8" type="text/css">
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/personal.css" charset="utf-8" type="text/css">
		$_html
HTML;
/*
		<style type="text/css" src="$_joomlaUrl/templates/estate-bank-template/css/layout.css" charset="utf-8"></style>
		<style type="text/css" src="$_joomlaUrl/templates/estate-bank-template/css/personal.css" charset="utf-8"></style>
*/
//	}

}