<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Building extends MY_Ctrl_crud_jspn {
	function __construct() {
		parent::__construct();
		$this->modelName = 'Mdl_building';
	}

	public function index() {
		$this->_pageTitle = "Building";
		$this->_pageOptions = array("type"=>"left_main");
		$this->_editPanelWidth = "95%";
		$this->_editPanelHeight = 600;
		$this->_prepareSelectOptions(array(
			'province'
			,'building_type'=>array("text"=>"name_en", "alias"=>"b_type")
			,'developer'=>array("text"=>"name_en", "table_prefix"=>"t_", "alias"=>"dev")
			,'srvc1'=>array("table_name"=> "available_service", "text"=>"name_jp", "where"=>"service_cat_rowid = 1", "alias"=>"srvc_internet")
			,'srvc2'=>array("table_name"=> "available_service", "text"=>"name_jp", "where"=>"service_cat_rowid = 2", "alias"=>"srvc_laundry")
			,'nearby_facility'=>array("text"=>"name_en", "where"=>"fac_cat_rowid = 1", "alias"=>"nrfc_super")
		));
		/*
		$this->load->model('Mdl_master_table', 'mt');
		$this->_selOptions['dev'] = $this->mt->list_all('developer', "name_en", 't_');
		array_unshift($this->_selOptions['dev'], array("rowid"=>"", "name_en"=>"&nbsp;"));
		*/

		parent::index();
	}

	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(2);
		$_custom_columns = <<<CCLMN

{"sTitle":"Map", "sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { return '<img class="list-row-button" onclick="_doPinMapPosition(this, ' + full.disp_map_position + ', ' + full.disp_map_area + ', ' + full.rowid + ', \'' + full.name_en + '\');" src="./public/images/icons/16/pin_map.png" title="Pin map position" />'; },"bSortable":false}

CCLMN;
//,{"sTitle":"Display", "sWidth":"50","sClass":"center","mData":"client_temp_id","mRender":function(data,type,full) { return '<img class="list-row-button" onclick="_doManageDisplay(this, '+ full.link_article_id + ', \'' + full.name_en + '\');" src="./public/images/icons/16/pictures_folder.png" title="Display content" />'; },"bSortable":false}

		$_html = $this->add_view(
			'_public/_list'
			,array(
				'index' => 1
				,'dataview_fields' => $this->_arrDataViewFields
				,'custom_columns' => $_custom_columns //array(array("column"=>$_custom_columns, "order"=>12)) //
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight)
				)
			), TRUE
		);
		$_html .= '<div class="cls-map-canvas-container"><div id="map_canvas" class="cls-map-canvas"></div></div>';
		return $_html;
	}

	function __getEditForm($intFormIndex = 2) {
		$_intFrmIndex = ($intFormIndex > 0)?($intFormIndex):2;

		$this->add_js(array(
				'public/js/leaflet/0.7.3/leaflet.js'
				,'public/js/leaflet/mouseposition/L.Control.MousePosition.js'
				,'public/js/jsGlobalMAP.js'
				,'https://open.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=2SmV87sAmjsMevMkGEASaUQlCkHV5RKy'
				,'public/js/_mapserver_adaptor/__mapquest.js'
			));

		$this->add_onload_css_file('public/css/leaflet/0.7.3/leaflet.css');
		$this->add_onload_css_file('public/css/leaflet/mouseposition/L.Control.MousePosition.css');
		$this->add_onload_css_file('public/css/estate_design.css', TRUE);
		//$this->add_onload_css_file('public/css/room/main.css', TRUE);

		$this->add_onload_js_file('public/js/jquery/fileupload/load-image.min.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/canvas-to-blob.min.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.iframe-transport.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload-process.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.fileupload-image.js');
		$this->add_onload_js_file('public/js/jquery/fileupload/jquery.form.js');
		$this->add_onload_js_file('public/js/_public/_fmg_controller.js');

		$_arrAmphoe = $this->mt->list_all('amphoe', "province_rowid");
		$_jsonAmphoe = '';
		$_jsonEach = '';
		$_intProvinceRowid = -1;
		if (is_array($_arrAmphoe)) foreach ($_arrAmphoe as $_row) {
				if ($_intProvinceRowid != $_row['province_rowid']) {
					if (($_intProvinceRowid > 0) && (strlen($_jsonEach) > 0)) {
						$_jsonEach = substr($_jsonEach, 0, -1);
						$_jsonAmphoe .= '"' . $_intProvinceRowid . '":[' . $_jsonEach . '],';
						$_jsonEach = '';
					}
					$_intProvinceRowid = (int)$_row['province_rowid'];					
				}
				$_jsonEach .= '{rowid:"' . $_row['rowid'] . '", name:"' . $_row['name_en'] . '"},';
			}
		if (strlen($_jsonAmphoe) > 0) {
			if (($_intProvinceRowid > 0) && (strlen($_jsonEach) > 0)) {
				$_jsonEach = substr($_jsonEach, 0, -1);
				$_jsonAmphoe .= '"' . $_intProvinceRowid . '":[' . $_jsonEach . '],';
			}
			$_jsonAmphoe = substr($_jsonAmphoe, 0, -1);
		}
		//$this->_onload_scripts[1] .= "_MAIN_FRM_INDEX = " . $_intFrmIndex . ";\n";
		$this->_onload_scripts[1] .= "_JSON_AMPHOE = {" . $_jsonAmphoe . "};\n";
		
		$this->_prepareControlsDefault();
		$this->_setController("rowid", "", array("type"=>"hdn"));
		$this->_setController("code", "Code"); //รหัส //array("class"=>"input-required")
		$this->_setController("b_type_rowid", "B.Type", array("sel_text"=>"name_en")); //ประเภทอาคาร
		$this->_setController("name_en", "Name (English)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0)); //ชื่อ
		//$this->_setController("name_th", "ชื่อ (ไทย)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>1));
		$this->_setController("name_jp", "Name (日本語)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>2));
		$this->_setController("address", "Address", array("type"=>"txa")); //ที่อยู่
		$this->_setController("province_rowid", "Province", array("class"=>"sel-province input-required", "on_change"=>"evnt_onSelectProvince(str, ev, ui);", 'sel_options'=>$this->_selOptions['province'], 'sel_val'=>'rowid', 'sel_text'=>'name_en')); //จังหวัด
		$this->_setController("city_rowid", "City", array("class"=>"sel-amphoe set-disabled",'sel_options'=>array())); //เมือง
		$this->_setController("postal_code", "Post.Code", array("class"=>"input-integer", "maxlangth"=>"5")); //รหัสไปรษณีย์
		$this->_setController("email", "E-mail", NULL, array("selectable"=>TRUE,"default"=>FALSE,"class"=>"center","order"=>7)); //อีเมล์
		$this->_setController("tel", "Tel.", NULL, array("selectable"=>TRUE,"default"=>FALSE,"class"=>"center","order"=>8)); //โทรศัพท์
		$this->_setController("fax", "Fax.", NULL, array("selectable"=>TRUE,"default"=>FALSE,"class"=>"center","order"=>9)); //โทรสาร
		$this->_setController("description", "Detail", array("type"=>"txa")); //รายละเอียด
		$this->_setController("units", "R.No.", array("class"=>"input-integer")); //จำนวนห้อง
		$this->_setController("floors", "Floor", array("class"=>"input-integer")); //จำนวนชั้น
		$this->_setController("year", "Year", array("class"=>"input-integer", "maxlangth"=>"4")); //ปีที่สร้าง
		$this->_setController("srvc_internet_rowid", "Internet Service", array("type"=>"sel", 'sel_options'=>$this->_selOptions['srvc_internet'], 'sel_text'=>'name_jp'));
		$this->_setController("srvc_laundry_rowid", "Laundry Service", array("type"=>"sel", 'sel_options'=>$this->_selOptions['srvc_laundry'], 'sel_text'=>'name_jp'));
		$this->_setController("nrfc_super_rowid", "Near Facilities", array("type"=>"sel", 'sel_options'=>$this->_selOptions['nrfc_super'], 'sel_text'=>'name_en'));
		$this->_setController("is_pet", "Pet");
		$this->_setController("is_pool", "Pool");
		$this->_setController("is_sauna", "Sauna");
		$this->_setController("is_gym", "Gym");
		$this->_setController("is_conv_store", "", array("type"=>"hdn"));
		$this->_setController("is_nhk", "NHK");
		$this->_setController("is_jp_srvc", "Japanese Service");
		$this->_setController("is_playground", "Playgound");
		$this->_setController("is_shuttle", "Shuttle");
		$this->_setController("is_cleaner", "Cleaner");
		$this->_setController("dev_rowid", "Dev.", array("class"=>"jspn-addable", "jspn-controller"=>"developer", "sel_text"=>"name_en"));	
		$this->_setController("remark", "Remark", array("type"=>"txa")); //บันทึกเพิ่มเติม
		$this->_setController("details", "", array("type"=>"hdn"));
		$this->_setController("map_position", "", array("type"=>"hdn"));
		
		$this->_setController("disp_map_position", "", array("type"=>"hdn"));
		$this->_setController("city", "City", NULL, array("selectable"=>TRUE,"default"=>TRUE,"class"=>"center","order"=>5)); //เมือง
		$this->_setController("province", "Province", NULL, array("selectable"=>TRUE,"default"=>TRUE,"class"=>"center","order"=>6)); //จังหวัด
		//$this->_setController("link_article_id", "", array("type"=>"hdn"));
		//$this->_setController("abbr_description", "รายละเอียด", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>10));
		$this->_setController("img_rowid1", "");
		$this->_setController("img_rowid2", "");
		$this->_setController("img_rowid3", "");
		$this->_setController("img_rowid4", "");
		$this->_setController("img_rowid5", "");
		$this->_setController("img_rowid6", "");
		$this->_setController("img_rowid7", "");
		$this->_setController("img_rowid8", "");
		$this->_setController("image1", "");
		$this->_setController("image2", "");
		$this->_setController("image3", "");
		$this->_setController("image4", "");
		$this->_setController("image5", "");
		$this->_setController("image6", "");
		$this->_setController("image7", "");
		$this->_setController("image8", "");
		$this->_setController("img_title1", "");
		$this->_setController("img_title2", "");
		$this->_setController("img_title3", "");
		$this->_setController("img_title4", "");
		$this->_setController("img_title5", "");
		$this->_setController("img_title6", "");
		$this->_setController("img_title7", "");
		$this->_setController("img_title8", "");

		/* ++ custom controllers for edit form */
		$_arrImgControls = array();
		hlpr_setController($_arrImgControls, "img_rowid1", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid2", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid3", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid4", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid5", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid6", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid7", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "img_rowid8", "", array("type"=>"hdn", "add_class"=>"data-constant no-validate"));
		hlpr_setController($_arrImgControls, "image1", "รูปประกอบ 1", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image2", "รูปประกอบ 2", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image3", "รูปประกอบ 3", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image4", "รูปประกอบ 4", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image5", "รูปประกอบ 5", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image6", "รูปประกอบ 6", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image7", "รูปประกอบ 7", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "image8", "รูปประกอบ 8", array("type"=>"fmg"));
		hlpr_setController($_arrImgControls, "img_title1", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title2", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title3", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title4", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title5", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title6", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title7", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		hlpr_setController($_arrImgControls, "img_title8", "IMAGE TITLE", array("type"=>"txt", "add_class"=>"cls-image-title"));
		/* -- custom controllers for edit form */
		$_strImagesPanel = $this->add_view('building/_imagesPanel', array(
			'controls' => $this->_arrGetEditControls($_arrImgControls)
			,'_gen_control_elements'=>TRUE
		), TRUE);
//echo $_strImagesPanel;exit;
		$_toReturn = $this->add_view(
			'_public/_form'
			,array(
				'index'=>$_intFrmIndex
				,'crud_controller'=>'building'
				,'controls'=>$this->_arrGetEditControls()
				,'layout'=> array(
					array("code", "")
					,array("name_en", "name_th", "name_jp")
					,array("b_type_rowid", "units", "floors")
					,array("dev_rowid", "year", "")
					,array("address") //, ""
					,array("province_rowid", "city_rowid", "postal_code")
					,array("email", "tel", "fax")
					,array("description")
					,array("remark")
					,array("")
					,"Services & Facilities" => array(
						array("srvc_internet_rowid", "srvc_laundry_rowid")
						,array("nrfc_super_rowid", "")
						,array("is_pet", "is_pool", "is_sauna", "is_gym", "is_cleaner", "")
						,array("is_nhk", "is_jp_srvc", "is_playground", "is_shuttle", "", "")
					)
					,array("return " . $_strImagesPanel)
					/*
					,array("return " . $this->add_view('building/_detail', array(
							'controls' => array(
									array("name"=>"srvc_internet_rowid", "label"=>"INTERNET", "type"=>"sel", 'sel_options'=>$this->_selOptions['srvc_internet'], 'sel_text'=>'name_jp')
									,array("name"=>"srvc_laundry_rowid", "label"=>"LAUNDRY", "type"=>"sel", 'sel_options'=>$this->_selOptions['srvc_laundry'], 'sel_text'=>'name_en')
									,array("name"=>"nrfc_super_rowid", "label"=>"FACILITIES", "type"=>"sel", 'sel_options'=>$this->_selOptions['nrfc_super'], 'sel_text'=>'name_en')
									,array("name"=>"is_pet", "label"=>"", "type"=>"chk")
									,array("name"=>"is_pool", "label"=>"", "type"=>"chk")
									,array("name"=>"is_sauna", "label"=>"", "type"=>"chk")
									,array("name"=>"is_gym", "label"=>"", "type"=>"chk")
									,array("name"=>"is_conv_store", "label"=>"", "type"=>"chk")
									,array("name"=>"is_nhk", "label"=>"", "type"=>"chk")
									,array("name"=>"is_jp_srvc", "label"=>"", "type"=>"chk")
									,array("name"=>"is_playground", "label"=>"", "type"=>"chk")
									,array("name"=>"is_shuttle", "label"=>"", "type"=>"chk")
								)
							,'_gen_control_elements'=>TRUE
						), TRUE))
					*/
				)
			), TRUE
		);
		$this->add_onload_js_file('public/js/building/form.js'); //add latest (below _form.js)
		return $_toReturn;
	}

	function _getLeftPanel() {
		$_arrReturn = $this->add_view('_public/_search_panel'
			, array(
				'controls' => array(
					array("type"=>"txt", "label"=>"Name","name"=>"building_name") //ชื่ออาคาร
					,array("type"=>"sel","label"=>"Province","name"=>"province_rowid","class"=>"sel-province","sel_options"=>$this->_selOptions['province'],"sel_val"=>"rowid","sel_text"=>"name_en","on_change"=>"evnt_onSelectProvince(str, ev, ui);") //จังหวัด
					,array("type"=>"sel","label"=>"City","name"=>"amphoe_rowid","class"=>"sel-amphoeset-disabled","sel_options"=>array()) //'อำเภอ'
					,array("type"=>"txt","label"=>"Cont.No.","name"=>"contact_no") //เบอร์ติดต่อ
					,array("type"=>"txt","label" => "E-mail","name"=>"email") //อีเมล์
				)
			), TRUE
		);
		return $_arrReturn;
	}
	
	function commit_map_position() {
		$_blnSuccess = FALSE;
		$_strError = '';
		$_strMessage = '';
		$_arr = $this->__getAjaxPostParams();
		$this->load->model($this->modelName, 'm');
		$this->m->commit_map_position($_arr);
		$_strError = $this->m->error_message;
		if ($_strError == "") {
			$_blnSuccess = TRUE;
			$_strMessage = "";
		}
		$json = json_encode(
			array(
				'success' => $_blnSuccess,
				'error' => $_strError,
				'message' => $_strMessage
			)
		);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$json.")": $json;
	}
	
	function display_panel($rowid = NULL) {
		if (! is_numeric($rowid)) return FALSE;
		
		$_rowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_params = $this->m->get_by_id($_rowid);
		if (empty($_params)) return FALSE;
		
		if ((isset($_params['link_article_id'])) && (! empty($_params['link_article_id'])) && is_numeric($_params['link_article_id'])) {
			$this->load->helper('joomla_content_helper');
			$_params['content'] = getArticleIntro(array("id"=>(int)$_params['link_article_id']));
		} else {
			$_params['content'] = $_params['description'];
		}
		
		$_html = $this->add_view(
			'building/display_panel/_template'
			,$_params
			,TRUE
		);
		$_joomlaUrl = APPNAME;
		echo <<<HTML
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/layout.css" charset="utf-8" type="text/css">
		<link rel="stylesheet" href="$_joomlaUrl/templates/estate-bank-template/css/personal.css" charset="utf-8" type="text/css">
		$_html
HTML;
/*
		<style type="text/css" src="$_joomlaUrl/templates/estate-bank-template/css/layout.css" charset="utf-8"></style>
		<style type="text/css" src="$_joomlaUrl/templates/estate-bank-template/css/personal.css" charset="utf-8"></style>
*/
	}
	
	function get_building_image($rowid = FALSE) {
		if ($rowid === FALSE) return FALSE;

		$_roomImgRowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_row = $this->m->getImageByBuildingImageID($_roomImgRowid);
		if (is_array($_row) && ($_row['image'])) {
			// outputing HTTP headers
			header('Content-Length: ' . strlen($_row['image']));
			header("Content-type: image/" . $_row['ext']);
			// outputing image
			echo $_row['image'];
			exit();
		}
	}

	function get_image_by_building_rowid($rowid = FALSE) {
		if ($rowid === FALSE) return FALSE;

		$_rowid = (int) $rowid;
		$this->load->model($this->modelName, 'm');
		$_row = $this->m->getImageByBuildingRowID($_rowid);
		if (is_array($_row) && ($_row['image'])) {
			// outputing HTTP headers
			header('Content-Length: ' . strlen($_row['image']));
			header("Content-type: image/" . $_row['ext']);
			// outputing image
			echo $_row['image'];
			exit();
		}
	}
	
	function commit() {
		$this->_serviceCheckRight(($this->_blnCheckRight('insert') || $this->_blnCheckRight('edit')));
		$this->load->helper('upload_helper');

		$_arrResult = array('success' => FALSE, 'error' => '');
		$_arrData = $this->__getAjaxPostParams();
		$_arrCommitImages = array();
		$_upload_path = _file_temp_upload_path();
		if (is_array($_arrData)) {
			$this->load->model($this->modelName, 'm');
			$this->db->trans_begin();
			try {
				foreach ($_arrData as $_key=>$_value) {
					if ((strlen($_key) > 5) && (strpos($_key, 'image') === 0)) {
						$_indx = (int) substr($_key, 5);
						$_img_rowid = (isset($_arrData['img_rowid' . $_indx]) && (! empty($_arrData['img_rowid' . $_indx]))) ? $_arrData['img_rowid' . $_indx] : -1;
						$_value = trim($_value);
						if ((strtolower($_value) == 'remove') && ($_img_rowid > 0)) {
							$this->db->delete('t_building_images', array('rowid' => $_img_rowid)); 
							$this->db->flush_cache();
							$_arrData[$_key] = NULL;
							$_arrData['img_title' . $_indx] = NULL;
						} else if ((! empty($_value)) && (is_string($_value)) && (strtolower($_value) != 'unchanged')) {
							array_push($_arrCommitImages, array(
									"index"=>$_indx
									, "img_rowid"=>$_img_rowid
									, "title"=> (isset($_arrData['img_title' . $_indx])) ? $_arrData['img_title' . $_indx] : ''
									, "path" => $_upload_path . $_value
								));
						} else {
							unset($_arrData[$_key]);
							unset($_arrData['img_title' . $_indx]);
						}
					}
				}
				$_aff_rows = call_user_func(array($this->m, $this->_model_fncCommit), $_arrData); //$this->m->commit($_arrData);
				$_arrResult['error'] .= $this->m->error_message;

				$_bldRowID = -1;
				if (isset($_arrData['rowid']) && (trim($_arrData['rowid']) > 0)) {
					$_bldRowID = $this->db->escape((int) $_arrData['rowid']);
				} elseif ($this->m->last_insert_id > 0) {
					$_bldRowID = $this->m->last_insert_id;
				} else {
					$_arrResult['error'] .= ', Invalid rowid after commit "t_building".';
				}
				if (($_bldRowID > 0) && empty($_arrResult['error'])) {
					$_strSQL = '';
					$_userID = $this->db->escape((int) $this->session->userdata("user_id"));
					
					foreach ($_arrCommitImages as $_ea) {
						$this->db->flush_cache();
						$_indx = (isset($_ea['index']) && (! empty($_ea['index']))) ? $this->db->escape((int) $_ea['index']) : -1;
						$_img_rowid = (isset($_ea['img_rowid']) && (! empty($_ea['img_rowid']))) ? $this->db->escape((int) $_ea['img_rowid']) : -1;
						$_title = (isset($_ea['title']) && (! empty($_ea['title']))) ? $_ea['title'] : '';
						$_strFilePath = (isset($_ea['path']) && (! empty($_ea['path']))) ? $_ea['path'] : '';

						if (($_indx > 0) && file_exists($_strFilePath)) {
							$this->db->set('building_rowid', $_bldRowID);
							$this->db->set('img_index', $_indx);
							if ((! empty($_title))) {
								$this->db->set('title', $_title);
							} else {
								$this->db->set('title', NULL);
							}
							$_strImgExt = pathinfo($_strFilePath, PATHINFO_EXTENSION);
							$_strImgBLOB = mysqli_real_escape_string($this->db->conn_id, file_get_contents($_strFilePath));
							//$_strImgBLOB = mysql_real_escape_string(file_get_contents($_strFilePath));
							//LOAD_FILE($_strFilePath)
							$this->db->set('image', "'" . $_strImgBLOB . "'", FALSE);
							$this->db->set('ext', $_strImgExt);
							
							if ($_img_rowid <= 0) {
								$this->db->set('create_by', $_userID);
								$this->db->set('create_date', 'NOW()', FALSE);
								$this->db->insert('t_building_images');
							} else {
								$this->db->set('update_by', $_userID);
								$this->db->set('update_date', 'NOW()', FALSE);
								$this->db->where('rowid', $_img_rowid);
								$this->db->update('t_building_images');
							}
							if (! empty($_strFilePath)) { //remove temp files
								unlink($_strFilePath);
								clearstatcache();
							}
							if (! empty($this->db->_error_message())) {
								$_arrResult['error'] .= ", " . $this->db->_error_message();
								break;
							}
						}
					}
				}
			} catch (Exception $e) {
				$_arrResult['error'] .= $e->getMessage();
			}

			if (($this->db->trans_status() === FALSE) || (! empty($_arrResult['error']))) {
				$_arrResult['error'] .= "::DB Transaction rollback";
				$this->db->trans_rollback();
			} else {
				$this->db->trans_complete();
				unset($_arrResult['error']);
				$_arrResult['success'] = TRUE;
				$_arrResult['message'] = $_aff_rows;
			}
		}
		$_json = json_encode($_arrResult);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(" . $_json . ")": $_json;
	}
}