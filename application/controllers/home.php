<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MY_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index() {
		echo "Welcome, " . $this->session->userdata('user_display') . " ( " . $this->session->userdata('user_name') . " ) !!";
	}
	public function display_recommend() {
		$this->load->model('Mdl_room', 'r');
		$this->load->helper('joomla_content_helper');
		$_arrResult = $this->r->search();
		if (is_array($_arrResult)) {
			$this->add_css(
				array(
					'../templates/estate-bank-template/css/layout.css'
					,'../templates/estate-bank-template/css/personal.css'
					,'public/css/home/display_recommend.css'
				)
			);
			$this->add_js(
				array(
					'public/js/jquery/1.11.0/jquery.js'
					,'public/js/jquery/ui/1.10.4/jquery-ui.min.js'
				)
			);

			$_html = '<h4>RECOMMENDED LISTINGS</h4>';
			foreach ($_arrResult as $_row) {
				$_content = '';
				/*
				if ((isset($_row['link_article_id'])) && (! empty($_row['link_article_id'])) && is_numeric($_row['link_article_id'])) {
					$_content = getArticleIntro(array("id"=>(int)$_row['link_article_id']));
				}
				*/ 
				if (strlen(trim($_content)) < 5) {
					$_content =<<<DISP
	<div class="panel">
		<h6>{$_row['building_name']}: <a href="room/display_panel/{$_row['rowid']}" target="_new">Room {$_row['room_name']}</a></h6>
		<hr>
		<div class="cols-3">
			<div class="column-1">Floor: {$_row['floor']}</div>
			<div class="column-2">Bed room: {$_row['bed_room']}</div>
			<div class="column-3">Bath room: {$_row['bath_room']}</div>
		</div>
		{$_row['description']}
	</div>
DISP;
				}
				$_html .= '<div class="items_row">' . $_content . '</div>';
			}
			$pass['middle_panel'] = $_html;
			$pass['title'] = "RECOMMENDED LISTINGS";
			$this->add_view_with_script_header('_public/_template_main', $pass);		
		}
	}
	public function display_map($region_rowid = 0, $province_rowid = 0, $city_rowid = 0, $building_rowid = 0) {
		$this->add_css(
			array(
				'public/css/jquery/ui/1.10.4/cupertino/jquery-ui.min.css'
				,'public/css/leaflet/0.7.3/leaflet.css'
				,'public/css/leaflet/mouseposition/L.Control.MousePosition.css'
				//,'public/css/leaflet/leaftlet.draw/0.2.4/leaflet.draw.css'
				//,'public/css/leaflet/markercluster/MarkerCluster.css'
				//,'public/css/leaflet/markercluster/MarkerCluster.Default.css'
				,'public/css/leaflet/measure/leaflet.measure.css'
				,'public/css/leaflet/label/leaflet.label.css'
				//,array('public/css/leaflet/markercluster/MarkerCluster.Default.ie.css', 'ie6')
				//,array('public/css/leaflet/markercluster/MarkerCluster.Default.ie.css', 'ie7')
				,'public/css/home/display_map.css'
			)
		);
		$this->add_js(
			array(
				'public/js/jquery/1.11.0/jquery.js'
				,'public/js/jquery/ui/1.10.4/jquery-ui.min.js'
				,'public/js/jquery/jQuery.fullscreen.min.js'
				,'public/js/leaflet/0.7.3/leaflet.js'
				,'public/js/leaflet/mouseposition/L.Control.MousePosition.js'
				//,'public/js/leaflet/leaftlet.draw/0.2.4/leaflet.draw.js'
				//,'public/js/leaflet/markercluster/leaflet.markercluster.js'
				,'public/js/leaflet/measure/leaflet.measure.js'
				,'public/js/leaflet/label/Label.js'
				,'public/js/leaflet/label/BaseMarkerMethods.js'
				,'public/js/leaflet/label/Marker.Label.js'
				,'public/js/leaflet/label/Map.Label.js'
				//,'http://maps.google.com/maps/api/js?v=3.2&sensor=false'
				//,'public/js/leaflet/layer/tile/Google.js'
				,'https://open.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=2SmV87sAmjsMevMkGEASaUQlCkHV5RKy'
				,'public/js/_mapserver_adaptor/__mapquest.js'
				,'public/js/jsUtilities.js'
				,'public/js/jsGlobalConstants.js'
				,'public/js/jsGlobalMAP.js'
				,'public/js/home/display_map.js'
				//,array('alert("You are using IE");', 'ie_custom')
				//,array('alert("Stop using IE6 (test period :D)");', 'ie6_custom')
			)
		);
		$this->load->model('Mdl_building', 'b');
		$_arrResult = $this->b->list_map_objects();
		$_geoJson = '{}';
//var_dump($_arrResult);
		if (is_array($_arrResult) && count($_arrResult) > 0) {
			$_geoJson = json_encode($_arrResult);
		}
//echo $_geoJson;exit;
		$this->_onload_scripts[1] .= <<<SCR
				var _dataGeoJson = $_geoJson;
				_doInitMap(_dataGeoJson);
SCR;
		$pass['body'] = '<div id="disp_layout_map" style="position:absolute;width:100%;height:100%;"></div>';
		$pass['title'] = "MAP";
		$this->add_view_with_script_header('_public/_template_popup_map', $pass);
	}
	
	function get_building_display_image($rowid = 0) {
		$_img_file_path = '';
		if ((int) $rowid > 0) {
			$this->load->helper('file');
			$_filesList = get_filenames('public/images/building_popup_images/', TRUE);
			foreach ($_filesList as $_file) {
				$_arr = explode('.', $_file);
				if (count($_arr) > 1) {
					if (substr($_arr[0], (strrpos($_arr[0], '\\') + 1)) == $rowid) {
						$_img_file_path = $_file;
					}
				}
			}
		}
		if (strlen($_img_file_path) == 0) {
			$_img_file_path = "public/images/img_not_found.png";
		}
		//echo ('Content-Type:' . get_mime_by_extension($_img_file_path));
		$_str = read_file($_img_file_path);
		header('Content-Type:' . get_mime_by_extension($_img_file_path));
		echo ($_str);
	}
}