<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search__purchase extends MY_Ctrl_crud {
	function __construct() {
		parent::__construct();
		$this->_pageTitle = 'PURCHASE';
		$this->modelName = 'Mdl_advance_search';
	}

	function _getMainPanel() { //override
		$this->add_css(array(
			'../templates/estate-bank-template/css/layout.css'
			,'../templates/estate-bank-template/css/personal.css'
			,'public/css/leaflet/0.7.3/leaflet.css'
			,'public/css/_public/_search_panel.css'
			,'public/css/_public/_list.css'
			,'public/css/advance_search/search_panel.css'
			,'public/css/advance_search/search_result.css'
		));
		$this->add_js(array(
			'public/js/leaflet/0.7.3/leaflet.js'
			,'public/js/jsGlobalMAP.js'
			,'https://open.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=2SmV87sAmjsMevMkGEASaUQlCkHV5RKy'
			,'public/js/_mapserver_adaptor/__mapquest.js'
			,'public/js/advance_search/main.js'
		));

		return $this->add_view(
			'search__purchase/search_result'
			,array(
				'index' => ($this->_defaultBaseIndex + 1)
				,'autosearch' => FALSE
				,'end_script' => "\t\t$('#frm_edit div.cls-main-form-button').css('display', 'none');\n"
			), TRUE
		);
	}

	function _getLeftPanel() {
		$this->load->model($this->modelName, 'm');
		$this->_selOptions['landmarks'] = $this->m->list_stations();
		$this->_selOptions['areas'] = $this->m->list_districts();
		
		return $this->add_view(
			'search__purchase/search_panel'
			, array(
				'lst_landmark' => $this->_selOptions['landmarks']
				,'lst_area' => $this->_selOptions['areas']
			)
			, TRUE);
	}

	function json_search() {
		$this->_serviceCheckRight('view');

		$_arrReturn = array("success"=>FALSE, "error"=>"");
		$_arrData = $this->__getAjaxPostParams();
		if (! (is_array($_arrData)) ) {
			$_arrReturn["error"] = str_replace('v_XX_1', '( json_search: [price_type, price_unit, price_start, price_limit, search_name] )', $this->_AC->_MSG_INVALID_PARAMETERS);		
		} else {			
			$this->load->model($this->modelName, 'm');
			$_arrReturn["data"] = $this->m->search_with_ex_rate(array(), $_arrData);
			$_arrReturn["error"] = $this->m->error_message;
		}
		if (empty($_arrReturn["error"])) {
			unset($_arrReturn["error"]);
			$_arrReturn["success"] = TRUE;
			if (! is_array($_arrReturn["data"])) $_arrReturn["data"] = array();
		} else {
			unset($_arrReturn["data"]);
		}
		$_json = json_encode($_arrReturn);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$_json.")":$_json;
	}

	function ajax_aac_search_name() {
		$this->_serviceCheckRight('view');

		$_arrReturn = array("success"=>FALSE, "error"=>"");
		$_arrData = $this->__getAjaxPostParams();
		if (! (is_array($_arrData) && (array_key_exists('search_name', $_arrData))) ) {
			$_arrReturn["error"] = str_replace('v_XX_1', '( ajax_aac_search_name: search_name )', $this->_AC->_MSG_INVALID_PARAMETERS);		
		} else {
			$_search_name = $this->db->escape_like_str($_arrData['search_name']);

			$this->load->model('Mdl_building', 'b');
			$_arrReturn["data"] = $this->b->search(array("building_name"=>$_search_name));
			$_arrReturn["error"] = $this->b->error_message;
		}
		if (empty($_arrReturn["error"])) {
			unset($_arrReturn["error"]);
			$_arrReturn["success"] = TRUE;
			if (! is_array($_arrReturn["data"])) $_arrReturn["data"] = array();
		} else {
			unset($_arrReturn["data"]);
		}
		$_json = json_encode($_arrReturn);
		header('content-type: application/json; charset=utf-8');
		echo isset($_GET['callback'])? "{" . $_GET['callback']. "}(".$_json.")":$_json;
	}
}
