<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Developer extends MY_Ctrl_crud_jspn {
	function __construct() {
		parent::__construct();
		$this->modelName = 'Mdl_developer';
		$this->_pageTitle = "Developers";
		$this->_pageOptions = array("type"=>"left_main");
		$this->_editPanelWidth = 900;
		$this->_editPanelHeight = 400;		
	}

	function __getEditForm($intFormIndex = 2) {
		$_intFrmIndex = ($intFormIndex > 0)?($intFormIndex):2;

		$this->_prepareControlsDefault();
		$this->_setController("rowid", "", array("type"=>"hdn"));
		$this->_setController("code", "รหัส", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
		$this->_setController("name_en", "ชื่อ (English)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>0));
		$this->_setController("name_th", "ชื่อ (ไทย)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>1));
		$this->_setController("name_jp", "ชื่อ (日本語)", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>2));
		$this->_setController("description", "รายละเอียด", array("type"=>"txa"));
		$this->_setController("remark", "บันทึกเพิ่มเติม", array("type"=>"txa"));
		
		//$this->_setController("abbr_description", "รายละเอียด", NULL, array("selectable"=>TRUE,"default"=>TRUE,"order"=>3));

		$_toReturn = $this->add_view(
				'_public/_form'
				,array(
					'index'=>$_intFrmIndex
					,'crud_controller'=>'developer'
					,'controls'=>$this->_arrGetEditControls()
					,'layout'=>array(
						array('code', "")
						,array("name_en", "name_th", "name_jp")
						,array("description")
						,array("remark")
					)
				), TRUE
			);
		return $_toReturn;
	}
	
/*
	function _getMainPanel() { //override
		$_editForm = $this->__getEditForm(2);
		return $this->add_view(
			'_public/_list'
			,array(
				'index' => 1
				,'dataview_fields' => $this->_arrDataViewFields
				,'edit_dlg' => array(
					'template' => $_editForm
					,'options' => array('width'=>$this->_editPanelWidth, 'height'=>$this->_editPanelHeight)
				)
			),TRUE
		);
	}

	function _getLeftPanel() {
		$_arrReturn = $this->add_view('_public/_search_panel'
			, array(
				'controls' => array(
					array(
						"type" => "txt"
						,"label" => 'ขื่อลูกค้า'
						,"name" => "cus_name"
					)
					,array(
						"type" => "sel"
						,"label" => 'จังหวัด'
						,"name" => "province_rowid"
						,"class"=>"sel-province"
						,"sel_options" => $this->_selOptions['province']
						,"sel_val" => "rowid"
						,"sel_text" => "name_th"
						,"on_change" => "evnt_onSelectProvince(str, ev, ui);"
					)
					,array(
						"type" => "sel"
						,"label" => 'อำเภอ'
						,"name" => "amphoe_rowid"
						,"class"=>"sel-amphoe set-disabled"
						,"sel_options" => array()
					)
					,array(
						"type" => "txt"
						,"label" => 'เบอร์ติดต่อ'
						,"name" => "contact_no"
					)
					,array(
						"type" => "txt"
						,"label" => 'อีเมล์'
						,"name" => "email"
					)
				)
			), TRUE
		);
		return $_arrReturn;
	}
*/
}