<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Display_map extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->modelName = 'Mdl_building';
	}
	public function index($region_rowid = 0, $province_rowid = 0, $city_rowid = 0, $building_rowid = 0) {
		$this->add_css(
			array(
				'public/css/jquery/ui/1.10.4/cupertino/jquery-ui.min.css'
				,'public/css/leaflet/0.7.3/leaflet.css'
				,'public/css/leaflet/mouseposition/L.Control.MousePosition.css'
				//,'public/css/leaflet/leaftlet.draw/0.2.4/leaflet.draw.css'
				//,'public/css/leaflet/markercluster/MarkerCluster.css'
				//,'public/css/leaflet/markercluster/MarkerCluster.Default.css'
				,'public/css/leaflet/measure/leaflet.measure.css'
				,'public/css/leaflet/label/leaflet.label.css'
				//,array('public/css/leaflet/markercluster/MarkerCluster.Default.ie.css', 'ie6')
				//,array('public/css/leaflet/markercluster/MarkerCluster.Default.ie.css', 'ie7')
			)
		);
		$this->add_js(
			array(
				'public/js/jquery/1.11.0/jquery.js'
				,'public/js/jquery/ui/1.10.4/jquery-ui.min.js'
				,'public/js/jquery/jQuery.fullscreen.min.js'
				,'public/js/leaflet/0.7.3/leaflet.js'
				,'public/js/leaflet/mouseposition/L.Control.MousePosition.js'
				//,'public/js/leaflet/leaftlet.draw/0.2.4/leaflet.draw.js'
				//,'public/js/leaflet/markercluster/leaflet.markercluster.js'
				,'public/js/leaflet/measure/leaflet.measure.js'
				,'public/js/leaflet/label/Label.js'
				,'public/js/leaflet/label/BaseMarkerMethods.js'
				,'public/js/leaflet/label/Marker.Label.js'
				,'public/js/leaflet/label/Map.Label.js'
				//,'http://maps.google.com/maps/api/js?v=3.2&sensor=false'
				//,'public/js/leaflet/layer/tile/Google.js'
				,'public/js/jsUtilities.js'
				,'public/js/jsGlobalConstants.js'
				,'public/js/display_map/main.js'
				//,array('alert("You are using IE");', 'ie_custom')
				//,array('alert("Stop using IE6 (test period :D)");', 'ie6_custom')
			)
		);
		$this->load->model('Mdl_building', 'b');
		$_arrResult = $this->b->list_map_objects();
		$_geoJson = '{}';
		if (is_array($_arrResult) && count($_arrResult) > 0) {
			$_geoJson = $_arrResult[0]['geo_json'];
		}

		$_html = <<<HTML

		<style>
			#disp_layout_map {width:100%;height:100%;}
		</style>
		<script>
			$(function() {
				var _dataGeoJson = $_geoJson;
				_doInitMap(_dataGeoJson);
			});
		</script>
		<div id="disp_layout_map"></div>

HTML;
		
		$pass['middle_panel'] = $_html;
		$pass['title'] = "แสดงผลบนแผนที่";
		$this->add_view_with_script_header('_public/_template_main', $pass);
	}

	function get_building_display_image($rowid = 0) {
		$_img_file_path = '';
		if ((int) $rowid > 0) {
			$this->load->helper('file');
			$_filesList = get_filenames('public/images/building_popup_images/', TRUE);
			foreach ($_filesList as $_file) {
				$_arr = explode('.', $_file);
				if (count($_arr) > 1) {
					if (substr($_arr[0], (strrpos($_arr[0], '\\') + 1)) == $rowid) {
						$_img_file_path = $_file;
					}
				}
			}
		}
		if (strlen($_img_file_path) == 0) {
			$_img_file_path = "public/images/img_not_found.png";
		}
		//echo ('Content-Type:' . get_mime_by_extension($_img_file_path));
		$_str = read_file($_img_file_path);
		header('Content-Type:' . get_mime_by_extension($_img_file_path));
		echo ($_str);
	}	
}