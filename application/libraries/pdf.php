<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

define('_MPDF_URI', base_url() . 'public/mPDF/'); // must be  a relative or absolute URI - not a file system path
class pdf { 
	function pdf() {
		$CI = & get_instance();
		log_message('Debug', 'mPDF class is loaded.');
		$this->_is_draft = FALSE;
	}
	
	function isDraft($blnDraft = TRUE) {
		$this->_is_draft = $blnDraft;
	}
	
 	function exportMPDF($htmView, $fileName = 'mpdf.pdf', $m_left = 10, $m_right = 5, $m_top = 5, $m_bottom = 5, $m_header = 0, $m_footer = 0) {
		include_once APPPATH . '/third_party/mPDF/mpdf.php';
		
		ob_end_clean();
		
		$mpdf = new mPDF('th', 'A4', 0, '', $m_left, $m_right, $m_top, $m_bottom, $m_header, $m_footer);
		$mpdf->setAutoTopMargin = 'stretch';
		$mpdf->autoMarginPadding = 0; //pad between header and top content, default = 2 (mm)
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->SetAutoFont();

		$mpdf->progbar_heading = 'Generating report progress';
		$mpdf->StartProgressBarOutput(2);

		//$mpdf->mirrorMargins = 1;
		//$mpdf->list_number_suffix = ')';
		//$mpdf->hyphenate = true;
		
		//$mpdf->debug = true;
		//$mpdf->showImageErrors = true;
		//$mpdf->allow_output_buffering = true;
		
		$mpdf->WriteHTML($htmView);
		
		/*++ disable page num if page = 1 */
		if (count($mpdf->pages) == 1) {
			$mpdf->pagenumPrefix = '';
			$mpdf->pagenumSuffix = '';
			$mpdf->PageNumSubstitutions[] = array('from'=>1, 'suppress'=>'on');
		} else {
			$mpdf->pagenumPrefix = '( หน้าที่ ';
			$mpdf->pagenumSuffix = ' )';			
		}
		/*-- disable page num if page = 1 */
		
		if ($this->_is_draft) {
			$mpdf->SetWatermarkText('DRAFT');
			$mpdf->watermarkImageAlpha = 0.1;
			$mpdf->showWatermarkText = true;
		}
		$mpdf->Output($fileName, 'I');
	}	
}