<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class access_control {
	var $_MSG_ACCESS_NOT_ALLOWED = 'v_XX_1 ไม่มีสิทธิ์ใช้งานหน้านี้';
	var $_MSG_FUNCTION_NOT_ALLOWED = 'v_XX_1 ไม่มีสิทธิ์ใช้งานฟังก์ชันนี้';
	var $_MSG_NO_POST_PARAMETERS = 'ไม่พบตัวแปรใน POST method:: v_XX_1';
	var $_MSG_INVALID_PARAMETERS = 'ตัวแปรไม่ถูกต้องตามมาตรฐาน v_XX_1';
	function access_control() {
		$this->_ACR = array();
        log_message('Debug', '"Access Control" class is loaded.');
    }
	
	function _arrListACR($userid) {
		$_arrReturn = array();
		if ( ! empty($userid) ) {
			$_db = JFactory::getDbo();
			$_viewlevels = implode(",", array_unique(JAccess::getAuthorisedViewLevels($userid)));
			if (strlen(trim($_viewlevels)) == 0) $_viewlevels = '0';
//echo $_viewlevels;exit;
			$_sql = $_db->getQuery(true);
			$_sql
				->select($_db->quoteName('v.title'))
				->from($_db->quoteName('#__viewlevels', 'v'))
				->where("v.id IN ($_viewlevels) AND v.title LIKE '-%'")
				->order('ordering ASC')
			;
			$_db->setQuery($_sql);
			$_result = $_db->loadObjectList();
			if ($_result) foreach($_result as $_row) $_arrReturn[] = $_row->title;
		}
		return $_arrReturn;
	}
	
	function _loadPageAccessRight($userid) {
		$CI =& get_instance();
		$_AC = $CI->session->userdata('_AC');
		if (! empty($_AC)) {
			$_objAC = json_decode($_AC, true);
			$this->_ACR = $_objAC;
		}
		if (count($this->_ACR) == 0) {
			$_user_id = (int) $userid;
			$_arr = $this->_arrListACR($_user_id);
			if ($_arr) {
				/* format is "-[name]__[access_right]" */
				foreach($_arr as $acr) {
					$_acr = strtolower($acr);
					$_parts = explode('__', $_acr);
					if (is_array($_parts) && (count($_parts) > 0)) {
						$_group = substr($_parts[0], 1);
						$_medthod = '_ALLOW';
						if (count($_parts) > 1) {
							$_medthod = $_parts[1];
						}
						if ( ! array_key_exists($_group, $this->_ACR)) $this->_ACR[$_group] = array();
						$this->_ACR[$_group][] = $_medthod;
					}
				}
			}
			$CI->session->set_userdata(
				'_AC', 
				json_encode($this->_ACR)
			);
		}
	}

	function blnCheckRight($name = '', $group = NULL) {
		$_group = ( ! empty($group) )?strtolower($group):'';
		if (array_key_exists($_group, $this->_ACR)) {
			if (in_array('_ALLOW', $this->_ACR[$_group])) {
				if (empty($name) || in_array(strtolower($name), $this->_ACR[$_group])) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}
}