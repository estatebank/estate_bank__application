<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class exchange_rate { 
	function exchange_rate() {
		$CI = & get_instance();
		$CI->load->driver('cache', array('adapter'=>'file')); //, array('adapter' => 'apc', 'backup' => 'file'));
		if ( ! $this->_ListRate = $CI->cache->get('list_exchange_rate')) {
			$this->_IS_CACHE = FALSE;
			$this->_performQueryServices();
			//Save into the cache for 60 minutes
			$CI->cache->save('list_exchange_rate', $this->_ListRate, 3600);
		} else {
			$this->_IS_CACHE = TRUE;
		}
	}
	function _performQueryServices() {
		$this->_ListRate = array();
		$this->_BASE_URL = "http://query.yahooapis.com/v1/public/yql";
		$_yql_query = 'SELECT * FROM yahoo.finance.xchange WHERE pair IN ';
		$_yql_query .= '("USDTHB","USDJPY","USDCNY","THBUSD","THBJPY","THBCNY","JPYUSD","JPYTHB","JPYCNY","CNYTHB","CNYJPY","CNYUSD")';
		$this->_QUERY_URL = $this->_BASE_URL . "?q=" . urlencode($_yql_query) . "&format=json&env=store://datatables.org/alltableswithkeys";
		// Make call with cURL
		$_session = curl_init($this->_QUERY_URL);
		curl_setopt($_session, CURLOPT_RETURNTRANSFER, TRUE);
		$_json = curl_exec($_session);
		$_objJson = json_decode($_json);
		if (! (isset($_objJson->query->results->rate) && is_array($_objJson->query->results->rate))) return FALSE;
		$_arrResult = $_objJson->query->results->rate;
		foreach ($_arrResult as $_eaObj) {
			if ((isset($_eaObj->Name)) && (strpos($_eaObj->Name, '/') !== FALSE)) {
				list($_from, $_to) = explode('/', $_eaObj->Name);
				$_from = strtolower($_from);
				$_to = strtolower($_to);
				$_rate = (isset($_eaObj->Rate)) ? (float) $_eaObj->Rate : 0;
				$_date = (isset($_eaObj->Date)) ? $_eaObj->Date : '';
				$_time = (isset($_eaObj->Time)) ? $_eaObj->Time : '';
				if ((!empty($_from)) && (!empty($_to)) && ($_rate > 0)) {
					if (! array_key_exists($_from, $this->_ListRate)) $this->_ListRate[$_from] = array();
					$this->_ListRate[$_from][$_to] = array("rate"=>$_rate, "date"=>$_date, "time"=>$_time);
				}
			}
		}
		if (! ((is_array($this->_ListRate)) && (count($this->_ListRate) > 0))) $this->_ListRate = FALSE;		
	}
	function arrGetListRate($from = NULL, $to = NULL) {
		if (! ((is_array($this->_ListRate)) && (count($this->_ListRate) > 0))) return FALSE;
		if (empty($from)) return $this->_ListRate;
		$_from = strtolower($from);
		if (isset($this->_ListRate[$_from])) {
			if (empty($to)) return array($_from => $this->_ListRate[$_from]);
			$_to = strtolower($to);
			if (isset($this->_ListRate[$_from][$_to])) return array($_from => array($_to => $this->_ListRate[$_from][$_to]));
		}
		return FALSE;
	}
	function fltGetExchangeRate($from = NULL, $to = NULL) {
		if (empty($from) || empty($to)) return FALSE;
		$_from = strtolower($from);
		if (isset($this->_ListRate[$_from])) {
			if (empty($to)) return FALSE;
			$_to = strtolower($to);
			if (isset($this->_ListRate[$_from][$_to])) return $this->_ListRate[$_from][$_to]['rate'];
		}
		return FALSE;
	}
}