<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class php_excel { 
	function php_excel() {
		require_once APPPATH . '/third_party/PHPExcel/Classes/PHPExcel.php';
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE); 
		ini_set('display_startup_errors', TRUE);
//		date_default_timezone_set('Europe/London'); //Asia/Bangkok
//		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

		log_message('Debug', 'PHPExcel class is loaded.');
		
		$this->_objPE = new PHPExcel();
	}
	function get_object() {
		return $this->_objPE;
	}
	
	function save_excel($fileName = 'excel.xls') {
		$CI = & get_instance();
		$CI->load->helper('excel_helper');
		$_filpath = _get_excel_file_path($fileName);
		$_urlpath = _get_excel_url_path($fileName);
		$_ext = pathinfo($_filpath, PATHINFO_EXTENSION);
		if (strtolower($_ext) != 'xlsx') {
			$_filpath = str_replace('.' . $_ext, '.xlsx', $_filpath);
		}
		//$callStartTime = microtime(true);
		// Save Excel 2007 file
		$objWriter = PHPExcel_IOFactory::createWriter($this->_objPE, 'Excel2007');
		$objWriter->save($_filpath);
		return $_urlpath;
/*
		// Save Excel 95 file
		$objWriter = PHPExcel_IOFactory::createWriter($this->_objPE, 'Excel5');
		$objWriter->save($_filpath); //.xls
*/
		//$callEndTime = microtime(true);
		//$callTime = $callEndTime - $callStartTime;
	}
/*	
	function test() {
		//Set document properties
		$this->_objPE->getProperties()->setCreator("PRMS")
									 ->setLastModifiedBy("PRMS")
									 ->setTitle('Title Test')
									 ->setSubject('Subject Test')
									// ->setDescription("Test document for PHPExcel, generated using PHP classes.")
									// ->setKeywords("office PHPExcel php")
									// ->setCategory("Test result file")
									;


		// Add some data
		$this->_objPE->setActiveSheetIndex(0)
					->setCellValue('A1', 'Hello')
					->setCellValue('B2', 'world!')
					->setCellValue('C1', 'Hello')
					->setCellValue('D2', 'world!');

		// Miscellaneous glyphs, UTF-8
		$this->_objPE->setActiveSheetIndex(0)
					->setCellValue('A4', 'Miscellaneous glyphs')
					->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');


		$this->_objPE->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
		$this->_objPE->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
		$this->_objPE->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

		// Rename worksheet
		$this->_objPE->getActiveSheet()->setTitle('Test');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$this->_objPE->setActiveSheetIndex(0);

		echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo($_filpath, PATHINFO_BASENAME)) , EOL;
		echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
		// Echo memory usage
		echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;
		// Echo memory peak usage
		echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
		// Echo done
		echo date('H:i:s') , " Done writing files" , EOL;
		echo 'Files have been created in ' , $_filpath , EOL;

	}
*/
}