
/* ++ MRT lm_type_rowid = 2 ++ */
/*

SELECT * FROM t_landmark WHERE lm_type_rowid = 2 AND name_th LIKE '%หัวลำโพง%'

UPDATE t_landmark 
SET name_jp = 'バーンスー駅'
WHERE lm_type_rowid = 2 AND name_th LIKE '%บางซื่อ%'

*/

UPDATE t_landmark 
SET name_jp = CASE 
	WHEN name_th LIKE '%บางซื่อ%' THEN 'バーンスー駅'
	WHEN name_th LIKE '%กำแพงเพชร%' THEN 'カムペーンペット駅'
	WHEN name_th LIKE '%จตุจักร%' THEN 'チャトゥチャック公園駅'
	WHEN name_th LIKE '%พหลโยธิน%' THEN 'パホンヨーティン駅'
	WHEN name_th LIKE '%ลาดพร้าว%' THEN 'ラートプラーオ駅'
	WHEN name_th LIKE '%รัชดา%' THEN 'ラッチャダーピセーク駅'
	WHEN name_th LIKE '%สุทธิสาร%' THEN 'スッティサーン駅'
	WHEN name_th LIKE '%ห้วยขวาง%' THEN 'フワイクワーン駅'
	WHEN name_th LIKE '%วัฒนธรรม%' THEN 'タイ文化センター駅'
	WHEN name_th LIKE '%พระราม%' THEN 'ラーマ9世駅'
	WHEN name_th LIKE '%เพชรบุรี%' THEN 'ペッチャブリー駅'
	WHEN name_th LIKE '%สุขุมวิท%' THEN 'スクムウィット駅'
	WHEN name_th LIKE '%สิริกิต%' THEN 'シリキット・コンベンション・センター駅'
	WHEN name_th LIKE '%คลองเตย%' THEN 'クローントゥーイ駅'
	WHEN name_th LIKE '%ลุมพินี%' THEN 'ルンピニー駅'
	WHEN name_th LIKE '%สีลม%' THEN 'シーロム駅'
	WHEN name_th LIKE '%สามย่าน%' THEN 'サームヤーン駅'
	WHEN name_th LIKE '%หัวลำโพง%' THEN 'フワランポーン駅'
END
WHERE lm_type_rowid = 2
AND COALESCE(name_jp, '') = ''
;

/* -- MRT lm_type_rowid = 2 -- */


/* ++ BTS lm_type_rowid = 4 ++ */
/*
--future list
	-- silom
	WHEN name_th LIKE '%%' THEN 'スックサーウィッタヤー駅（予定駅）'
	
	WHEN name_th LIKE '%%' THEN 'バンウェーク駅'
	WHEN name_th LIKE '%%' THEN 'クラチョームトーン駅'
	WHEN name_th LIKE '%%' THEN 'バンプロム駅'
	WHEN name_th LIKE '%%' THEN 'イントラワート駅'
	WHEN name_th LIKE '%%' THEN 'プロムラチャチョンニー駅'
	WHEN name_th LIKE '%%' THEN 'タリンチャン駅'
	-- sukhumvit
	WHEN name_th LIKE '%%' THEN 'セーナールアム駅（予定駅）'
	
	WHEN name_th LIKE '%%' THEN 'サムロン駅'
	WHEN name_th LIKE '%%' THEN 'プーチャオサミングプライ駅'
	WHEN name_th LIKE '%%' THEN 'エラワン・ミュージアム駅'
	WHEN name_th LIKE '%%' THEN 'ネーヴァル・アカデミー駅'
	WHEN name_th LIKE '%%' THEN 'サムット・プラーカーン・シティホール駅'
	WHEN name_th LIKE '%%' THEN 'シーナカリン駅'
	WHEN name_th LIKE '%%' THEN 'プレックサー駅'
	WHEN name_th LIKE '%%' THEN 'サイ・ルアット駅'
	WHEN name_th LIKE '%%' THEN 'ケーハ・サムット・プラーカーン駅'
	WHEN name_th LIKE '%%' THEN 'サワンカニワット駅'
	WHEN name_th LIKE '%%' THEN 'ムアン・ボラン駅'
	WHEN name_th LIKE '%%' THEN 'シチャンプラディット駅'
	WHEN name_th LIKE '%%' THEN 'バンプー駅'
	

SELECT * FROM t_landmark WHERE lm_type_rowid = 4 AND name_th LIKE '%%'

*/

UPDATE t_landmark 
SET name_jp = CASE 
	WHEN name_th LIKE '%สนามกีฬา%' THEN 'サナームキラーヘンチャート駅（国立競技場駅）'
	WHEN name_th LIKE '%สยาม%' THEN 'サイアム駅'
	WHEN name_th LIKE '%ราชดำริ%' THEN 'ラーチャダムリ駅'
	WHEN name_th LIKE '%ศาลาแดง%' THEN 'サラデーン駅'
	WHEN name_th LIKE '%ช่องนนทรี%' THEN 'チョーンノンシー駅'
	WHEN name_th LIKE '%สุรศักดิ์%' THEN 'スラサック駅'
	WHEN name_th LIKE '%สะพานตากสิน%' THEN 'サパーンタークシン駅'
	WHEN name_th LIKE '%กรุงธนบุรี%' THEN 'クルン・トンブリー駅'
	WHEN name_th LIKE '%วงเวียนใหญ่%' THEN 'ウォンウィアン・ヤイ駅'
	WHEN name_th LIKE '%โพธิ์นิมิตร%' THEN 'ポーニミット駅'
	WHEN name_th LIKE '%ตลาดพลู%' THEN 'タラートプルー駅'
	WHEN name_th LIKE '%วุฒากาศ%' THEN 'ウターカート駅'
	WHEN name_th LIKE '%บางหว้า%' THEN 'バーンワー駅'
	WHEN name_th LIKE '%หมอชิต%' THEN 'モーチット駅'
	WHEN name_th LIKE '%สะพานควาย%' THEN 'サパーンクワーイ駅'
	WHEN name_th LIKE '%อารีย์%' THEN 'アーリー駅'
	WHEN name_th LIKE '%สนามเป้า%' THEN 'サナームパオ駅'
	WHEN name_th LIKE '%อนุสาวรีย์ชัยสมรภูมิ%' THEN 'アヌサーワリーチャイサモーラプーム駅（戦勝記念塔駅）'
	WHEN name_th LIKE '%พญาไท%' THEN 'パヤータイ駅'
	WHEN name_th LIKE '%ราชเทวี%' THEN 'ラーチャテーウィー駅'
	WHEN name_th LIKE '%ชิดลม%' THEN 'チットロム駅'
	WHEN name_th LIKE '%เพลินจิต%' THEN 'プルンチット駅'
	WHEN name_th LIKE '%นานา%' THEN 'ナーナー駅'
	WHEN name_th LIKE '%อโศก%' THEN 'アソーク駅'
	WHEN name_th LIKE '%พร้อมพงษ์%' THEN 'プロームポン駅'
	WHEN name_th LIKE '%ทองหล่อ%' THEN 'トンロー駅'
	WHEN name_th LIKE '%เอกมัย%' THEN 'エカマイ駅'
	WHEN name_th LIKE '%พระโขนง%' THEN 'プラカノン駅'
	WHEN name_th LIKE '%อ่อนนุช%' THEN 'オンヌット駅'
	WHEN name_th LIKE '%บางจาก%' THEN 'バーンチャーク駅'
	WHEN name_th LIKE '%ปุณณวิถี%' THEN 'プナウィティ駅'
	WHEN name_th LIKE '%อุดมสุข%' THEN 'ウドムスック駅'
	WHEN name_th LIKE '%บางนา%' THEN 'バンナー駅'
	WHEN name_th LIKE '%แบริ่ง%' THEN 'ベーリング駅'
END
WHERE lm_type_rowid = 4
AND COALESCE(name_jp, '') = ''
;
/* -- BTS lm_type_rowid = 4 -- */


/* ++ Building ++ */


DROP TABLE IF EXISTS tmp_port_building;

CREATE TABLE tmp_port_building (
  name_en VARCHAR(200)
  , name_jp VARCHAR(200)
  , b_type_rowid INTEGER
  , name_th VARCHAR(200)
  , postal_code VARCHAR(100)
  , province VARCHAR(100)
  , district VARCHAR(100)
  , address VARCHAR(500)
  , area VARCHAR(100)
  , bts_skv VARCHAR(50)
  , bts_silom VARCHAR(50)
  , mrt VARCHAR(50)
  , apl VARCHAR(50)
  , walktime_to_station_min VARCHAR(5)
  , years VARCHAR(10)
  , floors VARCHAR(10)
  , units VARCHAR(10)
  , internet VARCHAR(5)
  , pet VARCHAR(5)
  , pool VARCHAR(5)
  , sauna VARCHAR(5)
  , gym VARCHAR(5)
  , conv_store VARCHAR(5)
  , nhk VARCHAR(5)
  , jp_srvc VARCHAR(5)
  , playground VARCHAR(5)
  , shuttle VARCHAR(5)
  , nearest_super VARCHAR(100)
  , laundry VARCHAR(100)
  , parking VARCHAR(5)
  , security VARCHAR(5)
  , reception VARCHAR(5)
  , cleaner VARCHAR(5)
  , site_area VARCHAR(100)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


LOAD DATA INFILE 'E:\\#wamp_root\\www\\real_estate\\app\\#db script\\20160224_port_from_excel\\db_adjusted.csv' 
INTO TABLE tmp_port_building 
FIELDS TERMINATED BY ',' 
ENCLOSED BY ''
LINES TERMINATED BY '\r\n'
;


TRUNCATE TABLE t_building;

/*
INSERT INTO t_building (b_type_rowid, name_en, name_jp, name_th, address
, description, srvc_internet_rowid, srvc_laundry_rowid, nrfc_super_rowid
, is_pet, is_pool, is_gym, is_conv_store, is_nhk, is_jp_srvc, is_playground, is_shuttle
, province_rowid, postal_code, units, floors, year, create_by, create_date)
*/
SELECT b_type_rowid, name_en, name_jp, name_th, address
, TRIM(CONCAT(COALESCE(NULLIF(area, 'その他'), ''), CONCAT(' walking time to nearest station ', walktime_to_station_min, ' (min.)'))) AS description
, CASE WHEN internet LIKE '%プロバイダとの直接契約%' THEN 1 WHEN internet LIKE '%有料%' THEN 2 WHEN internet LIKE '%無料Wifi%' THEN 4 WHEN internet LIKE '%無料%' THEN 3 END AS srvc_internet_rowid
, CASE WHEN laundry LIKE '%ランドリーサービス%' THEN 5 WHEN laundry LIKE '%コインランドリー%' THEN 6 END AS srvc_laundry_rowid
, CASE WHEN nearest_super LIKE '%Fuji Super%' THEN 1 WHEN nearest_super LIKE '%Villa Market%' THEN 2 WHEN nearest_super LIKE '%Gourmet Market%' THEN 3 
	WHEN nearest_super LIKE '%Central Food Hall%' THEN 4 WHEN nearest_super LIKE '%Big C%' THEN 5 WHEN nearest_super LIKE '%TESCO Lotus%' THEN 10 
	WHEN nearest_super LIKE '%TOPS%' THEN 7 WHEN nearest_super LIKE '%Max Value%' THEN 8 WHEN nearest_super LIKE '%Foodland%' THEN 9 
	WHEN nearest_super LIKE '%Lotus%' THEN 6 WHEN nearest_super LIKE '%その他%' THEN 11 
END AS nrfc_super_rowid
, CASE WHEN pet LIKE '%無%' THEN 0 WHEN pet LIKE '%有%' THEN 1 END AS is_pet
, CASE WHEN pool LIKE '%無%' THEN 0 WHEN pool LIKE '%有%' THEN 1 END AS is_pool
, CASE WHEN gym LIKE '%無%' THEN 0 WHEN gym LIKE '%有%' THEN 1 END AS is_gym
, CASE WHEN conv_store LIKE '%無%' THEN 0 WHEN conv_store LIKE '%有%' THEN 1 END AS is_conv_store
, CASE WHEN nhk LIKE '%無%' THEN 0 WHEN nhk LIKE '%有%' THEN 1 END AS is_nhk
, CASE WHEN jp_srvc LIKE '%無%' THEN 0 WHEN jp_srvc LIKE '%有%' THEN 1 END AS is_jp_srvc
, CASE WHEN playground LIKE '%無%' THEN 0 WHEN playground LIKE '%有%' THEN 1 END AS is_playground
, CASE WHEN shuttle LIKE '%無%' THEN 0 WHEN shuttle LIKE '%有%' THEN 1 END AS is_shuttle
, 10, postal_code, units, floors, years, 0, CURRENT_TIMESTAMP
FROM tmp_port_building;


/*
UPDATE t_building
SET map_position = PointFromText(CONCAT('POINT(',100.5725556,' ',13.7297082,')'))
WHERE rowid = 1 

*/
