﻿-- DROP TABLE IF EXISTS m_nearby_facility CASCADE;

CREATE TABLE IF NOT EXISTS m_nearby_facility (
  rowid INTEGER NOT NULL,
  fac_cat_rowid INTEGER,
  code VARCHAR(10),
  name_en VARCHAR(100),
  name_th VARCHAR(100),
  name_jp VARCHAR(100),
  description VARCHAR(500),
  remark VARCHAR(500),
  icon_path VARCHAR(500),
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date DATETIME DEFAULT NULL,
  update_by INTEGER,
  update_date DATETIME DEFAULT NULL,
  PRIMARY KEY (rowid)
  , FOREIGN KEY (fac_cat_rowid) REFERENCES m_facility_category(rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

INSERT INTO m_nearby_facility (rowid, fac_cat_rowid, code, name_en, name_th, name_jp, icon_path, create_by, create_date) VALUES 
 (1, 1, 'FCL001', 'Fuji Super', 'Fuji Super', 'Fuji Super', NULL, 0, CURRENT_TIMESTAMP)
,(2, 1, 'FCL002', 'Villa Market', 'Villa Market', 'Villa Market', NULL, 0, CURRENT_TIMESTAMP)
,(3, 1, 'FCL003', 'Gourmet Market', 'Gourmet Market', 'Gourmet Market', NULL, 0, CURRENT_TIMESTAMP)
,(4, 1, 'FCL004', 'Central Food Hall', 'Central Food Hall', 'Central Food Hall', NULL, 0, CURRENT_TIMESTAMP)
,(5, 1, 'FCL005', 'Big C', 'Big C', 'Big C', NULL, 0, CURRENT_TIMESTAMP)
,(6, 1, 'FCL006', 'Lotus', 'Lotus', 'Lotus', NULL, 0, CURRENT_TIMESTAMP)
,(7, 1, 'FCL007', 'TOPS', 'TOPS', 'TOPS', NULL, 0, CURRENT_TIMESTAMP)
,(8, 1, 'FCL008', 'Max Value', 'Max Value', 'Max Value', NULL, 0, CURRENT_TIMESTAMP)
,(9, 1, 'FCL009', 'Foodland', 'Foodland', 'Foodland', NULL, 0, CURRENT_TIMESTAMP)
,(10, 1, 'FCL010', 'TESCO Lotus', 'TESCO Lotus', 'TESCO Lotus', NULL, 0, CURRENT_TIMESTAMP)
,(11, 1, 'FCL011', 'Other', 'อื่นๆ', 'その他', NULL, 0, CURRENT_TIMESTAMP)
;
--,(, NULL, '', '', '', '', NULL, 0, CURRENT_TIMESTAMP)
------------------------------------------


--- Rule instead of delete 
DROP TRIGGER IF EXISTS trg_m_nearby_facility_delete; 

DELIMITER //

CREATE TRIGGER trg_m_nearby_facility_delete BEFORE DELETE ON m_nearby_facility 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE m_nearby_facility 
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------
