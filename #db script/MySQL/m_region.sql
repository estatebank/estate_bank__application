-- DROP TABLE IF EXISTS m_region;

CREATE TABLE m_region
(
  rowid INTEGER NOT NULL,
  name_th VARCHAR(30),
  name_en VARCHAR(30),
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS trg_m_province_delete; 

DELIMITER //

CREATE TRIGGER trg_m_region_delete BEFORE DELETE ON m_region
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
END;//

DELIMITER ;

INSERT INTO m_region (rowid, name_th, name_en) VALUES (1, 'กรุงเทพและปริมณฑล', 'Bangkok and vincity');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (2, 'ภาคกลาง', 'Central Region');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (3, 'ภาคตะวันออก', 'Eastern Region');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (4, 'ภาคตะวันตก', 'Western Region');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (5, 'ภาคเหนือ', 'Northern Region');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (6, 'ภาคตะวันออกเฉียงเหนือ', 'North-Eastern Region');
INSERT INTO m_region (rowid, name_th, name_en) VALUES (7, 'ภาคใต้', 'Southern Region');

