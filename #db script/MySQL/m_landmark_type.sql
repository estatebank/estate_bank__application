﻿-- DROP TABLE IF EXISTS m_landmark_type CASCADE;

CREATE TABLE IF NOT EXISTS m_landmark_type (
  rowid INTEGER NOT NULL,
  code VARCHAR(10),
  name_en VARCHAR(100),
  name_th VARCHAR(100),
  name_jp VARCHAR(100),
  description VARCHAR(500),
  remark VARCHAR(500),
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date DATETIME DEFAULT NULL,
  update_by INTEGER,
  update_date DATETIME DEFAULT NULL,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

TRUNCATE TABLE m_landmark_type;

INSERT INTO m_landmark_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (1, 'BTSSK', 'BTS-Sukhumvit Ln.', 'สถานีรถไฟฟ้า BTS - สายสุขุมวิท', NULL, NULL, 0, SYSDATE())
,(2, 'BTSAS', 'BTS-Silom Ln.', 'สถานีรถไฟฟ้า BTS - สายสีลม', NULL, NULL, 0, SYSDATE())
,(5, 'MRT', 'MRT Station', 'สถานีรถไฟฟ้าใต้ดิน', NULL, NULL, 0, SYSDATE())
,(6, 'BRT', 'BRT Station', 'สถานีรถ BRT', NULL, NULL, 0, SYSDATE())
,(7, 'BUSST', 'Bus Stop', 'ป้ายรถเมล์', NULL, NULL, 0, SYSDATE())
,(8, 'TRAIN', 'Train Station', 'สถานีรถไฟ', NULL, NULL, 0, SYSDATE())
,(9, 'PIER', 'Pier', 'ท่าเรือ', NULL, NULL, 0, SYSDATE())
,(10, 'AIRPT', 'Airport', 'สนามบิน', NULL, NULL, 0, SYSDATE())
,(11, 'TRANS', 'Transportation', 'สถานีขนส่งสินค้าและผู้โดยสาร', NULL, NULL, 0, SYSDATE())
;


------------------------------------------

DROP TRIGGER IF EXISTS trg_m_landmark_type_delete; 

DELIMITER //

CREATE TRIGGER trg_m_landmark_type_delete BEFORE DELETE ON m_landmark_type 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE m_landmark_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------


-- Foreign Key
ALTER TABLE t_landmark
  ADD CONSTRAINT t_landmark_lm_type_rowid_fkey FOREIGN KEY (lm_type_rowid)
      REFERENCES m_landmark_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
