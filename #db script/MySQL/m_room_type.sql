﻿-- DROP TABLE IF EXISTS m_room_type CASCADE;

CREATE TABLE IF NOT EXISTS m_room_type (
  rowid INTEGER NOT NULL,
  code VARCHAR(10),
  name_en VARCHAR(100),
  name_th VARCHAR(100),
  name_jp VARCHAR(100),
  description VARCHAR(500),
  remark VARCHAR(500),
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date DATETIME DEFAULT NULL,
  update_by INTEGER,
  update_date DATETIME DEFAULT NULL,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

INSERT INTO m_room_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (1, '1LK', '1LK', '1LK', NULL, NULL, 1, CURRENT_TIMESTAMP)
;
------------------------------------------


--- Rule instead of delete 
DROP TRIGGER IF EXISTS trg_m_room_type_delete; 

DELIMITER //

CREATE TRIGGER trg_m_room_type_delete BEFORE DELETE ON m_room_type 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE m_room_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------

-- Foreign Key
ALTER TABLE t_room
  ADD CONSTRAINT t_room_room_type_rowid_fkey FOREIGN KEY (room_type_rowid)
      REFERENCES m_room_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
