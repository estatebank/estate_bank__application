-- DROP VIEW v_room_images;

CREATE OR REPLACE VIEW _v_main_room_img
AS 
	SELECT room_rowid
	FROM t_room_images
	GROUP BY room_rowid
;

CREATE OR REPLACE VIEW _v_room_img_indx1
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 1
;

CREATE OR REPLACE VIEW _v_room_img_indx2
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 2
;

CREATE OR REPLACE VIEW _v_room_img_indx3
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 3
;

CREATE OR REPLACE VIEW _v_room_img_indx4
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 4
;

CREATE OR REPLACE VIEW _v_room_img_indx5
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 5
;

CREATE OR REPLACE VIEW _v_room_img_indx6
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 6
;

CREATE OR REPLACE VIEW _v_room_img_indx7
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 7
;

CREATE OR REPLACE VIEW _v_room_img_indx8
AS 
	SELECT rowid, image, ext, title, room_rowid, img_index FROM t_room_images WHERE img_index = 8
;

CREATE OR REPLACE VIEW v_room_images
AS
	SELECT t.room_rowid
	, i1.rowid AS img_rowid1, i1.ext AS img_ext1, i1.image AS image1, i1.title AS img_title1
	, i2.rowid AS img_rowid2, i2.ext AS img_ext2, i2.image AS image2, i2.title AS img_title2
	, i3.rowid AS img_rowid3, i3.ext AS img_ext3, i3.image AS image3, i3.title AS img_title3
	, i4.rowid AS img_rowid4, i4.ext AS img_ext4, i4.image AS image4, i4.title AS img_title4
	, i5.rowid AS img_rowid5, i5.ext AS img_ext5, i5.image AS image5, i5.title AS img_title5
	, i6.rowid AS img_rowid6, i6.ext AS img_ext6, i6.image AS image6, i6.title AS img_title6
	, i7.rowid AS img_rowid7, i7.ext AS img_ext7, i7.image AS image7, i7.title AS img_title7
	, i8.rowid AS img_rowid8, i8.ext AS img_ext8, i8.image AS image8, i8.title AS img_title8
	FROM _v_main_room_img t 
		LEFT JOIN _v_room_img_indx1 i1 ON i1.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx2 i2 ON i2.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx3 i3 ON i3.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx4 i4 ON i4.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx5 i5 ON i5.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx6 i6 ON i6.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx7 i7 ON i7.room_rowid = t.room_rowid
		LEFT JOIN _v_room_img_indx8 i8 ON i8.room_rowid = t.room_rowid
;
