﻿DROP TABLE IF EXISTS t_building CASCADE;

CREATE TABLE IF NOT EXISTS t_building (
  rowid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
  , b_type_rowid INTEGER
  , code VARCHAR(10)
  , name_en VARCHAR(100)
  , name_th VARCHAR(100)
  , name_jp VARCHAR(100)
  , address VARCHAR(500)
  , city_rowid INTEGER
  , province_rowid INTEGER
  , postal_code INTEGER(5)
  , email VARCHAR(100)
  , tel VARCHAR(50)
  , fax VARCHAR(50)
  , description VARCHAR(500)
  , units INTEGER DEFAULT 1
  , floors INTEGER DEFAULT 1
  , year INTEGER(4)
  , srvc_internet_rowid INTEGER
  , srvc_laundry_rowid INTEGER
  , nrfc_super_rowid INTEGER
  , is_pet TINYINT(1) DEFAULT 0
  , is_pool TINYINT(1) DEFAULT 0
  , is_sauna TINYINT(1) DEFAULT 0
  , is_gym TINYINT(1) DEFAULT 0
  , is_conv_store TINYINT(1) DEFAULT 0
  , is_nhk TINYINT(1) DEFAULT 0
  , is_jp_srvc TINYINT(1) DEFAULT 0
  , is_playground TINYINT(1) DEFAULT 0
  , is_shuttle TINYINT(1) DEFAULT 0
  , is_cleaner TINYINT(1) DEFAULT 0
  , dev_rowid INTEGER
  , remark VARCHAR(500)
  , details TEXT
  , owner_rowid INTEGER
  , map_position GEOMETRY
  , link_article_id INTEGER
  , is_cancel INTEGER DEFAULT 0
  , create_by INTEGER
  , create_date DATETIME DEFAULT NULL
  , update_by INTEGER
  , update_date DATETIME DEFAULT NULL
  , PRIMARY KEY (rowid)
  , FOREIGN KEY (b_type_rowid) REFERENCES m_building_type(rowid)
  , FOREIGN KEY (city_rowid) REFERENCES m_amphoe(rowid)
  , FOREIGN KEY (province_rowid) REFERENCES m_province(rowid)
  , FOREIGN KEY (srvc_internet_rowid) REFERENCES m_available_service(rowid)
  , FOREIGN KEY (srvc_laundry_rowid) REFERENCES m_available_service(rowid)
  , FOREIGN KEY (nrfc_super_rowid) REFERENCES m_nearby_facility(rowid)
  , FOREIGN KEY (dev_rowid) REFERENCES t_developer(rowid)
  , FOREIGN KEY (owner_rowid) REFERENCES j343_users(id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


/* ++ 20160412

ALTER TABLE t_building ADD COLUMN is_cleaner TINYINT(1) DEFAULT 0;
ALTER TABLE t_building ADD COLUMN owner_rowid INTEGER REFERENCES j343_users(id);

ALTER TABLE t_building ADD FOREIGN KEY (owner_rowid) REFERENCES j343_users(id);

UPDATE t_building SET
owner_rowid = create_by
WHERE create_by IS NOT NULL;

ALTER TABLE t_building ADD FOREIGN KEY (b_type_rowid) REFERENCES m_building_type(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (city_rowid) REFERENCES m_amphoe(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (province_rowid) REFERENCES m_province(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (srvc_internet_rowid) REFERENCES m_available_service(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (srvc_laundry_rowid) REFERENCES m_available_service(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (nrfc_super_rowid) REFERENCES m_nearby_facility(rowid);
ALTER TABLE t_building ADD FOREIGN KEY (dev_rowid) REFERENCES t_developer(rowid);

--- ** or use this

CREATE TABLE tmp_building AS SELECT * FROM t_building;
DROP TABLE t_building;
-- create table

INSERT INTO t_building(b_type_rowid, code, name_en, name_th, name_jp, address, city_rowid, province_rowid, postal_code, email, tel, fax
, description, units, floors, year, srvc_internet_rowid, srvc_laundry_rowid, nrfc_super_rowid, is_pet, is_pool, is_sauna, is_gym
, is_conv_store, is_nhk, is_jp_srvc, is_playground, is_shuttle, dev_rowid, remark, details, map_position, link_article_id, is_cancel
, create_by, create_date, update_by, update_date)
SELECT b_type_rowid, code, name_en, name_th, name_jp, address, city_rowid, province_rowid, postal_code, email, tel, fax
, description, units, floors, year, srvc_internet_rowid, srvc_laundry_rowid, nrfc_super_rowid, is_pet, is_pool, is_sauna, is_gym
, is_conv_store, is_nhk, is_jp_srvc, is_playground, is_shuttle, dev_rowid, remark, details, map_position, link_article_id, is_cancel
, create_by, create_date, update_by, update_date 
FROM tmp_building;

DROP table tmp_building;

*/

DROP TRIGGER IF EXISTS trg_t_building_delete; 

DELIMITER //

CREATE TRIGGER trg_t_building_delete BEFORE DELETE ON t_building 
FOR EACH ROW 
BEGIN 
/*
	DECLARE msg VARCHAR(50);

	UPDATE t_building
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
*/	
	SET msg = "DIE: No delete for important data table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;


/*
-- Foreign Key
ALTER TABLE t_building_detail
  ADD CONSTRAINT t_building_detail_builing_rowid_fkey FOREIGN KEY (builing_rowid)
      REFERENCES t_building_detail (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------


--- Before update / delete 
CREATE OR REPLACE FUNCTION trg_bfud_t_building() RETURNS trigger AS $trg$
	DECLARE v_count_used INTEGER;
    BEGIN
        IF (TG_OP = 'DELETE') THEN
			SELECT COUNT(*)
			INTO v_count_used
			FROM t_building_detail
			WHERE builing_rowid = OLD.rowid;
			
			IF v_count_used = 0 THEN
				DELETE FROM t_building_detail
				WHERE builing_rowid = OLD.rowid;
				
				RETURN OLD;
			ELSE				
				UPDATE t_building
				SET is_cancel = 1,
				update_by = -1,
				update_date = CURRENT_TIMESTAMP
				WHERE rowid = OLD.rowid;
				
				RETURN NULL;
			END IF;
        ELSIF (TG_OP = 'UPDATE') THEN
			IF COALESCE(NEW.is_cancel, 0) = 1 THEN
				UPDATE t_building_detail
				SET is_cancel = 1,
				update_by = -1,
				update_date = CURRENT_TIMESTAMP
				WHERE builing_rowid = OLD.rowid;
			END IF;
			
			RETURN NEW;
        END IF;		
    END;

$trg$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS trg_bfud_t_building ON t_building CASCADE;
CREATE TRIGGER trg_bfud_t_building BEFORE UPDATE OR DELETE ON t_building 
    FOR EACH ROW EXECUTE PROCEDURE trg_bfud_t_building();

---------------------------------------------------------------------
*/

/* remove new line
select *
, regexp_replace(code, '\r|\n', 'x', 'g')
, regexp_replace("name", '\r|\n', 'x', 'g')
, regexp_replace(name_th, '\r|\n', 'x', 'g')
from t_building
where code ~* '\r|\n' or name_en ~* '\r|\n' or name_th ~* '\r|\n' or name_jp ~* '\r|\n'

*/
