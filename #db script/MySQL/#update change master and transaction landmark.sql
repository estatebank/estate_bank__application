
/*
TRUNCATE TABLE t_landmark;

-- #recreate m_landmark_type with create scripts

DROP TABLE IF EXISTS tmp_t_landmark;

CREATE TABLE tmp_t_landmark
(
  rowid INTEGER
  , lm_type_rowid INTEGER
  , object_id INTEGER
  , name_en VARCHAR(100)
  , name_th VARCHAR(100)
  , name_jp VARCHAR(100)
  , location_en VARCHAR(500)
  , location_th VARCHAR(500)
  , amphoe_rowid INTEGER
  , province_rowid INTEGER
  , point_x DOUBLE(20, 12)
  , point_y DOUBLE(20, 12)
  , geom_32647 LONGTEXT
  , geom_4326 LONGTEXT
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

LOAD DATA INFILE 'E:\\#wamp_root\\www\\real_estate\\app\\#db script\\MySQL\\t_landmark__DATA.csv' 
INTO TABLE tmp_t_landmark 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
;
INSERT INTO t_landmark(lm_type_rowid, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, geom_32647, geom_4326)
SELECT CASE 
	WHEN lm_type_rowid = 4 THEN 1
	WHEN lm_type_rowid = 1 THEN 7
	ELSE (lm_type_rowid + 5) 
END, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, ST_GeomFromText(geom_32647, 32647) AS geom_32647, ST_GeomFromText(geom_4326, 4326) AS geom_4326
FROM tmp_t_landmark
ORDER BY 1, 2;
DROP TABLE IF EXISTS tmp_t_landmark;
*/
-- or


CREATE TABLE bkup20160817_landmark AS SELECT * FROM t_landmark;

TRUNCATE TABLE t_landmark;

-- #recreate m_landmark_type with create scripts

INSERT INTO t_landmark(lm_type_rowid, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, geom_32647, geom_4326)
SELECT CASE 
	WHEN lm_type_rowid = 4 THEN 1
	WHEN lm_type_rowid = 1 THEN 7
	ELSE (lm_type_rowid + 5) 
END, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, ST_GeomFromText(geom_32647, 32647) AS geom_32647, ST_GeomFromText(geom_4326, 4326) AS geom_4326
FROM bkup20160817_landmark
ORDER BY 1, 2;



UPDATE t_landmark
SET lm_type_rowid = 2
WHERE lm_type_rowid = 1
AND (
	name_en LIKE 'SAPHAN TAKSIN%'
	OR name_en LIKE 'SURASAK%'
	OR name_en LIKE 'CHONG NONSI%'
	OR name_en LIKE 'PHO NIMIT%'
	OR name_en LIKE 'KRUNG THON BURI%'
	OR name_en LIKE 'WONGWIAN YAI%'
	OR name_en LIKE 'TALAT PHLU%'
	OR name_en LIKE 'SALA DAENG%'
	OR name_en LIKE 'NATIONAL STADIUM%'
	OR name_en LIKE 'RATCHADAMRI%'
	OR name_en LIKE '%BANGWA%'
	OR name_en LIKE '%WUTTHAKAT'
);


