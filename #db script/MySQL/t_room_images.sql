DROP TABLE IF EXISTS t_room_images;

CREATE TABLE IF NOT EXISTS t_room_images (
  rowid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
  ,room_rowid INTEGER REFERENCES t_room(rowid)
  ,img_index INTEGER NOT NULL
  ,image LONGBLOB NOT NULL
  ,ext VARCHAR(5) NOT NULL
  ,title VARCHAR(200)
  ,is_default TINYINT DEFAULT 0
  ,description VARCHAR(200)
  ,remark VARCHAR(200)
  ,create_by INTEGER
  ,create_date DATETIME DEFAULT NULL
  ,update_by INTEGER
  ,update_date DATETIME DEFAULT NULL
  ,PRIMARY KEY (rowid)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
