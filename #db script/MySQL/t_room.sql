﻿DROP TABLE IF EXISTS t_room CASCADE;

CREATE TABLE IF NOT EXISTS t_room (
  rowid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
  , building_rowid INTEGER
  , room_type_rowid INTEGER
  , code VARCHAR(10)
  , name_en VARCHAR(100)
  , name_th VARCHAR(100)
  , name_jp VARCHAR(100)
  , room INTEGER
  , floor INTEGER
  , bed_room INTEGER
  , bath_room INTEGER
  , area_m2 NUMERIC
  , info TEXT
  , details TEXT
  , rent_price DECIMAL(10, 2)
  , rent_type_rowid INTEGER DEFAULT 1 /* 1 = per month, */
  , deposit_price DECIMAL(16, 2)
  , purchase_price DECIMAL(16, 2)
  , price_unit VARCHAR(5) DEFAULT 'thb'
  , owner_type_rowid INTEGER DEFAULT 1 /* 1 = room owner, 2 = building owner (should check) */
  , owner_rowid INTEGER
  , owner_tel VARCHAR(200)
  , description VARCHAR(500)
  , remark VARCHAR(500)
  , link_article_id INTEGER
  , is_cancel INTEGER DEFAULT 0
  , create_by INTEGER
  , create_date DATETIME DEFAULT NULL
  , update_by INTEGER
  , update_date DATETIME DEFAULT NULL
  , PRIMARY KEY (rowid)
  , FOREIGN KEY (building_rowid) REFERENCES t_building(rowid)
  , FOREIGN KEY (room_type_rowid) REFERENCES m_room_type(rowid)
  , FOREIGN KEY (owner_rowid) REFERENCES j343_users(id) 
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

/* ++ 20160412

ALTER TABLE t_room CHANGE room_number room INTEGER;

ALTER TABLE t_room ADD COLUMN owner_type_rowid INTEGER;
ALTER TABLE t_room ADD COLUMN owner_rowid INTEGER;
ALTER TABLE t_room ADD COLUMN owner_tel VARCHAR(500);
ALTER TABLE t_room ADD COLUMN deposit_price DECIMAL(16, 2);

ALTER TABLE t_room ADD FOREIGN KEY (building_rowid) REFERENCES t_building(rowid);
ALTER TABLE t_room ADD FOREIGN KEY (room_type_rowid) REFERENCES m_room_type(rowid);
ALTER TABLE t_room ADD FOREIGN KEY (owner_rowid) REFERENCES j343_users(id);

--- ** or use this

CREATE TABLE tmp_room AS SELECT * FROM t_room;
DROP TABLE t_room;
-- create table

INSERT INTO t_room(building_rowid, room_type_rowid, code, name_en, name_th, name_jp, room, floor, bed_room, bath_room, area_m2, info
, details, rent_price, rent_type_rowid, purchase_price, price_unit, description, remark, link_article_id, is_cancel, create_by
, create_date, update_by , update_date)
SELECT building_rowid, room_type_rowid, code, name_en, name_th, name_jp, room_number, floor, bed_room, bath_room, area_m2, info
, details, rent_price, rent_type_rowid, purchase_price, price_unit, description, remark, link_article_id, is_cancel, create_by
, create_date, update_by , update_date
FROM tmp_room;

DROP table tmp_room;

---------
UPDATE t_room SET
owner_rowid = create_by, owner_type_rowid = 1
WHERE create_by IS NOT NULL;

UPDATE t_room SET
owner_rowid = create_by, owner_type_rowid = 2
WHERE create_by IS NULL;

-- */


DROP TRIGGER IF EXISTS trg_t_room_delete; 

DELIMITER //

CREATE TRIGGER trg_t_room_delete BEFORE DELETE ON t_room 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
/*
	UPDATE t_room
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
*/
	SET msg = "DIE: No delete for important data table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;


/* remove new line 
select *
, regexp_replace(code, '\r|\n', 'x', 'g')
, regexp_replace("name_en", '\r|\n', 'x', 'g')
, regexp_replace(name_th, '\r|\n', 'x', 'g')
from t_room
where code ~* '\r|\n' or name_en ~* '\r|\n' or name_th ~* '\r|\n' or name_jp ~* '\r|\n'


create table tmp_room as select * from t_room
drop table t_room

insert into t_room (floor,bed_room,bath_room,rent_price,area_m2,building_rowid,room_type_rowid,code,name_en,name_th,name_jp,info,details,description,remark,link_article_id,is_cancel,create_by,create_date,update_by,update_date)
select floor,bed_room,bath_room,rent_price,area_m2,building_rowid,room_type_rowid,code,name_en,name_th,name_jp,info,details,description,remark,link_article_id,is_cancel,create_by,create_date,update_by,update_date
from tmp_room

drop table tmp_room
*/