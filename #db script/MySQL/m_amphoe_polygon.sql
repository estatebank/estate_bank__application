-- DROP TABLE IF EXISTS m_amphoe_polygon;

CREATE TABLE m_amphoe_polygon
(
  rowid INTEGER NOT NULL,
  amphoe_rowid INTEGER,
  area DOUBLE(20, 2),
  perimeter DOUBLE(12, 3),
  geom GEOMETRY,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TRIGGER IF EXISTS trg_m_amphoe_polygon_delete; 

DELIMITER //

CREATE TRIGGER trg_m_amphoe_polygon_delete BEFORE DELETE ON m_amphoe_polygon
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
END;//

DELIMITER ;


DROP TABLE IF EXISTS tmp_m_amphoe_polygon;

CREATE TABLE tmp_m_amphoe_polygon
(
  rowid INTEGER NOT NULL,
  amphoe_rowid INTEGER,
  area DOUBLE(20, 2),
  perimeter DOUBLE(12, 3),
  geom LONGTEXT,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

LOAD DATA INFILE 'c:\\wamp\\www\\real_estate\\app\\#db script\\MySQL\\m_amphoe_polygon__DATA.csv' 
INTO TABLE tmp_m_amphoe_polygon 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
;


TRUNCATE TABLE m_amphoe_polygon;

INSERT INTO m_amphoe_polygon(rowid, amphoe_rowid, area, perimeter, geom)
SELECT rowid, amphoe_rowid, area, perimeter, ST_GeomFromText(geom, 4326) AS geom 
FROM tmp_m_amphoe_polygon;

DROP TABLE IF EXISTS tmp_m_amphoe_polygon;

