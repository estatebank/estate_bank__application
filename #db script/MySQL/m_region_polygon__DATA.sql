DROP TABLE IF EXISTS tmp_m_region_polygon;

CREATE TABLE tmp_m_region_polygon
(
  rowid INTEGER NOT NULL,
  region_rowid INTEGER,
  area DOUBLE(20, 2),
  perimeter DOUBLE(12, 3),
  geom LONGTEXT,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

LOAD DATA INFILE 'c:\\wamp\\www\\real_estate\\app\\#db script\\MySQL\\m_region_polygon__DATA.csv' 
INTO TABLE tmp_m_region_polygon 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
;


TRUNCATE TABLE m_region_polygon;

INSERT INTO m_region_polygon(rowid, region_rowid, area, perimeter, geom)
SELECT rowid, region_rowid, area, perimeter, ST_GeomFromText(geom, 4326) AS geom 
FROM tmp_m_region_polygon;

DROP TABLE IF EXISTS tmp_m_region_polygon;
