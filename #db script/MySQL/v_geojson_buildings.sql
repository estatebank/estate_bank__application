﻿SET GLOBAL max_allowed_packet = 1073741824;

SET  GLOBAL group_concat_max_len = 1073741824;


-- DROP VIEW v_geojson_each_building;

CREATE OR REPLACE VIEW v_geojson_each_building
AS
	SELECT b.rowid, CONCAT(
		'{"type":"Feature"'
		, ',"geometry":{"type":"Point","coordinates":[', ST_X(b.map_position), ',', ST_Y(b.map_position), ']}'
		, ',"properties":{"rowid":', b.rowid, ',"b_type_rowid":', IFNULL(b.b_type_rowid, 0)
		, ',"disp_b_type":"', IFNULL(bt.name_th, ''), '"', ', "code":"', IFNULL(b.code, ''), '"'
		, ',"name_en":"', IFNULL(b.name_en, ''), '"', ', "name_th":"', IFNULL(b.name_th, ''), '"'
		, ',"name_jp":"', IFNULL(b.name_jp, ''), '"', ', "description":"', IFNULL(b.description, ''), '"'
		, ',"disp_name":"', COALESCE(b.name_th, b.name_en, b.name_jp, ''), '"'
		, '}}') AS each_feature
	FROM t_building b 
		LEFT OUTER JOIN m_building_type bt ON bt.rowid = b.b_type_rowid
	WHERE COALESCE(b.is_cancel, -1) < 1
	AND b.map_position IS NOT NULL
;


-- DROP VIEW v_geojson_buildings;

CREATE OR REPLACE VIEW v_geojson_buildings
AS
	SELECT CONCAT('{"type":"FeatureCollection","features":['
		, GROUP_CONCAT(f.each_feature ORDER BY f.rowid SEPARATOR ',')
		, ']}'
	) AS geojson
	FROM v_geojson_each_building f
	HAVING COUNT(f.each_feature) > 0;
;