﻿DROP TABLE IF EXISTS t_landmark CASCADE;

CREATE TABLE t_landmark
(
  rowid INTEGER NOT NULL AUTO_INCREMENT
  , lm_type_rowid INTEGER REFERENCES m_landmark_type(rowid)
  , object_id INTEGER
  , name_en VARCHAR(100)
  , name_th VARCHAR(100)
  , name_jp VARCHAR(100)
  , location_en VARCHAR(500)
  , location_th VARCHAR(500)
  , amphoe_rowid INTEGER REFERENCES m_amphoe(rowid)
  , province_rowid INTEGER REFERENCES m_province(rowid)
  , description VARCHAR(500)
  , remark VARCHAR(500)
  , is_cancel INTEGER DEFAULT 0
  , point_x DOUBLE(20, 12)
  , point_y DOUBLE(20, 12)
  , geom_32647 GEOMETRY
  , geom_4326 GEOMETRY
  , CONSTRAINT PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TRIGGER IF EXISTS trg_t_landmark_delete; 

DELIMITER //

CREATE TRIGGER trg_t_landmark_delete BEFORE DELETE ON t_landmark 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE t_landmark
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for important data table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

DROP TABLE IF EXISTS tmp_t_landmark;

CREATE TABLE tmp_t_landmark
(
  rowid INTEGER
  , lm_type_rowid INTEGER
  , object_id INTEGER
  , name_en VARCHAR(100)
  , name_th VARCHAR(100)
  , name_jp VARCHAR(100)
  , location_en VARCHAR(500)
  , location_th VARCHAR(500)
  , amphoe_rowid INTEGER
  , province_rowid INTEGER
  , point_x DOUBLE(20, 12)
  , point_y DOUBLE(20, 12)
  , geom_32647 LONGTEXT
  , geom_4326 LONGTEXT
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

LOAD DATA INFILE 'c:\\wamp\\www\\real_estate\\app\\#db script\\MySQL\\t_landmark__DATA.csv' 
INTO TABLE tmp_t_landmark 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
;


TRUNCATE TABLE t_landmark;

INSERT INTO t_landmark(lm_type_rowid, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, geom_32647, geom_4326)
SELECT lm_type_rowid, object_id, name_en, name_th, name_jp, location_en, location_th, amphoe_rowid, province_rowid
, point_x, point_y, ST_GeomFromText(geom_32647, 32647) AS geom_32647, ST_GeomFromText(geom_4326, 4326) AS geom_4326
FROM tmp_t_landmark;

DROP TABLE IF EXISTS tmp_t_landmark;
