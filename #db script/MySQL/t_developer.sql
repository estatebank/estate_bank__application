﻿DROP TABLE IF EXISTS t_developer;

CREATE TABLE IF NOT EXISTS t_developer (
  rowid INTEGER NOT NULL AUTO_INCREMENT,
  code VARCHAR(10),
  name_en VARCHAR(100),
  name_th VARCHAR(100),
  name_jp VARCHAR(100),
  description VARCHAR(500),
  remark VARCHAR(500),
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date DATETIME DEFAULT NULL,
  update_by INTEGER,
  update_date DATETIME DEFAULT NULL,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-----------------------------------------

DROP TRIGGER IF EXISTS trg_t_developer_delete; 

DELIMITER //

CREATE TRIGGER trg_t_developer_delete BEFORE DELETE ON t_developer 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE t_developer
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for important data table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------


-- Foreign Key
ALTER TABLE t_building
  ADD CONSTRAINT t_building_dev_rowid_fkey FOREIGN KEY (dev_rowid)
      REFERENCES t_developer (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
