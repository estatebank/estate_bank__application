﻿-- DROP TABLE IF EXISTS m_available_service CASCADE;

CREATE TABLE IF NOT EXISTS m_available_service (
  rowid INTEGER NOT NULL
  ,service_cat_rowid INTEGER
  ,code VARCHAR(10)
  ,name_en VARCHAR(100)
  ,name_th VARCHAR(100)
  ,name_jp VARCHAR(100)
  ,description VARCHAR(500)
  ,remark VARCHAR(500)
  ,icon_path VARCHAR(500)
  ,is_cancel INTEGER DEFAULT 0
  ,create_by INTEGER
  ,create_date DATETIME DEFAULT NULL
  ,update_by INTEGER
  ,update_date DATETIME DEFAULT NULL
  ,PRIMARY KEY (rowid)
  ,FOREIGN KEY (service_cat_rowid) REFERENCES m_available_service_category(rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

INSERT INTO m_available_service (rowid, service_cat_rowid, code, name_en, name_th, name_jp, icon_path, create_by, create_date) VALUES 
 (1, 1, 'SVI001', '', '', 'プロバイダとの直接契約', NULL, 0, CURRENT_TIMESTAMP)
,(2, 1, 'SVI002', '', '', '有料', NULL, 0, CURRENT_TIMESTAMP)
,(3, 1, 'SVI003', '', '', '無料', NULL, 0, CURRENT_TIMESTAMP)
,(4, 1, 'SVI004', '', '', '無料Wifi', NULL, 0, CURRENT_TIMESTAMP)
,(5, 2, 'SVL001', 'laundry service', 'บริการซักรีด', 'ランドリーサービス', NULL, 0, CURRENT_TIMESTAMP)
,(6, 2, 'SVL002', 'coin laundry', 'เครื่องซักผ้าหยอดเหรียญ', 'コインランドリー', NULL, 0, CURRENT_TIMESTAMP)
;
--,(, NULL, '', '', '', '', NULL, 0, CURRENT_TIMESTAMP)
------------------------------------------


--- Rule instead of delete 
DROP TRIGGER IF EXISTS trg_m_available_service_delete; 

DELIMITER //

CREATE TRIGGER trg_m_available_service_delete BEFORE DELETE ON m_available_service 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
/*
	UPDATE m_available_service
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
*/
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------
