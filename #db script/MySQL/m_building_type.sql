﻿-- DROP TABLE IF EXISTS m_building_type CASCADE;

CREATE TABLE IF NOT EXISTS m_building_type (
  rowid INTEGER NOT NULL,
  cat_rowid INTEGER NOT NULL DEFAULT 12,
  code VARCHAR(10),
  name_en VARCHAR(100),
  name_th VARCHAR(100),
  name_jp VARCHAR(100),
  description VARCHAR(500),
  remark VARCHAR(500),
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date DATETIME DEFAULT NULL,
  update_by INTEGER,
  update_date DATETIME DEFAULT NULL,
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

/* ++ Update on data sent via excel 20150224 ++
INSERT INTO m_building_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (11, 'BT-011', 'detached house', 'บ้านเดี่ยว', NULL, NULL, 0, SYSDATE())
,(12, 'BT-012', 'twin house', 'บ้านแฝด', NULL, NULL, 0, SYSDATE())
,(13, 'BT-013', 'townhouse/townhome', 'ทาวน์เฮาส์', NULL, NULL, 0, SYSDATE())
,(21, 'BT-021', 'condomenium', 'อาคารชุด', 'コンドミニアム', NULL, 0, SYSDATE())
,(22, 'BT-022', 'apartment', 'ห้องชุด', NULL, NULL, 0, SYSDATE())
,(31, 'BT-031', 'rent room', 'ห้องเช่า', NULL, NULL, 0, SYSDATE())
,(32, 'BT-032', 'dormitory', 'หอพัก', NULL, NULL, 0, SYSDATE())
*/

/*

-- ALTER TABLE t_building DISABLE CONSTRAINT t_building_b_type_rowid_fkey;

TRUNCATE TABLE m_building_type;

UPDATE t_building 
SET update_date = NULL
;

*/

INSERT INTO m_building_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
(1, 'CNDO', 'condomenium', 'อาคารชุด', 'コンドミニアム', NULL, 0, SYSDATE())
,(2, 'SVAP', 'serviced apartment', 'เซอร์วิสอพาร์ทเม้นท์ ', 'サービスアパート', NULL, 0, SYSDATE())
,(3, 'APMT', 'apartment', 'ห้องชุด', 'アパートメント', NULL, 0, SYSDATE())
,(4, 'HOUS', 'house', 'บ้านเดี่ยว', '一軒家', NULL, 0, SYSDATE())
,(5, 'TOWH', 'townhouse', 'ทาวน์เฮาส์', 'タウンハウス', NULL, 0, SYSDATE())
,(6, 'LAND', 'land', 'ที่ดิน', '土地', NULL, 0, SYSDATE())
,(7, 'STOR', 'store', 'ร้านค้า', '店舗', NULL, 0, SYSDATE())
,(8, 'FIRM', 'firm', 'บริษัท', '事務所', NULL, 0, SYSDATE())
,(9, 'BFAC', 'business facilities', 'อาคารธุรกิจ', '商業施設', NULL, 0, SYSDATE())
,(10, 'FCWH', 'factories/warehouses', 'โรงงาน/คลังสินค้า', '工場・倉庫', NULL, 0, SYSDATE())
,(11, 'INDP', 'industrial park', 'นิคมอุตสาหกรรม', '工業団地', NULL, 0, SYSDATE())
,(12, 'OTHR', 'other', 'อื่นๆ', 'その他', NULL, 0, SYSDATE())
;

/*

UPDATE t_building 
SET b_type_rowid = CASE b_type_rowid WHEN 11 THEN 4 WHEN 21 THEN 1 WHEN 22 THEN 3 ELSE 12 END
, update_by = 0, update_date = SYSDATE()
WHERE update_by IS NULL
;

-- ALTER TABLE t_building DISABLE CONSTRAINT t_building_b_type_rowid_fkey;

*/

/* -- Update on data sent via excel 20150224 -- */


------------------------------------------

DROP TRIGGER IF EXISTS trg_m_building_type_delete; 

DELIMITER //

CREATE TRIGGER trg_m_building_type_delete BEFORE DELETE ON m_building_type 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);

	UPDATE m_building_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------


-- Foreign Key
ALTER TABLE t_building
  ADD CONSTRAINT t_building_b_type_rowid_fkey FOREIGN KEY (b_type_rowid)
      REFERENCES m_building_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
