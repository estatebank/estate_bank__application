DELIMITER $$

DROP FUNCTION IF EXISTS `fnc_distanceKm`$$

CREATE FUNCTION `fnc_distanceKm`(g1 GEOMETRY, g2 GEOMETRY) RETURNS DECIMAL(10, 6)
BEGIN
	IF (ST_GeometryType(g1) = 'POINT') THEN
		SET @lat1 = ST_Y(g1);
		SET @lng1 = ST_X(g1);
	ELSE
		SET @lat1 = ST_Y(ST_Centroid(g1));
		SET @lng1 = ST_X(ST_Centroid(g1));
	END IF;
	
	IF (ST_GeometryType(g2) = 'POINT') THEN
		SET @lat2 = ST_Y(g2);
		SET @lng2 = ST_X(g2);
	ELSE
		SET @lat2 = ST_Y(ST_Centroid(g2));
		SET @lng2 = ST_X(ST_Centroid(g2));
	END IF;
	RETURN (6353 * 2 * ASIN(SQRT(POWER(SIN((@lat1 - abs(@lat2)) * pi()/180 / 2),2) + COS(@lat1 * pi()/180 ) * COS(abs(@lat2) *  pi()/180) * POWER(SIN((@lng1 - @lng2) *  pi()/180 / 2), 2))));
END$$

DELIMITER ;

