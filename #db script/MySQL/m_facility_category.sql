-- DROP TABLE IF EXISTS m_facility_category CASCADE;

CREATE TABLE IF NOT EXISTS m_facility_category (
  rowid INTEGER NOT NULL
  ,code VARCHAR(10)
  ,name_en VARCHAR(100)
  ,name_th VARCHAR(100)
  ,name_jp VARCHAR(100)
  ,description VARCHAR(500)
  ,remark VARCHAR(500)
  ,is_cancel INTEGER DEFAULT 0
  ,create_by INTEGER
  ,create_date DATETIME DEFAULT NULL
  ,update_by INTEGER
  ,update_date DATETIME DEFAULT NULL
  ,PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

------------------------------------------

INSERT INTO m_facility_category (rowid, code, name_en, create_by, create_date) VALUES 
 (1, NULL, 'Super Market', 0, CURRENT_TIMESTAMP)
;
------------------------------------------


--- Rule instead of delete 
DROP TRIGGER IF EXISTS trg_m_facility_category_delete; 

DELIMITER //

CREATE TRIGGER trg_m_facility_category_delete BEFORE DELETE ON m_facility_category 
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
/*
	UPDATE m_facility_category
	SET is_cancel = 1 
	, update_by = -1
	, update_date = SYSDATE()
	WHERE rowid = OLD.rowid;
*/
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;

END;//

DELIMITER ;

------------------------------------------
