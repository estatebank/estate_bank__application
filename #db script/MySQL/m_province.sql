-- DROP TABLE IF EXISTS m_province CASCADE;

CREATE TABLE m_province
(
  rowid INTEGER NOT NULL,
  region_rowid INTEGER,
  name_th VARCHAR(50),
  name_en VARCHAR(50),
  nrd2c VARCHAR(2),
  PRIMARY KEY (rowid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS trg_m_province_delete; 

DELIMITER //

CREATE TRIGGER trg_m_province_delete BEFORE DELETE ON m_province
FOR EACH ROW 
BEGIN 
	DECLARE msg VARCHAR(50);
	
	SET msg = "DIE: No delete for master table";
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
END;//

DELIMITER ;


INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (10, 1, 'กรุงเทพมหานคร', 'BANGKOK', '32');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (11, 1, 'สมุทรปราการ', 'SAMUT PRAKARN', '57');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (12, 1, 'นนทบุรี', 'NONTHABURI', '24');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (13, 1, 'ปทุมธานี', 'PATHUM THANI', '28');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (14, 2, 'พระนครศรีอยุธยา', 'PHRA NAKHON SI AYUDHYA', '33');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (15, 2, 'อ่างทอง', 'ANG THONG', '67');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (16, 2, 'ลพบุรี', 'LOPBURI', '49');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (17, 2, 'สิงห์บุรี', 'SINGBURI', '61');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (18, 2, 'ชัยนาท', 'CHAINAT', '09');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (19, 2, 'สระบุรี', 'SARABURI', '60');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (20, 3, 'ชลบุรี', 'CHONBURI', '08');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (21, 3, 'ระยอง', 'RAYONG', '47');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (22, 3, 'จันทบุรี', 'CHANTHABURI', '06');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (23, 3, 'ตราด', 'TRAD', '15');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (24, 3, 'ฉะเชิงเทรา', 'CHACHOENGSAO', '07');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (25, 3, 'ปราจีนบุรี', 'PHACHINBURI', '30');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (26, 3, 'นครนายก', 'NAKHON NAYOK', '18');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (27, 3, 'สระแก้ว', 'SA KAEO', '74');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (30, 6, 'นครราชสีมา', 'NAKHON RATCHASIMA', '21');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (31, 6, 'บุรีรัมย์', 'BURIRAM', '27');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (32, 6, 'สุรินทร์', 'SURIN', '65');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (33, 6, 'ศรีสะเกษ', 'SI SAKET', '53');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (34, 6, 'อุบลราชธานี', 'UBON RATCHATHANI', '71');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (35, 6, 'ยโสธร', 'YASOTHON', '17');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (36, 6, 'ชัยภูมิ', 'CHAIYAPHUM', '10');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (37, 6, 'อำนาจเจริญ', 'AMNAT CHAROEN', '76');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (39, 6, 'หนองบัวลำภู', 'NONG BUA LAMPHU', '75');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (40, 6, 'ขอนแก่น', 'KHON KAEN', '05');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (41, 6, 'อุดรธานี', 'UDON THANI', '68');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (42, 6, 'เลย', 'LOEI', '52');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (43, 6, 'หนองคาย', 'NONG KHAI', '66');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (44, 6, 'มหาสารคาม', 'MAHA SARAKHAM', '42');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (45, 6, 'ร้อยเอ็ด', 'ROI ET', '45');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (46, 6, 'กาฬสินธุ์', 'KALASIN', '03');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (47, 6, 'สกลนคร', 'SAKON NAKHON', '54');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (48, 6, 'นครพนม', 'NAKHON PHANOM', '20');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (49, 6, 'มุกดาหาร', 'MUKDAHAN', '73');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (50, 5, 'เชียงใหม่', 'CHIANG MAI', '13');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (51, 5, 'ลำพูน', 'LAMPHUN', '51');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (52, 5, 'ลำปาง', 'LAMPANG', '50');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (53, 5, 'อุตรดิตถ์', 'UTTARADIT', '69');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (54, 5, 'แพร่', 'PHRAE', '40');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (55, 5, 'น่าน', 'NAN', '26');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (56, 5, 'พะเยา', 'PHAYAO', '72');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (57, 5, 'เชียงราย', 'CHIANG RAI', '12');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (58, 5, 'แม่ฮ่องสอน', 'MAE HONG SON', '43');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (60, 5, 'นครสวรรค์', 'NAKHON SAWAN', '23');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (61, 5, 'อุทัยธานี', 'UTHAI THANI', '70');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (62, 5, 'กำแพงเพชร', 'KAMPAENG PHET', '04');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (63, 5, 'ตาก', 'TAK', '16');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (64, 5, 'สุโขทัย', 'SUKHOTHAI', '62');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (65, 5, 'พิษณุโลก', 'PHITSANULOK', '37');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (66, 5, 'พิจิตร', 'PHICHIT', '36');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (67, 5, 'เพชรบูรณ์', 'PHETCHABUN', '39');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (70, 4, 'ราชบุรี', 'RATCHABURI', '48');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (71, 4, 'กาญจนบุรี', 'KANCHANABURI', '02');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (72, 4, 'สุพรรณบุรี', 'SUPHANBURI', '63');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (73, 1, 'นครปฐม', 'NAKHON PATHOM', '19');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (74, 1, 'สมุทรสาคร', 'SAMUT SAKHON', '59');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (75, 4, 'สมุทรสงคราม', 'SAMUT SONGKHAM', '58');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (76, 4, 'เพชรบุรี', 'PHETCHABURI', '38');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (77, 4, 'ประจวบคีรีขันธ์', 'PRACHUAP KHILIKHAN', '29');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (80, 7, 'นครศรีธรรมราช', 'NAKHON SI THAMMARAT', '22');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (81, 7, 'กระบี่', 'KRABI', '01');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (82, 7, 'พังงา', 'PHANGNGA', '34');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (83, 7, 'ภูเก็ต', 'PHUKET', '41');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (84, 7, 'สุราษฎร์ธานี', 'SURAT THANI', '64');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (85, 7, 'ระนอง', 'RANONG', '46');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (86, 7, 'ชุมพร', 'CHUMPHON', '11');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (90, 7, 'สงขลา', 'SONGKHLA', '55');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (91, 7, 'สตูล', 'SATUN', '56');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (92, 7, 'ตรัง', 'TRANG', '14');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (93, 7, 'พัทลุง', 'PHATTHALUNG', '35');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (94, 7, 'ปัตตานี', 'PATTANI', '31');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (95, 7, 'ยะลา', 'YALA', '44');
INSERT INTO m_province (rowid, region_rowid, name_th, name_en, nrd2c) VALUES (96, 7, 'นราธิวาส', 'NARATHIWAT', '25');
