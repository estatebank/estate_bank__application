DROP TABLE IF EXISTS t_room_images;

CREATE TABLE IF NOT EXISTS t_room_images (
  rowid BIGSERIAL PRIMARY KEY
  ,room_rowid INTEGER REFERENCES t_room(rowid)
  ,image BYTEA
  ,title TEXT
  ,description TEXT
  ,remark TEXT
  ,create_by INTEGER
  ,create_date TIMESTAMP WITHOUT TIME ZONE
  ,update_by INTEGER
  ,update_date TIMESTAMP WITHOUT TIME ZONE
);
