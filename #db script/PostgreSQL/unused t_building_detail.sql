﻿DROP TABLE IF EXISTS t_building_detail;

CREATE TABLE IF NOT EXISTS t_building_detail (
  rowid SERIAL PRIMARY KEY,
  builing_rowid INT NOT NULL REFERENCES t_building(rowid),
  code TEXT,
  is_default int DEFAULT 0,
  remark TEXT,
  is_cancel int DEFAULT 0,
  create_by int,
  create_date timestamp without time zone,
  update_by int,
  update_date timestamp without time zone
);


