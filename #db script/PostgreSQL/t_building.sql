﻿DROP TABLE IF EXISTS t_building;

CREATE TABLE IF NOT EXISTS t_building (
  rowid SERIAL PRIMARY KEY
  ,b_type_rowid INTEGER REFERENCES m_building_type(rowid)
  ,code TEXT
  ,name_en TEXT
  ,name_th TEXT
  ,name_jp TEXT
  ,address TEXT
  ,city_rowid INTEGER REFERENCES m_amphoe(rowid)
  ,province_rowid INTEGER REFERENCES m_province(rowid)
  ,postal_code NUMERIC(5)
  ,email TEXT
  ,tel TEXT
  ,fax TEXT
  ,description TEXT
  ,units INTEGER DEFAULT 1
  ,floors INTEGER DEFAULT 1
  ,"year" NUMERIC(4)
  ,dev_rowid INTEGER REFERENCES t_developer(rowid)
  ,remark TEXT
  ,details JSON
  ,map_position geometry
  ,link_article_id INTEGER
  ,is_cancel INTEGER DEFAULT 0
  ,create_by INTEGER
  ,create_date TIMESTAMP WITHOUT TIME ZONE
  ,update_by INTEGER
  ,update_date TIMESTAMP WITHOUT TIME ZONE
);

--- Rule instead of delete 
CREATE OR REPLACE RULE rid_t_building AS ON 
	DELETE TO t_building
    DO INSTEAD 
	UPDATE t_building
	SET is_cancel = 1 
	, update_by = -1
	, update_date = CURRENT_TIMESTAMP
	WHERE rowid = OLD.rowid;

------------------------------------------

/*
-- Foreign Key
ALTER TABLE t_building_detail
  ADD CONSTRAINT t_building_detail_builing_rowid_fkey FOREIGN KEY (builing_rowid)
      REFERENCES t_building_detail (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------


--- Before update / delete 
CREATE OR REPLACE FUNCTION trg_bfud_t_building() RETURNS trigger AS $trg$
	DECLARE v_count_used INTEGER;
    BEGIN
        IF (TG_OP = 'DELETE') THEN
			SELECT COUNT(*)
			INTO v_count_used
			FROM t_building_detail
			WHERE builing_rowid = OLD.rowid;
			
			IF v_count_used = 0 THEN
				DELETE FROM t_building_detail
				WHERE builing_rowid = OLD.rowid;
				
				RETURN OLD;
			ELSE				
				UPDATE t_building
				SET is_cancel = 1,
				update_by = -1,
				update_date = CURRENT_TIMESTAMP
				WHERE rowid = OLD.rowid;
				
				RETURN NULL;
			END IF;
        ELSIF (TG_OP = 'UPDATE') THEN
			IF COALESCE(NEW.is_cancel, 0) = 1 THEN
				UPDATE t_building_detail
				SET is_cancel = 1,
				update_by = -1,
				update_date = CURRENT_TIMESTAMP
				WHERE builing_rowid = OLD.rowid;
			END IF;
			
			RETURN NEW;
        END IF;		
    END;

$trg$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS trg_bfud_t_building ON t_building CASCADE;
CREATE TRIGGER trg_bfud_t_building BEFORE UPDATE OR DELETE ON t_building 
    FOR EACH ROW EXECUTE PROCEDURE trg_bfud_t_building();

---------------------------------------------------------------------
*/

/* remove new line */
select *
, regexp_replace(code, '\r|\n', 'x', 'g')
, regexp_replace("name", '\r|\n', 'x', 'g')
, regexp_replace(name_th, '\r|\n', 'x', 'g')
from t_building
where code ~* '\r|\n' or name_en ~* '\r|\n' or name_th ~* '\r|\n' or name_jp ~* '\r|\n'

