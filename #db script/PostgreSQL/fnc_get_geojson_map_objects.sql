﻿-- Function: fnc_get_geojson_map_objects()

-- DROP FUNCTION fnc_get_geojson_map_objects();

CREATE OR REPLACE FUNCTION fnc_get_geojson_map_objects()
  RETURNS json AS
$BODY$
DECLARE v_toReturn JSON;
BEGIN

	SELECT ROW_TO_JSON(f_col) AS geojson
	INTO v_toReturn
	FROM (
		SELECT 'FeatureCollection' As "type", ARRAY_TO_JSON(ARRAY_AGG(f)) As features
		FROM (
			SELECT 'Feature' As "type"
			, ST_AsGeoJSON(b.map_position)::JSON AS geometry
			, (
				SELECT ROW_TO_JSON(_row.*) 
				FRoM (
					SELECT b.rowid, b.b_type_rowid, bt.name_th AS disp_b_type, b.code
					, b.name_en, b.name_th, b.name_jp, b.description
					, COALESCE(b.name_th, INITCAP(b.name_en), b.name_jp, '') AS disp_name
				) _row
			) AS properties
			FROM t_building b 
				LEFT OUTER JOIN m_building_type bt ON bt.rowid = b.b_type_rowid
			WHERE COALESCE(b.is_cancel, -1) < 1
			AND b.map_position IS NOT NULL
		) f
	) f_col
	;

	RETURN v_toReturn;
EXCEPTION
	WHEN OTHERS THEN
		RAISE '% %', SQLERRM, SQLSTATE;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnc_get_geojson_map_objects()
  OWNER TO postgres;
