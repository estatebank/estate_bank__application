﻿-- DROP TABLE IF EXISTS t_landmark CASCADE;

CREATE TABLE t_landmark
(
  rowid SERIAL NOT NULL, 
  lm_type_rowid INT REFERENCES m_landmark_type(rowid),
  object_id integer,
  name_en TEXT,
  name_th TEXT,
  name_jp TEXT,
  location_en TEXT,
  location_th TEXT,
  amphoe_rowid INT REFERENCES m_amphoe(rowid),
  province_rowid INT REFERENCES m_province(rowid),
  point_x double precision,
  point_y double precision,
  geom_32647 geometry(Point, 32647),
  geom_4326 geometry(Point, 4326),
  CONSTRAINT t_landmark_pkey PRIMARY KEY (rowid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE t_landmark
  OWNER TO postgres;
