﻿DROP TABLE IF EXISTS t_developer;

CREATE TABLE IF NOT EXISTS t_developer (
  rowid SERIAL PRIMARY KEY,
  code TEXT,
  name_en TEXT,
  name_th TEXT,
  name_jp TEXT,
  description TEXT,
  remark TEXT,
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date timestamp without time zone,
  update_by INTEGER,
  update_date timestamp without time zone
);

-- Foreign Key
ALTER TABLE t_building
  ADD CONSTRAINT t_building_dev_rowid_fkey FOREIGN KEY (dev_rowid)
      REFERENCES t_developer (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------

--- Rule instead of delete 
CREATE OR REPLACE RULE rid_t_developer AS ON 
	DELETE TO t_developer
    DO INSTEAD 
	UPDATE t_developer
	SET is_cancel = 1 
	, update_by = -1
	, update_date = CURRENT_TIMESTAMP
	WHERE rowid = OLD.rowid;

------------------------------------------
