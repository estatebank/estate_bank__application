﻿-- DROP TABLE IF EXISTS m_room_type CASCADE;

CREATE TABLE IF NOT EXISTS m_room_type (
  rowid INTEGER PRIMARY KEY,
  code TEXT,
  name_en TEXT,
  name_th TEXT,
  name_jp TEXT,
  description TEXT,
  remark TEXT,
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date timestamp without time zone,
  update_by INTEGER,
  update_date timestamp without time zone
);


--- Rule instead of delete 
CREATE OR REPLACE RULE rid_m_room_type AS ON 
	DELETE TO m_room_type
    DO INSTEAD 
	UPDATE m_room_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = CURRENT_TIMESTAMP
	WHERE rowid = OLD.rowid;

------------------------------------------

INSERT INTO m_room_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (1, '1LK', '1LK', '1LK', NULL, NULL, 1, CURRENT_TIMESTAMP)


-- Foreign Key
ALTER TABLE t_room
  ADD CONSTRAINT t_room_room_type_rowid_fkey FOREIGN KEY (room_type_rowid)
      REFERENCES m_room_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
