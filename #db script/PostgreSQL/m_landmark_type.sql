﻿-- DROP TABLE IF EXISTS m_landmark_type CASCADE;

CREATE TABLE IF NOT EXISTS m_landmark_type (
  rowid INTEGER PRIMARY KEY,
  code TEXT,
  name_en TEXT,
  name_th TEXT,
  name_jp TEXT,
  description TEXT,
  remark TEXT,
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date timestamp without time zone,
  update_by INTEGER,
  update_date timestamp without time zone
);


--- Rule instead of delete 
CREATE OR REPLACE RULE rid_m_landmark_type AS ON 
	DELETE TO m_landmark_type
    DO INSTEAD 
	UPDATE m_landmark_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = CURRENT_TIMESTAMP
	WHERE rowid = OLD.rowid;

------------------------------------------

INSERT INTO m_landmark_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (1, 'LM-001', 'bus stop', 'ป้ายรถเมล์', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(2, 'LM-002', 'MRT station', 'สถานีรถไฟฟ้าใต้ดิน', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(3, 'LM-003', 'BRT station', 'สถานีรถ BRT', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(4, 'LM-004', 'BTS station', 'สถานีรถไฟฟ้า BTS', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(5, 'LM-005', 'train station', 'สถานีรถไฟ', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(6, 'LM-006', 'pier', 'ท่าเรือ', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(7, 'LM-007', 'airport', 'สนามบิน', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(8, 'LM-008', 'transportation', 'สถานีขนส่งสินค้าและผู้โดยสาร', NULL, NULL, 1, CURRENT_TIMESTAMP)
;




-- Foreign Key
ALTER TABLE t_landmark
  ADD CONSTRAINT t_landmark_lm_type_rowid_fkey FOREIGN KEY (lm_type_rowid)
      REFERENCES m_landmark_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------
