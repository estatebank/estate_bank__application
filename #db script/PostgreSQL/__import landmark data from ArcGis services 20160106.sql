﻿WITH _impData AS (
	select 2 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	from imp_mrt t
	union all
	select 3 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	from imp_brt t
	union all
	select 4 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	from imp_bts t
	union all
	SELECT 5 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	FROM imp_train t
	UNION ALL
	SELECT 6 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	FROM imp_pier t
	UNION ALL
	SELECT 7 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAME_E" AS name_en, t."NAME_T" AS name_th
	, t."LOCATION_E" AS location_en, t."LOCATION_T" AS location_th, t."P_CODE" AS province, t."A_CODE" AS amphoe
	, t.geom, t."POINT_X" AS point_x, t."POINT_Y" AS point_y
	FROM imp_airport t
	UNION ALL
	SELECT 8 AS lm_type_rowid, t."OBJECTID" AS object_id, t."NAMEE" AS name_en, t."NAMET" AS name_th
	, NULL AS location_en, t."ADDRESS" AS location_th
	, SUBSTR(t."PROVINCE", 1, 2) AS province, SUBSTR(t."AMPHOR", 1, 2) AS amphoe, t.geom, NULL AS point_x, NULL AS point_y
	from imp_bus t
)
INSERT INTO t_landmark(lm_type_rowid, object_id, name_en, name_th, location_en, location_th, amphoe_rowid, province_rowid, geom_32647
, point_x, point_y)
select t.lm_type_rowid AS lm_type_rowid--, CONCAT(t.province, t.AMPHOE) AS code
, t.object_id AS object_id
, t.name_en AS name_en
, t.name_th AS name_th
, t.location_en AS location_en--, a.name_th AS amphoe
, t.location_th AS location_th
, a.rowid AS amphoe_rowid
, p.rowid AS province_rowid
, t.geom AS geom_32647
, t.point_x AS point_x
, t.point_y AS point_y
--, NULL AS geom_4326
FROM _impData t 
	LEFT OUTER JOIN m_province p ON p.rowid = t.province::INT 
	LEFT OUTER JOIN m_amphoe a ON a.code = CONCAT(t.province, t.AMPHOE) 


UPDATE t_landmark
SET geom_4326 = ST_Transform (geom_32647, 4326)


UPDATE t_landmark
SET point_x = ST_X(geom_4326)
, point_y = ST_Y(geom_4326)



SELECT ST_X(geom_4326), ST_Y(geom_4326), ST_X(geom_32647), ST_Y(geom_32647)
FROM t_landmark