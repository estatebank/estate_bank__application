﻿-- DROP TABLE IF EXISTS m_building_type CASCADE;

CREATE TABLE IF NOT EXISTS m_building_type (
  rowid INTEGER PRIMARY KEY,
  code TEXT,
  name_en TEXT,
  name_th TEXT,
  name_jp TEXT,
  description TEXT,
  remark TEXT,
  is_cancel INTEGER DEFAULT 0,
  create_by INTEGER,
  create_date timestamp without time zone,
  update_by INTEGER,
  update_date timestamp without time zone
);


-- Foreign Key
ALTER TABLE t_building
  ADD CONSTRAINT t_building_b_type_rowid_fkey FOREIGN KEY (b_type_rowid)
      REFERENCES m_building_type (rowid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

------------------------------------------

--- Rule instead of delete 
CREATE OR REPLACE RULE rid_m_building_type AS ON 
	DELETE TO m_building_type
    DO INSTEAD 
	UPDATE m_building_type
	SET is_cancel = 1 
	, update_by = -1
	, update_date = CURRENT_TIMESTAMP
	WHERE rowid = OLD.rowid;

------------------------------------------

INSERT INTO m_building_type (rowid, code, name_en, name_th, name_jp, description, create_by, create_date) VALUES 
 (11, 'BT-011', 'detached house', 'บ้านเดี่ยว', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(12, 'BT-012', 'twin house', 'บ้านแฝด', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(13, 'BT-013', 'townhouse/townhome', 'ทาวน์เฮาส์', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(21, 'BT-021', 'condomenium', 'อาคารชุด', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(22, 'BT-022', 'apartment', 'ห้องชุด', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(31, 'BT-031', 'rent room', 'ห้องเช่า', NULL, NULL, 1, CURRENT_TIMESTAMP)
,(32, 'BT-032', 'dormitory', 'หอพัก', NULL, NULL, 1, CURRENT_TIMESTAMP)
