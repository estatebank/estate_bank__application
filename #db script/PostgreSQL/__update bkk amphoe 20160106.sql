﻿INSERT INTO m_amphoe(rowid, code, province_rowid, region_rowid, name_th, name_en)

select CONCAT('32', t."AMP_CODE")::BIGINT AS rowid, CONCAT(t."PROV_CODE", t."AMP_CODE") AS code
, 10 AS province_rowid, 1 AS region_rowid, t."AMP_NAMT" AS name_th, t."AMP_NAME" AS name_en
from imp_bkk_amphoe_20160106 t
	LEFT OUTER JOIN m_amphoe a ON a.code = CONCAT(t."PROV_CODE", t."AMP_CODE")
WHERE t."PROV_CODE" = '10'
AND a.rowid IS NULL


CREATE SEQUENCE m_amphoe_polygon_rowid_seq;
ALTER TABLE m_amphoe_polygon ALTER COLUMN rowid SET DEFAULT nextval('m_amphoe_polygon_rowid_seq');
ALTER TABLE m_amphoe_polygon ALTER COLUMN rowid SET NOT NULL;
ALTER SEQUENCE m_amphoe_polygon_rowid_seq OWNED BY m_amphoe_polygon.rowid;    -- 8.2 or later
SELECT setval('m_amphoe_polygon_rowid_seq', (SELECT MAX(rowid) FROM m_amphoe_polygon)); 


INSERT INTO m_amphoe_polygon(amphoe_rowid, geom)
SELECT a.rowid, t.geom 
FROM imp_bkk_amphoe_20160106 t 
	INNER JOIN m_amphoe a ON a.rowid = CONCAT('32', t."AMP_CODE")::BIGINT AND COALESCE(a.nrd2c, '') = ''
	LEFT OUTER JOIN m_amphoe_polygon ap ON ap.amphoe_rowid = a.rowid 
WHERE ap.rowid IS NULL


-- select * from m_amphoe_polygon

ALTER TABLE m_region_polygon 
  ALTER COLUMN geom TYPE geometry(POLYGON, 4326) 
    USING ST_SetSRID(geom,4326);


ALTER TABLE m_province_polygon 
  ALTER COLUMN geom TYPE geometry(POLYGON, 4326) 
    USING ST_SetSRID(geom,4326);


ALTER TABLE m_amphoe_polygon 
  ALTER COLUMN geom TYPE geometry(POLYGON, 4326) 
    USING ST_SetSRID(geom,4326);