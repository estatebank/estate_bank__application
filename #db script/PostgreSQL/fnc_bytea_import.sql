-- Function: fnc_bytea_import(TEXT, OUT BYTEA)

CREATE OR REPLACE FUNCTION fnc_bytea_import(p_path TEXT, p_result OUT BYTEA) 
LANGUAGE plpgsql AS $$
DECLARE
	l_oid oid;
	r RECORD;
BEGIN
	p_result := '';
	SELECT LO_IMPORT(p_path) INTO l_oid;
	FOR r in (
		SELECT data 
		FROM pg_largeobject 
		WHERE loid = l_oid 
		ORDER BY pageno
	) LOOP
		p_result = p_result || r.data;
	END LOOP;
	PERFORM LO_UNLINK(l_oid);
END;$$;